@extends('frontend.master')
@section('styles')
    <!--<link rel="stylesheet" href="/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Bhaina+2:wght@500&display=swap" rel="stylesheet">
    <style>
     .hero-section
        {
            height: 200px;
            display: flex;
            align-items: center;
            justify-content: center;
            overflow: hidden
        }
        .hero-section img
        {
            position: absolute;
            z-index: -1;
            filter: blur(4px);
            height: inherit;
            width: 100% !important
        }
        .bg
        {
            position: absolute;
            height: 220px;
            width: 100%;
            filter: blur(2px);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            z-index: -1;
            background: url('/images/her-bg.jpg')
        }

    </style>
@endsection

@section('content')
<div class="bootstrap-menu">
        <div class="hero-section">
            <div class="bg"></div>
            <h2 class="text-uppercase">Track Your Order</h2>
        </div>
        <div class="container mt-5 pt-2 mb-5 pb-2">
            <p>
            Your tracking number is "$(TrackingNumber)" and was shipped via $(ShippingMethod). You may try the corresponding link below to track your package. Please note that some shippers may not have tracking details for 24 hours after we post the tracking number.
            </p>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
@endsection
