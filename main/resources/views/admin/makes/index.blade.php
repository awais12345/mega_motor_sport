@extends('admin.master')
@section('title') Dashboard @stop
@section('content')


    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Begin Breadcrumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Makes</li>
            </ol>
        </nav>
        <!-- end Breadcrumb -->

        <!-- Page Heading -->

        <div class="card">
            <div class="card-body">

                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="col-sm-8">
                        <h1 class="h3 mb-0 text-gray-800">Makes</h1>
                    </div>
                    <div>
                        <a href="#add-make" class="btn btn-primary btn-sm" data-toggle="modal">Add Make</a>
                    </div>
                </div>

                @include('shared.errors')

                <div class="table-responsive">
                    <table class="table table-hover table-sm" id="maketable">
                        <thead>
                        <tr class="text-center">
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($makes as $key => $make)
                            <tr class="text-center">
                                <th scope="row">{{ $key+1 }}</th>
                                <td>{{ $make->name }}</td>
                                <td>
                                    <a href="Javascript:void(0)" data-id="{{$make->id}}" data-toggle="modal" class="btn btn-sm btn-success edit-make">edit</a>
                                    <a href="{{route('admin.makes.destroy', $make->id)}}" class="btn btn-sm btn-danger">delete</a>
                                </td>
                            </tr>
                        @empty
                        <tr>
                            <th colspan="5" class="text-center text-danger">Record Not Found!</th>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- The Modal -->
        <div class="modal fade" id="add-make">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add Make</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form method="POST" action="{{route('admin.makes.store')}}">

                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name">Name <sup>*</sup></label>
                                <input id="name" type="text" class="form-control" name="name" required>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
        <div class="modal fade" id="edit-make-modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Edit make</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div id="edit-body">

                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
@section('scripts')
    <script>
        $('.edit-make').on('click', function (e) {
            let make_id = $(this).data('id');
            e.preventDefault();
            $.ajax({
                url: '/admin/makes/' + make_id + '/edit',
                type: 'get',
                success: function (response) {
                    $('#edit-body').html(response);
                    $('#edit-make-modal').modal('show');
                }
            });
        })
    </script>
@endsection
