
<form method="POST" action="{{route('admin.categories.update', $category)}}" enctype="multipart/form-data">

    {{ csrf_field() }}

    <div class="modal-body" id="edit-body">
        <div class="form-group">
            <label for="name">Name <sup>*</sup></label>
            <input id="name" type="text" class="form-control" value="{{$category->name}}" name="name" required>
        </div>

        <div class="custom-file">
            <input type="file" class="custom-file-input" name="img" id="customFile">
            <label class="custom-file-label" for="customFile">Choose Image</label>
        </div>

        <div class="form-group mt-2">
            <img src="{{$category->img ? asset('main/storage/app/public/'.$category->img) :'/admin/img/no-photo.png'}}" id="profile-img-tag" class="img-profile" alt="not found" style="height: 200px;width:100%">
        </div>
    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</form>
