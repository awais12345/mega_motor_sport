@extends('frontend.master')
@section('styles')
    <!--<link rel="stylesheet" href="/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Bhaina+2:wght@500&display=swap" rel="stylesheet">
    <style>
        .body-section {
            margin-top: 5%;
        }

        .side-sec-body h5 {
            font-family: 'Baloo Bhaina 2', cursive;
        }

        .side-sec-body ul {
            list-style: none;
            padding: 0px;
        }

        .side-sec-body li {
            width: 100%;
            height: 30px;
            padding: 3px 3px 5px 5px;
            list-style: none;
        }

        .side-sec-body li input {
            width: 18px;
            height: 17px;
        }

        .product-Section .col-md-4 {
            padding: 0px;
        }

        .SubCategoryNameTitle {
            display: inline-block;
            font-family: 'Baloo Bhaina 2', cursive;
        }

        .customSelect {
            padding: 12px 8px 10px 2px;
        }

        .prCard {
            border: 2px solid #111;
            transition: border 0.5s ease;
        }

        .prCard:hover {
            border: 5px solid #111;
            transition: border 0.5s ease;
        }

        .prCard img {
            position: relative;
            transform: scale(0.90);
            transition: box-shadow 0.5s, transform 0.5s;
        }

        .prCard:hover img {
            transform: scale(1);
        }

        .prCard:hover {
            box-shadow: 5px 20px 30px rgba(0, 0, 0, 0.3);
        }

        .aCartbtn {
            background-color: #e26c1e;
            transition: background-color 0.7s ease;
        }

        .prCard button:hover {
            cursor: pointer;
            background-color: royalblue;
            transition: background-color 0.5s ease;
        }

        @media (max-width: 880px) {
            .sside-section {
                display: none;
            }
        }



        .ribbon3 {
            width: 59px;
            height: 27px;
            font-size: 11px;
            line-height: 27px;
            padding-left: 15px;
            position: absolute;
            left: -8px;
            top: 20px;
            font-weight: bolder;
            color: #fff;
            z-index: 2;
            background: var(--red-color);
        }
        .ribbon3:before, .ribbon3:after {
            content: "";
            position: absolute;
        }
        .ribbon3:before {
            height: 0;
            width: 0;
            top: -8.5px;
            left: 0.1px;
            border-bottom: 9px solid black;
            border-left: 9px solid transparent;
        }
        .ribbon3:after {
            height: 0;
            width: 0;
            right: -14.5px;
            border-top: 13px solid transparent;
            border-bottom: 14px solid transparent;
            border-left: 16px solid var(--red-color);
        }
        .buy-now-btn{
            padding: 4px 38px;
            border: 2px solid var(--blue-color);
            background-color: var(--blue-color);
            color: #FFF;
        }
        .buy-now-price
        {
            font-size: 110%; font-weight: 700; padding: 4px 40px; color: var(--blue-color); border: 2px solid var(--blue-color); text-align: center !important; width: 100%
        }
        @media only screen and (max-width: 360px) {
            .buy-now-btn{
                padding: 4px 12px
            }
            .buy-now-price{
                padding: 4px 12px
            }
            .price-section{
                flex-direction: column !important;
            }
        }
        @media only screen and (max-width: 500px)
        {
            .price-section{
                flex-direction: column !important;
            }
            .ico
            {
                position: relative !important;
            }
            del::after
            {
                content: '\a';
                white-space: pre
            }
        }
        @media only screen and (max-width: 1557px) {
            .buy-now-btn{
                padding: 4px 17px
            }
            .buy-now-price{
                padding: 4px 34px
            }
            .price-section{
                flex-direction: row;
            }

        }
        @media only screen and (max-width: 1290px) {
            .buy-now-btn{
                padding: 4px 18px
            }
            .buy-now-price{
                padding: 4px 9px
            }
            .price-section{
                flex-direction: row;
            }
        }
        .price-section{
            flex-direction: row;
        }





    </style>
@endsection

@section('content')
    <section class="body-section bootstrap-menu">
        <div class="container">
            <div class="row">
                <h2>My Account</h2>
                <p>
                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                </p>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection
