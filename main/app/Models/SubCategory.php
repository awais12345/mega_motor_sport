<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
