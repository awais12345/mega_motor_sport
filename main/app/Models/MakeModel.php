<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MakeModel extends Model
{
    protected $guarded = [];

    public function make()
    {
        return $this->belongsTo(Make::class);
    }
}
