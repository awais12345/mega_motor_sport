<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- For Search Engine Meta Data  -->
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="yoursite.com" />

    <title>Login - MEGA MOTORS</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="images/favicon-16x16.png"/>

    <!-- Main structure css file -->
    <link  rel="stylesheet" href="/css/login6-style.css">
    <link  rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap-iso.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if IE]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <style>
        </style>
</head>

<body>
    <div class="bootstrap-menu">
<div class="container-fluid">
    @error('register') <input type="hidden" id="register-error" value="1"> @enderror

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-5 col-lg-4 authfy-panel-left">
            <!-- brand-logo start -->
            <div class="brand-logo text-center">
                <a href="/"><img src="/images/logo.png" width="150" alt="brand-logo"></a>
            </div><!-- ./brand-logo -->
            <!-- authfy-login start -->
            <div class="authfy-login">
                <!-- panel-login start -->
                <div class="authfy-panel panel-login text-center active">
                    <div class="authfy-heading">
                        <h3 class="auth-title">Login to your account</h3>
                        <p>Don’t have an account? <a class="lnk-toggler" id="panel-signup-open" data-panel=".panel-signup" href="#">Sign Up Free!</a></p>
                    </div>
                    <!-- social login buttons start -->
                    <div class="row social-buttons">
                        <div class="col-xs-6 col-sm-6">
                            <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-lg btn-block btn-facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </div>

                        <div class="col-sm-6 col-sm-6">
                            <a href="{{ url('/auth/redirect/google') }}" class="btn btn-lg btn-block btn-google">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </div>

{{--                        <div class="col-xs-4 col-sm-4">--}}
{{--                            <a href="{{ url('/auth/redirect/twitter') }}" class="btn btn-lg btn-block btn-facebook">--}}
{{--                                <i class="fa fa-twitter"></i>--}}
{{--                            </a>--}}
{{--                        </div>--}}
                    </div><!-- ./social-buttons -->
                    <div class="row loginOr">
                        <div class="col-xs-12 col-sm-12">
                            <span class="spanOr">or</span>
                        </div>
                    </div>
                    <div class="row" style="margin-right: 15px">
                        <div class="col-xs-12 col-sm-12">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group wrap-input">
                                    <input id="email" placeholder="Email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group wrap-input">
                                    <div class="pwdMask">
                                        <input id="password" placeholder="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               name="password" required autocomplete="current-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row remember-row">
                                    <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                                        <label class="checkbox text-left">

                                            <input type="checkbox" name="remember"
                                                   id="remember" {{ old('remember') ? 'checked' : '' }}><span
                                                class="label-text">Remember me</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
{{--                                        @if (Route::has('password.request'))--}}
{{--                                            <p class="forgotPwd">--}}
{{--                                                <a class="lnk-toggler" data-panel=".panel-forgot"--}}
{{--                                                   href="{{ route('password.request') }}">Forgot password?</a>--}}
{{--                                            </p>--}}
{{--                                        @endif--}}

                                    </div>
                                </div> <!-- ./remember-row -->
                                <div class="form-group">
                                    <button class="btn btn-lg btn-primary btn-block" type="submit">Login with email
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> <!-- ./panel-login -->
                <!-- panel-signup start -->
                <div class="authfy-panel panel-signup text-center" >
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="authfy-heading">
                                <h3 class="auth-title">Sign up for free!</h3>
                            </div>
                            <form name="signupForm" class="signupForm"  method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group wrap-input">
                                    <input id="name" placeholder="Full Name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group wrap-input">
                                    <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group wrap-input">
                                    <div class="pwdMask">
                                        <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group wrap-input">
                                    <div class="pwdMask">
                                        <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="term-policy text-muted small">I agree to the <a href="#">privacy policy</a> and <a href="#">terms of service</a>.</p>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up with email</button>
                                </div>
                            </form>
                            <a class="lnk-toggler" data-panel=".panel-login" href="#">Already have an account?</a><br><br>

                        </div>
                    </div>
                </div> <!-- ./panel-signup -->
                <!-- panel-forget start -->
                <div class="authfy-panel panel-forgot">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="authfy-heading">
                                <h3 class="auth-title">Recover your password</h3>
                                <p>Fill in your e-mail address below and we will send you an email with further instructions.</p>
                            </div>
                            <form name="forgetForm" class="forgetForm" action="#" method="POST">
                                <div class="form-group wrap-input">
                                    <input type="email" class="form-control" name="username" placeholder="Email address">
                                    <span class="focus-input"></span>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-lg btn-primary btn-block" type="submit">Recover your password</button>
                                </div>
                                <div class="form-group">
                                    <a class="lnk-toggler" data-panel=".panel-login" href="#">Already have an account?</a>
                                </div>
                                <div class="form-group">
                                    <a class="lnk-toggler" data-panel=".panel-signup" href="#">Don’t have an account?</a>
                                    <br><br>

                                </div>
                            </form>
                        </div>
                    </div>
                </div> <!-- ./panel-forgot -->
            </div> <!-- ./authfy-login -->
        </div> <!-- ./authfy-panel-left -->
        <div class="col-md-7 col-lg-8 authfy-panel-right hidden-xs hidden-sm">
            <div class="hero-heading">
                <div class="headline">
                    <h3>Welcome to MEGA MOTORS</h3>
                    <p>Sharing Smile through Car Caring.</p>
                </div>
            </div>

        </div>
    </div> <!-- ./row -->
</div> <!-- ./container -->
</div>
<!-- Javascript Files -->

<!-- initialize jQuery Library -->
<script src="/js/jquery-2.2.4.min.js"></script>

<!-- for Bootstrap js -->
<script src="/js/bootstrap.min.js"></script>

<!-- Custom js-->
<script src="/js/custom.js"></script>

<script>
    $(document).ready(function() {
        if($('#register-error').val() == "1"){
            $('#panel-signup-open').click();
        }
    });
</script>
</body>
</html>
