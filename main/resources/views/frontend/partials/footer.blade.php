<div class="bootstrap-menu">

<footer class="page-footer font-small unique-color-dark bootstrap-menu" style="font-size: 14px;">

    <div style="background-color: var(--blue-color);">
        <div class="container">

            <!-- Grid row-->
            <div class="row py-4 d-flex align-items-center">

                <!-- Grid column -->
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                    <h6 class="mb-0">Get connected with us on social networks!</h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-6 col-lg-7 text-center text-md-right">

                    <!-- Facebook -->
                    <a href="https://www.facebook.com/Megamotorsports.pk/" class="fb-ic" style="font-size: x-large">
                        <i class="fab fa-facebook-f white-text mr-4"> </i>
                    </a>
                    <!--Linkedin -->
                    <a href="https://www.youtube.com/channel/UCBq1MuR0-n5OOVAsTJk2NSQ" class="li-ic" style="font-size: x-large">
                        <i class="fab fa-youtube white-text mr-4"> </i>
                    </a>
                    <!--Instagram-->
                    <a href="https://www.instagram.com/megamotorsports.pk/" class="ins-ic" style="font-size: x-large">
                        <i class="fab fa-instagram white-text"> </i>
                    </a>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row-->

        </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

        <!-- Grid row -->
        <div class="row mt-3">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

                <!-- Content -->
                <h6 class="text-uppercase font-weight-bold">Why MegaMotorSports.PK?</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p><b>Pakistan’s</b> Largest and No. 1  Auto Customization Store.<br><br> <b>Megamotorsports.pk</b>, that provides all modification accessories for all brands and models.</p>   

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 col-lg-3 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold" style="font-size: 112%">Customer Services</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="/track-my-order">Track My Order</a>
                </p>
                <p>
                    <a href="/my-account">My Account</a>
                </p>
                <p>
                    <a href="/return-policy">Return Policy</a>
                </p>
                <p>
                    <a href="/faqs">FAQs</a>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">Information</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="/about-us">About Us</a>
                </p>
                <p>
                    <a href="/contact-us">Contact Us</a>
                </p>
                <p>
                    <a href="/feedback">Feedback</a>
                </p>
                <p>
                    <a href="/terms-and-conditions">Terms & Conditions</a>
                </p>
                <p>
                    <a href="/privacy-policy">Privacy & Policy</a>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">Contact</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <i class="glyphicon glyphicon-home mr-3"></i> Shop# 45, Ground Floor, Rimpa Plaza, Abbot Road, Near Mintgomery Road, Lahore</p>
                <p>
                    <i class="glyphicon glyphicon-envelope mr-3"></i> info@megamotorsports.pk</p>
                <p>
                    <i class="glyphicon glyphicon-earphone mr-3"></i> +923224677726</p>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">

    <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">© 2020 Copyright
        <a href="#">MegaMotorSports.PK</a></div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"><span>Designed and Developed with &#10084;&#65039; by <a href="https://brandsvalley.net"> Brands Valley</a></span> </div>
    </div>
    </div>
    <!-- Copyright -->

</footer>
</div>
