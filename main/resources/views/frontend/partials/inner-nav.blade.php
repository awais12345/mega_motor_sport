<style>
 #make{
     padding-top: 22px !important;
     font-size: 16px;
     border: 3px solid var(--blue-color);
 }
 #model{
     padding-top: 22px !important;
     font-size: 16px;
     border: 3px solid var(--blue-color);
 }
 #year{
     padding-top: 22px !important;
     font-size: 16px;
     border: 3px solid var(--blue-color);
 }
 .vehicle-text
 {
   font-size: 25px;
   text-align: left;
   background: #363d47;
   width: 335px;
   padding: 25px;
   margin-bottom: 0px;
   border-top-left-radius: 10px;
   border-top-right-radius: 10px;
 }
 .head-nav-subtitle
 {
   font-size: 25px
 }
  
@media screen and (max-width: 735px) {
  #make {
    padding-top: 12px !important;
    padding-bottom: 12px !important;
  }
  .vehicle-text
 {
   color: white;
   font-size: 30px;
   width: 100%;
   text-align: center;
   margin-bottom: -20px !important;
 }
 .select-boxes
  {
    border-top-right-radius: 0px !important;
  }
  #model {
    padding-top: 12px !important;
    padding-bottom: 12px !important;
  }
  #year {
    padding-top: 12px !important;
    padding-bottom: 12px !important;
  }
  .aside_slct_wrap {
    height: 47px;/*height: 27px;*/
    margin-left: -3%;/*margin-left: 16%;*/
    width: 106%;/*width: 65%;*/
  }
  .section-top
    {
      background: white !important;
    margin-top: 0px !important
    }
  .search-text
  {
      display: none;
  }
  #carouselExampleSlidesOnly1
  {
    display:none
  }
  .hlt
  {
    display:none !important
  }
  .marbot-90
  {
    margin-bottom: -90px
  }
}
.section-top
{
    margin-top: 25px;
}

.carousel-item img {
    min-height: 200px;
}
.select-boxes
{
  background: #363d47;
  padding:25px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  border-top-right-radius: 10px;
}
</style>
<div class="bootstrap-menu marbot-90" >
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" style="width: 100%; height: 100%; z-index: 1">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                      <img src="/images/F1.jpg" class="d-block w-100 sldimg" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="/images/F2.jpg" class="d-block w-100 sldimg" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="/images/F3.jpg" class="d-block w-100 sldimg" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="/images/F4.jpg" class="d-block w-100 sldimg" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="/images/F5.jpg" class="d-block w-100 sldimg" alt="...">
                  </div>
                </div>
              </div>
<div class="row" style = "display: flex; align-items: center; justify-content: center; margin-top: -10px">
  <div class="container-fluid" style = "display: flex; align-items: center; justify-content: center">

  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
   <div class="container-fluid p-1 section-top" >
    <div class="head-nav-inner p-2" style="margin: 10px auto 10px auto !important; width: 100%" >
        <div class="head-nav-title vehicle-text">Select your vehicle</div>
        
        <div class="select-vehicle-spacer -index js-select-vehicle-namespace js-select-vehicle-fixed-point select-boxes"
             id="select-vehicle-popup" data-namespace="select_vehicle_home_page" style="">
            <div class="select-vehicle" data-purpose="home-page">
                <div class="content -reset-styles">
                    <form method="GET" action="{{route('products.search')}}" id="search-form">
                        <div class="tab -active js-select-vehicle-mmy-tab">
                            <div class="select-vehicle-content-spacer js-vehicle-spacer">
                                <div class="select-vehicle-items _clear-modern">
                                    <div class="tab-garage-popup-15 hidden"
                                         data-namespace="select_vehicle_home_page"
                                         data-change="'', 'p', '61', '11', '0', [], false" data-vehicle-type="15"
                                         data-vehicle-subtype=""
                                         data-url-family=""
                                         data-submodel-preload-needed="">
                                    </div>

                                    <div class="select-vehicle-col">
                                        <div class="aside_slct_wrap">
                                            <select class="form-control aside_slct -not-refine -with-marker make"
                                                    id="make" name="make_id" >
                                                <option value="" disabled selected>1 &nbsp;&nbsp;|&nbsp;&nbsp; MAKE
                                                </option>
                                                @foreach(getMakes() as $make)
                                                    <option value="{{ $make->id }}"
                                                        {{isset($make_id) && $make_id == $make->id ? 'selected' : ''}}>
                                                        {{ $make->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="select-vehicle-col">
                                        <div class="aside_slct_wrap">
                                            <select
                                                class="form-control aside_slct -not-refine -with-marker model"
                                                name="model_id" id="model">
                                                <option value="" disabled selected>2 &nbsp;&nbsp;|&nbsp;&nbsp;MODEL</option>
                                                @if(isset($model_id))
                                                <option value="{{ $model_id }}"
                                                    {{\App\Models\MakeModel::where('id', $model_id)->exists() ? 'selected' : ''}}>
                                                    {{ \App\Models\MakeModel::where('id', $model_id)->first()->name }}</option>
                                                    @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="select-vehicle-col">
                                        <div class="aside_slct_wrap">
                                            <select class="form-control aside_slct -not-refine -with-marker"
                                                    name="year" id="year">
                                                <option value="" disabled selected>3 &nbsp;&nbsp;|&nbsp;&nbsp;YEAR
                                                </option>
                                                @foreach(getYears() as $loopYear)
                                                    <option value="{{ $loopYear }}" {{isset($year) && $loopYear == $year ? 'selected' : ''}}>
                                                        {{ $loopYear }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div onclick="submitSearchForm()"
                                         class="js-select-vehicle-btn select-vehicle-button -after-selects"
                                         tabindex="0"
                                         data-namespace="select_vehicle_home_page">GO
                                    </div>
                                    <div class="aside_slct_load -index-mmy"></div>


                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <span class="select-vehicle-fixed-close"></span></div>
        </div>
    
    </div>
   </div>
  </div>
</div>
</div>
  
</div>
