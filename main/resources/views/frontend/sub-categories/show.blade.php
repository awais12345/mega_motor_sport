@extends('frontend.master')
@section('styles')
    <!--<link rel="stylesheet" href="/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Bhaina+2:wght@500&display=swap" rel="stylesheet">
    <style>
        .body-section {
            margin-top: 5%;
        }

        .side-sec-body h5 {
            font-family: 'Baloo Bhaina 2', cursive;
        }

        .side-sec-body ul {
            list-style: none;
            padding: 0px;
        }

        .side-sec-body li {
            width: 100%;
            height: 30px;
            padding: 3px 3px 5px 5px;
            list-style: none;
        }

        .side-sec-body li input {
            width: 18px;
            height: 17px;
        }

        .product-Section .col-md-4 {
            padding: 0px;
        }

        .SubCategoryNameTitle {
            display: inline-block;
            font-family: 'Baloo Bhaina 2', cursive;
        }

        .customSelect {
            padding: 12px 8px 10px 2px;
        }

        .prCard {
            border: 2px solid #111;
            transition: border 0.5s ease;
        }

        .prCard:hover {
            border: 5px solid #111;
            transition: border 0.5s ease;
        }

        .prCard img {
            position: relative;
            transform: scale(0.90);
            transition: box-shadow 0.5s, transform 0.5s;
        }

        .prCard:hover img {
            transform: scale(1);
        }

        .prCard:hover {
            box-shadow: 5px 20px 30px rgba(0, 0, 0, 0.3);
        }

        .aCartbtn {
            background-color: #e26c1e;
            transition: background-color 0.7s ease;
        }

        .prCard button:hover {
            cursor: pointer;
            background-color: royalblue;
            transition: background-color 0.5s ease;
        }

        @media (max-width: 880px) {
            .sside-section {
                display: none;
            }
        }



.ribbon3 {
    width: 59px;
    height: 27px;
    font-size: 11px;
    line-height: 27px;
    padding-left: 15px;
    position: absolute;
    left: -8px;
    top: 20px;
    font-weight: bolder;
    color: #fff;
    z-index: 2;
    background: var(--red-color);
  }
  .ribbon3:before, .ribbon3:after {
    content: "";
    position: absolute;
  }
  .ribbon3:before {
    height: 0;
    width: 0;
    top: -8.5px;
    left: 0.1px;
    border-bottom: 9px solid black;
    border-left: 9px solid transparent;
  }
  .ribbon3:after {
    height: 0;
    width: 0;
    right: -14.5px;
    border-top: 13px solid transparent;
    border-bottom: 14px solid transparent;
    border-left: 16px solid var(--red-color);
  }
  .buy-now-btn{
      padding: 4px 38px;
      border: 2px solid var(--blue-color);
      background-color: var(--blue-color);
      color: #FFF;
  }
  .buy-now-price
  {
      font-size: 110%; font-weight: 700; padding: 4px 40px; color: var(--blue-color); border: 2px solid var(--blue-color); text-align: center !important; width: 100%
  }
  @media only screen and (max-width: 360px) {
      .buy-now-btn{
          padding: 4px 12px
      }
      .buy-now-price{
          padding: 4px 12px
      }
      .price-section{
          flex-direction: column !important;
      }
  }
  @media only screen and (max-width: 500px)
  {
      .price-section{
          flex-direction: column !important;
      }
      .ico
      {
          position: relative !important;
      }
      del::after
      {
          content: '\a';
          white-space: pre
      }
  }
  @media only screen and (max-width: 1557px) {
      .buy-now-btn{
          padding: 4px 17px
      }
      .buy-now-price{
          padding: 4px 34px
      }
      .price-section{
          flex-direction: row;
      }

  }
  @media only screen and (max-width: 1290px) {
      .buy-now-btn{
          padding: 4px 18px
      }
      .buy-now-price{
          padding: 4px 9px
      }
      .price-section{
          flex-direction: row;
      }
  }
  .price-section{
      flex-direction: row;
  }





    </style>
@endsection

@section('content')
    <section class="body-section bootstrap-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class=" text-left"> <!--side-sec-body-->
                    <!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-md btn-block" data-toggle="modal" data-target="#exampleModalCenter01" style="font-size: 15px">
  Filter By Models
</button>
<style>
.modal {
    padding-right: 0px !important;
}
</style>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter01" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Filter By Models</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <ul class="list-unstyled">
                            @foreach($models as $model)
                                <li>
                                    <a href="{{ route('products.search', ['make_id' => $model->make_id, 'model_id' => $model->id]) }}"
                                       class="btn btn-link"> {{ $model->name }}
                                    </a>
                                </li><br>
                            @endforeach
                        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger d-flex" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 product-Section mt-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="SubCategoryNameTitle">
                                <h3 class="text-uppercase">{{ $subCategory->name }}</h3>
                            </div>
{{--                            <div class="customSelect float-right">--}}
{{--                                <select class="custom-select">--}}
{{--                                    <option selected>Sort by newest</option>--}}
{{--                                    <option>Sort by price: low to high</option>--}}
{{--                                    <option>Sort by price: High to log</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
                        </div>
                        <div class="productSlider"
                             style="border-right:1px solid #e1e1e1;border-left:1px solid #e1e1e1; border-top:2px solid #7a3901"
                             bis_skin_checked="1">
                            <div bis_skin_checked="1">
                                <ul style="padding:0px;"
                                    class="owl-carousel owl-theme catProSlider list-inline indexProductList p-hover shopList">
                                    @foreach($subCategory->products as $product)

                                            <li class="transall item " style="min-height: 281px"><!---->
                                                <div class="li-item" bis_skin_checked="1">
                                                    @if($product->price != $product->discount_price)

      <span class="ribbon3">{{ $product->discount }} % Off</span>

                                                    @endif
                                                    <div class="p-image" bis_skin_checked="1">

                                                    <a href="{{route('products.show', $product->id)}}">
                                                        <img id="ProductImage-16129"
                                                             productimage="{{$product->images()->first() ? asset('main/storage/app/public/'.$product->images()->first()->image) :'/admin/img/no-photo.png'}}"
                                                             src="{{$product->images()->first() ? asset('main/storage/app/public/'.$product->images()->first()->image) :'/admin/img/no-photo.png'}}"
                                                             alt="{{$product->name}}"
                                                             style="height: 135px !important; width: auto !important;"
                                                             class="img-responsive">
                                                             </a>
                                                    </div>
                                                    <p id="ProductTitle-16129"
                                                       producttitle="{{$product->name}}">
                                                        {{$product->name}}
                                                    </p>
                                                    <input type="hidden" id="pProductSku-16129" value="16129">
                                                    <div class="container-fluid">
                                                    <div class="row text-center">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 price-section" style="display: flex; justify-content: center; flex-direction: row !important">

                                                        <a class="m-0 buy-now-btn" style="width: 10% !important; color: #FFF; background-color:var(--blue-color);border: inherit" href="{{ route('products.add.cart', [$product->id]) }}">
                                                            <span  style="font-size: large">	<i class="fa fa-shopping-cart ico" style="position: absolute; top: 50%; transform: translate(-30%,-50%); color:#fff"></i></span>
                                                        </a>
                                                    <span class="m-0 buy-now-price">
                                                    @if($product->price != $product->discount_price)
                                                        <del style="color: var(--red-color); font-size: 80%;">Rs: {{ thousand_separator($product->price) }}</del>
                                                        @endif
                                                        Rs: {{ thousand_separator($product->discount_price) }}</span></div>
                                                    </div>


                                                </div>
                                                </a>
                                            </li>


                                    @endforeach
                                    {{--                                <li class="transall item ">--}}
                                    {{--                                    <a href="#" bis_skin_checked="1">--}}

                                    {{--                                            <span id="productCircle-7772">--}}
                                    {{--                                                <span class="circle">--}}
                                    {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                    {{--                                                    <span class="usp">--}}
                                    {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                    {{--                                                            off<br>--}}
                                    {{--                                                            <span class="usp_off">--}}
                                    {{--                                                                25 %--}}
                                    {{--                                                            </span>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                    </span>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </span>--}}

                                    {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                    {{--                                            <img id="ProductImage-7772"--}}
                                    {{--                                                 productimage="images/productimages/636583695190348246.jpg"--}}
                                    {{--                                                 src="/images/productimages/636583695190348246.jpg" alt=""--}}
                                    {{--                                                 class="img-responsive">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <p id="ProductTitle-7772"--}}
                                    {{--                                           producttitle="Honda BRV Fog Lamps / Fog Lights DRL Covers Dual - Model 2017-2019">--}}
                                    {{--                                            Honda BRV Fog Lamps / Fog Lights DRL Covers Dual - Model 2017-2019--}}
                                    {{--                                        </p>--}}
                                    {{--                                        <input type="hidden" id="pProductSku-7772" value="304289">--}}
                                    {{--                                        <div class="p-price" id="ProductPrice-7772" productprice="Rs: 6,000"--}}
                                    {{--                                             bis_skin_checked="1">--}}
                                    {{--                                                <span class="sale">--}}
                                    {{--                                                    <s>--}}
                                    {{--                                                        Rs: 8,015--}}
                                    {{--                                                    </s>--}}
                                    {{--                                                </span>Rs: 6,000--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </a>--}}
                                    {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                    {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                    {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="7772"--}}
                                    {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                    {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                    {{--                                        <a href="javascript:void(0)" productid="7772" onclick="addToCart(this)"--}}
                                    {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </li>--}}
                                    {{--                                <li class="transall item ">--}}
                                    {{--                                    <a href="#" bis_skin_checked="1">--}}

                                    {{--                                            <span id="productCircle-15525">--}}
                                    {{--                                                <span class="circle">--}}
                                    {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                    {{--                                                    <span class="usp">--}}
                                    {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                    {{--                                                            off<br>--}}
                                    {{--                                                            <span class="usp_off">--}}
                                    {{--                                                                36 %--}}
                                    {{--                                                            </span>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                    </span>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </span>--}}

                                    {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                    {{--                                            <img id="ProductImage-15525"--}}
                                    {{--                                                 productimage="images/productimages/636818612535444534.jpg"--}}
                                    {{--                                                 src="/images/productimages/636818612535444534.jpg"--}}
                                    {{--                                                 alt="Dual Switch Button | Change Over Switch | Switch Allowing to Switch between Two Devices-SehgalMotors.Pk"--}}
                                    {{--                                                 class="img-responsive">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <p id="ProductTitle-15525"--}}
                                    {{--                                           producttitle="Dual Switch Button | Change Over Switch | Switch Allowing to Switch between Two Devices">--}}
                                    {{--                                            Dual Switch Button | Change Over Switch | Switch Allowing to Switch between--}}
                                    {{--                                            Two Devices--}}
                                    {{--                                        </p>--}}
                                    {{--                                        <input type="hidden" id="pProductSku-15525" value="15525">--}}
                                    {{--                                        <div class="p-price" id="ProductPrice-15525" productprice="Rs: 350"--}}
                                    {{--                                             bis_skin_checked="1">--}}
                                    {{--                                                <span class="sale">--}}
                                    {{--                                                    <s>--}}
                                    {{--                                                        Rs: 550--}}
                                    {{--                                                    </s>--}}
                                    {{--                                                </span>Rs: 350--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </a>--}}
                                    {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                    {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                    {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="15525"--}}
                                    {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                    {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                    {{--                                        <a href="javascript:void(0)" productid="15525" onclick="addToCart(this)"--}}
                                    {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </li>--}}
                                    {{--                                <li class="transall item ">--}}
                                    {{--                                    <a href="#" bis_skin_checked="1">--}}

                                    {{--                                            <span id="productCircle-14471">--}}
                                    {{--                                                <span class="circle">--}}
                                    {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                    {{--                                                    <span class="usp">--}}
                                    {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                    {{--                                                            off<br>--}}
                                    {{--                                                            <span class="usp_off">--}}
                                    {{--                                                                52 %--}}
                                    {{--                                                            </span>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                    </span>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </span>--}}

                                    {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                    {{--                                            <img id="ProductImage-14471"--}}
                                    {{--                                                 productimage="images/productimages/637060701338663032.jpg"--}}
                                    {{--                                                 src="/images/productimages/637060701338663032.jpg"--}}
                                    {{--                                                 alt="Universal LED SMD Eye Shape Cree Bar - Each | Super Bright Vision-SehgalMotors.Pk"--}}
                                    {{--                                                 class="img-responsive">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <p id="ProductTitle-14471"--}}
                                    {{--                                           producttitle="Universal LED SMD Eye Shape Cree Bar - Each | Super Bright Vision">--}}
                                    {{--                                            Universal LED SMD Eye Shape Cree Bar - Each | Super Bright Vision--}}
                                    {{--                                        </p>--}}
                                    {{--                                        <input type="hidden" id="pProductSku-14471" value="14471">--}}
                                    {{--                                        <div class="p-price" id="ProductPrice-14471" productprice="Rs: 799"--}}
                                    {{--                                             bis_skin_checked="1">--}}
                                    {{--                                                <span class="sale">--}}
                                    {{--                                                    <s>--}}
                                    {{--                                                        Rs: 1,690--}}
                                    {{--                                                    </s>--}}
                                    {{--                                                </span>Rs: 799--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </a>--}}
                                    {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                    {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                    {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="14471"--}}
                                    {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                    {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                    {{--                                        <a href="javascript:void(0)" productid="14471" onclick="addToCart(this)"--}}
                                    {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </li>--}}
                                    {{--                                <li class="transall item ">--}}
                                    {{--                                    <a href="#" bis_skin_checked="1">--}}

                                    {{--                                            <span id="productCircle-3827">--}}
                                    {{--                                                <span class="circle">--}}
                                    {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                    {{--                                                    <span class="usp">--}}
                                    {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                    {{--                                                            off<br>--}}
                                    {{--                                                            <span class="usp_off">--}}
                                    {{--                                                                19 %--}}
                                    {{--                                                            </span>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                    </span>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </span>--}}

                                    {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                    {{--                                            <img id="ProductImage-3827"--}}
                                    {{--                                                 productimage="images/productimages/636945713459425357.jpg"--}}
                                    {{--                                                 src="/images/productimages/636945713459425357.jpg"--}}
                                    {{--                                                 alt="Maximus Flexible Headlight / Head Lamp Dual Color DRL with 200 SMD | Audi Style Look | InstalLED Inside-SehgalMotors.Pk"--}}
                                    {{--                                                 class="img-responsive">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <p id="ProductTitle-3827"--}}
                                    {{--                                           producttitle="Maximus Flexible Headlight / Head Lamp Dual Color DRL with 200 SMD | Audi Style Look | InstalLED Inside">--}}
                                    {{--                                            Maximus Flexible Headlight / Head Lamp Dual Color DRL with 200 SMD | Audi--}}
                                    {{--                                            Style Look | InstalLED Inside--}}
                                    {{--                                        </p>--}}
                                    {{--                                        <input type="hidden" id="pProductSku-3827" value="6592">--}}
                                    {{--                                        <div class="p-price" id="ProductPrice-3827" productprice="Rs: 3,053"--}}
                                    {{--                                             bis_skin_checked="1">--}}
                                    {{--                                                <span class="sale">--}}
                                    {{--                                                    <s>--}}
                                    {{--                                                        Rs: 3,780--}}
                                    {{--                                                    </s>--}}
                                    {{--                                                </span>Rs: 3,053--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </a>--}}
                                    {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                    {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                    {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="3827"--}}
                                    {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                    {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                    {{--                                        <a href="javascript:void(0)" productid="3827" onclick="addToCart(this)"--}}
                                    {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </li>--}}
                                    {{--                                <li class="transall item ">--}}
                                    {{--                                    <a href="#" bis_skin_checked="1">--}}

                                    {{--                                            <span id="productCircle-4577">--}}
                                    {{--                                                <span class="circle">--}}
                                    {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                    {{--                                                    <span class="usp">--}}
                                    {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                    {{--                                                            off<br>--}}
                                    {{--                                                            <span class="usp_off">--}}
                                    {{--                                                                30 %--}}
                                    {{--                                                            </span>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                    </span>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </span>--}}

                                    {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                    {{--                                            <img id="ProductImage-4577"--}}
                                    {{--                                                 productimage="images/productimages/636441206517772102-SMD-Fog-angel-eyes.jpg"--}}
                                    {{--                                                 src="/images/productimages/636441206517772102-SMD-Fog-angel-eyes.jpg"--}}
                                    {{--                                                 alt="Police Red and Blue Flashers For Dashboard With LED-SehgalMotors.Pk"--}}
                                    {{--                                                 class="img-responsive">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <p id="ProductTitle-4577"--}}
                                    {{--                                           producttitle="Police Red and Blue Flashers For Dashboard With LED">--}}
                                    {{--                                            Police Red and Blue Flashers For Dashboard With LED--}}
                                    {{--                                        </p>--}}
                                    {{--                                        <input type="hidden" id="pProductSku-4577" value="5074">--}}
                                    {{--                                        <div class="p-price" id="ProductPrice-4577" productprice="Rs: 700"--}}
                                    {{--                                             bis_skin_checked="1">--}}
                                    {{--                                                <span class="sale">--}}
                                    {{--                                                    <s>--}}
                                    {{--                                                        Rs: 1,000--}}
                                    {{--                                                    </s>--}}
                                    {{--                                                </span>Rs: 700--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </a>--}}
                                    {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                    {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                    {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="4577"--}}
                                    {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                    {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                    {{--                                        <a href="javascript:void(0)" productid="4577" onclick="addToCart(this)"--}}
                                    {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </li>--}}
                                    {{--                                <li class="transall item ">--}}
                                    {{--                                    <a href="#" bis_skin_checked="1">--}}

                                    {{--                                            <span id="productCircle-15525">--}}
                                    {{--                                                <span class="circle">--}}
                                    {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                    {{--                                                    <span class="usp">--}}
                                    {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                    {{--                                                            off<br>--}}
                                    {{--                                                            <span class="usp_off">--}}
                                    {{--                                                                36 %--}}
                                    {{--                                                            </span>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                    </span>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </span>--}}

                                    {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                    {{--                                            <img id="ProductImage-15525"--}}
                                    {{--                                                 productimage="images/productimages/636818612535444534.jpg"--}}
                                    {{--                                                 src="/images/productimages/636818612535444534.jpg"--}}
                                    {{--                                                 alt="Dual Switch Button | Change Over Switch | Switch Allowing to Switch between Two Devices-SehgalMotors.Pk"--}}
                                    {{--                                                 class="img-responsive">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <p id="ProductTitle-15525"--}}
                                    {{--                                           producttitle="Dual Switch Button | Change Over Switch | Switch Allowing to Switch between Two Devices">--}}
                                    {{--                                            Dual Switch Button | Change Over Switch | Switch Allowing to Switch between--}}
                                    {{--                                            Two Devices--}}
                                    {{--                                        </p>--}}
                                    {{--                                        <input type="hidden" id="pProductSku-15525" value="15525">--}}
                                    {{--                                        <div class="p-price" id="ProductPrice-15525" productprice="Rs: 350"--}}
                                    {{--                                             bis_skin_checked="1">--}}
                                    {{--                                                <span class="sale">--}}
                                    {{--                                                    <s>--}}
                                    {{--                                                        Rs: 550--}}
                                    {{--                                                    </s>--}}
                                    {{--                                                </span>Rs: 350--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </a>--}}
                                    {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                    {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                    {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="15525"--}}
                                    {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                    {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                    {{--                                        <a href="javascript:void(0)" productid="15525" onclick="addToCart(this)"--}}
                                    {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </li>--}}
                                    {{--                                <li class="transall item ">--}}
                                    {{--                                    <a href="#" bis_skin_checked="1">--}}

                                    {{--                                            <span id="productCircle-7772">--}}
                                    {{--                                                <span class="circle">--}}
                                    {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                    {{--                                                    <span class="usp">--}}
                                    {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                    {{--                                                            off<br>--}}
                                    {{--                                                            <span class="usp_off">--}}
                                    {{--                                                                25 %--}}
                                    {{--                                                            </span>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                    </span>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </span>--}}

                                    {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                    {{--                                            <img id="ProductImage-7772"--}}
                                    {{--                                                 productimage="images/productimages/636583695190348246.jpg"--}}
                                    {{--                                                 src="/images/productimages/636583695190348246.jpg" alt=""--}}
                                    {{--                                                 class="img-responsive">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <p id="ProductTitle-7772"--}}
                                    {{--                                           producttitle="Honda BRV Fog Lamps / Fog Lights DRL Covers Dual - Model 2017-2019">--}}
                                    {{--                                            Honda BRV Fog Lamps / Fog Lights DRL Covers Dual - Model 2017-2019--}}
                                    {{--                                        </p>--}}
                                    {{--                                        <input type="hidden" id="pProductSku-7772" value="304289">--}}
                                    {{--                                        <div class="p-price" id="ProductPrice-7772" productprice="Rs: 6,000"--}}
                                    {{--                                             bis_skin_checked="1">--}}
                                    {{--                                                <span class="sale">--}}
                                    {{--                                                    <s>--}}
                                    {{--                                                        Rs: 8,015--}}
                                    {{--                                                    </s>--}}
                                    {{--                                                </span>Rs: 6,000--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </a>--}}
                                    {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                    {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                    {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="7772"--}}
                                    {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                    {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                    {{--                                        <a href="javascript:void(0)" productid="7772" onclick="addToCart(this)"--}}
                                    {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </li>--}}


                                </ul>
                            </div>
                        </div>
                    </div>


                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}

                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}

                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}

                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-4">--}}
                    {{--                            <center>--}}
                    {{--                                <div class="card border-light prCard" style="max-width: 18rem;">--}}
                    {{--                                    <img class="card-img-top" src="https://previews.123rf.com/images/aleksanderdn/aleksanderdn1809/aleksanderdn180900026/107753781-set-of-car-parts-isolated-on-white-background-3d.jpg" alt="Card image cap">--}}
                    {{--                                    <div class="card-body text-center x">--}}
                    {{--                                        <p class="card-text">Universal Evo Rack Air Flow <br>Hood Pair | Automative</p>--}}
                    {{--                                        <h5 class="card-title text-danger">Product Name</h5>--}}
                    {{--                                        <button class="btn btn-dark text-white"><i class="fa fa-search"></i></button>--}}
                    {{--                                        <button class="btn text-white aCartbtn"><i class="fa fa-cart-plus"></i> Add to cart</button>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </center>--}}
                    {{--                        </div>--}}

                    {{--                    </div>--}}

                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection
