<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    protected $appends = ['full_name', 'status_label'];
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELED = 'canceled';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_PENDING = 'pending';

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getStatusLabelAttribute()
    {
        if ($this->status == self::STATUS_PENDING) {
            return '<span class="badge badge-warning">' . ucfirst(self::STATUS_PENDING) . ' </span>';
        } elseif ($this->status == self::STATUS_DELIVERED) {
            return '<span class="badge badge-info">' . ucfirst(self::STATUS_DELIVERED) . ' </span>';
        } elseif ($this->status == self::STATUS_CANCELED) {
            return '<span class="badge badge-danger">' . ucfirst(self::STATUS_CANCELED) . ' </span>';
        } elseif ($this->status == self::STATUS_COMPLETED) {
            return '<span class="badge badge-success">' . ucfirst(self::STATUS_COMPLETED) . ' </span>';
        } else {
            return '<span class="badge badge-warning">' . ucfirst(self::STATUS_PENDING) . ' </span>';
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity');
    }
}
