<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\MakeModel;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function show(SubCategory $subCategory)
    {
        $models = MakeModel::query()->limit(10)->groupBy('make_id')->get();
        return view('frontend.sub-categories.show', compact('subCategory', 'models'));
    }
}
