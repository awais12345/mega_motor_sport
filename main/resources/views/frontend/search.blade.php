@extends('frontend.master')
@section('title'){{'Search'}}@stop
@section('content')
    @include('frontend.partials.inner-nav',[
    'model_id' => $make_id,
    'make_id' => $model_id,
    'year' => $year]);

{{--    <div class="head-shop-by-links"><span class="head-shop-by-links-item"><a class="link"--}}
{{--                                                                             href="#">shop by product</a><span--}}
{{--                class="count"> (14,304,493)</span></span><span class="head-shop-by-links-item"><a class="link" href="#">shop by brand</a><span--}}
{{--                class="count"> (3,251)</span></span></div>--}}
{{--    </div>--}}

    <style>
        .ptype-grid {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            list-style: none;
            margin: 16px 0 32px !important;
            overflow: hidden;
        }

        .ptype-grid.-tile > .li {
            background: #fff;
        }

        .ptype-grid > .li {
            box-sizing: border-box;
            height: auto;
            margin: 2% !important;
            overflow: hidden;
            position: relative;
            text-align: center;
            width: 46% !important;
        }

        .ptype-grid > .li::before {
            color: #a9a9a9;
            content: attr(data-qty);
            font-size: 0.8em !important;
            left: 0;
            position: absolute;
            right: 0;
            top: 4% !important;
            z-index: 2;
        }

        .ptype-grid.-col-5 > .li > .lazy-loading {
            display: block;
            padding-top: 52% !important;
            top: 22% !important;
            width: 80% !important;
        }

        .ptype-grid-img {
            height: auto;
            left: 0 !important;
            margin: auto !important;
            max-width: 100% !important;
            position: absolute !important;
            right: 0 !important;
            top: 22% !important;
            width: 90% !important;
        }

        .ptype-grid-a {
            background: url(/images/search/44d36080.svg) 0 0 no-repeat;
            box-sizing: border-box;
            color: #111;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex !important;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap !important;
            font-size: 0.8em !important;
            font-weight: 500 !important;
            left: 0;
            line-height: 1.3 !important;
            padding: 95% 8px 12px !important;
            position: relative;
            table-layout: fixed;
            text-decoration: none;
            width: 100%;
        }

        .ptype-grid-a:hover {
            text-decoration: none;
        }

        .ptype-grid-a:hover > .ptype-grid-title, .ptype-grid-a.-simple-title:hover {
            text-decoration: none;
        }

        .ptype-grid-a::before {
            background: url(/images/search/44d36080.svg) 0 0 no-repeat;
            content: '';
            display: block;
            height: 150% !important;
            left: 0 !important;
            position: absolute !important;
            top: 0 !important;
            width: 100%;
            z-index: 2;
        }

        .ptype-grid-a::after {
            color: #757575;
            content: attr(data-descr);
            font-size: 12px !important;
            font-weight: 400 !important;
            line-height: 1.25 !important;
            position: relative;
            text-transform: none;
        }

        .ptype-grid-a.-simple-title::after {
            padding-top: 6px !important;
        }

        .ptype-grid-a.-simple-title, .ptype-grid-title {
            font-weight: 400;
        }

        .ptype-grid-title {
            display: block;
            padding-bottom: 6px !important;
        }

        .ptype-grid-title.-truncate {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 100%;
        }

        .ptype-grid > .li {
            margin: 6px 1% !important;
            /*width: 31.333%;*/
            width: 48% !important;
        }

        .ptype-grid.-col-5 .ptype-grid-a, .ptype-grid.-col-6 .ptype-grid-a {
            padding-bottom: 16px !important;
            padding-top: 100% !important;
        }

        .ptype-grid.-col-5 .ptype-grid-img, .ptype-grid.-col-6 .ptype-grid-img {
            top: 14%!important;
        }

        .ptype-grid.-col-5 > .li > .lazy-loading {
            padding-top: 58%!important;
            top: 14%!important;
            width: 90%!important;
        }

        .ptype-grid-a {
            padding-top: 97%!important;
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px) {
            .ptype-grid > .li {
                margin: 8px 1%!important;
                width: 31.333%!important;
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 58%!important;
                width: 90%!important;
            }

            .ptype-grid-a {
                font-size: 0.9em!important;
                padding-top: 105%!important;
            }

            .ptype-grid-a::after {
                font-size: 11px!important;
                line-height: 1!important;
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px) {
            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 52%!important;
                width: 80%!important;
            }

            .ptype-grid-img {
                width: 80% !important;
            }

            .ptype-grid-a {
                padding-top: 88% !important;
            }

        }

        @media only screen and (min-width: 1281px) and (max-width: 1600.98px) {
            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 46%!important;
                top: 60px!important;
                width: auto !important;
            }
        }

        @media only screen and (min-width: 1601px) {
            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 46% !important;
                top: 60px !important;
                width: auto !important;
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .ptype-grid > .li::before {
                top: 10px !important;
            }

            .ptype-grid {
                font-size-adjust: 100%;
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .ptype-grid > .li {
                margin: 8px 0.65%;
                width: 23.5% !important;
            }

            .ptype-grid.-col-5 > .li {
                width: 18% !important;
            }

            .ptype-grid-a {
                font-size: 0.9em !important;
                left: auto !important;
            }

            .ptype-grid-a:hover > .ptype-grid-title, .ptype-grid-a.-simple-title:hover {
                text-decoration: underline;
            }
        }

        @media only screen and (max-width: 420.98px), only screen and (min-width: 421px) and (max-width: 740.98px) {
            .ptype-grid.-col-5 .ptype-grid-a.-descr-outer::after, .ptype-grid.-col-6 .ptype-grid-a.-descr-outer::after {
                display: none !important;
            }

            .ptype-grid.-col-5 > li::before {
                display: none !important;
            }

        }

        @media only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .ptype-grid-img {
                /*top: 60px !important;
                width: auto !important;*/
            }

            .ptype-grid-a {
                padding-top: 200px !important;
            }
        }

        @media only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .ptype-grid {
                margin-top: 32px !important;
            }

            .ptype-grid-a {
                padding-bottom: 24px !important;
            }
        }
        

.ribbon3 {
    width: 59px;
    height: 27px;
    font-size: 11px;
    line-height: 27px;
    padding-left: 15px;
    position: absolute;
    left: -8px;
    top: 20px;
    font-weight: bolder;
    color: #fff;
    z-index: 2;
    background: var(--red-color);
  }
  .ribbon3:before, .ribbon3:after {
    content: "";
    position: absolute;
  }
  .ribbon3:before {
    height: 0;
    width: 0;
    top: -8.5px;
    left: 0.1px;
    border-bottom: 9px solid black;
    border-left: 9px solid transparent;
  }
  .ribbon3:after {
    height: 0;
    width: 0;
    right: -14.5px;
    border-top: 13px solid transparent;
    border-bottom: 14px solid transparent;
    border-left: 16px solid var(--red-color);
  }
  .buy-now-btn{
      padding: 4px 38px;
      border: 2px solid var(--blue-color);
      background-color: var(--blue-color);
      color: #FFF;
  }
  .buy-now-price
  {
      font-size: 110%; font-weight: 700; padding: 4px 40px; color: var(--blue-color); border: 2px solid var(--blue-color); text-align: center !important; width: 100%
  }
  @media only screen and (max-width: 360px) {
      .buy-now-btn{
          padding: 4px 12px
      }
      .buy-now-price{
          padding: 4px 12px
      }
      .price-section{
          flex-direction: column !important;
      }
  }
  @media only screen and (max-width: 500px)
  {
      .price-section{
          flex-direction: column !important;
      }
      .buy-now-price{
          font-size: smaller;
      }
      .ico
      {
          position: relative !important;
      }
      del::after
      {
          content: '\a';
          white-space: pre
      }
  }
  @media only screen and (max-width: 1557px) {
      .buy-now-btn{
          padding: 4px 17px
      }
      .buy-now-price{
          padding: 4px 34px
      }
      .price-section{
          flex-direction: row;
      }

  }
  @media only screen and (max-width: 1290px) {
      .buy-now-btn{
          padding: 4px 18px
      }
      .buy-now-price{
          padding: 4px 9px
      }
      .price-section{
          flex-direction: row;
      }
  }
  .price-section{
      flex-direction: row;
  }


    </style>
    <div class="container-fluid text-center" style="background-color: rgba(232, 236, 241, 0.8); padding-bottom: 20px; width: 100%">

        @forelse($data as $category => $products)
        
            <h3 style="padding-top: 25px; padding-bottom: 25px;" >{{$category}}</h3>
            <ul style="padding:0px;"
                                    class="owl-carousel owl-theme catProSlider list-inline indexProductList p-hover shopList">
           
                                    @foreach($products as $product)
                                    <li class="transall item " style="min-height: 281px"><!---->
                                                <div class="li-item" bis_skin_checked="1">
                                                    @if($product->price != $product->discount_price)

                                                        <span class="ribbon3">{{ $product->discount }} % Off</span>

                                                    @endif
                                                    <a href="{{route('products.show', $product->id)}}">
                                                    <div class="p-image" bis_skin_checked="1">                                                    
                                                        <img id="ProductImage-16129"
                                                            <?php 
                                                                if(!is_null($product->img)){
                                                                    $imgSrc = asset('main/storage/app/public/'.$product->img);
                                                                }
                                                                elseif(!is_null($product->images()->first()->image)){
                                                                    $imgSrc = asset('main/storage/app/public/'.$product->images()->first()->image);
                                                                }
                                                                else{
                                                                    $imgSrc = '/admin/img/no-photo.png';
                                                                }
                                                            ?>
                                                             productimage="{{$imgSrc}}"
                                                             src="{{$imgSrc}}"
                                                             alt="{{$product->name}}"
                                                             style="height: 135px !important; width: auto !important;"
                                                             class="img-responsive">                                                    
                                                    </div>
                                                    <p id="ProductTitle-16129"
                                                       producttitle="{{$product->name}}">
                                                        {{$product->name}}
                                                    </p>
                                                    </a>
                                                    <input type="hidden" id="pProductSku-16129" value="16129">
                                                    <div class="container-fluid">
                                                    <div class="row text-center">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 price-section" style="display: flex; justify-content: center; flex-direction: row !important">

                                                        <a class="m-0 buy-now-btn" style="width: 10% !important; color: #FFF; background-color:var(--blue-color);border: inherit" href="{{ route('products.add.cart', [$product->id]) }}">
                                                            <span  style="font-size: large">	<i class="fa fa-shopping-cart ico" style="position: absolute; top: 50%; transform: translate(-30%,-50%); color:#fff"></i></span>
                                                        </a>
                                                    <span class="m-0 buy-now-price"> 
                                                    @if($product->price != $product->discount_price)
                                                        <del style="color: var(--red-color); font-size: 80%;">Rs: {{ thousand_separator($product->price) }}</del>
                                                        @endif
                                                        Rs: {{ thousand_separator($product->discount_price) }}</span></div>
                                                    </div>


                                                </div>
                                                </a>
                                            </li>
                                            @endforeach
            </ul>
           
           
           
          

                    
        
        @empty
            <h3 class="text-danger text-center mt-5">Record Not Matched!</h3>
        @endforelse
    </div>

@endsection
