<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::query()->get();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|min:2|max:64']);
        DB::beginTransaction();
        try {
            $category = Category::query()->create([
                'slug' => Str::slug($request->name),
                'name' => $request->name,
            ]);
            $this->saveImage($request, $category);
            DB::commit();
            session()->flash('success', 'Added Successfully!');
            return redirect()->route('admin.categories.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', 'Something went wrong!');
            return back()->withInput($request->all());
        }

    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request, ['name' => 'required|min:2|max:64']);
        DB::beginTransaction();
        try {
            $category->update([
                'slug' => Str::slug($request->name),
                'name' => $request->name,
            ]);
            $this->saveImage($request, $category);
            DB::commit();
            session()->flash('success', 'Updated Successfully!');
            return redirect()->route('admin.categories.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', 'Something went wrong!');
            return back()->withInput($request->all());
        }

    }

    /**
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Category $category)
    {
        DB::beginTransaction();
        try {
            $category->childrens()->delete();
            $category->delete();
            DB::commit();
            session()->flash('success', 'Deleted Successfully!');
            return redirect()->route('admin.categories.index');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Something Went Wrong!');
            return back();
        }
    }

    /**
     * @param Request $request
     * @param $subCategory
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveImage(Request $request, $category)
    {
        if ($request->hasFile('img')) {
            $this->validate($request, [
                'img' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            $category->img = storeImage($request->file('img'), 'categories');
        }
        $category->save();
    }
}
