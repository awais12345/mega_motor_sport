<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Make;
use App\models\MakeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MakeController extends Controller
{

    public function index()
    {
        $makes = Make::query()->get();
        return view('admin.makes.index', compact('makes'));
    }


    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|min:2|max:64']);
        DB::beginTransaction();
        try {
            Make::query()->create([
                'slug' => Str::slug($request->name),
                'name' => $request->name,
            ]);
            DB::commit();
            session()->flash('success', 'Added Successfully!');
            return redirect()->route('admin.makes.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', 'Something went wrong!');
            return back()->withInput($request->all());
        }

    }


    public function edit(Make $make)
    {
        return view('admin.makes.edit', compact('make'));
    }


    public function update(Request $request, Make $make)
    {
        $this->validate($request, ['name' => 'required|min:2|max:64']);
        DB::beginTransaction();
        try {
            $make->update([
                'slug' => Str::slug($request->name),
                'name' => $request->name,
            ]);
            DB::commit();
            session()->flash('success', 'Added Successfully!');
            return redirect()->route('admin.makes.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', 'Something went wrong!');
            return back()->withInput($request->all());
        }

    }


    public function destroy(Make $make)
    {
        DB::beginTransaction();
        try {
            MakeModel::query()->where('make_id', $make->id)->delete();
            $make->delete();
            DB::commit();
            session()->flash('success', 'Deleted Successfully!');
            return redirect()->route('admin.makes.index');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Something Went Wrong!');
            return back();
        }
    }
}
