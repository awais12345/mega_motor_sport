@extends('admin.master')
@section('title') Dashboard @stop

@section('content')


    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Begin Breadcrumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.orders.index')}}">Orders</a></li>
                <li class="breadcrumb-item active" aria-current="page">Order #{{ $order->id }}</li>
            </ol>
        </nav>
        <!-- end Breadcrumb -->

        <!-- Page Heading -->

        <div class="card">
            <div class="card-body">

                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="col-12">
                        <h1 class="h3 mb-0 text-gray-800">Order #{{ $order->id }} / {!! $order->status_label !!}
                            <div class="dropdown float-right" >
                                <a class="link" href="#" role="button" id="dropdownMenuLink1"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class='fa fa-ellipsis-v' style="font-size: 20px"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item"
                                       href="{{route('admin.orders.status', [$order->id, 'pending'])}}">Pending</a>
                                    <a class="dropdown-item"
                                       href="{{route('admin.orders.status', [$order->id, 'delivered'])}}">Deliver</a>
                                    <a class="dropdown-item"
                                       href="{{route('admin.orders.status', [$order->id, 'completed'])}}">Complete</a>
                                    <a class="dropdown-item"
                                       href="{{route('admin.orders.status', [$order->id, 'canceled'])}}">Cancel</a>
                                </div>
                            </div>
                        </h1>

                    </div>
                </div>

                @include('shared.errors')
                <div class="row">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card-body">
                                    <h4>Order Summary</h4>
                                    <h6><strong>Order ID:</strong> {{ $order->id }}</h6>
                                    <h6><strong>Order status:</strong> {!! $order->status_label !!}</h6>
                                    <h6><strong>Products:</strong> {{ count($order->products) }}</h6>
                                    <h6><strong>Total Qty:</strong> {{ $order->products()->sum('order_product.quantity') }}</h6>
                                    <h6><strong>Total Price:</strong> {{ $order->total }}</h6>
                                    <h6><strong>Order Date:</strong> {{ date('d-F-y', strtotime($order->created_at)) }}</h6>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card-body">
                                    <h4>Customer Details</h4>
                                    <h6><strong>Name</strong> {{ $order->first_name.' '. $order->last_name}}</h6>
                                    <h6><strong>Phone</strong> {{ $order->phone }}</h6>
                                    <h6><strong>Email:</strong> {{ $order->email }}
                                        <h6><strong>Shipping Address:</strong> {{ $order->address }}
                                            <h6><strong>City:</strong> {{ $order->city }}
                                                <h6><strong>Province:</strong> {{ $order->province }} </h6>
                                                <h6><strong>Zip:</strong> {{ $order->zip }}</h6>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-12">

                        <!-- Illustrations -->
                        <div class="card shadow mb-12">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Products</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="cart" class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Product#</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Subtotal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->products as $product)
                                            <tr>
                                                <td>#{{ $product->id }}</td>
                                                <td>
                                                    <img
                                                        src="{{ $product->images()->first() ? asset('main/storage/app/public/'.$product->images()->first()->image) :'/admin/img/no-photo.png'}}"
                                                        style="width: 60px;height: 60px" alt="..."
                                                        class="img-responsive"/>
                                                </td>
                                                <td>{{ $product->name }}</td>
                                                <td class="text-center">{{ $product->discount_price }}</td>
                                                <td>
                                                    {{ @$product->pivot->quantity }}
                                                </td>
                                                <td>{{ $product->discount_price * $product->qty }}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <th colspan="5"></th>
                                            <th><strong>Total: {{ $order->total }}</strong></th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
@section('scripts')

@endsection
