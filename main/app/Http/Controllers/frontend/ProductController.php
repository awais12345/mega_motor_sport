<?php

namespace App\Http\Controllers\frontend;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Models\Make;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class ProductController extends Controller
{
    public function create()
    {
        return view('frontend.productadd');
    }

    public function show(Product $product)
    {
        return view('frontend.products.show', compact('product'));
    }

    public function search()
    {
        $make_id = \request()->filled('make_id') ? request()->make_id : null;
        $model_id = \request()->filled('model_id') ? \request()->model_id : null;
        $year = \request()->filled('year') ? request()->year : null;
        $keyword = \request()->filled('keyword') ? request()->keyword : null;

        if (!is_null($keyword)) {
            $data = Product::query()->with(Product::$ALL_RELATIONS)
                ->where('name', 'like', '%' . $keyword . '%')
                ->get()
                ->groupBy('categories.*.name');

            if(count($data) == 0){
                $searchWords = explode(' ', $keyword);
                $data = Product::query()->with(Product::$ALL_RELATIONS);
                foreach($searchWords as $word){
                    $data->orWhere('name', 'LIKE', '%'.$word.'%');
                }
                $data = $data->distinct()->get();
                $data = $data->groupBy('categories.*.name');    
            }
        } else {
            $data = Product::query()->with(Product::$ALL_RELATIONS)
                ->when($make_id, function ($query) use ($make_id) {
                    $query->whereHas('makes', function ($q) use ($make_id) {
                        $q->where('makes.id', $make_id);
                    });
                })
                ->when($model_id, function ($query) use ($model_id) {
                    $query->whereHas('makeModels', function ($q) use ($model_id) {
                        $q->where('make_models.id', $model_id);
                    });
                })->when($year, function ($query) use ($year) {
                    $query->whereHas('years', function ($q) use ($year) {
                        $q->where('year', $year);
                    });
                })
                ->get()
                ->groupBy('categories.*.name');
        }

        return view('frontend.search', compact('data', 'make_id', 'model_id', 'year'));
    }

    public function modelOptions(Make $make)
    {
        return view('frontend.partials.model-options', compact('make'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewCart()
    {
        return view('frontend.products.cart');
    }

    public function checkout()
    {
//        if(Auth::user()){
            return view('frontend.products.checkout');
//        }
//        return redirect('/login');
    }

    public function addToCart(Product $product, Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $qty = $request->qty ? $request->qty : 1;
        $cart->addProduct($product, $qty);
        Session::put('cart', $cart);
        return back()->with('success', 'Product <b>' . $product->name . '</b> has been added successfully!');
    }

    public function updateCart(Product $product, Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->updateProduct($product, $request->qty);
        Session::put('cart', $cart);
        return back()->with('success', 'Product <b>' . $product->name . '</b> has been successfully Updated in the Cart');
    }

    public function removeCart(Product $product)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeProduct($product);
        Session::put('cart', $cart);
        return back()->with('success', 'Product <b>' . $product->name . '</b> has been successfully removed From the Cart');
    }

    public function placeOrder(Request $request)
    {
        $rules = [
            'first_name' => 'required|min:3',
            'phone' => 'required',
            'email' => 'sometimes|email',
            'address' => 'required'
        ];
        $this->validate($request, $rules);
        if (Session::has('cart')) {
            $cart = session()->get('cart');
            $total = $cart->getTotalPrice();
            if($cart->getTotalPrice() < 1000){
                $total = $total + 200;
            }
            try {
                $order = Order::query()->create([
                    'user_id' => auth()->user() ? auth()->user()->id : null,
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'city' => $request->city,
                    'province' => $request->province,
                    'zip' => $request->zip,
                    'total' => $total,
                ]);
                //insert into order_product table
                foreach ($cart->getContents() as $data) {
                    OrderProduct::query()->create([
                        'order_id' => $order->id,
                        'product_id' => $data['product']->id,
                        'quantity' => $data['qty'],
                    ]);
                }
            } catch (Exception $e) {
                return back()->with('warning', 'Something went wrong!');
            }
        } else {
            return back()->with('warning', 'Your cart is empty');
        }

        Session::forget('cart');
        return redirect('/thank-you');
    }

    public function thankyou()
    {
        return view('frontend.thankyou');
    }

}
