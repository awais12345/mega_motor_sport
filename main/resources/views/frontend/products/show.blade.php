@extends('frontend.master')

@section('title'){{'Product Detail Page'}}@stop

@section('styles')

    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"/>

    <link href="https://fonts.googleapis.com/css2?family=Baloo+Tamma+2&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Monda:wght@400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/css/easyzoom.css">

    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/splide-skyblue.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">

    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" type="text/javascript"></script>-->

    <link rel="stylesheet" href="/css/zoomy.css">
    <script src="/js/zoomy.js"></script>
    <script src="/js/splide.min.js"></script>
    <script>

   document.addEventListener( 'DOMContentLoaded', function () {
	var secondarySlider = new Splide( '#secondary-slider', {
        rewind      : true,
	fixedWidth  : 100,
	fixedHeight : 64,
	isNavigation: true,
	gap         : 10,
	focus       : 'center',
	pagination  : false,
	cover       : true,
	breakpoints : {
		'600': {
			fixedWidth  : 66,
			fixedHeight : 40,
		}
	},
	} ).mount();
	
	var primarySlider = new Splide( '#primary-slider', {
		type       : 'fade',
        heightRatio: 1,
    fixedWidth  : 550,
	fixedHeight : 370,
	pagination : false,
	arrows     : false,
	cover      : true,
	} ); // do not call mount() here.
	
	primarySlider.sync( secondarySlider ).mount();
} );
    </script>

    <style>

        #myTabContent {

            padding-bottom: 25px;

        }

        .mainpText {

            font-family: 'Monda', sans-serif;

        }

        .mainpText h2 {

            font-weight: 700;

        }

        .mainpText .pRice {

            font-size: 24px;

            font-weight: 700;


        }

        .mainpText select {

            background: none;

            border: none;

        }

        .mainpText a {

            text-decoration: none;

            color: #111111;

            transition: 0.5s ease;

        }

        .borderTopNav {

            border-top: solid 2.5px;

            transition: all .5s ease;

        }

        .mainpText p {

            font-size: 14px;

            font-family: 'Baloo Tamma 2', cursive;

        }

        .sidebarSec {

            width: 100%;

        }

        .sidebarSec div {

            display: inline-block;

        }

        .sidebarSec a {

            color: black;

            text-decoration: none;

            font-size: 11px;

        }

        .sidebarSec a:hover {

            color: royalblue;

            text-decoration-line: underline;

            cursor: pointer;

        }

        .product__carousel {

            display: block;

            max-width: 700px;

            margin: 1em auto 3em;

        }

        .product__carousel a {

            display: block;

            margin-bottom: 15px;

        }


        .product__carousel .gallery-top {

            border: 1px solid #ebebeb;

            border-radius: 3px;

            margin-bottom: 5px;

        }

        .product__carousel .gallery-top .swiper-slide {

            position: relative;

            overflow: hidden;

        }

        .product__carousel .gallery-top .swiper-slide a {

            position: relative;

            display: flex;

            justify-content: center;

            align-items: center;

            width: 100%;

            height: 100%;

        }

        .product__carousel .gallery-top .swiper-slide a img {

            width: 100%;

            height: 100%;

            object-fit: contain;

        }

        .product__carousel .gallery-top .swiper-slide .easyzoom-flyout img {

            min-width: 100%;

            min-height: 100%;

        }

        .product__carousel .swiper-button-next.swiper-button-white,
        .product__carousel .swiper-button-prev.swiper-button-white {

            color: #ff3720;

        }

        .product__carousel .gallery-thumbs .swiper-slide {

            position: relative;

            transition: border .15s linear;

            border: 1px solid #ebebeb;

            border-radius: 3px;

            cursor: pointer;

            overflow: hidden;

            height: calc(100% - 2px);

        }

        .product__carousel .gallery-thumbs .swiper-slide.swiper-slide-thumb-active {

            border-color: #000;

        }

        .product__carousel .gallery-thumbs .swiper-slide img {

            position: absolute;

            left: 50%;

            top: 50%;

            transform: translate(-50%, -50%);

            max-width: 100%;

        }

        /*	=======    Sliding Card css      ==============*/

        @media (min-width: 768px) {

            /* show 4 items */
            .carousel-inner .active,
            .carousel-inner .active + .carousel-item,
            .carousel-inner .active + .carousel-item + .carousel-item,
            .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item {

                display: block;

            }


            .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
            .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
            .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item,
            .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {

                transition: none;

            }


            .carousel-inner .carousel-item-next,
            .carousel-inner .carousel-item-prev {

                position: relative;

                transform: translate3d(0, 0, 0);

            }


            .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {

                position: absolute;

                top: 0;

                right: -25%;

                z-index: -1;

                display: block;

                visibility: visible;

            }


            /* left or forward direction */
            .active.carousel-item-left + .carousel-item-next.carousel-item-left,
            .carousel-item-next.carousel-item-left + .carousel-item,
            .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
            .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item,
            .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item {

                position: relative;

                transform: translate3d(-100%, 0, 0);

                visibility: visible;

            }


            /* farthest right hidden item must be absolue position for animations */
            .carousel-inner .carousel-item-prev.carousel-item-right {

                position: absolute;

                top: 0;

                left: 0;

                z-index: -1;

                display: block;

                visibility: visible;

            }


            /* right or prev direction */
            .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
            .carousel-item-prev.carousel-item-right + .carousel-item,
            .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
            .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item,
            .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item {

                position: relative;

                transform: translate3d(100%, 0, 0);

                visibility: visible;

                display: block;

                visibility: visible;

            }

        }

        .upsellCard {

            position: relative;

            transform: scale(0.90);

            transition: box-shadow 0.5s, transform 0.5s;

        }

        .upsellCard:hover {

            transform: scale(1);

            box-shadow: 5px 20px 30px rgba(0, 0, 0, 0.2);

        }

        .cartBtn {

            border-color: Black;

            color: #fff;

            background: var(--blue-color);

            position: relative;

            transform: scale(0.85);
            left: -4px
            transition: box-shadow 0.5s, border 0.5s, transform 0.5s;

        }

        .cartBtn:hover {

            transform: scale(1);

            border-color: black;

            background: black;

            color: #fff;

            transition: border .5s ease, background .5s ease, transform 0.5s ease;

        }


        /*===========   Top Soled Products ==================*/

        .topSaledProduct {

            display: flex;

            justify-content: center;

            align-items: center;

            flex-wrap: wrap;

            font-family: 'Montserrat', sans-serif;

        }


        .topSaledProduct .card {

            position: relative;

            width: 250px;

            height: 260px;

            margin: 10px;

            overflow: hidden;

            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);

            transition: all .2s linear;

        }

        @media screen and (max-width: 516px) {
            .topSaledProduct .card {

                width: 44.4% !important;
                display: inline-block;
                height: 180px;
            }

            .topSaledProduct .card-category {
                font-size: 14px !important;
            }

            .topSaledProduct .card-content {
                top: 1.5rem !important;
            }
        }

        @media screen and (max-width: 960px) and (min-width: 516px) {
            .topSaledProduct .card {
                width: 46.10% !important;
                display: inline-block;
                height: 180px;
            }

            .topSaledProduct .card-category {
                font-size: 15px !important;
            }

            .topSaledProduct .card-content {
                top: 0rem !important;
            }
        }

        .topSaledProduct img {

            width: 100%;

            transition: all .2s linear;

        }


        .topSaledProduct .card-content {

            position: absolute;

            /*bottom: 3rem;*/
            top: 3.5rem;
            height: 180px;
            right: 100%;

            width: 100%;

            text-align: center;

            background-color: rgba(255, 255, 255, .9);

            transition: all .2s linear;

        }


        .topSaledProduct a {

            text-decoration: none;


        }


        .topSaledProduct .card-category {

            font-size: 17px;

            line-height: 20px;

            letter-spacing: 0px;

            margin: 15px;

            color: #000;

        }


        .topSaledProduct i {

            margin-left: 5px;

        }


        .topSaledProduct .card:hover {

            box-shadow: 0 0px 28px rgba(0, 0, 0, 0.25), 0 4px 10px rgba(0, 0, 0, 0.22)

        }


        .topSaledProduct .card:hover .card-content {

            right: 0;

        }


        .topSaledProduct .card:hover img {

            transform: scale(1.1);

        }

        /*===========   Top Soled Products  End==================*/

    </style>
    <style>


        .ribbon3 {
            width: 59px;
            height: 27px;
            font-size: 11px;
            line-height: 27px;
            padding-left: 15px;
            position: absolute;
            left: -8px;
            top: 20px;
            font-weight: bolder;
            color: #fff;
            z-index: 2;
            background: var(--red-color);
        }

        .ribbon3:before, .ribbon3:after {
            content: "";
            position: absolute;
        }

        .ribbon3:before {
            height: 0;
            width: 0;
            top: -8.5px;
            left: 0.1px;
            border-bottom: 9px solid black;
            border-left: 9px solid transparent;
        }

        .ribbon3:after {
            height: 0;
            width: 0;
            right: -14.5px;
            border-top: 13px solid transparent;
            border-bottom: 14px solid transparent;
            border-left: 16px solid var(--red-color);
        }

        .buy-now-btn {
            padding: 4px 15px;
            border: 2px solid var(--blue-color);
            background-color: var(--blue-color);
            color: #FFF;
        }

        .buy-now-price {
            font-size: 110%;
            font-weight: 700;
            padding: 4px 40px;
            color: var(--blue-color);
            border: 2px solid var(--blue-color);
            text-align: center !important;
            width: 100%
        }

        @media only screen and (max-width: 360px) {
            .buy-now-btn {
                padding: 4px 12px
            }

            .buy-now-price {
                padding: 4px 12px
            }

            .price-section {
                flex-direction: column !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .price-section {
                flex-direction: column !important;
            }
            .buy-now-price{
          font-size: smaller;
              }
            .ico {
                position: relative !important;
                transform: translate(1, 0) !important;
            }

            del::after {
                content: '\a';
                white-space: pre
            }
            .txt
            {
                width: 260px !important;
            }
        }

        @media only screen and (max-width: 1557px) {
            .buy-now-btn {
                padding: 4px 17px
            }

            .buy-now-price {
                padding: 4px 34px
            }

            .price-section {
                flex-direction: row;
            }
            .txt
            {
                width: 450px;
            }

        }

        @media only screen and (max-width: 1290px) {
            .buy-now-btn {
                padding: 4px 18px
            }

            .buy-now-price {
                padding: 4px 9px
            }

            .price-section {
                flex-direction: row;
            }
        }

        .price-section {
            flex-direction: row;
        }

        @media only screen and (max-width: 730px) {
            .relProd {
                width: 50% !important;
            }
        }
        #carousel-example-generic {
    margin: 20px auto;
    width: 400px;
    height
}

#carousel-custom {
    margin: 20px auto;
    width: 100%;
}
#carousel-custom .carousel-indicators {
    margin: 10px 0 0;
    overflow: auto;
    position: static;
    text-align: left;
    white-space: nowrap;
    width: 100%;
}
#carousel-custom .carousel-indicators li {
    background-color: transparent;
    -webkit-border-radius: 0;
    border-radius: 0;
    display: inline-block;
    height: auto;
    margin: 0 !important;
    width: 250px;
}
#carousel-custom .carousel-indicators li img {
    display: block;
    opacity: 0.5;
}
#carousel-custom .carousel-indicators li.active img {
    opacity: 1;
}
#carousel-custom .carousel-indicators li:hover img {
    opacity: 0.75;
}
#carousel-custom .carousel-outer {
    position: relative;
}
.carousel-inner .item img
{
    margin: auto;
    width: 400px
}
    </style>

@endsection

@section('content')

    <!-- ========= Product-Section   ======== -->
    <div class="bootstrap-menu">
        <section class="Product-Section" style="width: 85%; margin: auto; margin-top: -30px !important">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            @include('shared.errors')
                        </div>
                        <div class="col-md-10">
                            <div class="col-md-6">
                                <div class="product__carousel">
                                    {{--<div class="swiper-container gallery-top" id="el">--}}
                                       {{-- <div class="swiper-wrapper">                                                    --}}
                        {{--                    @foreach($product->images as $image)--}}
                        {{--                        <div class="swiper-slide easyzoom easyzoom--overlay">--}}
                        {{--                            <!---->--}}
                        {{--                            <a href="{{asset('main/storage/app/public/'.$image->image)}}">--}}
                        {{--                                                        <img src="{{asset('main/storage/app/public/'.$image->image)}}">--}}
                        {{--                                                    </a>--}}
                        {{--                        </div>--}}
                        {{--                    @endforeach--}}
                        {{--                </div>--}}
                        {{--            <!-- Add Arrows -->--}}
                        {{--                <div class="swiper-button-next swiper-button-white"></div>--}}
                        {{--                <div class="swiper-button-prev swiper-button-white"></div>--}}
                        {{--            </div>--}}
                        {{--            <div class="swiper-container gallery-thumbs">--}}
                        {{--                <div class="swiper-wrapper">--}}
                        {{--                    @foreach($product->images as $image)--}}
                        {{--                        <div class="swiper-slide">--}}
                        {{--                            <img src="{{ asset('main/storage/app/public/'.$image->image) }}"--}}
                        {{--                                 alt="">--}}
                        {{--                       </div>--}}
                         {{--                                          @endforeach--}}
                         {{--               </div>--}}
                              {{--    </div>--}} 
                              <div id="primary-slider" class="splide">
	<div class="splide__track">
		<ul class="splide__list">
        @foreach($product->images as $image)
			<li class="splide__slide" id="default-zoom-{{ $loop->index }}">
				<img src="{{ asset('main/storage/app/public/'.$image->image) }}">
			</li>
            @endforeach
		</ul>
	</div>
</div>

<br>
                              <div id="secondary-slider" class="splide">
	<div class="splide__track">
		<ul class="splide__list">
        @foreach($product->images as $image)
			<li class="splide__slide">
				<img src="{{ asset('main/storage/app/public/'.$image->image) }}">
			</li>
            @endforeach
		</ul>
	</div>
</div>
                             <!-- <div id='carousel-custom' class='carousel slide' data-ride='carousel'>
    <div class='carousel-outer'>
         Wrapper for slides
        <div class='carousel-inner'>
        @foreach($product->images as $image)
            <div class='item @if($loop->first) {{"active"}} @endif' id="default-zoom-{{ $loop->index }}">
                <img src="{{ asset('main/storage/app/public/'.$image->image) }}" alt='' />
            </div>
        @endforeach
        </div>
            
         Controls 
        <a class='left carousel-control' href='#carousel-custom' data-slide='prev'>
            <span class='glyphicon glyphicon-chevron-left'></span>
        </a>
        <a class='right carousel-control' href='#carousel-custom' data-slide='next'>
            <span class='glyphicon glyphicon-chevron-right'></span>
        </a>
    </div>
    
     Indicators 
    <ol class='carousel-indicators mCustomScrollbar'>
        @foreach($product->images as $image)
           <li data-target='#carousel-custom' data-slide-to='{{ $loop->index }}' class='@if($loop->first) {{"active"}} @endif'><img src="{{ asset('main/storage/app/public/'.$image->image) }}" alt=''/></li>
        @endforeach
    </ol>
</div> -->

                    
                                    <!-- Swiper and EasyZoom plugins end -->
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 " style="padding: 0px 0px 0px 0px;">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 productDesc">
                                        <div class="slider_bottomside">
                                            <p id="text">
                                            </p>
                                            <h1 style="font-size:18px;" id="ProductTitle-14456"
                                                producttitle="{{ $product->name }}"><b>{{ $product->name }}</b></h1>
                                            <p></p>
                                            <p>
                                                SKU : <b>{{ $product->sku }}</b>
                                            </p>
                                            <div class="p-price" id="ProductPrice-14456"
                                                 productprice="RS {{ $product->discount_price }}">
                                                RS: {{ $product->discount_price }}
                                                @if($product->price != $product->discount_price)
                                                    <del class="ml-4"><s>Rs:{{ $product->price }}</s></del>
                                                @endif
                                            </div>
                                        </div>
                                        <form method="GET" action="{{ route('products.add.cart', [$product->id]) }}">
                                        <div class="box boxDec">
                                            <ul class="list-inline">
                                                <li>
                                                    <div class="center">
                                                        <div class="input-group mb-0">
                                                            <div class="input-group-prepend">
                                                                <span class="btn-dark btn-sm" id="minus-btn"
                                                                      style="height: 30px;"><i
                                                                        class="glyphicon glyphicon-minus"></i></span>
                                                            </div>
                                                            <input type="number" id="qty_input" name="qty"
                                                                   class="form-control form-control-sm" value="1"
                                                                   min="1">
                                                            <div class="input-group-prepend">
                                                                <span class="btn-dark btn-sm" id="plus-btn"
                                                                      style="height: 30px;"><i
                                                                        class="glyphicon glyphicon-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="button">
                                            <button type="submit" class="btn btn-primary btn-block" style="line-height: 3">Add to
                                                Cart
                                            </button>
                                        </div>
                                        </form>
                                    </div>
                                    <p class="haq">Have a Question: &nbsp; &nbsp;<b><a href="tel:+923224677726">+92 322
                                                46 77 726</a></b></p>
                                    <p class="haq">Availability: &nbsp; &nbsp;<b>{{ $product->is_available ? 'IN STOCK' : 'OUT OF STOCK' }}</b></p>
                                    <p class="haq">Category: &nbsp; &nbsp;
                                        <b>
                                            @foreach($product->subCategories as $key => $category)
                                                <a href="{{ route('sub-category.show', $category) }}"
                                                   id="haqBtn">{{ $category->name }}</a>
                                                @if($key+1 <>  count($product->subCategories))
                                                    <span>,</span>
                                                @endif
                                            @endforeach
                                        </b>
                                    </p>
                                    <div class="daysbox">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4 col-lg-4">
                                                <div class="box1">
                                                    <img src="/images/free-icon.png" class="img-responsive check_icon"
                                                         width="32" height="32">
                                                    <p class="text1">
                                                        Free Shipping
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4">
                                                <div class="box1">
                                                    <img src="/images/money-back.png" class="img-responsive check_icon">
                                                    <p class="text1">
                                                        Easy and Secure Payments
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4">
                                                <div class="box1">
                                                    <img src="/images/percentage.png" class="img-responsive check_icon">
                                                    <p class="text1">
                                                        Genuine Product
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <ul class="list-inline sm_icon text-center" class="display: flex; justify-content: center;">
                                    <li class="t_icon " style="letter-spacing: 0.1rem; padding-left: 0px"><a href="#" class="txt">Share with Your friends: </a></li>
                                        <li class="t_icon"><a href="#"><img src="/images/twitter_icon.png"
                                                                            class="img-responsive"></a></li>
                                        <li class="f_icon"><a href="#"><img src="/images/facebook_icon.png"
                                                                            class="img-responsive"></a></li>
                                        <li class="i_icon"><a href="#"><img src="/images/instagram_icon.png"
                                                                            class="img-responsive"></a></li>
                                        <li class="in_icon"><a href="#"><img src="/images/in_icon.png"
                                                                             class="img-responsive"></a></li>
                                    </ul>
                                </form>
                            </div>


                            <div class="col-md-12 mainpText">
                                <ul class="nav nav-tabs justify-content-start" id="myTab" role="tablist">
                                    <li class="nav-item tab1 borderTopNav">
                                        <a class="nav-link lead active" role="tab" data-toggle="tab"
                                           href="#tabDescription" id="tabdescription">Description</a>
                                    </li>
                                    <li class="nav-item tab2">
                                        <a class="nav-link lead" role="tab" data-toggle="tab" href="#tabVideo"
                                           id="tabvideo">Video</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane pt-4 active" role="tabpanel" id="tabDescription">
                                        <p> {!! $product->description  !!}</p>
                                    </div>
                                    <div class="tab-pane pt-4" role="tabpanel" id="tabVideo">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="embed-responsive-21by9">
                                                    @if(isset($product->video_url))
                                                        <iframe class="embed-responsive-item w-100 h-100"
                                                            src="{{ $product->video_url ? $product->video_url :"https://www.youtube.com/embed/xnVcQEp-_g8" }}"
                                                            allowfullscreen></iframe>
                                                    @else
                                                        <h3>No Video is attached to this Product.</h3>
                                                    @endif            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2">

                            <h4 class="text-center">Related Products</h4>
                            <div class="slider_rightside">
                            <ul style="padding:0px;"
                                        class="owl-carousel owl-theme catProSlider list-inline indexProductList p-hover shopList">
                                
                                    @if($product->subCategories()->first() && count($product->subCategories()->first()->products))
                                        @foreach($product->subCategories()->first()->products()->limit(4)->latest()->get() as $relatedProduct)
                                          


                                            <li class="transall item relProd" style="width: 100%">
                                                <div class="li-item" bis_skin_checked="1">
                                                    @if($relatedProduct->price != $relatedProduct->discount_price)

                                                        <span
                                                            class="ribbon3">{{ $relatedProduct->discount }} % Off</span>

                                                    @endif
                                                    <a href="{{ route('products.show',  $relatedProduct->id) }}">
                                                    <div class="p-image" bis_skin_checked="1">

                                                    
                                                        <img id="ProductImage-16129"
                                                             productimage="{{$relatedProduct->images()->first() ? asset('main/storage/app/public/'.$relatedProduct->images()->first()->image) :'/admin/img/no-photo.png'}}"
                                                             src="{{$relatedProduct->images()->first() ? asset('main/storage/app/public/'.$relatedProduct->images()->first()->image) :'/admin/img/no-photo.png'}}"
                                                             alt="{{$product->name}}"
                                                             style="height: 135px !important; width: auto !important;"
                                                             class="img-responsive">
                                                             
                                                    </div>
                                                    <p id="ProductTitle-16129"
                                                       producttitle="{{$product->name}}" style="padding: 2px 10px; height: 42px">
                                                       {{ $relatedProduct->name }}
                                                    </p>
                                                    </a>
                                                    <input type="hidden" id="pProductSku-16129" value="16129">
                                                    <div class="container">
                                                        <div class="row text-center">
                                                            <div
                                                                class="col-xs-12 col-sm-12 col-md-12 col-lg-12 price-section"
                                                                style="display: flex; justify-content: center; flex-direction: row !important; margin-top: 8px">

                                                                <a class="m-0 buy-now-btn" style=" width: 10%; color: #FFF; background-color:var(--blue-color);border: inherit" href="{{ route('products.add.cart', [$relatedProduct->id]) }}">
                                                                    <span  style="font-size: large;">	<i class="fa fa-shopping-cart ico" style="position: absolute; top: 50%; transform: translate(-30%,-50%); color:#fff"></i></span>
                                                                </a>

                                                                <span class="m-0 buy-now-price" >
                                                                    @if($relatedProduct->price != $relatedProduct->discount_price)

                                                                    <del
                                                                        style="color: var(--red-color); font-size: 80%;">Rs: {{ thousand_separator($relatedProduct->price) }}</del>
                                                                    @endif
                                                                    Rs: {{ thousand_separator($relatedProduct->discount_price) }}</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
{{--            <!----}}
{{--      <div class="col-md-3">--}}

{{--          <div class="sidebarSec">--}}

{{--              @foreach(\App\Models\Product::query()->limit(4)->latest()->get() as $latestProduct)--}}



{{--                <div class="text-center">--}}

{{--                    <img class="rounded"--}}

{{--                         style="width: 100%; height: 200px"--}}

{{--                         src="{{$latestProduct->images()->first() ? asset('main/storage/app/public/'.$latestProduct->images()->first()->image) :'/admin/img/no-photo.png'}}" alt="{{$latestProduct->name}}">--}}

{{--                  <p><a href="{{ route('products.show', $latestProduct->id) }}">{{ $latestProduct->name }}</a></p>--}}

{{--              </div>--}}

{{--              @endforeach--}}

{{--            --}}{{--                    <div class="text-center">--}}

{{--            --}}{{--                        <img class="rounded" src="/images/SideBar Img/2.jpg" alt="Product image cap">--}}

{{--            --}}{{--                        <p><a href="#!">acornfresh × rice cake mother join</a></p>--}}

{{--            --}}{{--                    </div>--}}

{{--            --}}{{--                    <div class="text-center">--}}

{{--            --}}{{--                        <img class="rounded" src="/images/SideBar Img/3.jpg" alt="Product image cap">--}}

{{--            --}}{{--                        <p><a href="#!">acornfresh real cod shrimp shrim</a></p>--}}

{{--            --}}{{--                    </div>--}}

{{--            --}}{{--                    <div class="text-center">--}}

{{--            --}}{{--                        <img class="rounded" src="/images/SideBar Img/4.png" alt="Product image cap">--}}

{{--            --}}{{--                        <p><a href="#!">American scallop column 500g</a></p>--}}

{{--            --}}{{--                    </div>--}}

{{--            --}}{{--                    <div class="text-center">--}}

{{--            --}}{{--                        <img class="rounded" src="/images/SideBar Img/6.jpg" alt="Product image cap">--}}

{{--            --}}{{--                        <p><a href="#!">acornfresh × rice cake mother join</a></p>--}}

{{--            --}}{{--                    </div>--}}

{{--                </div>--}}

{{--            </div>--}}
{{---->--}}
            </div>
        </section>

        <!-- ========= Product-Section End  ======== -->


        <!-- ========= Sliding Card-Section  ======== -->
{{--    <!----}}
{{--    <section class="Sliding-Card-Section">--}}

{{--        <div class="container">--}}

{{--            <h1 class="mb-3">Related Upsell Products</h1>--}}

{{--            <div id="myCarousel" class="carousel slide" data-ride="carousel">--}}

{{--                <div class="carousel-inner row w-100 mx-auto">--}}



{{--                    <div class="carousel-item col-md-3 active">--}}

{{--                        <div class="card upsellCard">--}}

{{--                            <img class="card-img-top img-fluid"--}}

{{--                                 src="{{$product->images()->first() ? asset('main/storage/app/public/'.$product->images()->first()->image) :'/admin/img/no-photo.png'}}"--}}

{{--                                 style="height: 260px;width: 100%"--}}



{{--                                 alt="Card image cap">--}}

{{--                            <div class="card-body">--}}

{{--                                <p class="card-text">{{$product->name}} <span class="m-2 text-danger" style="font-weight: 600;font-size: 24px;">{{ $product->discount_price }}</span></p>--}}

{{--                                <a href="{{ route('products.add.cart', [$product->id]) }}" class="btn btn-primary  w-100 cartBtn"><i class="fa fa-shopping-cart"></i> Add to Cart</a>--}}

{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}

{{--                    @if($product->subCategories()->first() && count($product->subCategories()->first()->products))--}}



{{--        @foreach($product->subCategories()->first()->products()->limit(5)->latest()->get() as $relatedProduct)--}}

{{--            <div class="carousel-item col-md-3 ">--}}

{{--                <div class="card upsellCard">--}}

{{--                    <img class="card-img-top img-fluid"--}}

{{--                         style="height: 260px;width: 100%"--}}

{{--                         src="{{$relatedProduct->images()->first() ? asset('main/storage/app/public/'.$relatedProduct->images()->first()->image) :'/admin/img/no-photo.png'}}" alt="Card image cap">--}}

{{--                            <div class="card-body">--}}

{{--                                <p class="card-text">{{ $relatedProduct->name }} <span class="m-2 text-danger" style="font-weight: 600;font-size: 24px;">{{ $relatedProduct->price }}</span></p>--}}

{{--                                <a href="{{ route('products.add.cart', [$relatedProduct->id]) }}" class="btn btn-primary  w-100 cartBtn"><i class="fa fa-shopping-cart"></i> Add to Cart</a>--}}

{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}

{{--                        @endforeach--}}

{{--    @endif--}}


{{--        </div>--}}

{{--        <a class="carousel-control-prev" style="background: royalblue; width: 30px;height: 30px;margin-top: 12%;" href="#myCarousel" role="button" data-slide="prev">--}}

{{--            <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}

{{--            <span class="sr-only">Previous</span>--}}

{{--        </a>--}}

{{--        <a class="carousel-control-next" style="background: royalblue; width: 30px;height: 30px;margin-top: 12%;" href="#myCarousel" role="button" data-slide="next">--}}

{{--            <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}

{{--            <span class="sr-only">Next</span>--}}

{{--        </a>--}}

{{--    </div>--}}

{{--</div>--}}

{{--</section>--}}
{{---->--}}
        <!-- ========= Sliding Card-Section End  ======== -->


        <!-- ========= Top-Soled-Section   ======== -->

        <section class="Top-Sold-Products-Section">

            <div class="container text-center">

                <h4 class="mb-3 mt-3">Top Sold Products</h4>

{{--            <!--<div class="topSaledProduct">--}}
{{--                <div class="row">--}}

{{--                    @foreach(\App\Models\Product::query()->limit(4)->latest()->get() as $latestProduct)--}}

{{--                <div class="card topSaledProductCard">--}}

{{--                    <img--}}

{{--                        style="height: 100%;width: 100%"--}}

{{--                        src="{{$latestProduct->images()->first() ? asset('main/storage/app/public/'.$latestProduct->images()->first()->image) :'/admin/img/no-photo.png'}}">--}}

{{--                    <div class="card-content">--}}

{{--                        <a href="{{ route('products.show',  $latestProduct->id) }}">--}}

{{--                            <p class="card-category text-center">{{ $latestProduct->name }}</p>--}}

{{--                            <p class="card-category text-center text-danger" style="font-size: 24px;font-weight: 600;">{{ $latestProduct->discount_price }}</p>--}}

{{--                            <a href="{{ route('products.add.cart', $latestProduct->id) }}" class="btn  w-100 cartBtn"><i class="fa fa-cart-plus"></i> Add to Cart</a>--}}

{{--                        </a>--}}

{{--                    </div>--}}

{{--                </div>--}}

{{--                @endforeach--}}


{{--                </div>--}}


{{--            </div>-->--}}
                <div class="container-fluid" bis_skin_checked="1">
                    <div class="row" bis_skin_checked="1">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadcat" bis_skin_checked="1">
                            <div class="productSlider"
                                 style="border-right:1px solid #e1e1e1;border-left:1px solid #e1e1e1; border-top:2px solid #7a3901"
                                 bis_skin_checked="1">
                                <div bis_skin_checked="1">
                                    <ul style="padding:0px;"
                                        class="owl-carousel owl-theme catProSlider list-inline indexProductList p-hover shopList">
                                    @foreach(\App\Models\Product::query()->limit(4)->latest()->get() as $latestProduct)

                                            <li class="transall item ">
                                                <div class="li-item" bis_skin_checked="1">
                                                    @if($latestProduct->price != $latestProduct->discount_price)

                                                        <span
                                                            class="ribbon3">{{ $latestProduct->discount }} % Off</span>

                                                    @endif
                                                    <div class="p-image" bis_skin_checked="1">

                                                    <a href="{{ route('products.show',  $latestProduct->id) }}">
                                                        <img id="ProductImage-16129"
                                                             productimage="{{$latestProduct->images()->first() ? asset('main/storage/app/public/'.$latestProduct->images()->first()->image) :'/admin/img/no-photo.png'}}"
                                                             src="{{$latestProduct->images()->first() ? asset('main/storage/app/public/'.$latestProduct->images()->first()->image) :'/admin/img/no-photo.png'}}"
                                                             alt="{{$product->name}}"
                                                             style="height: 135px !important; width: auto !important;"
                                                             class="img-responsive">
                                                             </a>
                                                    </div>
                                                    <p id="ProductTitle-16129"
                                                       producttitle="{{$product->name}}">
                                                        {{ $latestProduct->name }}
                                                    </p>
                                                    <input type="hidden" id="pProductSku-16129" value="16129">
                                                    <div class="container-fluid">
                                                        <div class="row text-center">
                                                            <div
                                                                class="col-xs-12 col-sm-12 col-md-12 col-lg-12 price-section"
                                                                style="display: flex; justify-content: center; flex-direction: row !important">

                                                                <a class="m-0 buy-now-btn" style=" width: 10%; color: #FFF; background-color:var(--blue-color);border: inherit" href="{{ route('products.add.cart', [$latestProduct->id]) }}">
                                                                    <span  style="font-size: large;">	<i class="fa fa-shopping-cart ico" style="position: absolute; top: 50%; transform: translate(-30%,-50%); color:#fff"></i></span>
                                                                </a>

                                                                <span class="m-0 buy-now-price">
                                                                    @if($latestProduct->price != $latestProduct->discount_price)

                                                                    <del
                                                                        style="color: var(--red-color); font-size: 80%;">Rs: {{ thousand_separator($latestProduct->price) }}</del>
                                                                    @endif
                                                                    Rs: {{ thousand_separator($latestProduct->discount_price) }}</span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


            </div>

        </section>
    </div>
@endsection



@section('scripts')



    <script type="text/javascript" src="/js/jquery.easing.min.js"></script>

    <script src="/js/swiper.min.js"></script>

    <script src="/js/easyzoom.js"></script>
    <script src="/js/jquery.zoom.min.js"></script>


    <script>
    @foreach($product->images as $image)
    $('#default-zoom-{{ $loop->index }}').zoom();
    @endforeach
 $(document).ready(function() {
    $(".mCustomScrollbar").mCustomScrollbar({axis:"x"});
});       var galleryThumbs = new Swiper('.gallery-thumbs', {

            spaceBetween: 5,

            freeMode: true,

            watchSlidesVisibility: true,

            watchSlidesProgress: true,

            breakpoints: {

                0: {

                    slidesPerView: 3,

                },

                992: {

                    slidesPerView: 4,

                },

            }

        });

        var galleryTop = new Swiper('.gallery-top', {

            spaceBetween: 10,

            navigation: {

                nextEl: '.swiper-button-next',

                prevEl: '.swiper-button-prev',

            },

            thumbs: {

                swiper: galleryThumbs

            },

        });

        // change carousel item height

        // gallery-top

        let productCarouselTopWidth = $('.gallery-top').outerWidth();

        $('.gallery-top').css('height', productCarouselTopWidth);


        // gallery-thumbs

        let productCarouselThumbsItemWith = $('.gallery-thumbs .swiper-slide').outerWidth();

        $('.gallery-thumbs').css('height', productCarouselThumbsItemWith);


        // activation zoom plugin

        var $easyzoom = $('.easyzoom').easyZoom();

    </script>

    
    <script>

        $(document).ready(function () {


            $("#tabvideo").click(function () {

                $(".tab2").addClass("borderTopNav");

                $(".tab1").removeClass("borderTopNav");

            });

            $("#tabdescription").click(function () {

                $(".tab1").addClass("borderTopNav");

                $(".tab2").removeClass("borderTopNav");

            });

        });

    </script>
    <script>
        $(document).ready(function () {
            // $('#qty_input').prop('disabled', true);
            $('#plus-btn').click(function () {
                $('#qty_input').val(parseInt($('#qty_input').val()) + 1);
            });
            $('#minus-btn').click(function () {
                $('#qty_input').val(parseInt($('#qty_input').val()) - 1);
                if ($('#qty_input').val() == 0) {
                    $('#qty_input').val(1);
                }

            });
        });
    </script>
@endsection

