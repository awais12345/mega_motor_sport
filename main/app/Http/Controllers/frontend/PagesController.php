<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class PagesController extends Controller
{
    public function trackMyOrder()
    {
        return view('frontend.pages.track-my-order');
    }

    public function myAccount()
    {
        return view('frontend.pages.my-account');
    }

    public function returnPolicy()
    {
        return view('frontend.pages.return-policy');
    }

    public function faqs()
    {
        return view('frontend.pages.faqs');
    }

    public function aboutUs()
    {
        return view('frontend.pages.about-us');
    }

    public function contactUs()
    {
        return view('frontend.pages.contact-us');
    }

    public function termsAndConditions()
    {
        return view('frontend.pages.terms-and-conditions');
    }

    public function privacyPolicy()
    {
        return view('frontend.pages.privacy-policy');
    }

    public function feedback()
    {
        return view('frontend.pages.feedback');
    }

    public function sendContactUsEmail(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'subject' => 'required|min:3',
            'body' => 'required|min:6',
            'attach_file' => 'mimes:jpeg,png,jpg,gif,svg,txt,pdf,doc,docx,ppt,xls'
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'bodyMessage' => $request->body,
            'attach_file' => $request->attach_file,
        );
        try {
            $body = "<h3>You have new contact From Contact Form</h3><div>$request->body</div><p>Sent Via $request->email</p>";

            if ($request->file('attach_file')) {
                Mail::send([], [], function ($message) use ($data, $body) {
                    $message->to('muhammadtalha353@gmail.com')
                        ->subject($data['subject'])
                        ->from($data['email'])
                        ->attach($data['attach_file']->getRealPath(), array(
                            'as' => 'attachment.' . $data['attach_file']->getClientOriginalExtension(),
                            'mime' => $data['attach_file']->getMimeType()
                        ))->setBody($body, 'text/html');
                });

            } else {
                Mail::send([], [], function ($message) use ($data, $body) {
                    $message->to('info@megamotorsports.pk')
                        ->subject($data['subject'])
                        ->from($data['email'])
                        ->setBody($body, 'text/html');
                });
            }

            return back()->with('success', 'Email has been sent successfully!');
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }

    }
}
