<!DOCTYPE html>
<html dir="ltr" lang="en" class="desktop win mozilla oc30 is-guest route-common-home store-0 skin-1 desktop-header-active mobile-sticky layout-1" data-jv="3.0.46" data-ov="3.0.2.0">

	<meta http-equiv="content-type" content="text/html;charset=utf-8" />

	<head typeof="og:website">
    <link rel="icon" href="/images/fav.png">
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>@yield('title')</title>
      <!--<link rel="stylesheet" href="{{ mix('css/app.css') }}">-->

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">
    <!-- Google Fonts -->
    <!--<link href="/css/acad9b0418789bcb6e18e274a272a78e.css" rel="stylesheet">-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="/css/bootstrap-iso.css">

    <link href="/css/enhance.css?v=e06beeea" rel="stylesheet">
    <link href="/css/fonts.css?v=1a68779c" rel="stylesheet">
    <link href="/css/custom.css?v=02" rel="stylesheet">
    <link rel="stylesheet" href="/css/custom-nav-menu.css">
    <script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
	<script>
    window['Journal'] = {
    "isPopup": false,
    "isPhone": false,
    "isTablet": false,
    "isDesktop": true,
    "filterScrollTop": false,
    "filterUrlValuesSeparator": ",",
    "countdownDay": "Day",
    "countdownHour": "Hour",
    "countdownMin": "Min",
    "countdownSec": "Sec",
    "globalPageColumnLeftTabletStatus": false,
    "globalPageColumnRightTabletStatus": false,
    "scrollTop": true,
    "scrollToTop": false,
    "notificationHideAfter": "2000",
    "quickviewPageStyleCloudZoomStatus": true,
    "quickviewPageStyleAdditionalImagesCarousel": true,
    "quickviewPageStyleAdditionalImagesCarouselStyleSpeed": "500",
    "quickviewPageStyleAdditionalImagesCarouselStyleAutoPlay": false,
    "quickviewPageStyleAdditionalImagesCarouselStylePauseOnHover": true,
    "quickviewPageStyleAdditionalImagesCarouselStyleDelay": "3000",
    "quickviewPageStyleAdditionalImagesCarouselStyleLoop": false,
    "quickviewPageStyleAdditionalImagesHeightAdjustment": "5",
    "quickviewPageStylePriceUpdate": true,
    "quickviewPageStyleOptionsSelect": "all",
    "quickviewText": "Quickview",
    "mobileHeaderOn": "tablet",
    "subcategoriesCarouselStyleSpeed": "500",
    "subcategoriesCarouselStyleAutoPlay": false,
    "subcategoriesCarouselStylePauseOnHover": true,
    "subcategoriesCarouselStyleDelay": "3000",
    "subcategoriesCarouselStyleLoop": false,
    "productPageStyleImageCarouselStyleSpeed": "500",
    "productPageStyleImageCarouselStyleAutoPlay": false,
    "productPageStyleImageCarouselStylePauseOnHover": true,
    "productPageStyleImageCarouselStyleDelay": "3000",
    "productPageStyleImageCarouselStyleLoop": false,
    "productPageStyleCloudZoomStatus": true,
    "productPageStyleCloudZoomPosition": "inner",
    "productPageStyleAdditionalImagesCarousel": true,
    "productPageStyleAdditionalImagesCarouselStyleSpeed": "500",
    "productPageStyleAdditionalImagesCarouselStyleAutoPlay": false,
    "productPageStyleAdditionalImagesCarouselStylePauseOnHover": true,
    "productPageStyleAdditionalImagesCarouselStyleDelay": "3000",
    "productPageStyleAdditionalImagesCarouselStyleLoop": false,
    "productPageStyleAdditionalImagesHeightAdjustment": "",
    "productPageStylePriceUpdate": true,
    "productPageStyleOptionsSelect": "all",
    "infiniteScrollStatus": true,
    "infiniteScrollOffset": "4",
    "infiniteScrollLoadPrev": "Load Previous Products",
    "infiniteScrollLoadNext": "Load Next Products",
    "infiniteScrollLoading": "Loading...",
    "infiniteScrollNoneLeft": "You have reached the end of the list.",
    "checkoutUrl": "https:\/\/www.journal-theme.com\/1\/index.php?route=checkout\/checkout",
    "headerHeight": "100",
    "headerCompactHeight": "50",
    "mobileMenuOn": "",
    "searchStyleSearchAutoSuggestStatus": true,
    "searchStyleSearchAutoSuggestDescription": true,
    "headerMiniSearchDisplay": "default",
    "stickyStatus": false,
    "stickyFullHomePadding": false,
    "stickyFullwidth": false,
    "stickyAt": "",
    "stickyHeight": "",
    "headerTopBarHeight": "35",
    "topBarStatus": true,
    "headerType": "classic",
    "headerMobileHeight": "60",
    "headerMobileStickyStatus": true,
    "headerMobileTopBarVisibility": true,
    "headerMobileTopBarHeight": "40",
    "popup": [{
        "m": 110,
        "c": "254dad8e"
    }],
    "notification": [{
        "m": 137,
        "c": "017df752"
    }],
    "headerNotice": [{
        "m": 56,
        "c": "9e4f882c"
    }],
    "columnsCount": 0
};
        </script>
				<script>(function() {
          if (Journal['isPhone']) {
              return;
          }
          var wrappers = ['search', 'cart', 'cart-content', 'logo', 'language', 'currency'];
          var documentClassList = document.documentElement.classList;

          function extractClassList() {
              return ['desktop', 'tablet', 'phone', 'desktop-header-active', 'mobile-header-active', 'mobile-menu-active'].filter(function(cls) {
                  return documentClassList.contains(cls);
              });
          }

          function mqr(mqls, listener) {
              Object.keys(mqls).forEach(function(k) {
                  mqls[k].addListener(listener);
              });
              listener();
          }

          function mobileMenu() {
              console.warn('mobile menu!');
              var element = document.querySelector('#main-menu');
              var wrapper = document.querySelector('.mobile-main-menu-wrapper');
              if (element && wrapper) {
                  wrapper.appendChild(element);
              }
              var main_menu = document.querySelector('.main-menu');
              if (main_menu) {
                  main_menu.classList.add('accordion-menu');
              }
              document.querySelectorAll('.main-menu .dropdown-toggle').forEach(function(element) {
                  element.classList.remove('dropdown-toggle');
                  element.classList.add('collapse-toggle');
                  element.removeAttribute('data-toggle');
              });
              document.querySelectorAll('.main-menu .dropdown-menu').forEach(function(element) {
                  element.classList.remove('dropdown-menu');
                  element.classList.remove('j-dropdown');
                  element.classList.add('collapse');
              });
          }

          function desktopMenu() {
              console.warn('desktop menu!');
              var element = document.querySelector('#main-menu');
              var wrapper = document.querySelector('.desktop-main-menu-wrapper');
              if (element && wrapper) {
                  wrapper.insertBefore(element, document.querySelector('#main-menu-2'));
              }
              var main_menu = document.querySelector('.main-menu');
              if (main_menu) {
                  main_menu.classList.remove('accordion-menu');
              }
              document.querySelectorAll('.main-menu .collapse-toggle').forEach(function(element) {
                  element.classList.add('dropdown-toggle');
                  element.classList.remove('collapse-toggle');
                  element.setAttribute('data-toggle', 'dropdown');
              });
              document.querySelectorAll('.main-menu .collapse').forEach(function(element) {
                  element.classList.add('dropdown-menu');
                  element.classList.add('j-dropdown');
                  element.classList.remove('collapse');
              });
              document.body.classList.remove('mobile-wrapper-open');
          }

          function mobileHeader() {
              console.warn('mobile header!');
              Object.keys(wrappers).forEach(function(k) {
                  var element = document.querySelector('#' + wrappers[k]);
                  var wrapper = document.querySelector('.mobile-' + wrappers[k] + '-wrapper');
                  if (element && wrapper) {
                      wrapper.appendChild(element);
                  }
                  if (wrappers[k] === 'cart-content') {
                      if (element) {
                          element.classList.remove('j-dropdown');
                          element.classList.remove('dropdown-menu');
                      }
                  }
              });
              var search = document.querySelector('#search');
              var cart = document.querySelector('#cart');
              if (search && (Journal['searchStyle'] === 'full')) {
                  search.classList.remove('full-search');
                  search.classList.add('mini-search');
              }
              if (cart && (Journal['cartStyle'] === 'full')) {
                  cart.classList.remove('full-cart');
                  cart.classList.add('mini-cart')
              }
          }

          function desktopHeader() {
              console.warn('desktop header!');
              Object.keys(wrappers).forEach(function(k) {
                  var element = document.querySelector('#' + wrappers[k]);
                  var wrapper = document.querySelector('.desktop-' + wrappers[k] + '-wrapper');
                  if (wrappers[k] === 'cart-content') {
                      if (element) {
                          element.classList.add('j-dropdown');
                          element.classList.add('dropdown-menu');
                          document.querySelector('#cart').appendChild(element);
                      }
                  } else {
                      if (element && wrapper) {
                          wrapper.appendChild(element);
                      }
                  }
              });
              var search = document.querySelector('#search');
              var cart = document.querySelector('#cart');
              if (search && (Journal['searchStyle'] === 'full')) {
                  search.classList.remove('mini-search');
                  search.classList.add('full-search');
              }
              if (cart && (Journal['cartStyle'] === 'full')) {
                  cart.classList.remove('mini-cart');
                  cart.classList.add('full-cart');
              }
              documentClassList.remove('mobile-cart-content-container-open');
              documentClassList.remove('mobile-main-menu-container-open');
              documentClassList.remove('mobile-overlay');
          }

          function moveElements(classList) {
              if (classList.includes('mobile-header-active')) {
                  mobileHeader();
                  mobileMenu();
              } else if (classList.includes('mobile-menu-active')) {
                  desktopHeader();
                  mobileMenu();
              } else {
                  desktopHeader();
                  desktopMenu();
              }
          }
          var mqls = {
              phone: window.matchMedia('(max-width: 768px)'),
              tablet: window.matchMedia('(max-width: 1023px)'),
              menu: window.matchMedia('(max-width: ' + Journal['mobileMenuOn'] + 'px)')
          };
          mqr(mqls, function() {
              var oldClassList = extractClassList();
              if (Journal['isDesktop']) {
                  if (mqls.phone.matches) {
                      documentClassList.remove('desktop');
                      documentClassList.remove('tablet');
                      documentClassList.add('mobile');
                      documentClassList.add('phone');
                  } else if (mqls.tablet.matches) {
                      documentClassList.remove('desktop');
                      documentClassList.remove('phone');
                      documentClassList.add('mobile');
                      documentClassList.add('tablet');
                  } else {
                      documentClassList.remove('mobile');
                      documentClassList.remove('phone');
                      documentClassList.remove('tablet');
                      documentClassList.add('desktop');
                  }
                  if (documentClassList.contains('phone') || (documentClassList.contains('tablet') && Journal['mobileHeaderOn'] === 'tablet')) {
                      documentClassList.remove('desktop-header-active');
                      documentClassList.add('mobile-header-active');
                  } else {
                      documentClassList.remove('mobile-header-active');
                      documentClassList.add('desktop-header-active');
                  }
              }
              if (documentClassList.contains('desktop-header-active') && mqls.menu.matches) {
                  documentClassList.add('mobile-menu-active');
              } else {
                  documentClassList.remove('mobile-menu-active');
              }
              var newClassList = extractClassList();
              if (oldClassList.join(' ') !== newClassList.join(' ')) {
                  if (documentClassList.contains('safari') && !documentClassList.contains('ipad') && navigator.maxTouchPoints && navigator.maxTouchPoints > 2) {
                      window.fetch('index7e38.json?route=journal3/journal3/device_detect', {
                          method: 'POST',
                          body: 'device=ipad',
                          headers: {
                              'Content-Type': 'application/x-www-form-urlencoded'
                          }
                      }).then(function(data) {
                          return data.json();
                      }).then(function(data) {
                          if (data.response.reload) {
                              window.location.reload();
                          }
                      });
                  }
                  if (document.readyState === 'loading') {
                      document.addEventListener('DOMContentLoaded', function() {
                          moveElements(newClassList);
                      });
                  } else {
                      moveElements(newClassList);
                  }
              }
          });
      })();
      (function() {
          var cookies = {};
          var style = document.createElement('style');
          var documentClassList = document.documentElement.classList;
          document.head.appendChild(style);
          document.cookie.split('; ').forEach(function(c) {
              var cc = c.split('=');
              cookies[cc[0]] = cc[1];
          });
          if (Journal['popup']) {
              for (var i in Journal['popup']) {
                  if (!cookies['p-' + Journal['popup'][i]['c']]) {
                      documentClassList.add('popup-open');
                      documentClassList.add('popup-center');
                      break;
                  }
              }
          }
          if (Journal['notification']) {
              for (var i in Journal['notification']) {
                  if (cookies['n-' + Journal['notification'][i]['c']]) {
                      style.sheet.insertRule('.module-notification-' + Journal['notification'][i]['m'] + '{ display:none }');
                  }
              }
          }
          if (Journal['headerNotice']) {
              for (var i in Journal['headerNotice']) {
                  if (cookies['hn-' + Journal['headerNotice'][i]['c']]) {
                      style.sheet.insertRule('.module-header_notice-' + Journal['headerNotice'][i]['m'] + '{ display:none }');
                  }
              }
          }
          if (Journal['layoutNotice']) {
              for (var i in Journal['layoutNotice']) {
                  if (cookies['ln-' + Journal['layoutNotice'][i]['c']]) {
                      style.sheet.insertRule('.module-layout_notice-' + Journal['layoutNotice'][i]['m'] + '{ display:none }');
                  }
              }
          }
      })();</script>

  <style>
          :root {
            --blue-color: #1b75bc;
            --red-color: #c1272d;
        }


        a {
          text-decoration: none !important;
        }
        .body {
            background-color: #fff;
            color: #111;
            font-size: 52%  !important;
            font-family: "Roboto", "Arial", "Helvetica", sans-serif;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            text-align: center;
            display: none;
            overflow: visible !important;
        }

        /*div, span, h1, h2, p, em, img, strong, b, u, i, dl, dt, dd, ol, ul, li, fieldset, form, label, table, tr, th, td {
            background: transparent;
            border: 0;
            font-size: 100%;
            margin: 0;
            outline: 0;
            padding: 0;
            vertical-align: baseline
        }*/

        a, [data-hlk] {
            background: transparent;
            color: #3761bf;
            cursor: pointer;
            font-size: 100%;
            margin: 0;
            padding: 0;
            text-decoration: none;
            vertical-align: baseline
        }

        a:hover, [data-hlk]:hover {
            text-decoration: underline
        }

        table {
            border-collapse: collapse;
            border-spacing: 0
        }

        table td, table th {
            vertical-align: top
        }

        [type='submit'] {
            -webkit-appearance: none;
            border-radius: 0
        }

        input[type='search'] {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none
        }

        input::-webkit-search-cancel-button {
            display: none
        }

        input::-webkit-search-decoration {
            display: none
        }

        input::-ms-clear {
            display: none
        }

        iframe {
            border: 0
        }

        html {
            overflow-x: hidden
        }

        html, body {
            min-height: 100%
        }

        body {
            position: relative
        }

        input::-ms-clear, input::-ms-reveal {
            display: none
        }

        textarea {
            resize: vertical
        }

        #scrollfix {
            position: relative;
            width: 100%;
            z-index: 5
        }

        .hiddenScroll #scrollfix {
            overflow: hidden
        }

        .left-panel, .right-panel {
            display: none;
            position: absolute;
            top: 0
        }

        @-webkit-keyframes appear {
            0% {
                opacity: 0
            }
            100% {
                opacity: 1
            }
        }

        @keyframes appear {
            0% {
                opacity: 0
            }
            100% {
                opacity: 1
            }
        }

        .main-content {
            position: relative;
            width: 100%;
            z-index: 1
        }

        .main-content.-overlay.-animate::after {
            -webkit-animation: appear 0.4s;
            animation: appear 0.4s
        }

        .main-content.-selects-overlay-show {
            z-index: auto
        }

        .wrap {
            margin: 0 auto;
            min-width: 320px;
            position: relative;
            text-align: left
        }

        .wrap .wrap {
            margin: 0;
            min-width: 0
        }

        .product-list-with-filter-holder {
            box-sizing: border-box;
            width: 100%
        }

        .mobile-small-show, .mobile-medium-show, .mobile-large-show, .desktop-hide, .mobile-show {
            display: none !important
        }

        .hidden {
            display: none !important
        }

        .ov_hidden {
            overflow: hidden !important
        }

        .ov-x-auto {
            overflow-x: auto !important
        }

        .block {
            display: block !important
        }

        .clear {
            clear: both;
            height: 0;
            overflow: hidden
        }

        ._clear-modern::before, ._clear-modern::after {
            clear: both;
            content: '';
            display: table
        }

        .float_right {
            float: right
        }

        .float_left {
            float: left
        }

        .pos_r {
            position: relative
        }

        .row::before, .row::after {
            clear: both;
            content: '';
            display: table
        }

        template {
            display: none
        }

        .break-word {
            word-wrap: break-word
        }

        .h1-change-btn-wrap {
            margin: 16px 8px
        }

        .h1-fat {
            color: #111;
            display: block;
            font: 500 2.2em/1.2 "Roboto", "Arial", "Helvetica", sans-serif;
            margin: 16px 2%
        }

        .h1-fat > span {
            font-weight: 300
        }

        ._store-red {
            background-color: #d41e2b !important
        }

        ._store-orange {
            background-color: #fea934 !important
        }

        ._store-grey-blue {
            background-color: #455874 !important
        }

        ._store-blue {
            background-color: #0883d4 !important
        }

        ._store-gray {
            background-color: #686c73 !important
        }

        ._store-dark-orange {
            background-color: #fc6200 !important
        }

        ._store-green {
            background-color: #1e824c !important
        }

        .head-stores-h {
            background-color: var(--blue-color);
            height: 48px
        }

        .head-stores {
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
            align-items: flex-start;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            justify-content: flex-start;
            list-style: none
        }
        /*
        .head-stores::after {
            background-color: #505b6a;
            border-radius: 4px;
            color: #fff;
            content: 'Shop our Stores';
            display: block;
            font-size: 1.1em;
            line-height: 1.5;
            padding: 6px 12px;
            position: absolute;
            right: 10px;
            top: 10px
        }
        */
        .head-stores.-active::after {
            background: url(https://cdn.carid.com/css/prod-images/6b14ad75.svg) 50% 50% no-repeat;
            background-size: 20px auto;
            border-radius: 0;
            content: '';
            height: 40px;
            opacity: 0.7;
            padding: 0;
            right: 8px;
            top: 4px;
            width: 40px
        }

        .head-stores > li {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            font-size: 1.4em;
            font-weight: 300;
            line-height: 1.17
        }

        .head-stores > li:hover > .item:not(.-active) {
            cursor: pointer;
            opacity: 1;
            text-decoration: none
        }

        .head-stores > li > .item {
            box-sizing: border-box;
            display: block;
            height: 48px;
            padding-left: 70px;
            padding-right: 25px;
            padding-top: 18px;
            position: relative;
            white-space: nowrap
        }

        .head-stores > li > .item.-active {
            color: #fff;
            cursor: default;
            font-weight: 700
        }

        .head-stores > li > .item:not(.-active) {
            display: none;
            text-indent: -999em
        }

        .head_header {
            width: 100%
        }

        .header-top {
            background-color: #f0f0f0;
            height: 48px
        }

        .header-top > .wrap {
            height: 100%
        }

        .header-top .header-dd-h {
            display: none
        }

        .header-account {
            display: none
        }

        .left-menu-icon {
            background: url(https://cdn.carid.com/css/prod-images/adf4276e.svg) 50% 50% no-repeat;
            background-size: 24px 18px;
            cursor: pointer;
            display: block;
            height: 48px;
            left: 0;
            position: absolute;
            top: 0;
            width: 56px;
            z-index: 1
        }

        .no-touch .left-menu-icon:hover {
            opacity: 0.7
        }
        @media screen and (max-width:767px){
            .head_logo_a
            {
               display: none;
            }
            .header-search-btn{
                left: 64px !important;
            }
            .-sm-white{
                display: block;
            }
            .imgwhitelogo{
                display: block !important;
            }
        }
        .imgwhitelogo{
                display: none;
                top: 50%;
                left: 50%;
                transform: translate(-50%,0);
                position: absolute;
                padding: 8px;
            }
        .head_logo_a {
            background: url(/images/logo.png) no-repeat 100% 12px;
            background-size: 60px auto;
            bottom: -4px;
            left: 56px;
            position: absolute;
            text-indent: -999em;
            top: -4px;
            width: 65px;
            z-index: 3
        }
        .-sm-white {
            background: url(/images/logo-white.png) no-repeat 100% 12px;
            background-size: 60px auto;
            bottom: -4px;
            left: 56px;
            position: absolute;
            text-indent: -999em;
            top: -4px;
            width: 65px;
            z-index: 3
        }

        .header-dd-hover-element:hover .header-dd-h {
            display: none;
            opacity: 1;
            -webkit-transform: translateY(0);
            transform: translateY(0);
            -webkit-transition: opacity 0.3s ease 0.2s, visibility 0s ease 0.2s, -webkit-transform 0.3s ease 0.2s;
            transition: opacity 0.3s ease 0.2s, visibility 0s ease 0.2s, -webkit-transform 0.3s ease 0.2s;
            transition: opacity 0.3s ease 0.2s, transform 0.3s ease 0.2s, visibility 0s ease 0.2s;
            transition: opacity 0.3s ease 0.2s, transform 0.3s ease 0.2s, visibility 0s ease 0.2s, -webkit-transform 0.3s ease 0.2s;
            visibility: visible
        }

        .no-transform .header-dd-hover-element:hover .header-dd-h {
            display: block
        }

        .nav-tool-wrap {
            float: right;
            list-style: none
        }

        .nav-tool-wrap > li {
            display: inline-block;
            vertical-align: top
        }

        .nav-tool {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            font-size: 13px;
            font-weight: 500;
            height: 48px;
            line-height: 1.2;
            min-width: 48px;
            position: relative
        }

        .nav-tool .-txt {
            display: none
        }

        .nav-tool > .icon {
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: 100% auto;
            height: 27px;
            left: 50%;
            margin-left: -14px;
            position: absolute;
            top: 11px;
            width: 27px
        }

        .nav-tool, .nav-tool .-txt {
            color: #111
        }

        .nav-tool .count-item {
            text-indent: 0
        }

        .acc-text {
            display: inline-block;
            overflow: hidden;
            width: 0
        }

        .header-garage-subtitle {
            display: none
        }

        .nav-tool.-account {
            display: none
        }

        .nav-tool.-wishlist, .nav-tool.-garage {
            display: none
        }

        .header-search-btn {
            background: #fff;
            border: 1px solid #d8d8d8;
            border-radius: 4px;
            bottom: -4px;
            display: block;
            font-size: 1.3em;
            font-weight: 300;
            left: 146px;
            position: absolute;
            right: 48px;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            top: 6px
        }

        .header-search-btn::before {
            content: "Search...";
            left: 16px;
            position: relative;
            top: 9px
        }

        .header-search-btn::after {
            background: transparent url(https://cdn.carid.com/css/prod-images/aa0c55ee.svg) no-repeat 50% 50%;
            background-size: 25px auto;
            bottom: 0;
            content: '';
            height: 34px;
            padding: 0;
            position: absolute;
            right: 8px;
            top: 0;
            width: 34px
        }

        .count-item {
            background-color: #d4252a;
            border-radius: 15px;
            box-sizing: border-box;
            color: #fff;
            font-size: 11px;
            line-height: 13px;
            min-width: 8px;
            padding: 0 3px;
            text-align: center
        }

        .count-item.-type-1 {
            border-radius: 50%;
            font-size: 8px;
            font-weight: 700;
            line-height: 16px;
            position: absolute;
            right: -3px;
            text-align: center;
            top: -1px;
            width: 16px
        }

        .count-item.-type-1::after {
            border: 1px solid #fff;
            border-radius: 50%;
            bottom: -1px;
            content: '';
            left: -1px;
            position: absolute;
            right: -1px;
            top: -1px
        }

        .count-item.-type-2 {
            border-radius: 50%;
            display: inline-block;
            font-size: 8px;
            line-height: 15px;
            text-align: center;
            vertical-align: middle;
            width: 15px
        }

        .left-menu-vehicles-title .count-item.-type-2 {
            margin-left: 2px
        }

        .count-item.-garage {
            left: 46px;
            position: absolute;
            top: 10px
        }

        .count-item.-garage::after {
            border: 1px solid #fff;
            border-radius: 50%;
            bottom: -1px;
            content: '';
            left: -1px;
            position: absolute;
            right: -1px;
            top: -1px
        }

        .head-dd-h {
            display: none
        }

        .head-dd {
            color: #757575;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            height: 35px;
            list-style: none;
            margin-left: 6px;
            overflow: hidden;
            padding-top: 5px;
            text-align: left;
            vertical-align: top
        }

        .head-dd > li {
            box-sizing: border-box;
            cursor: pointer;
            display: inline-block;
            position: relative;
            vertical-align: top
        }

        .head-dd > .it {
            height: 100%;
            -webkit-box-ordinal-group: 2;
            -webkit-order: 1;
            order: 1
        }

        .head-dd > .it > .head-dd-main {
            display: inline-block;
            font-size: 14px;
            font-weight: 500;
            line-height: 1;
            padding: 10px;
            position: relative;
            text-decoration: none
        }

        .head-dd > .it > .head-dd-main, .head-dd > .it > .head-dd-main > .link {
            color: #111
        }

        .head-dd > .it > .head-dd-main > .link:hover {
            text-decoration: none
        }

        .head-dd > .it > .head-dd-main > .link > span, .head-dd > .it > .head-dd-main .extra, .head-dd > .it > .head-dd-main .extra + .link {
            display: none
        }

        .head-dd > .it:hover > .head-dd-main, .head-dd > .it:hover > .head-dd-main > .link, .head-dd > .it.loading > .head-dd-main, .head-dd > .it.loading > .head-dd-main > .link, .head-dd > .it.active > .head-dd-main, .head-dd > .it.active > .head-dd-main > .link {
            color: #ffac00
        }

        .head-dd > .it.after-more {
            -webkit-box-ordinal-group: 4;
            -webkit-order: 3;
            order: 3
        }

        .head-dd .head-dd-more {
            -webkit-box-ordinal-group: 3 !important;
            -webkit-order: 2 !important;
            order: 2 !important;
            visibility: hidden
        }

        .head-dd-help-center {
            float: right;
            margin-right: 12px;
            padding-top: 5px
        }

        .head-dd-cont-holder {
            display: none
        }

        .head_img {
            background: #f0f0f0;
            line-height: 0;
            min-width: 1000px;
            overflow: hidden;
            position: relative;
            text-align: center
        }

        .head_img_minh {
            min-width: 0
        }

        .head_img_aligner {
            bottom: 0;
            height: 100%;
            left: 50%;
            margin-left: -2500px;
            position: absolute;
            top: 0;
            width: 5000px
        }

        .head_img_aligner .head-img-overlay > img, .head_img_aligner .head-img-overlay > a, .head_img_aligner .head-img-overlay > a > img {
            display: block;
            height: 100%;
            margin: 0 auto;
            width: auto
        }

        .head-img-overlay {
            display: inline-block;
            height: 100%;
            overflow: hidden;
            position: relative;
            width: 100%
        }

        .head-img-overlay::after {
            background: url(https://cdn.carid.com/css/prod-images/b2c6e3e8.svg) repeat 0 0;
            content: '';
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%
        }

        .head-img-overlay.-transparent::after {
            background: none
        }

        .head-nav {
            -webkit-box-align: center;
            -webkit-align-items: center;
            align-items: center;
            background: #757575;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            height: 230px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            position: relative;
            width: 100%
        }

        .head-nav::after {
            background: #111;
            content: '';
            display: block;
            height: 100%;
            left: 0;
            margin: 0 auto;
            max-width: 1920px;
            opacity: 0.3;
            position: absolute;
            right: 0;
            top: 0;
            width: 100%
        }

        .head-nav-img {
            display: block;
            height: 100%;
            left: 50%;
            position: absolute;
            top: 0;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
            width: auto
        }

        .head-nav-inner {
            box-sizing: border-box;
            margin-top: 8px;
            /*min-width: 320px;*/
            padding: 0 16px;
            position: relative;
            text-align: center;
            width: 100%;
            z-index: 3
        }

        .head-nav.-with-mmy .head-nav-inner {
            padding: 0 8px;
            z-index: 21
        }

        .head-nav-title {
            color: #fff;
            font-size: 1.7em;
            font-weight: 700;
            line-height: 1.4;
            margin-bottom: 10px;
            text-transform: uppercase
        }

        .head-nav.-with-mmy .head-nav-title {
            line-height: 1
        }

        .head-nav-subtitle {
            display: none
        }

        .head-nav.-with-mmy .head-nav-subtitle {
            margin-bottom: 32px
        }

        .simple-slider-wrap {
            display: block;
                height: 550px;
                left: 50%;
                margin-left: -960px;
                overflow: hidden;
                position: absolute;
                top: 0;
                width: 1350px;
        }

        .simple-slider {
            list-style: none;
            position: relative;
            -webkit-transform: translateX(1920px);
            transform: translateX(1920px);
            -webkit-transition: -webkit-transform 500ms ease;
            transition: -webkit-transform 500ms ease;
            transition: transform 500ms ease;
            transition: transform 500ms ease, -webkit-transform 500ms ease
        }

        .simple-slider.-loaded {
            -webkit-transform: translateX(0);
            transform: translateX(0)
        }

        .simple-slider.-no-transition {
            -webkit-transition: none;
            transition: none
        }

        .simple-slider > .slide {
            float: left;
            height: 0;
            opacity: 0;
            width: 1920px
        }

        .simple-slider > .slide.-loaded {
            height: auto;
            opacity: 1
        }

        .head-main-logo {
            bottom: 30px;
            left: 0;
            position: absolute;
            right: 0;
            z-index: 1
        }

        .head-main-logo.-with-main-select-bar {
            bottom: 50px
        }

        .head-main-logo > .logo {
            margin: 0 8px
        }

        .main-select-bar-h {
            position: relative;
            z-index: 21
        }

        .main-select-bar-h.-bottom-offset {
            margin-bottom: 64px
        }

        .head-nav-inner > .select-vehicle-spacer {
            margin: 0 0 18px
        }

        .head-shop-by-links {
            bottom: 32px;
            left: 0;
            position: absolute;
            text-align: center;
            width: 100%;
            z-index: 1
        }

        .head-shop-by-links-item, .head-shop-by-links-item > .link {
            color: #fff;
            font-size: 14px;
            line-height: 16px;
            text-align: center;
            text-transform: uppercase
        }

        .head-shop-by-links-item:first-child {
            margin-right: 17px;
            padding-right: 17px;
            position: relative
        }

        .head-shop-by-links-item:first-child::after {
            border: 1px solid #fff;
            content: '';
            height: 17px;
            position: absolute;
            right: 0;
            top: -1px
        }

        .head-shop-by-links-item > .link {
            font-weight: 700
        }

        .head-shop-by-links-item > .count {
            display: none
        }

        .select-vehicle-spacer {
            color: #a9a9a9;
            margin: -20px 8px 0;
            position: relative
        }

        .select-vehicle-spacer.-hidden {
            height: 0;
            margin: 0;
            overflow: hidden;
            position: relative
        }

        .select-vehicle-spacer.-hidden.-fixed {
            overflow: visible
        }

        .select-vehicle-spacer.-home-page {
            margin-bottom: 50px
        }

        .select-vehicle-spacer.-in-popup {
            margin: 0 !important;
            position: static
        }

        .select-vehicle-spacer.-in-popup::after {
            content: '';
            height: 350px;
            left: 0;
            position: absolute;
            top: 100%;
            width: 1px;
            z-index: -1
        }

        .select-vehicle-spacer.-top-indent {
            margin-top: 20px
        }

        .select-vehicle {
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        .select-vehicle-spacer.-fixed .select-vehicle {
            background: #363d47;
            left: 0;
            margin: 0 auto;
            min-width: 1000px;
            position: fixed;
            right: 0;
            top: 0;
            width: 100%;
            z-index: 101
        }

        .select-vehicle::after {
            display: none
        }

        .no-js .select-vehicle::after {
            content: '';
            display: block;
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%
        }

        .touch .select-vehicle {
            position: static
        }

        .select-vehicle > .nav {
            height: 50px
        }

        .select-vehicle-spacer.-fixed .select-vehicle > .nav {
            display: none
        }

        .select-vehicle > .nav > .link {
            background-color: #363d47;
            background-image: none;
            border-radius: 4px 4px 0 0;
            box-sizing: border-box;
            color: #fff;
            cursor: pointer;
            -webkit-box-flex: 1;
            -webkit-flex: 1;
            flex: 1;
            float: left;
            font-size: 1.6em;
            font-weight: 500;
            height: 101%;
            line-height: 1;
            margin-left: 2px;
            padding: 16px 24px 18px;
            position: relative
        }

        .select-vehicle > .nav > .link::after {
            content: '';
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%
        }

        .select-vehicle > .nav > .link:first-child {
            border-color: transparent;
            border-left: none;
            margin-left: 0
        }

        .select-vehicle-spacer.-in-popup .select-vehicle > .nav > .link {
            background-image: none
        }

        .select-vehicle > .nav > .link.-light {
            background-color: #d8d8d8;
            color: #464646
        }

        .select-vehicle > .nav > .link.-light:not(.-active) {
            box-shadow: inset 0 -10px 20px -10px rgba(0, 0, 0, 0.25)
        }

        .select-vehicle-spacer.-in-popup .select-vehicle > .nav > .link.-light {
            background-color: #d8d8d8
        }

        .select-vehicle > .nav > .link.-active {
            cursor: default;
            opacity: 1
        }

        .select-vehicle > .nav > .link.-active::after {
            display: none
        }

        .select-vehicle > .nav.-garage {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex
        }

        .select-vehicle > .nav.-garage > .link {
            background-repeat: no-repeat;
            display: block;
            padding-left: 24px;
            padding-right: 8px;
            width: auto
        }

        .select-vehicle > .nav.-garage > .link:first-child {
            min-width: auto;
            width: auto
        }

        .select-vehicle > .nav.-garage > .link:not(.-active) {
            background-color: #414955
        }

        .select-vehicle > .nav.-garage > .link.-i1-15 {
            background-image: url(https://cdn.carid.com/css/prod-images/ae5be278.svg)
        }

        .select-vehicle > .nav.-garage > .link.-i1-31 {
            background-image: url(https://cdn.carid.com/css/prod-images/e225162b.svg)
        }

        .select-vehicle > .nav.-garage > .link.-i1-6 {
            background-image: url(https://cdn.carid.com/css/prod-images/5494e65d.svg)
        }

        .select-vehicle > .nav.-garage > .link.-i1-20 {
            background-image: url(https://cdn.carid.com/css/prod-images/536dcc29.svg)
        }

        .select-vehicle > .nav.-garage > .link > .link-title {
            color: #fff;
            display: none
        }

        .select-vehicle > .nav.-garage > .link.-active {
            opacity: 1;
            padding-left: 54px
        }

        .select-vehicle > .nav.-garage > .link.-active > .link-title {
            display: block;
            font-weight: 700;
            white-space: nowrap
        }

        .select-vehicle > .content {
            background: #363d47;
            border-radius: 0 0 4px 4px;
            position: relative
        }

        .inspiration-gallery-indent .select-vehicle > .content {
            border-radius: 4px
        }

        .select-vehicle-spacer.-fixed .select-vehicle > .content {
            margin: 0 auto;
            max-width: 1296px;
            min-width: 1000px;
            padding: 0;
            width: 94%
        }

        .select-vehicle > .content.-reset-styles {
            background: transparent;
            border-radius: 0
        }

        .select-vehicle > .content > .tab {
            border-radius: 4px;
            display: none;
            height: 0;
            overflow: hidden;
            position: absolute;
            visibility: hidden;
            width: 100%
        }

        .select-vehicle > .content > .tab.-active {
            border-radius: 0 4px 4px;
            display: block;
            height: auto;
            overflow: visible;
            position: static;
            visibility: visible
        }

        .select-vehicle > .content > .tab.-light {
            background-color: #d8d8d8
        }

        .select-vehicle-content-spacer {
            padding: 24px 20px;
            position: relative
        }

        .select-vehicle-spacer:not(.-fixed) .content.-gallery .select-vehicle-content-spacer {
            padding: 20px 20px 12px
        }

        .select-vehicle-spacer.-index:not(.-fixed) .select-vehicle-content-spacer {
            padding: 0
        }

        .select-vehicle-spacer.-fixed .select-vehicle-content-spacer {
            padding: 10px 8px
        }

        .select-vehicle-content-spacer.-preload-height {
            min-height: 56px
        }

        .select-vehicle-col, .select-vehicle-col-container {
            -webkit-box-flex: 1;
            -webkit-flex: 1;
            flex: 1
        }

        .aside_slct_wrap {
            height: 48px;
            margin-bottom: 8px;
            position: relative;
            width: 100%
        }

        .ship-wordwide-popup .aside_slct_wrap {
            height: 48px
        }

        .select-vehicle-items-inner {
            display: block
        }

        .select-vehicle-items {
            display: block;
            position: relative;
            width: 100%;
            z-index: 22
        }

        .select-vehicle-items.-half {
            width: auto
        }

        .select-vehicle-items.-front-size, .select-vehicle-items.-rear-size.-rear-active {
            display: block;
            z-index: auto
        }

        .select-vehicle-items.-rear-size {
            display: none
        }

        .select-vehicle-items.-tires-load-speed-index {
            position: static
        }

        .aside_slct.-not-refine {
            background: #fff;
            border: none;
            border-radius: 4px !important;
            box-sizing: border-box;
            color: #464646;
            -webkit-box-flex: 1;
            -webkit-flex: 1;
            flex: 1;
            height: auto;
            margin: 0;
            padding: 25px 10px 5px !important;
            position: absolute;
            text-align: left;
            width: 100%
        }

        .aside_slct
        .-not-refine::before {
            background: url(https://cdn.carid.com/css/prod-images/f3dd7cc3.svg) no-repeat 0 0;
            background-size: 100% auto;
            content: '';
            height: 8px;
            position: absolute;
            right: 20px;
            top: 23px;
            -webkit-transition: -webkit-transform 0.1s;
            transition: -webkit-transform 0.1s;
            transition: transform 0.1s;
            transition: transform 0.1s, -webkit-transform 0.1s;
            width: 13px
        }

        .aside_slct.-not-refine::after {
            color: #464646;
            content: attr(data-placeholder);
            font-size: 0.8rem;
            left: 24px;
            overflow: hidden;
            position: absolute;
            text-overflow: ellipsis;
            text-transform: uppercase;
            top: 16px;
            -webkit-transition: top 0.1s;
            transition: top 0.1s;
            white-space: nowrap;
            width: 70%
        }

        .aside_slct.-not-refine.-active {
            box-shadow: 0 0 0 2px #3761bf;
            z-index: 1000
        }

        .aside_slct.-not-refine.-active::before {
            background-image: url(https://cdn.carid.com/css/prod-images/d1a95e81.svg);
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg)
        }

        .aside_slct.-not-refine.-active::after, .aside_slct.-not-refine.-selected::after {
            color: #757575;
            font-size: 0.6rem;
            top: 9px
        }

        .aside_slct.-not-refine.-tire-popup::after {
            top: 19px
        }

        .aside_slct.-not-refine.-active.-tire-popup::after, .aside_slct.-not-refine.-selected.-tire-popup::after {
            top: 12px
        }

        .select-vehicle-col.-with-border .aside_slct.-not-refine {
            border: 1px solid #757575
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine {
            padding-bottom: 9px;
            padding-top: 23px
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine::before {
            top: 22px
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine::after {
            top: 16px
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine.-active::after, .select-vehicle-spacer.-fixed .aside_slct.-not-refine.-selected::after {
            top: 8px
        }

        .aside_slct.-not-refine.-with-marker::after {
            left: 48px
        }

        .aside_slct.-not-refine.-with-marker .aside_slct_value {
            margin-left: 44px
        }

        .aside_slct.-not-refine.-disabled, .aside_slct.-not-refine.-disabled > .aside_slct_value {
            background-color: #f0f0f0 !important;
            color: #717d90
        }

        .aside_slct.-not-refine.-bordered {
            box-shadow: inset 0 0 0 1px #a9a9a9
        }

        .aside_slct.-not-refine.-bordered::before {
            right: 15px;
            top: 22px
        }

        .aside_slct.-not-refine.-bordered.-active {
            box-shadow: 0 0 0 2px #3761bf
        }

        .aside_slct.-not-refine.-unfocused {
            background: #d8d8d8
        }

        .aside_slct.-not-refine.-unfocused::after {
            color: #757575
        }

        .aside_slct.-not-refine .aside_slct_value {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            color: #111;
            font-size: 1.4em;
            height: 16px;
            line-height: 1.1;
            margin-left: 14px;
            padding-left: 0
        }

        .aside_slct.-not-refine .aside-slct-marker {
            border-right: 1px solid #a9a9a9;
            color: #757575;
            font-size: 1.4em;
            height: 26px;
            left: 0;
            line-height: 27px;
            padding: 0 12px 0 16px;
            position: absolute;
            top: 12px
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine .aside-slct-marker {
            top: 11px
        }

        .select-vehicle-button {
            background: #d4252a;
            border-radius: 4px;
            color: #fff;
            cursor: pointer;
            font: 500 1.4em/1.2 "Roboto", "Arial", "Helvetica", sans-serif;
            min-width: 40px;
            padding: 20px 0;
            text-align: center;
            text-transform: uppercase
        }

        .select-vehicle-spacer.-fixed .select-vehicle-button {
            padding: 16px 0 14px
        }

        .select-vehicle-button.-disabled, .select-vehicle-button.-disabled:hover {
            background-color: #757575;
            cursor: default;
            font-weight: 700
        }

        .select-vehicle-button:hover, .select-vehicle-button:focus:not(.-disabled) {
            background: #be2126;
            text-decoration: none
        }

        .select-vehicle-button.-after-selects {
            float: none;
            margin-left: 0
        }

        .select-vehicle-button.-full-width {
            box-sizing: border-box;
            width: 100%
        }

        .select-vehicle-button.-tire-size-pp {
            margin-top: 52px
        }

        .select-vehicle-items.-front-size.-rear-active .select-vehicle-button {
            display: none
        }

        .select-vehicle-bottom-line {
            line-height: 20px;
            margin: 15px 0 0 15px
        }

        .select-vehicle-bottom-line.-no-margin {
            margin: 0
        }

        .select-vehicle-spacer.-fixed .select-vehicle-bottom-line {
            display: none
        }

        .my-garage-line {
            background: url(https://cdn.carid.com/css/prod-images/e8eccc2d.svg) 16px 50% no-repeat;
            border-left: 1px solid #d8d8d8;
            display: inline-block;
            height: 18px;
            margin-left: 10px;
            padding-left: 16px;
            vertical-align: bottom
        }

        .my-garage-line > .header-dd-hover-element {
            display: inline-block
        }

        .my-garage-line .mygarage-dd {
            left: 0;
            margin-top: 12px;
            right: 0;
            top: 74%;
            z-index: 23
        }

        .my-garage-line-title {
            cursor: pointer;
            display: inline-block;
            line-height: 1.2;
            padding-left: 28px;
            padding-right: 10px;
            vertical-align: top
        }

        .main-select-bar-overlay {
            background: #111;
            height: 100%;
            left: 0;
            opacity: 0.5;
            position: fixed;
            top: 0;
            width: 100%
        }

        .breadcrumbs-holder {
            margin: 16px 8px -12px;
            overflow: hidden;
            position: relative
        }

        .breadcrumbs-holder::after {
            box-shadow: 0 0 4px 4px #fff;
            content: '';
            height: 100%;
            position: absolute;
            right: -1px;
            top: 0;
            width: 1px;
            z-index: 1
        }

        .body-bg-type1 .breadcrumbs-holder::after {
            box-shadow: 0 0 4px 4px #f0f0f0
        }

        .body-bg-type1.-dark .breadcrumbs-holder::after {
            box-shadow: 0 0 4px 4px #d8d8d8
        }

        .breadcrumbs-holder.-static {
            margin-top: 0;
            padding-top: 20px
        }

        .breadcrumbs-holder.-my-acc {
            float: left;
            margin-bottom: 0;
            margin-top: 10px
        }

        .breadcrumbs, .select-vehicle-dept-switchers {
            color: #111;
            font-size: 1.2em;
            font-weight: 300;
            list-style: none;
            position: relative;
            text-transform: capitalize;
            z-index: 1
        }

        .breadcrumbs > .item, .select-vehicle-dept-switchers > .item {
            display: inline-block
        }

        .breadcrumbs > .item.-active, .select-vehicle-dept-switchers > .item.-active {
            font-weight: 700
        }

        .breadcrumbs > .item::after, .select-vehicle-dept-switchers > .item::after {
            display: inline-block;
            font-style: normal;
            text-align: center;
            width: 22px
        }

        .breadcrumbs > .item:last-child, .select-vehicle-dept-switchers > .item:last-child {
            padding-right: 5px
        }

        .breadcrumbs > .item:last-child::after, .select-vehicle-dept-switchers > .item:last-child::after {
            display: none
        }

        .breadcrumbs > .item > .link, .select-vehicle-dept-switchers > .item > .link {
            color: #111
        }

        .breadcrumbs > .item::after {
            content: '\203a';
            font-size: 1.6em;
            line-height: 0.58;
            position: relative;
            top: 2px
        }

        .select-vehicle-dept-switchers {
            display: none
        }

        .select-vehicle-dept-switchers > .item::after {
            content: '\007c'
        }

        .ptype-grid-h {
            padding: 1px 0
        }

        .ptype-grid-h:last-child {
            margin-bottom: 0
        }

        .ptype-grid-h2 {
            color: #111;
            font-size: 2.2em;
            font-weight: 500;
            line-height: 1.17;
            margin-bottom: 16px;
            text-align: center
        }

        .ptype-grid-h2 > .inner {
            color: inherit;
            display: -webkit-inline-box;
            display: -webkit-inline-flex;
            display: inline-flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column
        }

        .ptype-grid-h2 > .inner > .descr {
            display: none
        }

        .ptype-grid {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            list-style: none;
            margin: 16px 0 32px;
            overflow: hidden
        }

        .ptype-grid.-no-margin {
            margin: 0
        }

        .ptype-grid.-center {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center
        }

        .ptype-grid.-tile > .li {
            background: #fff
        }

        .ptype-grid > .li {
            box-sizing: border-box;
            height: auto;
            margin: 2%;
            overflow: hidden;
            position: relative;
            text-align: center;
            width: 46%
        }

        .ptype-grid > .li::before {
            color: #a9a9a9;
            content: attr(data-qty);
            font-size: 1.1em;
            left: 0;
            position: absolute;
            right: 0;
            top: 8%;
            z-index: 2
        }

        .ptype-grid.-col-6 > .li::before {
            display: none
        }

        .ptype-grid.-col-6 > .li > .lazy-loading {
            display: block;
            padding-top: 52%;
            top: 22%;
            width: 80%
        }

        .ptype-grid.-col-5 > .li > .lazy-loading {
            display: block;
            padding-top: 52%;
            top: 22%;
            width: 80%
        }

        .ptype-grid.-col-9 > .li {
            margin: 8px 0.65%;
            width: 31.833333%;
            min-width: 100px;
        }

        .ptype-grid.-col-9 > .li::before {
            display: block
        }

        .ptype-grid.-col-9 > .li > .lazy-loading {
            background: #d8d8d8;
            display: block;
            padding-top: 46%;
            width: 70%
        }

        .ptype-grid-img {
            height: auto;
            left: 0;
            margin: auto;
            max-width: 100%;
            position: absolute;
            right: 0;
            top: 22%;
            width: 90%
        }

        .ptype-grid.-col-9 .ptype-grid-img {
            top: 0 !important
        }

        .ptype-grid-a {
            background: url(https://cdn.carid.com/css/prod-images/44d36080.svg) 0 0 no-repeat;
            box-sizing: border-box;
            color: #111;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            font-size: 1.4em;
            font-weight: 500;
            left: 0;
            line-height: 1.3;
            padding: 95% 8px 12px;
            position: relative;
            table-layout: fixed;
            text-decoration: none;
            width: 100%
        }

        .ptype-grid.-col-9 .ptype-grid-a {
            padding-bottom: 0
        }

        .ptype-grid-a:hover {
            text-decoration: none
        }

        .ptype-grid-a:hover > .ptype-grid-title, .ptype-grid-a.-simple-title:hover {
            text-decoration: none
        }

        .ptype-grid-a::before {
            background: url(https://cdn.carid.com/css/prod-images/44d36080.svg) 0 0 no-repeat;
            content: '';
            display: block;
            height: 150%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 2
        }

        .ptype-grid-a::after {
            color: #757575;
            content: attr(data-descr);
            font-size: 12px;
            font-weight: 400;
            line-height: 1.25;
            position: relative;
            text-transform: none
        }

        .ptype-grid-a.-simple-title::after {
            padding-top: 6px
        }

        .ptype-grid-a.-simple-title, .ptype-grid-title {
            font-weight: 400
        }

        .app-section-content .ptype-grid-a.-simple-title, .app-section-content .ptype-grid-title {
            font-weight: 300
        }

        .ptype-grid-title {
            display: block;
            padding-bottom: 6px
        }

        .ptype-grid-title.-truncate {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 100%
        }

        .ptype-grid6 {
            list-style: none;
            overflow: hidden
        }

        .ptype-grid6 > li {
            float: left;
            vertical-align: top;
            width: 16.666666%
        }

        .ptype-grid6-item {
            border: 2px solid transparent;
            border-radius: 2px;
            box-sizing: border-box;
            height: 100%;
            margin: 0 1px;
            position: relative;
            text-align: center;
            -webkit-transition: border-color 0.1s ease-out;
            transition: border-color 0.1s ease-out
        }

        .ptype-grid6-item:hover, .ptype-grid6-item.-active {
            border-color: #ffac00
        }

        .ptype-grid6-item > .title {
            color: #464646;
            display: block;
            font-size: 1.5em;
            line-height: 1.2;
            padding: 64% 7px 5%;
            position: relative
        }

        .ptype-grid6-item > .title.-linklike {
            cursor: pointer
        }

        .ptype-grid6-item > .title.-linklike:hover {
            text-decoration: underline
        }

        .ptype-grid6-item > .img {
            height: auto;
            left: 0;
            margin: auto;
            max-width: 100%;
            position: absolute;
            right: 0;
            top: 8px;
            width: 80%
        }

        .ptype-shifter-wrap {
            display: none
        }

        .ptype-lst-parent {
            margin-bottom: 32px;
            margin-top: 16px
        }

        .ptype-lst-nav-h {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
            z-index: 11
        }

        .ptype-lst-nav {
            height: 63px;
            letter-spacing: -0.31em;
            list-style: none;
            position: relative;
            width: 200%
        }

        .ptype-lst-nav > .item {
            box-sizing: border-box;
            display: inline-block;
            letter-spacing: normal;
            padding: 4px 0;
            position: relative;
            -webkit-transition: left 0.4s, background 0.2s ease;
            transition: left 0.4s, background 0.2s ease;
            vertical-align: top;
            width: 10%
        }

        .ptype-lst-nav > .item.-hide {
            display: none
        }

        .ptype-lst-nav > .item > .link {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            color: #111;
            display: block;
            font-size: 1em;
            font-weight: 400;
            line-height: 1.538em;
            text-align: center
        }

        .ptype-lst-nav > .item > .link:hover {
            text-decoration: none
        }

        .ptype-lst-nav > .item > .link.active {
            font-weight: 400;
            text-decoration: none
        }

        .ptype-lst-nav > .item > .link.active::after {
            border-bottom: 2px solid #717d90;
            bottom: -2px;
            content: '';
            display: block;
            left: 0;
            position: absolute;
            width: 100%
        }

        .ptype-lst-icon {
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: 0, auto 27px;
            display: block;
            height: 40px;
            position: relative;
            width: 100%
        }

        .ptype-lst-nav.-fixed .ptype-lst-icon {
            background-position: 50% 50%
        }

        .service-types-shifter {
            margin: 10px 0 40px !important
        }

        .service-types-list {
            list-style: none;
            overflow: hidden
        }

        .service-types-list > .service-types-item {
            float: left;
            height: 100%;
            position: relative;
            vertical-align: top;
            width: 150px
        }

        .service-types-list > .service-types-item > .link {
            color: #111
        }

        .service-types-list > .service-types-item > .link > .text {
            display: block;
            font-size: 16px;
            padding: 70% 6px 15%;
            position: relative
        }

        .service-types-list > .service-types-item > .link > .img {
            height: auto;
            left: 0;
            margin: auto;
            max-width: 100%;
            position: absolute;
            right: 0;
            width: 95%
        }

        .shifter {
            position: relative;
            width: 100%
        }

        .module-loader {
            position: relative
        }

        .module-loader::after {
            -webkit-animation: preloader-rotate 0.7s infinite linear;
            animation: preloader-rotate 0.7s infinite linear;
            background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMCIgdmlld0JveD0iMCAwIDMwIDMwIj48cGF0aCBkPSJNMTUgMzBhMTUgMTUgMCAxMTE1LTE1IDE1IDE1IDAgMDEtMTUgMTV6bTAtMjcuNzNBMTIuNzMgMTIuNzMgMCAxMDI3LjczIDE1IDEyLjc1IDEyLjc1IDAgMDAxNSAyLjI3eiIgZmlsbD0iIzFhMWExYSIgb3BhY2l0eT0iLjciLz48cGF0aCBkPSJNMjcuNzEgMTUuNDhBMTIuNzQgMTIuNzQgMCAwMTE1IDI3LjczVjMwYTE1IDE1IDAgMDAxNS0xNC41MnoiIGZpbGw9IiNmZmYiIG9wYWNpdHk9Ii43Ii8+PC9zdmc+);
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: contain;
            content: '';
            height: 30px;
            left: 50%;
            position: absolute;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            -webkit-transform-origin: 50% 50%;
            transform-origin: 50% 50%;
            width: 30px
        }

        .module-loader.nopreload::after {
            content: none !important
        }

        .gbox_loader {
            display: none
        }

        .gal_po_holder {
            display: none
        }

        .aside_slct_opts {
            display: none
        }

        .big-video-block {
            display: none
        }

        .subcats1-item {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            font-size: 1.6em;
            min-height: 4em
        }

        .prod_grd, .prod_lst {
            list-style: none
        }

        .prod_grd > li, .prod_lst > li {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex
        }

        .prod_grd .lst_a, .prod_lst .lst_a {
            color: #464646
        }

        .prod_grd .lst_ic_h::before, .prod_lst .lst_ic_h::before {
            content: '';
            display: block;
            padding-top: 100%;
            width: 100%
        }

        .prod_grd .lst_ic_h > .lazy-loading, .prod_lst .lst_ic_h > .lazy-loading {
            display: block;
            padding-bottom: 100%;
            width: 100%
        }

        .prod_grd .lst-descr-text, .prod_grd .lst_id, .prod_lst .lst-descr-text, .prod_lst .lst_id {
            display: none
        }

        .prod_grd {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap
        }

        .prod_grd > li {
            -webkit-box-align: stretch;
            -webkit-align-items: stretch;
            align-items: stretch;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column;
            margin: 0 1.8% 16px;
            width: 46.4%
        }

        .prod_grd .lst_ic_h {
            overflow: hidden;
            position: relative;
            width: 100%
        }

        .prod_grd .lst_ic {
            bottom: 0;
            height: auto;
            margin: auto;
            position: absolute;
            top: 0;
            width: 100%
        }

        .prod_grd .lst_featured_video, .prod_grd .lst_features, .prod_grd .lst-type-icons, .prod_grd .prod_avail {
            background-image: none;
            display: none
        }

        .prod_lst {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column
        }

        .prod_lst > li {
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            flex-direction: row;
            margin: 8px 8px 0;
            padding-bottom: 10px;
            position: relative
        }

        .prod_lst .lst_ic_h {
            -webkit-flex-shrink: 0;
            flex-shrink: 0;
            position: relative;
            width: 50%
        }

        .prod_lst .lst_ic {
            height: auto;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%
        }

        .prod_lst .lst_main {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column;
            -webkit-flex-shrink: 1;
            flex-shrink: 1;
            margin-left: 5%;
            min-width: 0;
            position: relative;
            width: 100%
        }

        @media only screen and (max-width: 420.98px) {
            #js-media-query-state {
                -webkit-transform: scale(7);
                transform: scale(7)
            }

            #js-media-query-state::after {
                content: 'mobile-small'
            }

            .mobile-small-show {
                display: block !important
            }

            .mobile-small-hide {
                display: none !important
            }

            .head_img_minh::after {
                padding-bottom: 50%
            }

            .head_img_minh.-big::after {
                padding-bottom: 70%
            }

            .head_img_aligner.-align-left {
                left: 86%
            }

            .head_img_minh.-big > .head_img_aligner.-has-mobile-src {
                left: 80%
            }

            .head-nav.-with-mmy, .head-nav.-with-mmy .simple-slider-wrap {
                height: 374px
            }

            .select-vehicle > .nav.-garage > .link {
                background-position: 2px 48%;
                font-size: 1.4em;
                line-height: 1.3
            }

            .select-vehicle > .nav.-garage > .link.-i1-31 {
                background-position: 8px 48%
            }

            .select-vehicle > .nav.-garage > .link.-i1-31.-active {
                padding-left: 60px
            }

            .ptype-grid.-col-6 > li {
                margin: 4px 1%;
                width: 31.333333%
            }

            .ptype-grid.-col-6 .ptype-grid-img {
                width: 80%
            }

            .ptype-grid.-col-5 > li {
                margin: 4px 1%;
                width: 48%;
            }

            .ptype-grid.-col-5 > li::before {
                display: none
            }

            .ptype-grid.-col-5 .ptype-grid-img {
                width: 80%
            }

            .ptype-grid.-col-6 .ptype-grid-a {
                padding-top: 100%
            }

            .ptype-grid.-col-9 .ptype-grid-a {
                padding-top: 60%
            }
        }

        @media only screen and (min-width: 421px) and (max-width: 740.98px) {
            #js-media-query-state {
                -webkit-transform: scale(6);
                transform: scale(6)
            }

            #js-media-query-state::after {
                content: 'mobile-medium'
            }

            .mobile-medium-show {
                display: block !important
            }

            .mobile-medium-hide {
                display: none !important
            }

            .h1-fat {
                font-size: 3em;
                margin: 16px 8px
            }

            .nav-tool {
                margin-right: 10px
            }

            .header-search-btn {
                right: 64px
            }

            .head_img_minh::after {
                padding-bottom: 44%
            }

            .head_img_aligner.-align-left {
                left: 75%
            }

            .head_img_minh.-big > .head_img_aligner.-has-mobile-src {
                left: 50% !important
            }

            .head-nav.-with-mmy, .head-nav.-with-mmy .simple-slider-wrap {
                height: 386px
            }

            .head-nav.-with-mmy .head-nav-inner {
                padding-left: 40px;
                padding-right: 40px
            }

            .select-vehicle > .nav.-garage > .link {
                font-size: 1.4em;
                line-height: 1.3;
                padding-right: 12px
            }

            .my-garage-line .mygarage-dd {
                box-sizing: border-box;
                max-width: 100%;
                min-width: 310px
            }

            .ptype-grid-h:last-child {
                margin-bottom: 10px
            }

            .ptype-grid > .li {
                margin: 6px 1%;
                width: 31.333%
            }

            .ptype-grid.-col-5 .ptype-grid-a, .ptype-grid.-col-6 .ptype-grid-a {
                padding-bottom: 16px;
                padding-top: 100% !important;
            }

            .ptype-grid.-col-5 .ptype-grid-img, .ptype-grid.-col-6 .ptype-grid-img {
                top: 14%
            }

            .ptype-grid.-col-6 > .li > .lazy-loading {
                padding-top: 58%;
                top: 14%;
                width: 90%
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 58%;
                top: 14%;
                width: 90%
            }

            .ptype-grid-a {
                padding-top: 97%
            }

            .ptype-grid.-col-9 .ptype-grid-a {
                padding-top: 56%
            }

            .ptype-lst-nav > .item {
                width: 7.14%
            }

            .service-types-shifter {
                padding-top: 20px
            }

            .service-types-list > .service-types-item {
                width: 33.33333333%
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px) {
            #js-media-query-state {
                -webkit-transform: scale(5);
                transform: scale(5)
            }

            #js-media-query-state::after {
                content: 'mobile-large'
            }

            .mobile-large-show {
                display: block !important
            }

            .mobile-large-hide {
                display: none !important
            }

            .h1-fat.-searchresult {
                margin-bottom: 18px
            }

            .head-stores {
                margin-left: 44px
            }

            .head-stores > li.hot::after {
                right: 0
            }

            .head-stores > li > .item {
                padding-right: 8px
            }

            .head-stores > li > .item.-active {
                padding-right: 25px
            }

            .header-top {
                height: 80px
            }

            .header-top .header-dd-h {
                top: 64px
            }

            .header-account {
                margin-top: 15px
            }

            .head_logo_a {

                background-size: 122px auto;
                bottom: 0;
                left: 80px;
            }
            .-sm-white {

              background-size: 122px auto;
              bottom: 0;
              left: 80px;
            }

            .head_logo_a.-carid::after {
                height: 20px;
                left: 96px;
                top: 30px;
                width: 120px
            }
            .-sm-white.-carid::after {
                height: 20px;
                left: 96px;
                top: 30px;
                width: 120px
            }

            .nav-tool-wrap {
                margin-top: 15px
            }

            .header-search-btn {
                margin: 0 210px 0 300px;
                top: 20px
            }

            .header-search-btn.-short-logo {
                margin-left: 164px
            }

            .header-search-btn.-no-garage {
                margin-right: 160px
            }

            .head-dd > .it > .head-dd-main {
                padding-right: 16px
            }

            .head_img_minh::after {
                padding-bottom: 38.4%
            }

            .head_img_minh.-big::after {
                padding-bottom: 336px
            }

            .head_img_aligner.-align-left {
                left: 66%
            }

            .head-nav.-with-mmy, .head-nav.-with-mmy .simple-slider-wrap {
                height: 360px
            }

            .head-nav.-with-mmy .head-nav-inner {
                max-width: 920px
            }

            .head-nav-title {
                font-size: 3.8em
            }

            .head-nav.-with-mmy .head-nav-title {
                font-size: 4em;
                line-height: 1.26;
                margin-bottom: 8px
            }

            .head-nav.-with-mmy .head-nav-subtitle {
                font-size: 0.9em !important;
            }

            .select-vehicle > .nav > .link {
                background-position: right 20px top 50%
            }

            .select-vehicle > .nav > .link:first-child {
                min-width: 300px
            }

            .select-vehicle-items.-tires {
                display: block
            }

            .aside_slct.-not-refine::after {
                width: 44%
            }

            .my-garage-line .mygarage-dd-container {
                min-width: 416px
            }

            .ptype-grid-h {
                margin-top: 20px
            }

            .ptype-grid-h:last-child {
                margin-bottom: 40px
            }

            .ptype-grid > .li {
                margin: 8px 1%;
                width: 23%
            }

            .ptype-grid.-col-6 > .li > .lazy-loading {
                padding-top: 58%;
                width: 90%
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 58%;
                width: 90%
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                padding-top: 39%;
                width: 60%
            }

            .ptype-grid.-col-9 .ptype-grid-img {
                width: 60%
            }

            .ptype-grid-a {
                font-size: 1.4em;
                padding-top: 105%
            }

            .ptype-grid.-col-9 .ptype-grid-a {
                padding-top: 48%
            }

            .ptype-grid-a::after {
                font-size: 11px;
                line-height: 1
            }

            .ptype-lst-nav-h {
                margin: 0 8px
            }

            .ptype-lst-nav > .item {
                width: 5.55%
            }

            .ptype-lst-nav:not(.-fixed) .ptype-lst-icon {
                background-size: 0, auto 36px;
                margin-bottom: 5px
            }

            .service-types-list > .service-types-item {
                width: 25%
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px) {
            #js-media-query-state {
                -webkit-transform: scale(4);
                transform: scale(4)
            }

            #js-media-query-state::after {
                content: 'desktop-small'
            }

            .desktop-small-hide {
                display: none !important
            }

            .head-stores {
                margin-left: 50px
            }

            .head-stores > li.hot::after {
                right: -8px
            }

            .head-stores > li > .item {
                padding-left: 54px;
                padding-right: 10px
            }

            .header-top {
                height: 88px
            }

            .header-top .header-dd-h {
                top: 84px
            }

            .head_img_minh {
                height: 346px
            }

            .head_img_aligner.-align-left {
                left: 62%
            }

            .head-nav-title {
                font-size: 4.2em
            }

            .select-vehicle > .nav > .link {
                width: 26%
            }

            .select-vehicle > .nav > .link:first-child {
                width: 36%
            }

            .ptype-grid.-col-5 > .li >
            .lazy-loading {
                padding-top: 52%;
                width: 80%
            }

            .ptype-grid.-col-9 {
                width: 80%
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                padding-top: 42%
            }

            .ptype-grid-img {
                width: 80%
            }

            .ptype-grid-a {
                padding-top: 88%
            }

            .ptype-grid6-item > .title {
                font-size: 1.4em
            }

            .ptype-lst-nav:not(.-fixed) {
                margin: 0 auto;
                width: 80%
            }

            .service-types-list > .service-types-item {
                width: 20%
            }
        }

        @media only screen and (min-width: 1281px) and (max-width: 1600.98px) {
            #js-media-query-state {
                -webkit-transform: scale(3);
                transform: scale(3)
            }

            #js-media-query-state::after {
                content: 'desktop-medium'
            }

            .head-dd {
                margin-left: 162px
            }

            .head-dd > .it > .head-dd-main {
                padding-left: 16px;
                padding-right: 16px
            }

            .head_img_minh {
                height: 372px
            }

            .head_img_aligner.-align-left {
                left: 58%
            }

            .head-nav-title {
                font-size: 4.0em
            }

            .ptype-grid.-col-6 > .li > .lazy-loading {
                padding-top: 46%;
                top: 60px;
                width: 170px
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 46%;
                top: 60px;
                width: 170px
            }

            .ptype-grid.-col-9 {
                width: 75%
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                padding-top: 35%
            }

            .ptype-lst-nav:not(.-fixed) {
                margin: 0 auto;
                width: 75%
            }
        }

        @media only screen and (min-width: 1601px) {
            #js-media-query-state {
                -webkit-transform: scale(2);
                transform: scale(2)
            }

            #js-media-query-state::after {
                content: 'desktop-large'
            }

            .product-list-with-filter-holder {
                margin-right: -300px;
                padding-right: 300px
            }

            .head-dd > .it > .head-dd-main > .link > span, .head-dd > .it > .head-dd-main .extra, .head-dd > .it > .head-dd-main .extra + .link {
                display: inline
            }

            .head_img_minh {
                height: 384px
            }

            .head_img_minh.-big {
                height: 550px
            }

            .head-img-holder {
                height: 460px;
                overflow: hidden
            }

            .head-img-holder .head_img_minh {
                top: 50%;
                -webkit-transform: translate(0, -50%);
                transform: translate(0, -50%)
            }

            .head-nav-title {
                font-size: 5.6em
            }

            .ptype-grid.-col-6 > .li {
                width: 15.16%
            }

            .ptype-grid.-col-6 > .li > .lazy-loading {
                padding-top: 56%;
                top: 60px;
                width: 170px
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 46%;
                top: 60px;
                width: 170px
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                padding-top: 50%
            }

            .ptype-lst-nav.-fixed .ptype-lst-icon {
                background-size: 0, auto 46px;
                height: 56px;
                margin-bottom: 8px
            }
        }

        @media only screen and (max-width: 420.98px), only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px) {
            .main_wide p img {
                height: auto;
                max-width: 100%
            }

            .wrap .wrap {
                padding: 0
            }

            .desktop-show {
                display: none !important
            }

            .desktop-hide {
                display: block !important
            }

            .mobile-show {
                display: block !important
            }

            .mobile-hide {
                display: none !important
            }

            .head_img_minh::after {
                content: '';
                display: block;
                width: 100%
            }

            .select-vehicle > .nav.enabled {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex;
                width: 100%
            }

            .select-vehicle > .nav.enabled > .link {
                background-image: none;
                float: none
            }

            .select-vehicle > .nav:not(.enabled) > .link {
                display: none
            }

            .select-vehicle > .nav:not(.enabled) > .link:first-child {
                display: block;
                width: 100%
            }

            .select-vehicle > .content > .tab.-light {
                border-radius: 0 0 4px 4px
            }

            .select-vehicle > .nav:not(.enabled) + .content .select-vehicle-content-spacer {
                padding-top: 0
            }

            .aside_slct.-not-refine.-with-marker .aside_slct_value {
                margin-left: 38px
            }

            .nav.enabled > .link:not(.-active) .my-garage-line {
                z-index: 0
            }

            .breadcrumbs-holder .breadcrumbs-ov {
                overflow-x: auto;
                padding-bottom: 12px
            }

            .breadcrumbs {
                white-space: nowrap
            }

            .ptype-lst-parent {
                width: 100%
            }

            .ptype-lst-nav > .item:first-child {
                margin-left: 0
            }
        }

        @media only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px) {
            .landing_pages_descr > a, .landing_pages_descr .text-height-overflow > a {
                margin: 0 !important;
                max-width: 48%
            }

            .landing_pages_descr > a > img, .landing_pages_descr .text-height-overflow > a > img {
                height: auto !important;
                margin: 32px 1% !important;
                max-width: 98%;
                width: auto !important
            }

            .head_img_minh.-big > .head_img_aligner.-align-left {
                left: 54%
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .landing_pages_descr h2 {
                font-size: 2.1em
            }

            .landing_pages_descr ul {
                margin-left: 3em
            }

            .main-content {
                margin-top: 0
            }

            .main-content.-overlay::after {
                background: rgba(0, 0, 0, 0.75);
                content: '';
                height: 100%;
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 25
            }

            .h1-fat {
                font-size: 3.6em;
                margin: 24px 8px 40px
            }

            .h1-fat.-searchresult {
                margin-top: 0
            }

            .head-stores-h {
                height: 32px
            }

            .head-stores::after {
                display: none
            }

            .head-stores > li {
                font-size: 1.2em;
                position: relative
            }

            .head-stores > li.hot::after {
                background: url(https://cdn.carid.com/css/prod-images/0f575720.svg) no-repeat left top;
                background-size: 100% auto;
                content: '';
                height: 10px;
                position: absolute;
                top: 2px;
                width: 22px
            }

            .head-stores > li > .item {
                height: 32px;
                padding-top: 9px
            }

            .head-stores > li > .item:not(.-active) {
                color: #fff;
                display: block;
                opacity: 0.6;
                -webkit-transition: opacity 0.25s;
                transition: opacity 0.25s
            }

            .header-top {
                background-color: #fff
            }

            .header-top .header-dd-h {
                display: block
            }

            .left-menu-icon {
                background-size: 29px 24px;
                height: 80px;
                width: 60px
            }

            .header-dd-h {
                font-size: 10px;
                opacity: 0;
                position: absolute;
                -webkit-transform: translateY(-10px);
                transform: translateY(-10px);
                -webkit-transition: opacity 0.3s ease 0.2s, visibility 0s ease 0.3s, -webkit-transform 0.3s ease 0.2s;
                transition: opacity 0.3s ease 0.2s, visibility 0s ease 0.3s, -webkit-transform 0.3s ease 0.2s;
                transition: opacity 0.3s ease 0.2s, transform 0.3s ease 0.2s, visibility 0s ease 0.3s;
                transition: opacity 0.3s ease 0.2s, transform 0.3s ease 0.2s, visibility 0s ease 0.3s, -webkit-transform 0.3s ease 0.2s;
                visibility: hidden
            }

            .no-transform .header-dd-h {
                display: none
            }

            .header-dd-hover-element:hover .header-dd-h {
                display: block
            }

            .help-center-nav-btn {
                color: #d4252a;
                cursor: pointer;
                line-height: 1;
                padding: 10px 14px 10px 10px;
                position: relative
            }

            .help-center-nav-btn > span {
                font-size: 14px;
                font-weight: 500
            }

            .nav-tool-wrap {
                margin-right: 0
            }

            .nav-tool.-account {
                display: block;
                padding-left: 30px
            }

            .nav-tool.-wishlist, .nav-tool.-garage {
                display: block
            }

            .header-search-btn {
                border-color: #757575;
                cursor: pointer;
                left: auto;
                line-height: 40px;
                padding: 0 75px 0 20px;
                position: relative;
                right: auto
            }

            .header-search-btn::before {
                display: block;
                left: auto;
                overflow: hidden;
                text-overflow: ellipsis;
                top: auto;
                white-space: nowrap
            }

            .header-search-btn::after {
                background-size: 100% auto;
                height: 23px;
                right: 12px;
                top: 8px;
                width: 25px
            }

            .head-dd-h {
                border-bottom: 3px solid #464646;
                display: block;
                height: 40px;
                width: 100%
            }

            .head-nav {
                height: 550px
            }

            .head-nav-inner {
                margin-top: 0
            }

            .head-nav-title {
                letter-spacing: 0.02em;
                line-height: 1.1;
                margin-bottom: 20px
            }

            .head-nav.-with-mmy .head-nav-title {
                letter-spacing: normal
            }

            .head-nav-subtitle {
                color: #fff;
                display: block;
                font-size: 2.1em;
                font-weight: 300;
                letter-spacing: 0.02em;
                line-height: 1.2;
                margin-bottom: 15px
            }

            .simple-slider-wrap {
                display: block;
                height: 550px;
                left: 50%;
                margin-left: -960px;
                overflow: hidden;
                position: absolute;
                top: 0;
                width: 1920px
            }

            .head-main-logo.-with-main-select-bar {
                bottom: 104px
            }

            .head-nav-inner > .select-vehicle-spacer {
                margin-bottom: 26px
            }

            .head-shop-by-links-item, .head-shop-by-links-item > .link {
                text-align: left
            }

            .head-shop-by-links-item > .count {
                display: inline
            }

            .select-vehicle-spacer {
                margin-top: -74px
            }

            .select-vehicle-spacer.-top-indent {
                margin-top: 32px
            }

            .select-vehicle > .nav.-garage > .link {
                padding-right: 24px
            }

            .select-vehicle > .nav.-garage > .link > .link-title {
                display: block
            }

            .select-vehicle > .content > .tab {
                display: block
            }

            .select-vehicle-spacer:not(.-fixed) .content.-gallery .select-vehicle-content-spacer {
                padding: 24px
            }

            .aside_slct_wrap {
                height: 56px;
                margin-bottom: 0
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct_wrap {
                height: 63px
            }

            .select-vehicle-spacer.-fixed .aside_slct_wrap {
                height: 48px
            }

            .select-vehicle-content-spacer.-two-columns .aside_slct_wrap {
                margin-bottom: 16px
            }

            .select-vehicle-content-spacer.-two-columns .-tires .aside_slct_wrap {
                margin-bottom: 0
            }

            .select-vehicle-items-inner {
                -webkit-box-align: center;
                -webkit-align-items: center;
                align-items: center;
                display: -webkit-box;
                display: -webkit-flex;
                display: flex;
                -webkit-flex-wrap: wrap;
                flex-wrap: wrap;
                width: 100%
            }

            .select-vehicle-items {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex
            }

            .select-vehicle-items.-half {
                width: 50%
            }

            .aside_slct.-not-refine {
                margin-bottom: 0;
                padding-bottom: 12px;
                padding-top: 28px
            }

            .aside_slct.-not-refine::before {
                top: 26px
            }

            .aside_slct.-not-refine::after {
                top: 21px
            }

            .aside_slct.-not-refine.-active::after, .aside_slct.-not-refine.-selected::after {
                top: 12px
            }

            .aside_slct.-not-refine.-tire-popup::after {
                top: 24px
            }

            .aside_slct.-not-refine.-active.-tire-popup::after, .aside_slct.-not-refine.-selected.-tire-popup::after {
                top: 15px
            }

            .select-vehicle-col + .select-vehicle-col > .aside_slct_wrap > .aside_slct.-not-refine {
                margin: 0 0 0 8px;
                width: calc(100% - 8px)
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine {
                padding-bottom: 22px !important;
                padding-top: 32px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine::before {
                right: 16px;
                top: 30px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine::after {
                top: 24px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine.-active::after, .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine.-selected::after {
                top: 16px
            }

            .aside_slct.-not-refine.-bordered::before {
                top: 26px
            }

            .select-vehicle-spacer.-index .aside_slct.-not-refine .aside_slct_value {
                font-size: 1.5em
            }

            .aside_slct.-not-refine .aside-slct-marker {
                top: 16px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine .aside-slct-marker {
                font-size: 1.5em;
                height: 26px;
                line-height: 1.75;
                top: 20px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .select-vehicle-button {
                padding-bottom: 23px;
                padding-top: 23px;
            }

            .select-vehicle-button.-after-selects {
                margin-left: 8px;
                width: 10%
            }

            .select-vehicle-button.-tire-size-pp {
                margin-top: 0
            }

            .tires-sizes-selection .select-vehicle-items.-front-size.-rear-active .select-vehicle-button {
                display: block;
                visibility: hidden
            }

            .my-garage-line .mygarage-dd-container.-both-empty {
                min-width: 302px
            }

            .breadcrumbs-container {
                margin-bottom: 24px;
                margin-top: 24px;
                overflow: hidden
            }

            .breadcrumbs-holder {
                margin: 24px 8px
            }

            .breadcrumbs-container .breadcrumbs-holder {
                margin-bottom: 0;
                margin-top: 0
            }

            .breadcrumbs-holder.-product {
                margin: 15px 8px
            }

            .select-vehicle-dept-switchers {
                display: block;
                float: right;
                margin: 0 8px 0 20px;
                z-index: 2
            }

            .ptype-grid-bg {
                background-position: 40% 0;
                background-repeat: no-repeat;
                margin-top: 18px;
                padding-top: 24px
            }

            .ptype-grid-h2 {
                font-size: 2.8em;
                margin-bottom: 32px
            }

            .ptype-grid-h2 > .inner > .name {
                margin-bottom: 14px
            }

            .ptype-grid-h2 > .inner > .descr {
                color: #757575;
                display: block;
                font-size: 0.428em;
                font-weight: 500;
                line-height: 1.16;
                -webkit-box-ordinal-group: 3;
                -webkit-order: 2;
                order: 2
            }

            .ptype-grid > .li::before {
                top: 20px
            }

            .ptype-grid.-col-6 > .li::before {
                display: block
            }

            .ptype-grid.-col-9 > .li {
                width: 126px
            }

            .ptype-lst-parent {
                margin-top: 0
            }
        }

        @media only screen and (orientation: landscape), only screen and (orientation: portrait) {
            body {
                -webkit-text-size-adjust: 100%
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            #scrollfix {
                min-width: 1000px
            }

            .wrap {
                max-width: 1296px;
                min-width: 1000px;
                width: 94%
            }

            .wrap .wrap {
                margin: 0 auto
            }

            .product-list-with-filter-holder {
                float: right
            }

            .h1-fat.-searchresult {
                margin-bottom: 34px
            }

            .head-stores > li {
                display: block;
                -webkit-box-flex: 0;
                -webkit-flex-grow: 0;
                flex-grow: 0;
                -webkit-flex-shrink: 1;
                flex-shrink: 1
            }

            .head-stores > li > .item:not(.-active) {
                text-indent: 0
            }

            .head_header {
                min-width: 1000px
            }

            .header-account {
                margin-right: 13px;
                margin-top: 38px
            }

            /*.header-account::before {*/
            /*    background: #757575;*/
            /*    content: '';*/
            /*    display: block;*/
            /*    float: right;*/
            /*    height: 15px;*/
            /*    margin-top: 8px;*/
            /*    width: 1px*/
            /*}*/

            .left-menu-icon {
                top: 35px
            }

            .head_logo_a {

                background-size: 155px auto;
                bottom: -46px;
                left: 104px;
                width: 170px;
            }
            .head_logo_a::before {
                bottom: 14%;
                color: #fff;
                content: attr(data-text);
                display: inline-block;
                font-size: 0.9em;
                left: 0;
                position: absolute;
                right: 0;
                text-align: center;
                text-indent: 0;
                z-index: 2
            }

            .head_logo_a.-carid::after {
                height: 24px;
                left: 106px;
                top: 42px;
                width: 149px
            }

            .-sm-white::before {
                bottom: 14%;
                color: #fff;
                content: attr(data-text);
                display: inline-block;
                font-size: 0.9em;
                left: 0;
                position: absolute;
                right: 0;
                text-align: center;
                text-indent: 0;
                z-index: 2
            }

            .-sm-white.-carid::after {
                height: 24px;
                left: 106px;
                top: 42px;
                width: 149px
            }

            .nav-tool-wrap {
                margin-top: 38px
            }

            .nav-tool {
                height: 34px;
                margin: 0 10px;
                min-width: 30px
            }

            .nav-tool .-txt {
                display: inline-block;
                padding-right: 14px;
                position: relative;
                text-align: left;
                text-indent: 0
            }

            .nav-tool > .icon {
                left: 10px;
                top: 1px
            }

            .header-garage-subtitle {
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
                color: #757575;
                display: block;
                font-weight: 400;
                text-transform: none
            }

            .nav-tool.-account {
                width: 124px
            }

            .nav-tool.-garage {
                padding-left: 32px;
                width: 140px
            }

            .header-search-btn {
                margin: 0 405px 0 340px;
                top: 32px
            }

            .header-search-btn::before { /*content:"Search by Make Model Year, Product Type, Part Number, or Brand..."*/
            }

            .header-search-btn.-short-logo {
                margin-left: 180px
            }

            .header-search-btn.-no-garage {
                margin-right: 252px
            }

            .header-search-btn.-no-garage::before {
                content: "Search by Product Type, Part Number, or Brand..."
            }

            .count-item.-type-1 {
                right: -6px;
                top: -3px
            }

            .head_img_minh {
                min-width: 1000px
            }

            .head-img-overlay {
                width: auto
            }

            .head-nav-inner {
                max-width: 1296px;
                min-width: 1000px;
                width: 94%
            }

            .head-nav.-with-mmy .head-nav-inner {
                max-width: 1296px
            }

            .select-vehicle > .nav > .link {
                background-position: 95% 48%;
                background-repeat: no-repeat;
                border-left: none;
                margin-left: 5px
            }

            .select-vehicle > .nav > .link.-icon-select-vehicle {
                background-image: url(https://cdn.carid.com/css/prod-images/b34eea73.svg)
            }

            .select-vehicle > .nav > .link.-icon-size {
                background-image: url(https://cdn.carid.com/css/prod-images/95367a29.svg)
            }

            .select-vehicle > .nav > .link.-icon-star {
                background-image: url(https://cdn.carid.com/css/prod-images/2c6612f4.svg)
            }

            .select-vehicle-spacer.-in-popup .select-vehicle > .nav > .link.-light {
                background-image: none
            }

            .select-vehicle > .nav > .link.-light.-icon-size {
                background-image: url(https://cdn.carid.com/css/prod-images/a08d6515.svg)
            }

            .select-vehicle > .nav > .link.-light.-icon-star {
                background-image: url(https://cdn.carid.com/css/prod-images/8a9215a3.svg)
            }

            .select-vehicle > .nav > .link.-light.-icon-specifications {
                background-image: url(https://cdn.carid.com/css/prod-images/986aa652.svg)
            }

            .select-vehicle > .nav.-garage {
                display: block
            }

            .select-vehicle > .content {
                border-radius: 0 4px 4px
            }

            .select-vehicle-col-container-flex {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex
            }

            .modal-wrap .footer .select-vehicle-col-container-flex {
                display: block
            }

            .select-vehicle-items.-front-size.-rear-active.-in-popup, .select-vehicle-items.-rear-size.-rear-active.-in-popup {
                margin-top: 12px
            }

            .select-vehicle-items.-front-size.-in-popup {
                margin-top: 12px
            }

            .select-vehicle-items .aside_slct.-not-refine::after {
                width: 65%
            }

            .aside_slct.-not-refine.-with-marker::after {
                left: 55px
            }

            .aside_slct.-not-refine .aside-slct-marker {
                padding: 0 17px
            }

            .my-garage-line .mygarage-dd-container {
                min-width: 480px
            }

            .ptype-grid-h {
                margin-top: 0
            }

            .ptype-grid-h:last-child {
                margin-bottom: 56px
            }

            .ptype-grid > .li {
                margin: 8px 0.65%;
                width: 23.5%
            }

            .ptype-grid.-col-5 > .li {
                width: 18.7%
            }

            .ptype-grid.-col-9 {
                margin: 44px auto
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                width: 100px
            }

            .ptype-grid.-col-9 .ptype-grid-img {
                width: 90px !important
            }

            .ptype-grid-a {
                font-size: 1.6em;
                left: auto
            }

            .ptype-grid.-col-9 .ptype-grid-a {
                padding-top: 86px
            }

            .ptype-grid-a:hover > .ptype-grid-title, .ptype-grid-a.-simple-title:hover {
                text-decoration: underline
            }

            .ptype-shifter-wrap {
                border-bottom: 1px solid #d8d8d8;
                display: block;
                margin: 0 1% 36px
            }

            .ptype-lst-nav-h {
                background-color: transparent
            }

            .ptype-lst-nav {
                height: auto;
                width: auto
            }

            .ptype-lst-nav:not(.-fixed) {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -webkit-flex-direction: row;
                flex-direction: row;
                -webkit-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -webkit-justify-content: center;
                justify-content: center
            }

            .ptype-lst-nav.-additional-row:not(.-fixed) {
                margin: 0 auto;
                width: 84%
            }

            .ptype-lst-nav > .item {
                margin: 8px 6px;
                width: 130px
            }

            .ptype-lst-nav > .item > .link {
                font-size: 1.6em;
                line-height: 1
            }

            .ptype-lst-nav.-additional-row:not(.-fixed) > .item {
                width: 13%
            }

            .ptype-lst-icon {
                background-position: 50% 0;
                background-size: 100px auto, 0;
                height: 88px
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px) {
            .product-list-with-filter-holder {
                margin-right: -250px;
                padding-right: 250px
            }

            .head_img_minh.-big > .head_img_aligner.-align-left {
                left: 50%
            }

            .head_img_minh.-big > .head_img_aligner img {
                height: 110.5%;
                margin-top: -16px
            }

            .ptype-grid.-col-6 > .li {
                width: 18.7%
            }

            .ptype-lst-nav.-fixed .ptype-lst-icon {
                background-size: 0, auto 32px;
                height: 40px;
                margin-bottom: 0
            }
        }

        @media only screen and (max-width: 420.98px), only screen and (min-width: 421px) and (max-width: 740.98px) {
            .h1-fat.-mobile-inline {
                display: inline;
                margin-left: 0
            }

            .head-stores > li > .item:not(.-active) {
                background: none
            }

            .head_header {
                border-bottom: 1px solid #d8d8d8;
                left: 0;
                position: relative;
                right: 0;
                top: 0;
                z-index: 3
            }

            .head-dd-cont-holder {
                display: none !important
            }

            .head-shop-by-links.-special-position {
                bottom: 24px
            }

            .select-vehicle > .nav:not(.enabled) > .link {
                cursor: auto
            }

            .select-vehicle > .content > .tab.-with-garage-tab {
                padding-top: 24px
            }

            .-two-columns .select-vehicle-col, .-two-columns .select-vehicle-col-container {
                -webkit-flex-basis: auto;
                flex-basis: auto
            }

            .select-vehicle-button, .select-vehicle-button.-after-selects {
                padding: 12px;
                margin-left: -3%;
                width: 106%;
            }

            .nav.enabled .my-garage-line {
                background-position: 0 50%;
                border-left: none;
                left: 20px;
                margin-left: 0;
                min-width: 118px;
                padding-left: 0;
                position: absolute;
                top: 60px;
                z-index: 24
            }

            .ptype-grid-bg {
                background: none !important
            }

            .ptype-grid.-col-5 .ptype-grid-a.-descr-outer::after, .ptype-grid.-col-6 .ptype-grid-a.-descr-outer::after {
                display: none
            }

            .ptype-grid.-col-5 > li::before {
                display: none
            }

            .ptype-grid.-col-9 .ptype-grid-img {
                width: 70%
            }
        }

        @media only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .head-stores {
                margin-left: 46px
            }

            .head-stores > li.hot::after {
                right: 6px
            }

            .header-top {
                height: 88px
            }

            .header-top .header-dd-h {
                top: 88px
            }

            .select-vehicle > .nav > .link {
                width: 28.5%
            }

            .select-vehicle > .nav > .link:first-child {
                width: 30.5%
            }

            .ptype-grid-img {
                top: 60px;
                width: 170px
            }

            .ptype-grid-a {
                padding-top: 200px
            }

            .service-types-list > .service-types-item {
                width: 16.666666%
            }
        }

        @media only screen and (max-width: 420.98px), only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px) {
            .head-stores > li > .item > .item-text-additional {
                display: none
            }
        }

        @media only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .header-account {
                display: block;
                float: right
            }

            .select-vehicle > .nav.-garage > .link {
                background-position: 12px 48%;
                padding-left: 64px
            }

            .select-vehicle > .nav.-garage > .link.-i1-31 {
                background-position: 22px 48%;
                padding-left: 74px
            }

            .select-vehicle > .nav.-garage > .link.-i1-31.-active {
                padding-left: 74px
            }

            .select-vehicle > .nav.-garage > .link.-active {
                padding-left: 64px
            }

            .my-garage-line .mygarage-dd {
                right: auto
            }

            .ptype-grid {
                margin-top: 32px
            }

            .ptype-grid-a {
                padding-bottom: 24px
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1601px) {
            .head-dd {
                margin-left: 168px
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px) {
            .my-garage-line .mygarage-dd {
                margin-top: 8px
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px) {
            .ptype-grid.-col-9 > .li {
                margin-left: 10px;
                margin-right: 10px
            }
        }

        #sidenav {
            font: "Roboto", "Arial", "Helvetica", sans-serif !important;
            font-size: 160%;
            text-align: left;
        }

        .img-icon {
            margin-top: -10px;
            margin-bottom: -15px;
            margin-right: 15px;
            width: 15%;

        }

        @media only screen and (min-width: 767px) and (max-width: 1024px) {
            .head_logo_a {
                width: 150px !important;
            }
        }
        @media screen and (max-width: 767px){
            .dnone{
                display: none !important;
            }

        }

        @media only screen and (max-width:440px){
            .search-form-button
            {
                top: 9px !important;
                left: 85% !important;
            }
        }
        .search-form-field
        {
            border: none;
            border-bottom: 0.1px solid #7e7e7e;
            height: 39px;
        }
        .search-form-field:focus{
            outline: none !important;
            border:none;
            border-bottom: 0.1px solid #7e7e7e;
        }

        .search-form-button{
            position: absolute;
            top: 00px;
            left: 90%;
            width: 10%;
            height: 100%;
            background: transparent;
            border: none;
            z-index:1000;
        }
        .srch-btn{
            pointer-events: none;
            z-index: 1000;
        }

        .search-form-button:focus, .search-form-button:hover{
            background-color : rgba(246, 244, 244, 0.68);
            outline: none !important;
            border:none;
        }
        @media only screen and (max-width:736px){
            .search-form-field
            {
                top: 37px;
                left: 10px;
                width: 95% !important;
                height: 33px !important;
                margin-top: -36px !important;
            }
            .search-form-button
            {
                z-index: 1000;
                top: 0px !important;
                font-size: larger;
            }

            .prop-name
            {
                display: none;
            }
            .prop-number
            {
                margin-top: -4px;
            }

        }

    </style>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.1/css/mdb.min.css" rel="stylesheet"><link href="/css/mmenu.css" rel="stylesheet">
    <link href="/css/mburger.css" rel="stylesheet">
    <link href="/css/custom.css?v=02" rel="stylesheet">
    @yield('styles')
<style>

    @media only screen and (min-width : 320px) {
        /*#cart {
            font-size: 10px;
            margin-top: 20px;
            margin-right: 22px;

        }*/
        .badge {
            background-color: #6394F8;
            border-radius: 9px;
            color: white;
            display: inline-block;
            font-size: 8px;
            line-height: 1;
            padding: 1px 2px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }

    /* Extra Small Devices, Phones */
    @media only screen and (min-width : 480px) {
        /*#cart {
            font-size: 12px;
            margin-top: 20px;
            margin-right: 27px;

        }*/
        .badge {
            background-color: #6394F8;
            border-radius: 10px;
            color: white;
            display: inline-block;
            font-size: 8px;
            line-height: 1;
            padding: 2px 4px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        /*#cart {
            font-size: 16px;
            margin-top: 30px;
            margin-right: 30px;
        }*/
        .badge {
            background-color: #6394F8;
            border-radius: 10px;
            color: white;
            display: inline-block;
            font-size: 12px;
            line-height: 1;
            padding: 3px 7px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }

    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {
        /*#cart {
            font-size: 16px;
            margin-top: 40px;
            margin-right: 30px;
        }*/
        .badge {
            background-color: #6394F8;
            border-radius: 10px;
            color: white;
            display: inline-block;
            font-size: 12px;
            line-height: 1;
            padding: 3px 7px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }

    /* Large Devices, Wide Screens */
    @media only screen and (min-width : 1200px) {
        /*#cart {
            font-size: 16px;
            margin-top: 40px;
            margin-right: 10px;
        }*/
        .badge {
            background-color: #6394F8;
            border-radius: 10px;
            color: white;
            display: inline-block;
            font-size: 12px;
            line-height: 1;
            padding: 3px 7px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }


    .shopping-cart {
        margin: 20px 0;
        float: right;
        background: white;
        width: 320px;
        position: relative;
        border-radius: 3px;
        padding: 20px;
    }

    .

    .shopping-cart-total {
        float: right;
    }

    .shopping-cart:after {
        bottom: 100%;
        left: 89%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-bottom-color: white;
        border-width: 8px;
        margin-left: -8px;
    }

    .cart-icon {
        color: #515783;
        font-size: 24px;
        margin-right: 7px;
        float: left;
    }

    }

        .boxed-layout .site-wrapper {
          overflow: hidden
        }
        .boxed-layout .header {
          padding: 0 20px
        }
        .boxed-layout .breadcrumb {
          padding-left: 20px;
          padding-right: 20px
        }
        .wrapper,
        .mega-menu-content,
        .site-wrapper > .container,
        .grid-cols,
        .desktop-header-active .is-sticky .header .desktop-main-menu-wrapper,
        .desktop-header-active .is-sticky .sticky-fullwidth-bg,
        .boxed-layout .site-wrapper,
        .breadcrumb,
        .title-wrapper,
        .page-title > span,
        .desktop-header-active .header .top-bar,
        .desktop-header-active .header .mid-bar,
         .desktop-main-menu-wrapper {
          max-width: 1280px
        }
        .desktop-main-menu-wrapper .main-menu>.j-menu>.first-dropdown::before {
          transform: translateX(calc(0px - (100vw - 1000px) / 2))
        }
        html[dir='rtl'] .desktop-main-menu-wrapper .main-menu>.j-menu>.first-dropdown::before {
          transform: none;
          right: calc(0px - (100vw - 1280px) / 2)
        }
        .desktop-main-menu-wrapper .main-menu>.j-menu>.first-dropdown.mega-custom::before {
          transform: translateX(calc(0px - (200vw - 1280px) / 2))
        }
        html[dir='rtl'] .desktop-main-menu-wrapper .main-menu>.j-menu>.first-dropdown.mega-custom::before {
          transform: none;
          right: calc(0px - (200vw - 1280px) / 2)
        }

        .page-title {
          display: block
        }
        .dropdown.drop-menu>.j-dropdown {
          left: 0;
          right: auto;
          transform: translate3d(0, -10px, 0)
        }
        .dropdown.drop-menu.animating>.j-dropdown {
          left: 0;
          right: auto;
          transform: none
        }
        .dropdown.drop-menu>.j-dropdown::before {
          left: 10px;
          right: auto;
          transform: translateX(0)
        }
        .dropdown.dropdown .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .dropdown.dropdown .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .dropdown.dropdown .j-menu .dropdown>a::after {
          display: block
        }
        .dropdown.dropdown .j-menu>li>a {
          font-size: 14px;
          color: rgba(58, 71, 84, 1);
          font-weight: 400;
          background: rgba(255, 255, 255, 1);
          padding: 10px;
          padding-left: 15px
        }
        .dropdown.dropdown .j-menu .links-text {
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        .dropdown.dropdown .j-menu>li>a::before {
          margin-right: 7px;
          min-width: 20px;
          font-size: 18px
        }
        .desktop .dropdown.dropdown .j-menu > li:hover > a,
        .dropdown.dropdown .j-menu>li.active>a {
          background: rgba(240, 242, 245, 1)
        }
        .dropdown.dropdown .j-menu > li > a,
        .dropdown.dropdown.accordion-menu .menu-item > a + div,
        .dropdown.dropdown .accordion-menu .menu-item>a+div {
          border-style: none
        }
        .dropdown.dropdown .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .dropdown.dropdown:not(.mega-menu) .j-dropdown {
          min-width: 200px
        }
        .dropdown.dropdown:not(.mega-menu) .j-menu {
          box-shadow: 30px 40px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .dropdown.dropdown .j-dropdown::before {
          display: block;
          border-bottom-color: rgba(255, 255, 255, 1);
          margin-left: 7px;
          margin-top: -10px
        }
        .nav-tabs>li>a,
        .nav-tabs>li.active>a,
        .nav-tabs>li.active>a:hover,
        .nav-tabs>li.active>a:focus {
            font-family: "Roboto", "Arial", "Helvetica", sans-serif;
          font-weight: 600;
          font-size: 14px;
          color: rgba(139, 145, 152, 1);
          text-transform: uppercase
        }
        .desktop .nav-tabs>li:hover>a,
        .nav-tabs>li.active>a {
          color: rgba(51, 51, 51, 1)
        }
        .nav-tabs>li.active>a,
        .nav-tabs>li.active>a:hover,
        .nav-tabs>li.active>a:focus {
          color: rgba(51, 51, 51, 1)
        }
        .nav-tabs>li {
          border-width: 0;
          border-bottom-width: 1px;
          border-style: solid;
          border-color: rgba(0, 0, 0, 0);
          flex-grow: 0
        }
        .nav-tabs>li.active {
          border-color: rgba(233, 102, 49, 1)
        }
        .nav-tabs>li.active::after {
          display: none;
          border-top-width: 10px
        }
        .nav-tabs>li>a {
          justify-content: center;
          white-space: nowrap;
          padding: 0px;
          padding-bottom: 8px
        }
        .nav-tabs>li>a::before {
          font-size: 18px
        }
        .nav-tabs {
          display: flex;
          justify-content: flex-start;
          flex-wrap: nowrap;
          overflow-x: auto;
          overflow-y: hidden;
          -webkit-overflow-scrolling: touch;
          ;
          min-width: 50px
        }
        .nav-tabs>li:not(:last-child) {
          margin-right: 20px
        }
        .mobile .nav-tabs {
          overflow-x: scroll
        }
        .nav-tabs::-webkit-scrollbar {
          -webkit-appearance: none;
          height: 1px;
          height: 1px;
          width: 1px
        }
        .nav-tabs::-webkit-scrollbar-track {
          background-color: white;
          background-color: rgba(238, 238, 238, 1)
        }
        .nav-tabs::-webkit-scrollbar-thumb {
          background-color: #999;
          background-color: rgba(139, 145, 152, 1)
        }
        body a {
          color: rgba(13, 82, 214, 1);
          text-decoration: underline;
          display: inline-block
        }
        body a:hover {
          color: rgba(233, 102, 49, 1);
          text-decoration: none
        }

        .count-badge {
          font-family: 'Roboto';
          font-weight: 400;
          font-size: 11px;
          color: rgba(255, 255, 255, 1);
          background: rgba(221, 14, 28, 1);
          border-width: 2px;
          border-style: solid;
          border-color: rgba(248, 248, 248, 1);
          border-radius: 50px
        }
        .count-zero {
          display: none !important
        }

        .btn-cart::before,
        .fa-shopping-cart::before {
          content: '\e99b' !important;
          font-family: icomoon !important
        }
        .btn-wishlist::before {
          content: '\eb67' !important;
          font-family: icomoon !important
        }
        .fa-times-circle::before,
        .fa-times::before,
        .reset-filter::before,
        .notification-close::before,
        .popup-close::before,
        .hn-close::before {
          content: '\e5cd' !important;
          font-family: icomoon !important
        }
        .j-loader .journal-loading>i::before {
          margin-top: -2px
        }
        .notification-cart.notification {
          max-width: 400px;
          margin: 20px;
          margin-bottom: 0px;
          padding: 10px;
          background: rgba(255, 255, 255, 1);
          border-radius: 7px;
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2);
          color: rgba(105, 105, 115, 1)
        }
        .notification-cart .notification-close {
          display: block;
          width: 25px;
          height: 25px;
          margin-right: 5px;
          margin-top: 5px
        }
        .notification-cart .notification-close::before {
          content: '\e5cd' !important;
          font-family: icomoon !important;
          font-size: 20px
        }
        .notification-cart .notification-close.btn,
        .notification-cart .notification-close.btn:visited {
          font-size: 12px;
          color: rgba(105, 105, 115, 1);
          text-transform: none
        }
        .notification-cart .notification-close.btn:hover {
          color: rgba(233, 102, 49, 1) !important;
          background: none !important
        }
        .notification-cart .notification-close.btn:active,
        .notification-cart .notification-close.btn:hover:active,
        .notification-cart .notification-close.btn:focus:active {
          color: rgba(233, 102, 49, 1) !important;
          background: none !important;
          box-shadow: none
        }
        .notification-cart .notification-close.btn:focus {
          color: rgba(233, 102, 49, 1) !important;
          background: none;
          box-shadow: none
        }
        .notification-cart .notification-close.btn {
          background: none;
          border-style: none;
          padding: 3px;
          box-shadow: none
        }
        .desktop .notification-cart .notification-close.btn:hover {
          box-shadow: none
        }
        .notification-cart .notification-close.btn.btn.disabled::after {
          font-size: 20px
        }
        .notification-cart img {
          display: block;
          margin-right: 10px;
          margin-bottom: 10px
        }
        .notification-cart .notification-buttons {
          display: flex;
          padding: 10px;
          margin: -10px;
          margin-top: 5px
        }
        .notification-cart .notification-view-cart {
          display: inline-flex;
          flex-grow: 1
        }
        .notification-cart .notification-checkout {
          display: inline-flex;
          flex-grow: 1;
          margin-left: 10px
        }
        .notification-cart .notification-checkout::after {
          content: '\e5c8' !important;
          font-family: icomoon !important;
          margin-left: 5px
        }


        #cart .cart-label {
          display: inline-block;
          color: rgba(240, 242, 245, 1)
        }
        #cart>a>i::before {
          font-size: 28px;
          color: #363d47;
          left: 1px;
          top: -1px
        }
        #cart>a>i {
          /*background: var(--blue-color);/*rgba(13, 82, 214, 1);*/
          border-radius: 3px;
          width: 40px;
          height: 40px
        }
        .desktop #cart:hover>a>i {
          /*background: rgba(15, 58, 141, 1)*/
          transform: scale(1.1)
        }
        #cart-items.count-badge {
          font-family: 'Roboto';
          font-weight: 400;
          font-size: 11px;
          color: rgba(255, 255, 255, 1);
          background: rgba(221, 14, 28, 1);
          border-width: 2px;
          border-style: solid;
          border-color: rgba(248, 248, 248, 1);
          border-radius: 50px
        }
        #cart-items .count-zero {
          display: none !important
        }
        #cart-items {
          transform: translateX(5px);
          margin-top: -7px;
          display: inline-flex;
          z-index: 1
        }
        #cart-total {
          display: flex;
          padding-right: 15px;
          padding-left: 15px;
          font-size: 14px;
          color: rgba(105, 105, 115, 1);
          font-weight: 400;
          order: 0
        }
        .desktop #cart:hover #cart-total {
          color: rgba(15, 58, 141, 1)
        }
        #cart {
          display: block
        }
        #cart-content {
          min-width: 300px
        }
        div.cart-content ul {
          background: rgba(255, 255, 255, 1);
          border-radius: 3px;
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2)
        }
        div.cart-content .cart-products tbody>tr>td {
          border-style: solid !important;
          border-color: rgba(226, 226, 226, 1) !important;
          vertical-align: middle
        }
        #cart-content::before {
          display: block;
          border-bottom-color: rgba(255, 255, 255, 1);
          margin-left: -2px;
          margin-top: -6px
        }
        div.cart-content .cart-products {
          max-height: 275px;
          min-height: 100px
          overflow-y: auto
        }
        div.cart-content .cart-totals tbody td {
          padding-top: 5px !important;
          padding-bottom: 5px !important;
          background: rgba(238, 238, 238, 1);
          border-style: none !important
        }
        div.cart-content .cart-products tbody .td-remove button {
          color: rgba(221, 14, 28, 1)
        }
        div.cart-content .cart-products tbody .td-remove button:hover {
          color: rgba(80, 173, 85, 1)
        }
        div.cart-content .cart-totals td {
          font-weight: 700
        }
        div.cart-content .cart-totals .td-total-text {
          font-weight: 700
        }
        div.cart-content .cart-buttons {
          background: rgba(230, 230, 230, 1)
        }
        div.cart-content .btn-cart {
          display: inline-flex
        }
        div.cart-content .btn.btn-cart::before {
          content: none !important
        }
        div.cart-content .btn-checkout {
          display: inline-flex
        }
        div.cart-content .btn-checkout.btn {
          background: rgba(80, 173, 85, 1)
        }
        div.cart-content .btn-checkout.btn:hover {
          background: rgba(13, 82, 214, 1) !important
        }
        div.cart-content .btn-checkout.btn.btn.disabled::after {
          font-size: 20px
        }
        div.cart-content .cart-buttons .btn {
          width: auto
        }
        .desktop-header-active #cart {
          margin-left: 25px
        }
        .desktop-header-active header {
          box-shadow: 0 10px 30px rgba(0, 0, 0, 0.1)
        }
        .desktop-header-active .header-lg .mid-bar {
          height: 100px
        }
        .desktop-header-active .header-default {
          height: 100px
        }
        .desktop-header-active .header-default::before {
          content: '';
          height: calc(100px / 3)
        }
        .desktop-header-active .header-sm .mid-bar {
          height: 50px
        }
        .info-blocks-wrapper {
          justify-content: flex-end
        }

        .desktop-header-active .header .top-bar .language-currency {
          position: absolute;
          left: 50%;
          transform: translateX(-50%)
        }
        .desktop-header-active .header .top-bar {
          justify-content: space-between;
          height: 35px
        }
        .desktop-header-active .header .desktop-logo-wrapper {
          width: 205px
        }
        .desktop-header-active .header-classic .mid-bar .desktop-logo-wrapper {
          width: 205px;
          order: 0;
          margin: 0
        }
        .desktop-header-active .header #logo a {
          justify-content: flex-start
        }
        .desktop-header-active .header-classic .mid-bar .desktop-search-wrapper {
          order: 2;
          flex-grow: 1
        }
        .desktop-logo-wrapper {
          width: auto
        }
        .desktop-search-wrapper {
          width: auto;
          margin-right: 40px;
          margin-left: 25px
        }
        .classic-cart-wrapper {
          width: auto
        }
        .desktop-header-active header:not(.header-slim) .header-compact .mid-bar {
          justify-content: flex-start
        }
        .desktop-header-active header:not(.header-slim) .header-compact .mid-bar>div {
          max-width: none
        }
        .desktop-header-active header:not(.header-slim) .header-compact .header-cart-group {
          margin-left: auto
        }
        .desktop-header-active header:not(.header-slim) .header-compact .mid-bar .desktop-logo-wrapper {
          position: relative;
          left: 0;
          transform: translateX(0)
        }
        .desktop-main-menu-wrapper .first-dropdown::before {
          display: block !important;
          background-color: rgba(0, 0, 0, 0.5)
        }
        .desktop.safari {
          overflow-x: hidden
        }
        .main-menu > .j-menu .dropdown>a>.count-badge {
          margin-right: 5px
        }
        .main-menu > .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 0
        }
        .main-menu > .j-menu .dropdown>a::after {
          display: none
        }
        .main-menu>.j-menu>li>a {
            font-family: "Roboto", "Arial", "Helvetica", sans-serif;
          font-weight: 600;
          font-size: 15px;
          color: #363d47;
          text-transform: unset;
          padding: 10px;
          padding-right: 15px;
          padding-left: 15px
        }
        .desktop .main-menu>.j-menu>li:hover>a,
        .main-menu>.j-menu>li.active>a {
          color: rgba(51, 51, 51, 1);
          background: rgba(255, 255, 255, 1)
        }
        .main-menu > .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .main-menu>.j-menu>li>a::before {
          margin-right: 5px;
          font-size: 18px
        }
        .main-menu > .j-menu li .count-badge {
          font-family: 'Roboto';
          font-weight: 400;
          font-size: 11px;
          color: rgba(255, 255, 255, 1);
          background: rgba(221, 14, 28, 1);
          border-width: 2px;
          border-style: solid;
          border-color: rgba(248, 248, 248, 1);
          border-radius: 50px
        }
        .main-menu > .j-menu li .count-zero {
          display: none !important
        }
        .main-menu > .j-menu a .count-badge {
          display: inline-flex;
          position: relative;
          margin-top: -3px
        }
        .main-menu>ul>.drop-menu>.j-dropdown {
          left: 0;
          right: auto;
          width: max-content;
          transform: translate3d(0, -10px, 0)
        }
        .main-menu>ul>.drop-menu.animating>.j-dropdown {
          left: -10% !important;
          right: 10% !important;
          transform: none
        }
        .main-menu>ul>.drop-menu>.j-dropdown::before {
          left: 10px;
          right: auto;
          transform: translateX(0)
        }
        .main-menu > ul >.dropdown .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .main-menu > ul >.dropdown .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .main-menu > ul >.dropdown .j-menu .dropdown>a::after {
          display: block
        }
        .main-menu > ul >.dropdown .j-menu>li>a {
          font-size: 14px;
          color: rgba(58, 71, 84, 1);
          font-weight: 400;
          background: rgba(255, 255, 255, 1);
          padding: 10px;
          padding-left: 15px
        }
        .main-menu > ul >.dropdown .j-menu .links-text {
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        .main-menu > ul >.dropdown .j-menu>li>a::before {
          margin-right: 7px;
          min-width: 20px;
          font-size: 18px
        }
        .desktop .main-menu > ul >.dropdown .j-menu > li:hover > a,
        .main-menu > ul >.dropdown .j-menu>li.active>a {
          background: rgba(240, 242, 245, 1)
        }
        .main-menu > ul >.dropdown .j-menu > li > a,
        .main-menu > ul >.dropdown.accordion-menu .menu-item > a + div,
        .main-menu > ul >.dropdown .accordion-menu .menu-item>a+div {
          border-style: none
        }
        .main-menu > ul >.dropdown .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .main-menu > ul >.dropdown:not(.mega-menu) .j-dropdown {
          min-width: 200px
        }
        .main-menu > ul >.dropdown:not(.mega-menu) .j-menu {
          box-shadow: 30px 40px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .main-menu > ul >.dropdown .j-dropdown::before {
          display: block;
          border-bottom-color: rgba(255, 255, 255, 1);
          margin-left: 7px;
          margin-top: -10px
        }
        .mega-menu-content {
          background: rgba(255, 255, 255, 1)
        }
        .j-dropdown>.mega-menu-content {
          box-shadow: 30px 40px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .desktop-header-active .header-compact .desktop-main-menu-wrapper {
         /* height: 100%*/
        }
        .header-lg .desktop-main-menu-wrapper .main-menu .main-menu-item>a {
          /*height: 100%*/
        }
        .desktop-header-active .header-compact .desktop-logo-wrapper {
          order: 0
        }
        .desktop-main-menu-wrapper #main-menu {
          margin-left: auto;
          margin-right: auto
        }
        .desktop-main-menu-wrapper .desktop-cart-wrapper {
          margin-left: 0
        }
        .mid-bar #main-menu-2 {
          order: 5
        }
        .desktop-header-active .header .menu-stretch .main-menu-item > a .links-text {
          text-align: center
        }
        .desktop-main-menu-wrapper::before {
          background: #FFF;/*rgba(13, 82, 214, 1);*/
          height: 43px
        }
        .desktop-main-menu-wrapper {
          height: 43px;
          top: -43px
        }
        .desktop-main-menu-wrapper .main-menu-item>a {
          padding: 0 15px
        }
        .header-compact .desktop-main-menu-wrapper #main-menu {
          margin-left: initial;
          margin-right: auto
        }
        .desktop-header-active .menu-trigger a {
          padding: 0 15px
        }
        .desktop-header-active .menu-trigger a::before {
          content: '\f0c9' !important;
          font-family: icomoon !important;
          margin-right: 7px
        }
        .desktop-header-active .mobile-wrapper-header>span {
            font-family: "Roboto", "Arial", "Helvetica", sans-serif;
          font-weight: 700;
          font-size: 16px;
          color: rgba(105, 105, 115, 1);
          text-transform: uppercase
        }
        .desktop-header-active .mobile-wrapper-header {
          background: rgba(240, 242, 245, 1);
          height: 45px
        }
        .desktop-header-active .mobile-wrapper-header>a::before {
          content: '\e981' !important;
          font-family: icomoon !important;
          color: rgba(105, 105, 115, 1);
          margin-right: 3px
        }
        .desktop-header-active .mobile-wrapper-header>a {
          width: 45px
        }
        .desktop-header-active .mobile-cart-content-wrapper {
          padding-bottom: 45px
        }
        .desktop-header-active .mobile-filter-wrapper {
          padding-bottom: 45px
        }
        .desktop-header-active .mobile-main-menu-wrapper {
          padding-bottom: 45px
        }
        .desktop-header-active .mobile-filter-container-open .journal-loading-overlay {
          top: 45px
        }
        .desktop-header-active.mobile-header-active .mobile-container {
          width: 30%
        }
        .desktop-header-active.desktop-header-active .mobile-main-menu-container {
          width: 300px
        }
        .desktop-header-active .mobile-main-menu-container {
          background: rgba(255, 255, 255, 1);
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .desktop-header-active .mobile-main-menu-wrapper .main-menu {
          padding: 10px
        }
        .desktop-header-active .mobile-cart-content-container {
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .desktop-header-active.mobile-overlay .site-wrapper::before {
          background: rgba(0, 0, 0, 0.5)
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .dropdown>a>.count-badge {
          margin-right: 5px
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 0
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .dropdown>a::after {
          display: none
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu>li>a {
          font-size: 15px;
          color: rgba(51, 51, 51, 1);
          font-weight: 400;
          padding: 8px;
          padding-right: 0px;
          padding-left: 0px
        }
        .desktop .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu > li:hover > a,
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu>li.active>a {
          color: rgba(13, 82, 214, 1)
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu>li>a::before {
          color: rgba(139, 145, 152, 1);
          margin-right: 10px;
          min-width: 20px;
          font-size: 18px
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .desktop-header-active .mobile-main-menu-container .main-menu .open-menu i::before {
          content: '\eba1' !important;
          font-family: icomoon !important;
          font-size: 16px;
          left: 5px
        }
        .desktop-header-active .mobile-main-menu-container .main-menu .open-menu[aria-expanded='true'] i::before {
          content: '\eb86' !important;
          font-family: icomoon !important;
          font-size: 16px;
          color: rgba(80, 173, 85, 1);
          left: 5px
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .j-menu .dropdown>a>.count-badge {
          margin-right: 5px
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 0
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .j-menu .dropdown>a::after {
          display: none
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .j-menu>li>a {
          font-size: 14px;
          color: rgba(105, 105, 115, 1);
          font-weight: 400;
          text-transform: none;
          padding: 5px
        }
        .desktop .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .j-menu > li:hover > a,
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .j-menu>li.active>a {
          color: rgba(13, 82, 214, 1)
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .desktop-header-active .mobile-main-menu-container .main-menu.accordion-menu .j-menu .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .desktop-header-active .mobile-main-menu-container .main-menu .j-menu > li > div .j-menu>li>a {
          padding-left: 18px !important
        }
        .desktop-header-active .mobile-main-menu-container .main-menu .j-menu > li > div .j-menu>li>div>.j-menu>li>a {
          padding-left: 30px !important
        }
        .desktop-header-active .mobile-main-menu-container .main-menu .j-menu > li > div .j-menu>li>div>.j-menu>li>div>.j-menu>li>a {
          padding-left: 40px !important
        }
        .desktop-header-active .mobile-main-menu-container .main-menu .j-menu > li > div .j-menu>li>div>.j-menu>li>div>.j-menu>li>div>.j-menu>li>a {
          padding-left: 50px !important
        }
        .header-search {
          border-width: 1px;
          border-style: solid;
          border-color: rgba(223, 231, 246, 1);
          border-radius: 3px
        }
        .desktop .header-search.focused,
        .header-search.focused:hover {
          border-color: rgba(13, 82, 214, 1)
        }
        .header-search>.search-button::before {
          content: '\f002' !important;
          font-family: icomoon !important;
          font-size: 20px;
          color: rgba(223, 231, 246, 1);
          top: -1px
        }
        .desktop .header-search>.search-button:hover::before {
          color: rgba(255, 255, 255, 1) !important
        }
        .header-search .search-button {
          background: var(--blue-color);/*rgba(13, 82, 214, 1);*/
          min-width: 40px
        }
        .desktop .header-search .search-button:hover {
          background: rgba(15, 58, 141, 1)
        }
        #search input::-webkit-input-placeholder {
          color: rgba(105, 105, 115, 1)
        }
        #search input::-moz-input-placeholder {
          color: rgba(105, 105, 115, 1)
        }
        #search input:-ms-input-placeholder {
          color: rgba(105, 105, 115, 1)
        }
        #search {
          display: block
        }
        .desktop-header-active .header-default .desktop-search-wrapper {
          order: 1;
          flex-grow: 0
        }
        .desktop-header-active .header-default .top-menu-group {
          order: -1;
          flex-grow: 1
        }
        .desktop-header-active .header-search .search-button {
          order: 5;
          border-top-left-radius: 0;
          border-bottom-left-radius: 0;
          border-top-right-radius: inherit;
          border-bottom-right-radius: inherit
        }
        .desktop-header-active .header-search>input:first-child {
          border-top-left-radius: inherit;
          border-bottom-left-radius: inherit;
          border-top-right-radius: 0;
          border-bottom-right-radius: 0
        }
        .desktop-header-active .header-search>input {
          border-top-left-radius: 0;
          border-bottom-left-radius: 0;
          border-top-right-radius: 0;
          border-bottom-right-radius: 0
        }
        .desktop-header-active .header-search>span:first-child {
          border-top-left-radius: inherit;
          border-bottom-left-radius: inherit;
          border-top-right-radius: 0;
          border-bottom-right-radius: 0
        }
        .desktop-header-active .header-search>span {
          border-top-left-radius: 0;
          border-bottom-left-radius: 0;
          border-top-right-radius: 0;
          border-bottom-right-radius: 0
        }

        .desktop-header-active .mini-search .header-search input {
          min-width: 150px
        }
        .desktop-header-active .header .full-search #search {
          height: 40px
        }
        .mini-search .search-trigger::before {
          content: '\f002' !important;
          font-family: icomoon !important
        }
        .mini-search #search>.dropdown-menu::before {
          display: block;
          margin-top: -10px
        }
        .secondary-menu .top-menu .j-menu>li>a {
          flex-direction: column;
          font-size: 14px;
          padding: 5px
        }
        .secondary-menu .top-menu .j-menu .dropdown>a>.count-badge {
          margin-right: 5px
        }
        .secondary-menu .top-menu .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 0
        }
        .secondary-menu .top-menu .j-menu .dropdown>a::after {
          display: none
        }
        .secondary-menu .top-menu .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          letter-spacing: 1px
        }
        .secondary-menu .top-menu .j-menu>li>a::before {
          /*color: #fff;*/
          margin: 0px;
          margin-bottom: 2px;
          font-size: 26px
        }
        .desktop .secondary-menu .top-menu .j-menu > li:hover > a::before,
        .secondary-menu .top-menu .j-menu>li.active>a::before {
          color: rgba(13, 82, 214, 1)
        }
        .secondary-menu .top-menu .j-menu>li+li {
          margin-left: 20px
        }
        .secondary-menu .top-menu .j-menu a .count-badge {
          display: inline-flex;
          position: absolute;
          margin: 0;
          transform: translateX(15px);
          margin-top: -5px
        }
        .secondary-menu .menu-item.drop-menu>.j-dropdown {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, -10px, 0)
        }
        .secondary-menu .menu-item.drop-menu.animating>.j-dropdown {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, 0, 0)
        }
        .secondary-menu .menu-item.drop-menu>.j-dropdown::before {
          left: 50%;
          right: auto;
          transform: translateX(-50%)
        }
        .secondary-menu .menu-item.dropdown .j-menu>li>a {
          flex-direction: row;
          font-size: 13px;
          color: rgba(255, 255, 255, 1);
          background: rgba(15, 58, 141, 1);
          padding: 10px;
          padding-right: 15px;
          padding-left: 15px
        }
        .secondary-menu .menu-item.dropdown .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .secondary-menu .menu-item.dropdown .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .secondary-menu .menu-item.dropdown .j-menu .dropdown>a::after {
          display: block
        }
        .secondary-menu .menu-item.dropdown .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .secondary-menu .menu-item.dropdown .j-menu>li>a::before {
          color: rgba(255, 255, 255, 1);
          margin: 0px;
          margin-right: 5px;
          font-size: 15px
        }
        .desktop .secondary-menu .menu-item.dropdown .j-menu > li:hover > a::before,
        .secondary-menu .menu-item.dropdown .j-menu>li.active>a::before {
          color: rgba(255, 255, 255, 1)
        }
        .desktop .secondary-menu .menu-item.dropdown .j-menu > li:hover > a,
        .secondary-menu .menu-item.dropdown .j-menu>li.active>a {
          background: rgba(13, 82, 214, 1)
        }
        .secondary-menu .menu-item.dropdown .j-menu>li+li {
          margin-left: 0px
        }
        .secondary-menu .menu-item.dropdown .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .secondary-menu .menu-item.dropdown:not(.mega-menu) .j-dropdown {
          min-width: 100px
        }
        .secondary-menu .menu-item.dropdown:not(.mega-menu) .j-menu {
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2);
          border-radius: 3px
        }
        .secondary-menu .menu-item.dropdown .j-dropdown::before {
          display: block;
          border-bottom-color: rgba(15, 58, 141, 1);
          margin-left: -2px;
          margin-top: -10px
        }
        .mid-bar .secondary-menu {
          justify-content: flex-end
        }
        .third-menu .top-menu .j-menu>li>a {
          flex-direction: row;
          font-size: 16px;
          color: #fff;
          font-weight: 400;
          padding: 5px
        }
        .third-menu .top-menu .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .third-menu .top-menu .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .third-menu .top-menu .j-menu .dropdown>a::after {
          display: block
        }
        .desktop .third-menu .top-menu .j-menu > li:hover > a,
        .third-menu .top-menu .j-menu>li.active>a {
          color: rgba(13, 82, 214, 1)
        }
        .third-menu .top-menu .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          letter-spacing: 1px
        }
        .third-menu .top-menu .j-menu>li>a::before {
          margin-right: 5px;
          font-size: 14px
        }
        .third-menu .top-menu .j-menu>li+li {
          margin-left: 5px
        }
        .third-menu .top-menu .j-menu a .count-badge {
          display: inline-flex;
          position: relative
        }
        .third-menu .menu-item.drop-menu>.j-dropdown {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, -10px, 0)
        }
        .third-menu .menu-item.drop-menu.animating>.j-dropdown {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, 0, 0)
        }
        .third-menu .menu-item.drop-menu>.j-dropdown::before {
          left: 50%;
          right: auto;
          transform: translateX(-50%)
        }
        .third-menu .menu-item.dropdown .j-menu>li>a {
          flex-direction: row;
          font-size: 13px;
          color: rgba(255, 255, 255, 1);
          background: rgba(15, 58, 141, 1);
          padding: 10px;
          padding-right: 15px;
          padding-left: 15px
        }
        .third-menu .menu-item.dropdown .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .third-menu .menu-item.dropdown .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .third-menu .menu-item.dropdown .j-menu .dropdown>a::after {
          display: block
        }
        .third-menu .menu-item.dropdown .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .third-menu .menu-item.dropdown .j-menu>li>a::before {
          color: rgba(255, 255, 255, 1);
          margin: 0px;
          margin-right: 5px;
          font-size: 15px
        }
        .desktop .third-menu .menu-item.dropdown .j-menu > li:hover > a::before,
        .third-menu .menu-item.dropdown .j-menu>li.active>a::before {
          color: rgba(255, 255, 255, 1)
        }
        .desktop .third-menu .menu-item.dropdown .j-menu > li:hover > a,
        .third-menu .menu-item.dropdown .j-menu>li.active>a {
          background: rgba(13, 82, 214, 1)
        }
        .third-menu .menu-item.dropdown .j-menu>li+li {
          margin-left: 0px
        }
        .third-menu .menu-item.dropdown .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .third-menu .menu-item.dropdown:not(.mega-menu) .j-dropdown {
          min-width: 100px
        }
        .third-menu .menu-item.dropdown:not(.mega-menu) .j-menu {
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2);
          border-radius: 3px
        }
        .third-menu .menu-item.dropdown .j-dropdown::before {
          display: block;
          border-bottom-color: rgba(15, 58, 141, 1);
          margin-left: -2px;
          margin-top: -10px
        }
        .desktop-header-active .is-sticky .header .desktop-main-menu-wrapper::before {
          width: 100vw;
          margin-left: -50vw;
          left: 50%;
          ;
          box-shadow: 30px 40px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .sticky-header
        {
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          transition: 0.5s;
        }
        .desktop-header-active header::before {
          content: '';
          height: 35px;
        }
        header::before {
          background: var(--blue-color);
          box-shadow: 0 5px 50px -10px rgba(0, 0, 0, 0.05)
        }
        .top-menu .j-menu>li>a {
          flex-direction: row;
          font-size: 13px;
          color: #fff;
          font-weight: 400;
          padding: 5px
        }
        .top-menu .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .top-menu .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .top-menu .j-menu .dropdown>a::after {
          display: block
        }
        .desktop .top-menu .j-menu > li:hover > a,
        .top-menu .j-menu>li.active>a {
          color: black
        }
        .top-menu .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          font-weight: normal;
          letter-spacing: 1px
        }
        .top-menu .j-menu>li>a::before {
          margin-right: 5px;
          font-size: 14px
        }
        .top-menu .j-menu>li+li {
          margin-left: 5px
        }
        .top-menu .j-menu a .count-badge {
          display: inline-flex;
          position: relative
        }
        .top-menu .dropdown.drop-menu>.j-dropdown {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, -10px, 0)
        }
        .top-menu .dropdown.drop-menu.animating>.j-dropdown {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, 0, 0)
        }
        .top-menu .dropdown.drop-menu>.j-dropdown::before {
          left: 50%;
          right: auto;
          transform: translateX(-50%)
        }
        .top-menu .dropdown.dropdown .j-menu>li>a {
          flex-direction: row;
          font-size: 13px;
          color: rgba(255, 255, 255, 1);
          background: rgba(15, 58, 141, 1);
          padding: 10px;
          padding-right: 15px;
          padding-left: 15px
        }
        .top-menu .dropdown.dropdown .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .top-menu .dropdown.dropdown .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .top-menu .dropdown.dropdown .j-menu .dropdown>a::after {
          display: block
        }
        .top-menu .dropdown.dropdown .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .top-menu .dropdown.dropdown .j-menu>li>a::before {
          color: rgba(255, 255, 255, 1);
          margin: 0px;
          margin-right: 5px;
          font-size: 15px
        }
        .desktop .top-menu .dropdown.dropdown .j-menu > li:hover > a::before,
        .top-menu .dropdown.dropdown .j-menu>li.active>a::before {
          color: rgba(255, 255, 255, 1)
        }
        .desktop .top-menu .dropdown.dropdown .j-menu > li:hover > a,
        .top-menu .dropdown.dropdown .j-menu>li.active>a {
          background: rgba(13, 82, 214, 1)
        }
        .top-menu .dropdown.dropdown .j-menu>li+li {
          margin-left: 0px
        }
        .top-menu .dropdown.dropdown .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .top-menu .dropdown.dropdown:not(.mega-menu) .j-dropdown {
          min-width: 100px
        }
        .top-menu .dropdown.dropdown:not(.mega-menu) .j-menu {
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2);
          border-radius: 3px
        }
        .top-menu .dropdown.dropdown .j-dropdown::before {
          display: block;
          border-bottom-color: rgba(15, 58, 141, 1);
          margin-left: -2px;
          margin-top: -10px
        }
        @media (max-width: 1300px) {
          #cart-items {
            transform: translateX(0px)
          }
          .desktop-header-active #cart {
            margin-right: 20px
          }
          .desktop-header-active .header #logo a {
            padding-left: 20px
          }
          .header .top-bar {
            padding-right: 10px;
            padding-left: 10px
          }
        }
        @media (max-width: 1024px) {
          .j-dropdown>.mega-menu-content {
            max-height: 500px !important;
            overflow-y: auto
          }
          .desktop-header-active.mobile-header-active .mobile-container {
            width: 40%
          }
          .third-menu .top-menu .j-menu>li>a {
            font-size: 12px;
            color: rgba(255, 255, 255, 1)
          }
          .top-menu .j-menu>li>a {
            font-size: 12px;
            color: rgba(255, 255, 255, 1)
          }
        }
        @media (max-width: 760px) {
          .desktop-header-active.mobile-header-active .mobile-container {
            width: 85%
          }
        }
        .mobile-header-active #cart>a>i::before {
          font-size: 28px;
          color: rgba(51, 51, 51, 1)
        }
        .mobile-cart-wrapper #cart>a>i {
          background: rgba(0, 0, 0, 0)
        }
        .mobile-header-active #cart>a>i {
          width: 65px;
          height: 60px
        }
        .mobile-header-active .mobile-wrapper-header>span {
            font-family: "Roboto", "Arial", "Helvetica", sans-serif;
          font-weight: 700;
          font-size: 16px;
          color: rgba(105, 105, 115, 1);
          text-transform: uppercase;
            font-family: "Roboto", "Arial", "Helvetica", sans-serif;
          font-weight: 700;
          font-size: 16px;
          color: rgba(105, 105, 115, 1);
          text-transform: uppercase
        }
        .mobile-header-active .mobile-wrapper-header {
          background: rgba(240, 242, 245, 1);
          height: 45px;
          background: rgba(240, 242, 245, 1);
          height: 45px
        }
        .mobile-header-active .mobile-wrapper-header>a::before {
          content: '\e981' !important;
          font-family: icomoon !important;
          color: rgba(105, 105, 115, 1);
          margin-right: 3px;
          content: '\e981' !important;
          font-family: icomoon !important;
          color: rgba(105, 105, 115, 1);
          margin-right: 3px
        }
        .mobile-header-active .mobile-wrapper-header>a {
          width: 45px;
          width: 45px
        }
        .mobile-header-active .mobile-cart-content-wrapper {
          padding-bottom: 45px;
          padding-bottom: 45px
        }
        .mobile-header-active .mobile-filter-wrapper {
          padding-bottom: 45px;
          padding-bottom: 45px
        }
        .mobile-header-active .mobile-main-menu-wrapper {
          padding-bottom: 45px;
          padding-bottom: 45px
        }
        .mobile-header-active .mobile-filter-container-open .journal-loading-overlay {
          top: 45px;
          top: 45px
        }
        .mobile-header-active.mobile-header-active .mobile-container {
          width: 30%;
          width: 30%
        }
        .mobile-header-active.desktop-header-active .mobile-main-menu-container {
          width: 300px;
          width: 300px
        }
        .mobile-header-active .mobile-main-menu-container {
          background: rgba(255, 255, 255, 1);
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2);
          background: rgba(255, 255, 255, 1);
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .mobile-header-active .mobile-main-menu-wrapper .main-menu {
          padding: 10px;
          padding: 10px
        }
        .mobile-header-active .mobile-cart-content-container {
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2);
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .mobile-header-active.mobile-overlay .site-wrapper::before {
          background: rgba(0, 0, 0, 0.5);
          background: rgba(0, 0, 0, 0.5)
        }
        .mobile-header-active #cart-items.count-badge {
          transform: translateX(-10px);
          margin-top: 10px;
          display: inline-flex
        }
        .mobile-header-active .mobile-bar {
          background: rgba(255, 255, 255, 1)
        }
        .mobile-header-active .mobile-1 .mobile-bar {
          height: 70px
        }
        .mobile-header-active .mobile-2 .mobile-bar {
          height: 70px
        }
        .mobile-header-active .mobile-3 .mobile-logo-wrapper {
          height: 70px
        }
        .mobile-header-active .mobile-bar-sticky {
          box-shadow: 0 10px 30px rgba(0, 0, 0, 0.1)
        }
        .mobile-header-active #logo a {
          padding: 15px 0px
        }
        .mobile-header-active .menu-trigger::before {
          content: '\e8d2' !important;
          font-family: icomoon !important;
          font-size: 33px
        }
        .mobile-header-active .menu-trigger {
          width: 50px;
          height: 60px
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .dropdown>a>.count-badge {
          margin-right: 5px
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 0
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .dropdown>a::after {
          display: none
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu>li>a {
            font-family: "Roboto", "Arial", "Helvetica", sans-serif;
          font-weight: 600;
          font-size: 16px;
          color: rgba(51, 51, 51, 1);
          text-transform: none;
          padding: 10px;
          padding-left: 0px
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu>li>a::before {
          color: rgba(139, 145, 152, 1);
          margin-right: 10px;
          min-width: 24px;
          font-size: 24px
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu a .count-badge {
          display: inline-flex;
          position: relative
        }
        .mobile-main-menu-wrapper .main-menu .open-menu i::before {
          content: '\eba1' !important;
          font-family: icomoon !important;
          font-size: 20px;
          left: 5px
        }
        .mobile-main-menu-wrapper .main-menu .open-menu[aria-expanded='true'] i::before {
          content: '\eb86' !important;
          font-family: icomoon !important;
          font-size: 20px;
          color: rgba(80, 173, 85, 1);
          left: 5px
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .j-menu>li>a {
          flex-direction: row;
            font-family: "Roboto", "Arial", "Helvetica", sans-serif;
          font-weight: 400;
          font-size: 15px;
          color: rgba(105, 105, 115, 1);
          padding: 8px
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .j-menu .dropdown>a>.count-badge {
          margin-right: 5px
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 0
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .j-menu .dropdown>a::after {
          display: none
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .j-menu .links-text {
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .j-menu>li>a::before {
          margin-right: 0px;
          font-size: 13px
        }
        .mobile-main-menu-wrapper .main-menu.accordion-menu .j-menu .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .mobile-main-menu-wrapper .main-menu .j-menu > li > div .j-menu>li>a {
          padding-left: 33px !important
        }
        .mobile-main-menu-wrapper .main-menu .j-menu > li > div .j-menu>li>div>.j-menu>li>a {
          padding-left: 40px !important
        }
        .mobile-main-menu-wrapper .main-menu .j-menu > li > div .j-menu>li>div>.j-menu>li>div>.j-menu>li>a {
          padding-left: 50px !important
        }
        .mobile-main-menu-wrapper .main-menu .j-menu > li > div .j-menu>li>div>.j-menu>li>div>.j-menu>li>div>.j-menu>li>a {
          padding-left: 60px !important
        }
        .mobile-custom-menu-1::before {
          content: '\eb67' !important;
          font-family: icomoon !important;
          font-size: 24px;
          color: rgba(51, 51, 51, 1)
        }
        .mobile-custom-menu {
          width: 45px
        }
        .mobile-custom-menu-2 {
          width: 35px
        }
        .mobile-custom-menu-2::before {
          content: '\eab6' !important;
          font-family: icomoon !important;
          font-size: 22px;
          color: rgba(51, 51, 51, 1)
        }
        .mobile-custom-menu-1 .count-badge {
          transform: translateX(3px);
          display: inline-flex
        }
        .mobile-custom-menu-2 .count-badge {
          transform: translateX(5px);
          display: inline-flex
        }
        .mobile-header-active .mini-search .search-trigger::before {
          content: '\f002' !important;
          font-family: icomoon !important;
          font-size: 24px
        }
        .mobile-header-active .header-search {
          border-width: 1px;
          border-style: solid;
          border-color: rgba(223, 231, 246, 1);
          border-radius: 3px
        }
        .desktop .mobile-header-active .header-search.focused,
        .mobile-header-active .header-search.focused:hover {
          border-color: rgba(13, 82, 214, 1)
        }
        .mobile-header-active .header-search>.search-button::before {
          content: '\f002' !important;
          font-family: icomoon !important;
          font-size: 20px;
          color: rgba(223, 231, 246, 1);
          top: -1px
        }
        .desktop .mobile-header-active .header-search>.search-button:hover::before {
          color: rgba(255, 255, 255, 1) !important
        }
        .mobile-header-active .header-search .search-button {
          background: rgba(13, 82, 214, 1);
          min-width: 40px
        }
        .desktop .mobile-header-active .header-search .search-button:hover {
          background: rgba(15, 58, 141, 1)
        }
        .mobile-header-active #search input::-webkit-input-placeholder {
          color: rgba(105, 105, 115, 1)
        }
        .mobile-header-active #search input::-moz-input-placeholder {
          color: rgba(105, 105, 115, 1)
        }
        .mobile-header-active #search input:-ms-input-placeholder {
          color: rgba(105, 105, 115, 1)
        }
        .mobile-header-active .tt-menu>div {
          box-shadow: 0 10px 30px -5px rgba(0, 0, 0, 0.2);
          border-radius: 3px
        }
        .mobile-header-active .tt-menu:not(.tt-empty)::before {
          display: block;
          margin-top: -10px
        }
        .mobile-header-active .search-result .product-name {
          font-weight: 700
        }
        .mobile-header-active .search-result>a>span {
          justify-content: flex-start
        }
        .mobile-header-active .search-result.view-more a::after {
          content: '\e5c8' !important;
          font-family: icomoon !important
        }
        .mobile-header-active .mini-search #search .search-trigger {
          width: 50px
        }
        .mobile-header-active .mobile-1 #search .header-search {
          padding: 8px
        }
        .mobile-header-active .mobile-search-group {
          padding: 8px
        }
        .mobile-header-active .mobile-3 .mobile-search-wrapper {
          padding: 0 8px
        }
        .mobile-header-active #search .header-search {
          height: 60px
        }
        .mobile-header-active .mobile-search-group,
        .mobile-header-active .mobile-1 .header-search {
          background: rgba(240, 242, 245, 1);
          box-shadow: 0 5px 50px -10px rgba(0, 0, 0, 0.05)
        }
        .mobile-header-active .mobile-1 #search>.dropdown-menu::before {
          display: none;
          margin-left: -60px;
          margin-top: -10px
        }
        .mobile-header-active .mobile-header .mobile-top-bar {
          display: flex;
          height: 40px;
          padding-right: 7px;
          padding-left: 7px;
          background: var(--blue-color);/*rgba(13, 82, 214, 1);*/
          justify-content: space-between
        }
        .mobile-header-active .top-menu .j-menu>li>a {
          flex-direction: row;
          font-size: 13px;
          color: #fff;
          font-weight: 400;
          padding: 5px
        }
        .mobile-header-active .top-menu .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .mobile-header-active .top-menu .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .mobile-header-active .top-menu .j-menu .dropdown>a::after {
          display: block
        }
        .desktop .mobile-header-active .top-menu .j-menu > li:hover > a,
        .mobile-header-active .top-menu .j-menu>li.active>a {
          color: rgba(13, 82, 214, 1)
        }
        .mobile-header-active .top-menu .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .mobile-header-active .top-menu .j-menu>li>a::before {
          margin-right: 5px;
          font-size: 14px
        }
        .mobile-header-active .top-menu .j-menu>li+li {
          margin-left: 5px
        }
        .mobile-header-active .top-menu .j-menu a .count-badge {
          display: inline-flex;
          position: relative
        }
        .mobile-header-active .top-menu.drop-menu>.j-dropdown {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, -10px, 0)
        }
        .mobile-header-active .top-menu.drop-menu.animating>.j-dropdown {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, 0, 0)
        }
        .mobile-header-active .top-menu.drop-menu>.j-dropdown::before {
          left: 50%;
          right: auto;
          transform: translateX(-50%)
        }
        .mobile-header-active .top-menu.dropdown .j-menu>li>a {
          flex-direction: row;
          font-size: 13px;
          color: rgba(255, 255, 255, 1);
          background: rgba(15, 58, 141, 1);
          padding: 10px;
          padding-right: 15px;
          padding-left: 15px
        }
        .mobile-header-active .top-menu.dropdown .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .mobile-header-active .top-menu.dropdown .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .mobile-header-active .top-menu.dropdown .j-menu .dropdown>a::after {
          display: block
        }
        .mobile-header-active .top-menu.dropdown .j-menu .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .mobile-header-active .top-menu.dropdown .j-menu>li>a::before {
          color: rgba(255, 255, 255, 1);
          margin: 0px;
          margin-right: 5px;
          font-size: 15px
        }
        .desktop .mobile-header-active .top-menu.dropdown .j-menu > li:hover > a::before,
        .mobile-header-active .top-menu.dropdown .j-menu>li.active>a::before {
          color: rgba(255, 255, 255, 1)
        }
        .desktop .mobile-header-active .top-menu.dropdown .j-menu > li:hover > a,
        .mobile-header-active .top-menu.dropdown .j-menu>li.active>a {
          background: rgba(13, 82, 214, 1)
        }
        .mobile-header-active .top-menu.dropdown .j-menu>li+li {
          margin-left: 0px
        }
        .mobile-header-active .top-menu.dropdown .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .mobile-header-active .top-menu.dropdown:not(.mega-menu) .j-dropdown {
          min-width: 100px
        }
        .mobile-header-active .top-menu.dropdown:not(.mega-menu) .j-menu {
          box-shadow: 0 15px 90px -10px rgba(0, 0, 0, 0.2);
          border-radius: 3px
        }
        .mobile-header-active .top-menu.dropdown .j-dropdown::before {
          display: block;
          border-bottom-color: rgba(15, 58, 141, 1);
          margin-left: -2px;
          margin-top: -10px
        }
        @media (max-width: 1024px) {
          .mobile-header-active.mobile-header-active .mobile-container {
            width: 40%;
            width: 40%
          }
          .mobile-header-active .language .dropdown-toggle > span,
          .mobile-header-active .language .dropdown::after {
            font-size: 11px;
            color: rgba(255, 255, 255, 1)
          }
          .mobile-header-active .currency .dropdown-toggle > span,
          .mobile-header-active .currency .dropdown::after {
            font-size: 11px;
            color: rgba(255, 255, 255, 1)
          }
          .mobile-header-active .top-menu .j-menu>li>a {
            font-size: 12px;
            color: rgba(255, 255, 255, 1)
          }
        }
        @media (max-width: 760px) {
          .mobile-header-active.mobile-header-active .mobile-container {
            width: 85%;
            width: 85%
          }
        }
        /*No top bar not over*/

        /*No top bar over*/

        /*Top bar not over*/

        /*Top bar over*/

        /*Title before breadcrumbs*/

        /*Shipping payment visibility*/

        /*Site overlay offset*/

        @media only screen and (max-width: 1280px) {
          .desktop-main-menu-wrapper .main-menu>.j-menu>.first-dropdown::before {
            transform: none !important;
          }
        }
        div.links-menu-206 .title.module-title {
          font-size: 15px;
          border-width: 0px;
          padding: 0px;
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        div.links-menu-206 .title.module-title::after {
          display: none
        }
        div.links-menu-206 .title.module-title.page-title>span::after {
          display: none
        }
        div.links-menu-206 .module-body {
          display: block;
          justify-content: flex-start;
          ;
          -webkit-overflow-scrolling: touch;
          ;
          column-count: initial;
          column-gap: 20px;
          column-rule-style: none
        }
        div.links-menu-206 .menu-item {
          border-width: 1px 0 0 0;
          flex-grow: 0;
          justify-content: flex-start;
          width: auto
        }
        div.links-menu-206 .menu-item a .links-text {
          white-space: normal;
          color: rgba(105, 105, 115, 1)
        }
        div.links-menu-206 .menu-item a {
          justify-content: flex-start;
          width: auto;
          padding-bottom: 4px
        }
        div.links-menu-206 .menu-item a:hover .links-text {
          color: rgba(233, 102, 49, 1);
          text-decoration: underline
        }
        div.links-menu-206 .count-badge {
          display: none
        }
        .phone footer div.links-menu-206 .module-title::before {
          display: block
        }
        .phone footer div.links-menu-206 .module-title.closed+.module-body>li {
          display: none
        }
        .phone footer div.links-menu-206 .module-title.closed {
          margin: 0
        }
        div.links-menu-208 .title.module-title {
          font-size: 15px;
          border-width: 0px;
          padding: 0px;
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        div.links-menu-208 .title.module-title::after {
          display: none
        }
        div.links-menu-208 .title.module-title.page-title>span::after {
          display: none
        }
        div.links-menu-208 .module-body {
          display: block;
          justify-content: flex-start;
          ;
          -webkit-overflow-scrolling: touch;
          ;
          column-count: initial;
          column-gap: 20px;
          column-rule-style: none
        }
        div.links-menu-208 .menu-item {
          border-width: 1px 0 0 0;
          flex-grow: 0;
          justify-content: flex-start;
          width: auto
        }
        div.links-menu-208 .menu-item a .links-text {
          white-space: normal;
          color: rgba(105, 105, 115, 1)
        }
        div.links-menu-208 .menu-item a {
          justify-content: flex-start;
          width: auto;
          padding-bottom: 4px
        }
        div.links-menu-208 .menu-item a:hover .links-text {
          color: rgba(233, 102, 49, 1);
          text-decoration: underline
        }
        div.links-menu-208 .count-badge {
          display: none
        }
        .phone footer div.links-menu-208 .module-title::before {
          display: block
        }
        .phone footer div.links-menu-208 .module-title.closed+.module-body>li {
          display: none
        }
        .phone footer div.links-menu-208 .module-title.closed {
          margin: 0
        }
        div.links-menu-207 .title.module-title {
          font-size: 15px;
          border-width: 0px;
          padding: 0px;
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        div.links-menu-207 .title.module-title::after {
          display: none
        }
        div.links-menu-207 .title.module-title.page-title>span::after {
          display: none
        }
        div.links-menu-207 .module-body {
          display: block;
          justify-content: flex-start;
          ;
          -webkit-overflow-scrolling: touch;
          ;
          column-count: initial;
          column-gap: 30px;
          column-rule-width: 1px;
          column-rule-style: solid
        }
        div.links-menu-207 .menu-item {
          border-width: 1px 0 0 0;
          flex-grow: 0;
          justify-content: flex-start;
          width: auto
        }
        div.links-menu-207 .menu-item a .links-text {
          white-space: normal;
          color: rgba(105, 105, 115, 1)
        }
        div.links-menu-207 .menu-item a {
          justify-content: flex-start;
          width: auto;
          padding-bottom: 4px
        }
        div.links-menu-207 .menu-item a:hover .links-text {
          color: rgba(233, 102, 49, 1);
          text-decoration: underline
        }
        div.links-menu-207 .count-badge {
          display: none
        }
        .phone footer div.links-menu-207 .module-title::before {
          display: block
        }
        .phone footer div.links-menu-207 .module-title.closed+.module-body>li {
          display: none
        }
        .phone footer div.links-menu-207 .module-title.closed {
          margin: 0
        }
        .icons-menu-209 .title.module-title {
          font-size: 15px;
          border-width: 0px;
          padding: 0px;
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        .icons-menu-209 .title.module-title::after {
          display: none
        }
        .icons-menu-209 .title.module-title.page-title>span::after {
          display: none
        }
        .icons-menu-209 a {
          background: rgba(240, 242, 245, 1);
          width: 80px;
          height: 80px
        }
        .desktop .icons-menu-209 a:hover {
          background: rgba(13, 82, 214, 1)
        }
        .icons-menu-209 a::before {
          color: rgba(105, 105, 115, 1);
          font-size: 40px
        }
        .icons-menu-209 .menu-item a {
          border-radius: 3px
        }
        .desktop .icons-menu-209 a:hover::before {
          color: rgba(255, 255, 255, 1)
        }
        .icons-menu-209 > ul > .menu-item > a .links-text {
          color: rgba(105, 105, 115, 1)
        }
        .desktop .icons-menu-209 > ul > .menu-item > a:hover .links-text {
          color: rgba(255, 255, 255, 1)
        }
        .icons-menu-209 .links-text {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis
        }
        .icons-menu-209>ul>.icons-menu-item {
          padding: calc(10px / 2)
        }
        .icons-menu-209 ul {
          justify-content: flex-start
        }
        .icons-menu-209 .module-title {
          text-align: left
        }
        .icons-menu-209 .module-title::after {
          left: 0;
          right: auto;
          transform: none
        }
        .icons-menu-209 .icons-menu-item-1.icon-menu-icon>a::before {
          content: '\e921' !important;
          font-family: icomoon !important;
          font-size: 45px
        }
        .icons-menu-209 .icons-menu-item-2.icon-menu-icon>a::before {
          content: '\e909' !important;
          font-family: icomoon !important;
          font-size: 45px
        }
        .icons-menu-209 .icons-menu-item-3.icon-menu-icon>a::before {
          content: '\e971' !important;
          font-family: icomoon !important
        }
        .icons-menu-209 .icons-menu-item-4.icon-menu-icon>a::before {
          content: '\e965' !important;
          font-family: icomoon !important;
          font-size: 45px
        }
        .icons-menu-209 .icons-menu-item-5.icon-menu-icon>a::before {
          content: '\e966' !important;
          font-family: icomoon !important
        }
        .icons-menu-209 .icons-menu-item-6.icon-menu-icon>a::before {
          content: '\eaf3' !important;
          font-family: icomoon !important
        }
        .icons-menu-209 .icons-menu-item-7.icon-menu-icon>a::before {
          content: '\e973' !important;
          font-family: icomoon !important
        }
        .icons-menu-209 .icons-menu-item-8.icon-menu-icon>a::before {
          content: '\eabb' !important;
          font-family: icomoon !important
        }
        .icons-menu-209 .icons-menu-item-9.icon-menu-icon>a::before {
          content: '\e96d' !important;
          font-family: icomoon !important
        }
        div.links-menu-205 .module-body {
          padding: 7px;
          display: flex;
          flex-direction: row;
          justify-content: flex-start;
          ;
          flex-wrap: nowrap;
          overflow-x: auto;
          overflow-y: hidden;
          -webkit-overflow-scrolling: touch;
          ;
          column-count: initial;
          column-gap: 30px;
          column-rule-width: 1px;
          column-rule-style: solid
        }
        div.links-menu-205 .menu-item {
          border-width: 0 0 0 1px;
          flex-grow: 0;
          justify-content: flex-start;
          width: auto;
          white-space: nowrap
        }
        div.links-menu-205 .menu-item a .links-text {
          white-space: nowrap;
          font-size: 13px
        }
        div.links-menu-205 .menu-item a {
          justify-content: flex-start;
          width: auto;
          padding-left: 7px
        }
        div.links-menu-205 .menu-item a::before {
          content: '\f111' !important;
          font-family: icomoon !important;
          font-size: 3px;
          margin-right: 7px
        }
        .mobile.touchevents div.links-menu-205 .module-body {
          overflow-x: scroll
        }
        div.links-menu-205 .module-body::-webkit-scrollbar {
          -webkit-appearance: none;
          height: 1px
        }
        div.links-menu-205 .module-body::-webkit-scrollbar-track {
          background-color: white
        }
        div.links-menu-205 .module-body::-webkit-scrollbar-thumb {
          background-color: #999
        }
        div.links-menu-205 .count-badge {
          display: none
        }
        .phone footer div.links-menu-205 .module-title::before {
          display: block
        }
        .phone footer div.links-menu-205 .module-title.closed+.module-body>li {
          display: none
        }
        .phone footer div.links-menu-205 .module-title.closed {
          margin: 0
        }
        div.links-menu-205 .links-menu-item-1>a::before {
          content: none !important
        }
        div.links-menu-205 .links-menu-item-1 a {
          padding-right: 3px
        }
        div.links-menu-205 .links-menu-item-1 a .links-text {
          color: rgba(105, 105, 115, 1);
          font-weight: 700;
          text-decoration: none
        }
        div.links-menu-205 .links-menu-item-1:hover a .links-text {
          text-decoration: none
        }
        div.links-menu-205 .links-menu-item-2>a::before {
          content: none !important
        }
        div.flyout-menu.flyout-menu-7 .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        div.flyout-menu.flyout-menu-7 .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        div.flyout-menu.flyout-menu-7 .j-menu .dropdown>a::after {
          display: block
        }
        div.flyout-menu.flyout-menu-7 .j-menu>li>a {
          font-size: 14px;
          color: rgba(105, 105, 115, 1);
          font-weight: 400;
          background: rgba(240, 242, 245, 1);
          padding: 15px;
          padding-top: 12px;
          padding-bottom: 12px
        }
        .desktop div.flyout-menu.flyout-menu-7 .j-menu > li:hover > a,
        div.flyout-menu.flyout-menu-7 .j-menu>li.active>a {
          color: rgba(51, 51, 51, 1);
          background: rgba(255, 255, 255, 1)
        }
        div.flyout-menu.flyout-menu-7 .j-menu .links-text {
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        div.flyout-menu.flyout-menu-7 .j-menu>li>a::before {
          color: rgba(139, 145, 152, 1);
          margin-right: 7px;
          min-width: 22px;
          font-size: 22px
        }
        div.flyout-menu.flyout-menu-7 .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .flyout-menu-7 .flyout-menu-item.drop-menu>.j-dropdown {
          left: 0;
          right: auto;
          transform: translate3d(0, -10px, 0)
        }
        .flyout-menu-7 .flyout-menu-item.drop-menu.animating>.j-dropdown {
          left: 0;
          right: auto;
          transform: none
        }
        .flyout-menu-7 .flyout-menu-item.drop-menu>.j-dropdown::before {
          left: 10px;
          right: auto;
          transform: translateX(0)
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu .dropdown>a>.count-badge {
          margin-right: 0
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu .dropdown>a>.count-badge+.open-menu+.menu-label {
          margin-left: 7px
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu .dropdown>a::after {
          display: block
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu>li>a {
          font-size: 14px;
          color: rgba(58, 71, 84, 1);
          font-weight: 400;
          background: rgba(255, 255, 255, 1);
          padding: 10px;
          padding-left: 15px
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu .links-text {
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu>li>a::before {
          margin-right: 7px;
          min-width: 20px;
          font-size: 18px
        }
        .desktop .flyout-menu-7 .flyout-menu-item.dropdown .j-menu > li:hover > a,
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu>li.active>a {
          background: rgba(240, 242, 245, 1)
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu > li > a,
        .flyout-menu-7 .flyout-menu-item.dropdown.accordion-menu .menu-item > a + div,
        .flyout-menu-7 .flyout-menu-item.dropdown .accordion-menu .menu-item>a+div {
          border-style: none
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-menu a .count-badge {
          display: none;
          position: relative
        }
        .flyout-menu-7 .flyout-menu-item.dropdown:not(.mega-menu) .j-dropdown {
          min-width: 200px
        }
        .flyout-menu-7 .flyout-menu-item.dropdown:not(.mega-menu) .j-menu {
          box-shadow: 30px 40px 90px -10px rgba(0, 0, 0, 0.2)
        }
        .flyout-menu-7 .flyout-menu-item.dropdown .j-dropdown::before {
          display: block;
          border-bottom-color: rgba(255, 255, 255, 1);
          margin-left: 7px;
          margin-top: -10px
        }
        .flyout-menu-7 .mega-menu-content {
          background: rgba(255, 255, 255, 1)
        }
        .flyout-menu-7 .j-dropdown>.mega-menu-content {
          box-shadow: 30px 40px 90px -10px rgba(0, 0, 0, 0.2)
        }
        @media (max-width: 1024px) {
          div.flyout-menu.flyout-menu-7 .j-menu>li>a {
            background: none
          }
          .flyout-menu-7 .j-dropdown>.mega-menu-content {
            max-height: 500px !important;
            overflow-y: auto
          }
        }
        .flyout-menu-7>ul.j-menu>li.flyout-menu-item-1>a::before {
          content: '\e8d2' !important;
          font-family: icomoon !important;
          font-size: 20px
        }
        .flyout-menu-7 .mega-menu.flyout-menu-item-1 .dropdown-menu {
          width: 900px
        }
        .desktop-header-active .flyout-menu-7 .flyout-menu-item-1.multi-level .dropdown-menu {
          left: 100%
        }
        .flyout-menu-7 .flyout-menu-item-1.mega-menu .grid-row-1 {
          background-image: url('../../journal-theme.digitalatelier.cloud/1/image/cache/catalog/journal3/misc/menu2-252x312.png');
          background-position: right bottom;
          background-repeat: no-repeat
        }
        .flyout-menu-7 .flyout-menu-item-1.mega-menu .grid-row-1::before {
          display: block;
          left: 0;
          width: 100vw
        }
        @media (max-width: 1024px) {
          .flyout-menu-7 .flyout-menu-item-1.mega-menu .grid-row-1 {
            background: none
          }
        }
        .flyout-menu-7 .flyout-menu-item-1.mega-menu .grid-row-1 .grid-col-1 {
          width: 80%;
          padding: 10px
        }
        .flyout-menu-7 .flyout-menu-item-1.mega-menu .grid-row-1 .grid-col-1 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .flyout-menu-7 .flyout-menu-item-1.mega-menu .grid-row-1 .grid-col-1 {
            width: 100%;
            padding: 0px
          }
        }
        .flyout-menu-7>ul.j-menu>li.flyout-menu-item-2>a::before {
          content: '\f179' !important;
          font-family: icomoon !important;
          font-size: 18px;
          top: -1px
        }
        .flyout-menu-7 .mega-menu.flyout-menu-item-2 .dropdown-menu {
          width: 900px
        }
        .desktop-header-active .flyout-menu-7 .flyout-menu-item-2.multi-level .dropdown-menu {
          left: 100%
        }
        .flyout-menu-7 .flyout-menu-item-2.mega-menu .grid-row-1 {
          background-image: url('../../journal-theme.digitalatelier.cloud/1/image/cache/catalog/journal3/misc/menubg3-344x351.jpg');
          background-position: right bottom;
          background-repeat: no-repeat
        }
        .flyout-menu-7 .flyout-menu-item-2.mega-menu .grid-row-1::before {
          display: block;
          left: 0;
          width: 100vw
        }
        @media (max-width: 1024px) {
          .flyout-menu-7 .flyout-menu-item-2.mega-menu .grid-row-1 {
            background: none
          }
        }
        .flyout-menu-7 .flyout-menu-item-2.mega-menu .grid-row-1 .grid-col-1 {
          width: 80%;
          padding: 10px
        }
        .flyout-menu-7 .flyout-menu-item-2.mega-menu .grid-row-1 .grid-col-1 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .flyout-menu-7 .flyout-menu-item-2.mega-menu .grid-row-1 .grid-col-1 {
            width: 100%;
            padding: 0px
          }
        }
        .flyout-menu-7>ul.j-menu>li.flyout-menu-item-3>a::before {
          content: '\e91f' !important;
          font-family: icomoon !important;
          font-size: 18px;
          top: -1px
        }
        .flyout-menu-7 .mega-menu.flyout-menu-item-3 .dropdown-menu {
          width: 800px
        }
        .desktop-header-active .flyout-menu-7 .flyout-menu-item-3.multi-level .dropdown-menu {
          left: 100%
        }
        .flyout-menu-7 .flyout-menu-item-3.mega-menu .grid-row-1::before {
          display: block;
          left: 0;
          width: 100vw
        }
        .flyout-menu-7 .flyout-menu-item-3.mega-menu .grid-row-1 .grid-col-1 {
          width: 30%
        }
        .flyout-menu-7 .flyout-menu-item-3.mega-menu .grid-row-1 .grid-col-1 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .flyout-menu-7 .flyout-menu-item-3.mega-menu .grid-row-1 .grid-col-1 {
            width: 100%;
            padding: 10px
          }
        }
        .flyout-menu-7 .flyout-menu-item-3.mega-menu .grid-row-1 .grid-col-2 {
          width: 70%;
          padding: 15px
        }
        .flyout-menu-7 .flyout-menu-item-3.mega-menu .grid-row-1 .grid-col-2 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .flyout-menu-7 .flyout-menu-item-3.mega-menu .grid-row-1 .grid-col-2 {
            width: 100%
          }
        }
        .flyout-menu-7>ul.j-menu>li.flyout-menu-item-4>a::before {
          content: '\e999' !important;
          font-family: icomoon !important;
          font-size: 17px
        }
        .flyout-menu-7 .mega-menu.flyout-menu-item-4 .dropdown-menu {
          width: 800px
        }
        .desktop-header-active .flyout-menu-7 .flyout-menu-item-4.multi-level .dropdown-menu {
          left: 100%
        }
        .flyout-menu-7 .flyout-menu-item-4.mega-menu .grid-row-1::before {
          display: block;
          left: 0;
          width: 100vw
        }
        .flyout-menu-7 .flyout-menu-item-4.mega-menu .grid-row-1 .grid-col-1 {
          width: 70%;
          padding: 15px
        }
        .flyout-menu-7 .flyout-menu-item-4.mega-menu .grid-row-1 .grid-col-1 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .flyout-menu-7 .flyout-menu-item-4.mega-menu .grid-row-1 .grid-col-1 {
            width: 100%
          }
        }
        .flyout-menu-7 .flyout-menu-item-4.mega-menu .grid-row-1 .grid-col-2 {
          width: 30%;
          background: rgba(240, 242, 245, 1);
          padding: 15px;
          padding-right: 20px;
          padding-left: 20px
        }
        .flyout-menu-7 .flyout-menu-item-4.mega-menu .grid-row-1 .grid-col-2 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .flyout-menu-7 .flyout-menu-item-4.mega-menu .grid-row-1 .grid-col-2 {
            width: 100%
          }
        }
        .flyout-menu-7>ul.j-menu>li.flyout-menu-item-5>a::before {
          content: '\e993' !important;
          font-family: icomoon !important;
          font-size: 15px
        }
        .flyout-menu-7 .mega-menu.flyout-menu-item-5 .dropdown-menu {
          width: 800px
        }
        .desktop-header-active .flyout-menu-7 .flyout-menu-item-5.multi-level .dropdown-menu {
          left: 100%
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 {
          padding: 20px
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1::before {
          display: block;
          left: 0;
          width: 100vw
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 .grid-col-1 {
          width: 20%;
          padding-right: 10px
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 .grid-col-1 .grid-items {
          justify-content: flex-start
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 .grid-col-2 {
          width: 20%;
          padding-right: 10px
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 .grid-col-2 .grid-items {
          justify-content: flex-start
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 .grid-col-3 {
          width: 20%;
          padding-right: 10px
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 .grid-col-3 .grid-items {
          justify-content: flex-start
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 .grid-col-4 {
          width: 40%
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-1 .grid-col-4 .grid-items {
          justify-content: flex-start
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-2 {
          background: rgba(240, 242, 245, 1)
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-2::before {
          display: block;
          left: 0;
          width: 100vw
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-2 .grid-col-1 {
          width: 100%
        }
        .flyout-menu-7 .flyout-menu-item-5.mega-menu .grid-row-2 .grid-col-1 .grid-items {
          justify-content: flex-start
        }
        .flyout-menu-7 > ul.j-menu > li.flyout-menu-item-6 > a .menu-label {
          background: rgba(80, 173, 85, 1);
          padding: 1px;
          padding-right: 5px;
          padding-left: 5px
        }
        .flyout-menu-7>ul.j-menu>li.flyout-menu-item-6>a::before {
          content: '\e998' !important;
          font-family: icomoon !important;
          font-size: 15px
        }
        .flyout-menu-7 .mega-menu.flyout-menu-item-6 .dropdown-menu {
          width: 800px
        }
        .desktop-header-active .flyout-menu-7 .flyout-menu-item-6.multi-level .dropdown-menu {
          left: 100%
        }
        div.links-menu-210 .title.module-title {
          font-size: 15px;
          border-width: 0px;
          padding: 0px;
          white-space: normal;
          overflow: visible;
          text-overflow: initial
        }
        div.links-menu-210 .title.module-title::after {
          display: none
        }
        div.links-menu-210 .title.module-title.page-title>span::after {
          display: none
        }
        div.links-menu-210 .module-body {
          padding-bottom: 10px;
          display: block;
          justify-content: flex-start;
          ;
          -webkit-overflow-scrolling: touch;
          ;
          column-count: 4;
          column-gap: 30px;
          column-rule-color: rgba(226, 226, 226, 1);
          column-rule-width: 1px;
          column-rule-style: solid
        }
        div.links-menu-210 .menu-item {
          border-width: 1px 0 0 0;
          flex-grow: 0;
          justify-content: flex-start;
          width: auto
        }
        div.links-menu-210 .menu-item a .links-text {
          white-space: normal;
          color: rgba(105, 105, 115, 1)
        }
        div.links-menu-210 .menu-item a {
          justify-content: flex-start;
          width: auto;
          padding-bottom: 4px;
          padding-left: 10px
        }
        div.links-menu-210 .menu-item a:hover .links-text {
          color: rgba(13, 82, 214, 1);
          text-decoration: underline
        }
        div.links-menu-210 .count-badge {
          display: none
        }
        .phone footer div.links-menu-210 .module-title::before {
          display: block
        }
        .phone footer div.links-menu-210 .module-title.closed+.module-body>li {
          display: none
        }
        .phone footer div.links-menu-210 .module-title.closed {
          margin: 0
        }
        @media (max-width: 1024px) {
          div.links-menu-210 .module-body {
            column-count: 2
          }
        }
        div.links-menu-210 .links-menu-item-4 .menu-label {
          margin-right: calc(0px - (5px))
        }
        div.links-menu-210 .links-menu-item-13 .menu-label {
          background: rgba(13, 82, 214, 1);
          padding: 1px;
          padding-right: 5px;
          padding-left: 5px;
          margin-right: calc(0px - (5px))
        }
        div.links-menu-210 .links-menu-item-15 .menu-label {
          background: rgba(80, 173, 85, 1);
          padding: 1px;
          padding-right: 5px;
          padding-left: 5px;
          margin-right: calc(0px - (5px))
        }
        div.links-menu-210 .links-menu-item-18 .menu-label {
          color: rgba(51, 51, 51, 1);
          background: rgba(254, 212, 48, 1);
          padding: 1px;
          padding-right: 5px;
          padding-left: 5px;
          margin-right: calc(0px - (5px))
        }
        div.main-menu.main-menu-3 > .j-menu .menu-item.main-menu-item-1>a::before {
          content: '\e8d2' !important;
          font-family: icomoon !important;
          font-size: 20px;
          top: -1px;
          margin-right: 5px
        }
        div.main-menu.main-menu-3 > .j-menu li.main-menu-item-1>a {
          color: #FFF !important/*rgba(51, 51, 51, 1) ;
          background: var(--blue-color)/*rgba(254, 212, 48, 1)*/
        }
        .desktop div.main-menu.main-menu-3 > .j-menu li.main-menu-item-1:hover>a {
          background: rgba(240, 242, 245, 1)
        }
        div.main-menu.main-menu-3>.j-menu>.main-menu-item-1>a {
          padding-left: 16px !important;
          min-width: 230px
        }
        .desktop-main-menu-wrapper .main-menu-3 .mega-custom.main-menu-item-1 .mega-menu-content {
          width: 500px;
          position: relative;
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-3 .mega-custom.main-menu-item-1>.dropdown-menu::before {
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-3 > .j-menu li.main-menu-item-1:not(.mega-fullwidth)>.dropdown-menu::before {
          display: none;
          margin-top: -10px
        }
        .desktop-main-menu-wrapper .main-menu-3 > .j-menu li.main-menu-item-1.multi-level .dropdown-menu ul li .dropdown-menu {
          left: 100%
        }
        .main-menu-3 > .j-menu li.main-menu-item-1>a>.menu-label {
          color: rgba(255, 255, 255, 1);
          font-weight: 400;
          text-transform: none;
          background: rgba(233, 102, 49, 1);
          padding: 1px;
          padding-right: 5px;
          padding-left: 5px
        }
        .desktop-main-menu-wrapper .main-menu-3 .main-menu-item-1>a>.menu-label {
          margin-right: calc(0px - (-7px));
          margin-top: -7px
        }
        @media (max-width: 1024px) {
          div.main-menu.main-menu-3 > .j-menu li.main-menu-item-1>a {
            background: none
          }
          div.main-menu.main-menu-3>.j-menu>.main-menu-item-1>a {
            padding-left: 0px !important
          }
        }
        .desktop-main-menu-wrapper .main-menu-3 .mega-custom.main-menu-item-2 .mega-menu-content {
          width: 500px;
          position: relative;
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-3 .mega-custom.main-menu-item-2>.dropdown-menu::before {
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-3 > .j-menu li.main-menu-item-2:not(.mega-fullwidth)>.dropdown-menu::before {
          display: none;
          margin-top: -10px
        }
        .desktop-main-menu-wrapper .main-menu-3 > .j-menu li.main-menu-item-2.multi-level .dropdown-menu ul li .dropdown-menu {
          left: 100%
        }
        .desktop-main-menu-wrapper .main-menu-3 .mega-custom.main-menu-item-3 .mega-menu-content {
          width: 500px;
          position: relative;
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-3 .mega-custom.main-menu-item-3>.dropdown-menu::before {
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-3 > .j-menu li.main-menu-item-3:not(.mega-fullwidth)>.dropdown-menu::before {
          display: block;
          margin-top: -10px
        }
        .desktop-main-menu-wrapper .main-menu-3 > .j-menu li.main-menu-item-3.multi-level .dropdown-menu ul li .dropdown-menu {
          left: 100%
        }
        .main-menu-3 > .j-menu li.main-menu-item-3>a>.menu-label {
          color: rgba(51, 51, 51, 1);
          background: rgba(254, 212, 48, 1);
          padding: 1px;
          padding-right: 5px;
          padding-left: 5px
        }
        .desktop-main-menu-wrapper .main-menu-3 .main-menu-item-3>a>.menu-label {
          margin-right: calc(0px - (-7px));
          margin-top: -7px
        }
        .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1 {
          padding: 10px
        }
        .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1::before {
          display: block;
          left: 0;
          width: 100vw
        }
        @media (max-width: 1024px) {
          .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1 {
            padding: 0px
          }
        }
        .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1 .grid-col-1 {
          width: 80%;
          padding: 10px
        }
        .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1 .grid-col-1 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1 .grid-col-1 {
            width: 100%;
            padding: 0px;
            padding-bottom: 20px
          }
        }
        .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1 .grid-col-2 {
          width: 20%;
          border-width: 0;
          border-left-width: 1px;
          border-style: solid;
          border-color: rgba(226, 226, 226, 1);
          padding: 20px;
          padding-right: 10px
        }
        .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1 .grid-col-2 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .main-menu-3 .main-menu-item-3.mega-menu .grid-row-1 .grid-col-2 {
            width: 100%;
            border-style: none;
            padding: 0px
          }
        }
        .desktop-main-menu-wrapper .menu-fullwidth>.j-dropdown {
          width: 100vw;
          left: 50%;
          margin-left: -50vw
        }
        .desktop-main-menu-wrapper .main-menu-3 .mega-custom.main-menu-item-4 .mega-menu-content {
          width: 500px;
          position: relative;
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-3 .mega-custom.main-menu-item-4>.dropdown-menu::before {
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-3 > .j-menu li.main-menu-item-4:not(.mega-fullwidth)>.dropdown-menu::before {
          display: block;
          margin-top: -10px
        }
        .desktop-main-menu-wrapper .main-menu-3 > .j-menu li.main-menu-item-4.multi-level .dropdown-menu ul li .dropdown-menu {
          left: 100%
        }
        .main-menu-3 .main-menu-item-4.mega-menu .grid-row-1 {
          padding: 10px
        }
        .main-menu-3 .main-menu-item-4.mega-menu .grid-row-1::before {
          display: block;
          left: 0;
          width: 100vw
        }
        @media (max-width: 1024px) {
          .main-menu-3 .main-menu-item-4.mega-menu .grid-row-1 {
            padding: 0px
          }
        }
        .main-menu-3 .main-menu-item-4.mega-menu .grid-row-1 .grid-col-1 {
          width: 66.66666666666666%;
          padding: 20px;
          padding-left: 0px
        }
        @media (max-width: 1024px) {
          .main-menu-3 .main-menu-item-4.mega-menu .grid-row-1 .grid-col-1 {
            width: 100%;
            padding: 0px;
            padding-bottom: 20px
          }
        }
        .main-menu-3 .main-menu-item-4.mega-menu .grid-row-1 .grid-col-2 {
          width: 33.33333333333333%;
          padding: 20px
        }
        .main-menu-3 .main-menu-item-4.mega-menu .grid-row-1 .grid-col-2 .grid-items {
          justify-content: flex-start
        }
        @media (max-width: 1024px) {
          .main-menu-3 .main-menu-item-4.mega-menu .grid-row-1 .grid-col-2 {
            width: 100%;
            padding: 0px
          }
        }
        div.main-menu.main-menu-64 > .j-menu .menu-item.main-menu-item-1>a::before {
          content: '\eb97' !important;
          font-family: icomoon !important;
          font-size: 20px
        }
        div.main-menu.main-menu-64 > .j-menu li.main-menu-item-1>a {
          font-size: 15px !important
        }
        div.main-menu.main-menu-64>.j-menu>.main-menu-item-1>a {
          padding-right: 20px !important
        }
        .desktop-main-menu-wrapper .main-menu-64 .mega-custom.main-menu-item-1 .mega-menu-content {
          width: 500px;
          position: relative;
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-64 .mega-custom.main-menu-item-1>.dropdown-menu::before {
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-64 > .j-menu li.main-menu-item-1:not(.mega-fullwidth)>.dropdown-menu::before {
          display: block;
          margin-top: -10px
        }
        .desktop-main-menu-wrapper .main-menu-64 > .j-menu li.main-menu-item-1.multi-level .dropdown-menu ul li .dropdown-menu {
          left: 100%
        }
        div.main-menu.main-menu-64 > .j-menu .menu-item.main-menu-item-2>a::before {
          content: '\e946' !important;
          font-family: icomoon !important;
          margin-right: 7px
        }
        div.main-menu.main-menu-64 > .j-menu li.main-menu-item-2>a {
          color: rgba(51, 51, 51, 1) !important;
          background: rgba(254, 212, 48, 1)
        }
        .desktop div.main-menu.main-menu-64 > .j-menu li.main-menu-item-2:hover>a {
          background: rgba(255, 255, 255, 1)
        }
        div.main-menu.main-menu-64>.j-menu>.main-menu-item-2>a {
          padding: 15px !important
        }
        .desktop-main-menu-wrapper .main-menu-64 .mega-custom.main-menu-item-2 .mega-menu-content {
          width: 500px;
          position: relative;
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-64 .mega-custom.main-menu-item-2>.dropdown-menu::before {
          left: 0;
          transform: none
        }
        .desktop-main-menu-wrapper .main-menu-64 > .j-menu li.main-menu-item-2:not(.mega-fullwidth)>.dropdown-menu::before {
          display: block;
          margin-top: -10px
        }
        .desktop-main-menu-wrapper .main-menu-64 > .j-menu li.main-menu-item-2.multi-level .dropdown-menu ul li .dropdown-menu {
          left: 100%
        }
        .top-menu-2 .j-menu li.top-menu-item-1>a::before {
          content: '\eb69' !important;
          font-family: icomoon !important;
          top: -1px
        }
        .top-menu-2>ul>.top-menu-item-1>a {
          text-align: left
        }
        .top-menu-2>ul>.top-menu-item-1>a>.links-text {
          display: block
        }
        .top-menu-2 > ul > .top-menu-item-1 > a .count-badge {
          position: relative
        }
        .top-menu-2 .j-menu li.top-menu-item-2>a::before {
          content: '\e97e' !important;
          font-family: icomoon !important;
          top: -1px
        }
        .top-menu-2>ul>.top-menu-item-2>a {
          text-align: left
        }
        .top-menu-2>ul>.top-menu-item-2>a>.links-text {
          display: block
        }
        .top-menu-2 > ul > .top-menu-item-2 > a .count-badge {
          position: relative
        }
        .top-menu-2 .j-menu li.top-menu-item-3>a::before {
          content: '\ea93' !important;
          font-family: icomoon !important
        }

        .top-menu-item-3>a::before {
          content: "\f232" !important;
          font-family: Font-Awesome-5 !important
        }
        
        .top-menu-2>ul>.top-menu-item-3>a {
          text-align: left
        }
        .top-menu-2>ul>.top-menu-item-3>a>.links-text {
          display: block
        }
        .top-menu-2 > ul > .top-menu-item-3 > a .count-badge {
          position: relative
        }
        .top-menu-2 .j-menu li.top-menu-item-4>a::before {
          content: '\f29c' !important;
          font-family: icomoon !important;
          top: -1px
        }
        .top-menu-2>ul>.top-menu-item-4>a {
          text-align: left
        }
        .top-menu-2>ul>.top-menu-item-4>a>.links-text {
          display: block
        }
        .top-menu-2 > ul > .top-menu-item-4 > a .count-badge {
          position: relative
        }
        .top-menu-2 li.top-menu-item-5>a::before {
          content: '\e5c8' !important;
          font-family: icomoon !important;
          margin-right: 5px
        }
        .top-menu-2 li.top-menu-item-6>a::before {
          content: '\e984' !important;
          font-family: icomoon !important;
          margin-right: 5px
        }
        .top-menu-2 li.top-menu-item-7>a::before {
          content: '\e901' !important;
          font-family: icomoon !important;
          margin-right: 5px
        }
        .top-menu-240 .j-menu li.top-menu-item-1>a::before {
          content: '\ead9' !important;
          font-family: icomoon !important
        }
        .top-menu-240>ul>.top-menu-item-1>a {
          text-align: left
        }
        .top-menu-240>ul>.top-menu-item-1>a>.links-text {
          display: block
        }
        .top-menu-240 > ul > .top-menu-item-1 > a .count-badge {
          position: relative
        }
        .top-menu-240 .j-menu li.top-menu-item-2>a::before {
          content: '\e90d' !important;
          font-family: icomoon !important
        }
        .top-menu-240>ul>.top-menu-item-2>a {
          text-align: left
        }
        .top-menu-240>ul>.top-menu-item-2>a>.links-text {
          display: block
        }
        .top-menu-240 > ul > .top-menu-item-2 > a .count-badge {
          position: relative
        }
        .top-menu-240 .j-menu li.top-menu-item-5>a::before {
          content: '\eaa7' !important;
          font-family: icomoon !important
        }
        .top-menu-240>ul>.top-menu-item-5>a {
          text-align: left
        }
        .top-menu-240>ul>.top-menu-item-5>a>.links-text {
          display: block
        }
        .top-menu-240 > ul > .top-menu-item-5 > a .count-badge {
          position: relative
        }
        .top-menu-240 .j-menu li.top-menu-item-6>a::before {
          content: '\eab6' !important;
          font-family: icomoon !important
        }
        .top-menu-240>ul>.top-menu-item-6>a {
          text-align: left
        }
        .top-menu-240>ul>.top-menu-item-6>a>.links-text {
          display: block
        }
        .top-menu-240 > ul > .top-menu-item-6 > a .count-badge {
          position: relative
        }
        .top-menu-14 .j-menu li.top-menu-item-1>a::before {
          content: '\e912' !important;
          font-family: icomoon !important
        }
        .top-menu-14>ul>.top-menu-item-1>a {
          text-align: left
        }
        .top-menu-14>ul>.top-menu-item-1>a>.links-text {
          display: block
        }
        .top-menu-14 > ul > .top-menu-item-1 > a .count-badge {
          position: relative
        }
        .top-menu-14 .j-menu li.top-menu-item-6>a::before {
          content: '\eab8' !important;
          font-family: icomoon !important
        }
        .top-menu-14>ul>.top-menu-item-6>a {
          text-align: left
        }
        .top-menu-14>ul>.top-menu-item-6>a>.links-text {
          display: block
        }
        .top-menu-14 > ul > .top-menu-item-6 > a .count-badge {
          position: relative
        }
        .top-menu-13 .j-menu li.top-menu-item-1>a::before {
          content: '\ead9' !important;
          font-family: icomoon !important
        }
        .top-menu-13>ul>.top-menu-item-1>a {
          text-align: left
        }
        .top-menu-13>ul>.top-menu-item-1>a>.links-text {
          display: block
        }
        .top-menu-13 > ul > .top-menu-item-1 > a .count-badge {
          position: relative
        }
        .top-menu-13 .j-menu li.top-menu-item-2>a::before {
          content: '\e90d' !important;
          font-family: icomoon !important
        }
        .top-menu-13>ul>.top-menu-item-2>a {
          text-align: left
        }
        .top-menu-13>ul>.top-menu-item-2>a>.links-text {
          display: block
        }
        .top-menu-13 > ul > .top-menu-item-2 > a .count-badge {
          position: relative
        }
        .icons-menu-61 a {
          background: rgba(13, 82, 214, 1);
          width: 40px;
          height: 40px
        }
        .desktop .icons-menu-61 a:hover {
          background: rgba(15, 58, 141, 1)
        }
        .icons-menu-61 a::before {
          color: rgba(255, 255, 255, 1)
        }
        .icons-menu-61 .menu-item a {
          border-radius: 50px
        }
        .icons-menu-61 .links-text {
          white-space: normal;
          overflow: visible;
          text-overflow: ellipsis;
          display: none
        }
        .icons-menu-61>ul>.icons-menu-item {
          padding: calc(10px / 2)
        }
        .icons-menu-61 ul {
          justify-content: center
        }
        .icons-menu-61 .module-title {
          text-align: center
        }
        .icons-menu-61 .module-title::after {
          left: 50%;
          right: auto;
          transform: translate3d(-50%, 0, 0)
        }
        .icons-menu-61 .icons-menu-item-1.icon-menu-icon>a::before {
          content: '\f09a' !important;
          font-family: icomoon !important
        }
        .icons-menu-61 .icons-menu-item-2.icon-menu-icon>a::before {
          content: '\f099' !important;
          font-family: icomoon !important
        }
        .icons-menu-61 .icons-menu-item-3.icon-menu-icon>a::before {
          content: '\e90e' !important;
          font-family: icomoon !important;
          font-size: 18px
        }
        .icons-menu-61 .icons-menu-item-4.icon-menu-icon>a::before {
          content: '\f0e1' !important;
          font-family: icomoon !important
        }
        .icons-menu-61 .icons-menu-item-5.icon-menu-icon>a::before {
          content: '\f167' !important;
          font-family: icomoon !important
        }
        .icons-menu-61 .icons-menu-item-6.icon-menu-icon>a::before {
          content: '\f17e' !important;
          font-family: icomoon !important
        }
        .icons-menu-61 .icons-menu-item-7.icon-menu-icon>a::before {
          content: '\f0d5' !important;
          font-family: icomoon !important
        }
        .links-menu-72 .module-body {
          display: block;
          justify-content: flex-start;
          align-items: flex-start;
          ;
          -webkit-overflow-scrolling: touch;
          ;
          column-count: initial;
          column-rule-style: solid
        }
        .links-menu-72 .menu-item {
          border-width: 1px 0 0 0;
          justify-content: flex-start;
          ;
          flex-grow: 0;
          width: auto
        }
        .links-menu-72 .menu-item a .links-text {
          white-space: normal
        }
        .links-menu-72 .menu-item a {
          justify-content: flex-start;
          ;
          padding-bottom: 9px
        }
        .links-menu-72 .count-badge {
          display: none
        }
        .phone footer .links-menu-72 .module-title::before {
          display: none
        }
        .phone footer .links-menu-72 .module-title+.module-body>li {
          display: flex
        }
        div.links-menu-72 .module-body {
          -webkit-overflow-scrolling: touch
        }
        @media (max-width: 760px) {
          div.links-menu-72 .title.module-title::after {
            display: none
          }
        }
        .links-menu-75 .module-body {
          display: block;
          justify-content: flex-start;
          align-items: flex-start;
          ;
          -webkit-overflow-scrolling: touch;
          ;
          column-count: initial;
          column-rule-style: solid
        }
        .links-menu-75 .menu-item {
          border-width: 1px 0 0 0;
          justify-content: flex-start;
          ;
          flex-grow: 0;
          width: auto
        }
        .links-menu-75 .menu-item a .links-text {
          white-space: normal
        }
        .links-menu-75 .menu-item a {
          justify-content: flex-start;
          ;
          padding-bottom: 9px
        }
        .links-menu-75 .count-badge {
          display: none
        }
        .phone footer .links-menu-75 .module-title::before {
          display: none
        }
        .phone footer .links-menu-75 .module-title+.module-body>li {
          display: flex
        }
        div.links-menu-75 .module-body {
          -webkit-overflow-scrolling: touch
        }
        @media (max-width: 760px) {
          div.links-menu-75 .title.module-title::after {
            display: none
          }
        }
        .links-menu-76 .module-body {
          display: block;
          justify-content: flex-start;
          align-items: flex-start;
          ;
          -webkit-overflow-scrolling: touch;
          ;
          column-count: initial;
          column-rule-style: solid
        }
        .links-menu-76 .menu-item {
          border-width: 1px 0 0 0;
          justify-content: flex-start;
          ;
          flex-grow: 0;
          width: auto
        }
        .links-menu-76 .menu-item a .links-text {
          white-space: normal
        }
        .links-menu-76 .menu-item a {
          justify-content: flex-start;
          ;
          padding-bottom: 9px
        }
        .links-menu-76 .count-badge {
          display: none
        }
        .phone footer .links-menu-76 .module-title::before {
          display: none
        }
        .phone footer .links-menu-76 .module-title+.module-body>li {
          display: flex
        }
        div.links-menu-76 .module-body {
          -webkit-overflow-scrolling: touch
        }
        @media (max-width: 760px) {
          div.links-menu-76 .title.module-title::after {
            display: none
          }
        }
        @media (max-width: 997px) {
          .header-search {
            margin-left: 5px;
            margin-top: 0px
          }
        }
        body, html {
    overflow-x: hidden !important;
    overflow-y: auto !important;
        }
        .imgCat
        {
          display: none;
        }
        @media (max-width: 1110px) {
          .imgCat
          {
            display: inline;
          }
        }




        .ptype-grid > .li {
    box-sizing: border-box !important;
    height: auto !important;
    margin: 2% !important;
    overflow: hidden !important;
    position: relative !important;
    text-align: center !important;
    width: 46% !important ;
    margin-top: 2% !important ;
    margin-right: 2% !important;
    margin-bottom: 2% !important;
    margin-left: 2% !important;
    overflow-x: hidden !important;
    overflow-y: hidden !important;
}

@media only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid > .li {
    margin: 8px 0.65%!important;
    width: 23.5%!important;
    margin-top: 8px!important;
    margin-right: 0.65%!important;
    margin-bottom: 8px!important;
    margin-left: 0.65%!important;
}
}

.ptype-grid.-col-9 > .li {
    margin: 8px 0.65%!important;
    width: 31.833333%!important;
    margin-top: 8px!important;
    margin-right: 0.65%!important;
    margin-bottom: 8px!important;
    margin-left: 0.65%!important;
}

@media only screen and (max-width: 1023.98px) and (min-width: 741px), only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px){
.ptype-grid.-col-9 > .li {
    margin-left: 10px;
    margin-right: 10px
}
}

@media only screen and (max-width: 1023.98px) and (min-width: 741px), only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid.-col-9 > .li {
    width: 8.701% !important
}
}

.ptype-grid {
    display: -webkit-box!important ;
    display: -webkit-flex!important;
    display: flex!important;
    -webkit-box-orient: horizontal!important;
    -webkit-box-direction: normal!important;
    -webkit-flex-direction: row!important;
    flex-direction: row!important;
    -webkit-flex-wrap: wrap!important;
    flex-wrap: wrap!important;
    list-style: none!important;
    margin: 16px 0 32px!important;
    overflow: hidden!important;
    list-style-position: initial!important;
    list-style-image: initial!important;
    list-style-type: none!important;
    margin-top: 16px !important;
    margin-right: 0px !important;
    margin-bottom: 32px !important;
    margin-left: 0px !important;
    overflow-x: hidden !important;
    overflow-y: hidden !important;
}

@media only screen and (max-width: 740.98px) and (min-width: 421px), only screen and (max-width: 1023.98px) and (min-width: 741px), only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid {
    margin-top: 32px !important;
}
}

.ptype-grid.-center {
    -webkit-box-pack: center !important;
    -webkit-justify-content: center !important;
    justify-content: center !important;
}

@media only screen and (max-width: 1600.98px) and (min-width: 1281px){
.ptype-grid.-col-9 {
    width: 95% !important;
    padding: 10px !important
}
}

@media only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid.-col-9 {
    margin: 44px auto !important;
    margin-top: 44px !important;
    margin-right: auto !important;
    margin-bottom: 44px !important;
    margin-left: auto !important;
}
}

.body-bg-type1 {
    background: #f0f0f0 !important;
    background-image: initial !important;
    background-position-x: initial !important;
    background-position-y: initial !important;
    background-size: initial !important;
    background-repeat-x: initial !important;
    background-repeat-y: initial !important;
    background-attachment: initial !important;
    background-origin: initial !important;
    background-clip: initial !important;
    background-color: rgb(240, 240, 240) !important;
}

.main-content {
    position: relative !important;
    width: 100% !important;
    z-index: 1 !important;
}

@media only screen and (max-width: 1023.98px) and (min-width: 741px), only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.main-content {
    margin-top: 0 !important;
}
}

.ptype-grid-a {
    background: url(https://cdn.carid.com/css/prod-images/94c8e853.svg) 0 0 no-repeat !important;
    box-sizing: border-box !important;
    color: #111 !important;
    display: -webkit-box !important;
    display: -webkit-flex !important;
    display: flex !important;
    -webkit-box-orient: vertical !important;
    -webkit-box-direction: normal !important;
    -webkit-flex-direction: column !important;
    flex-direction: column !important;
    -webkit-flex-wrap: wrap !important;
    flex-wrap: wrap !important;
    font-size: 1.4em !important;
    font-weight: 500 !important;
    left: 0 !important;
    line-height: 1.3 !important;
    padding: 95% 8px 12px !important;
    position: relative !important;
    table-layout: fixed !important;
    text-decoration: none !important;
    width: 100% !important;
    background-image: url("https://cdn.carid.com/css/prod-images/94c8e853.svg") !important;
    background-position-x: 0px !important;
    background-position-y: 0px !important;
    background-size: initial !important;
    background-repeat-x: no-repeat !important;
    background-repeat-y: no-repeat !important;
    background-attachment: initial !important;
    background-origin: initial !important;
    background-clip: initial !important;
    background-color: initial !important;
    padding-top: 95% !important;
    padding-right: 8px !important;
    padding-bottom: 12px !important;
    padding-left: 8px !important;
    text-decoration-line: none !important;
    text-decoration-style: initial !important;
    text-decoration-color: initial !important;
}

@media only screen and (max-width: 740.98px) and (min-width: 421px), only screen and (max-width: 1023.98px) and (min-width: 741px), only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid-a {
    padding-bottom: 24px !important;
}
}

@media only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid-a {
    padding-top: 110px !important;
    font-size: 84.5%;
}
}

@media only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid-a {
    font-size: 1em !important;
    left: auto !important;
}
}

.ptype-grid-a.-simple-title, .ptype-grid-title {
    font-weight: 400 !important;
}

.ptype-grid.-col-9 .ptype-grid-a  {
    padding-bottom: 0 !important;
}

@media only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid.-col-9 .ptype-grid-a  {
    padding-top: 125px !important;
}
}

.ptype-grid-a:before {
    background: url(https://cdn.carid.com/css/prod-images/94c8e853.svg) 0 0 no-repeat !important;
    content: '' !important;
    display: block !important;
    height: 150% !important;
    left: 0 !important;
    position: absolute !important;
    top: 0 !important;
    width: 100% !important;
    z-index: 2 !important;
    background-image: url("https://cdn.carid.com/css/prod-images/94c8e853.svg") !important;
    background-position-x: 0px !important;
    background-position-y: 0px !important;
    background-size: initial !important;
    background-repeat-x: no-repeat !important;
    background-repeat-y: no-repeat !important;
    background-attachment: initial !important;
    background-origin: initial !important;
    background-clip: initial !important;
    background-color: initial !important;
}

.ptype-grid-a:after {
    color: #757575 !important;
    content: attr(data-descr) !important;
    font-size: 12px !important;
    font-weight: 400 !important;
    line-height: 1.25 !important;
    position: relative !important;
    text-transform: none !important;
}

.ptype-grid-a.-simple-title:after {
    padding-top: 6px !important;
}

.li  {
    box-sizing: border-box !important;
    height: auto !important;
    margin: 2% !important;
    overflow: hidden !important;
    position: relative !important;
    text-align: center !important;
    width: 46% !important;
    margin-top: 2% !important;
    margin-right: 2% !important;
    margin-bottom: 2% !important;
    margin-left: 2% !important;
    overflow-x: hidden !important;
    overflow-y: hidden !important;
}

@media only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.li  {
    margin: 8px 0.65% !important;
    width: 23.5% !important;
    margin-top: 8px !important;
    margin-right: 0.65% !important;
    margin-bottom: 8px !important;
    margin-left: 0.65% !important;
}
}

.li  {
    margin: 8px 0.65% !important;
    width: 31.833333% !important;
    margin-top: 8px !important;
    margin-right: 0.65% !important;
    margin-bottom: 8px !important;
    margin-left: 0.65% !important;
}

@media only screen and (max-width: 1023.98px) and (min-width: 741px), only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px){
.li  {
    margin-left: 10px !important;
    margin-right: 10px !important;
}
}

@media only screen and (max-width: 1023.98px) and (min-width: 741px), only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.li  {
    width: 126px !important;
}
}

.li:before  {
    color: #a9a9a9 !important;
    content: attr(data-qty) !important;
    font-size: 1.1em !important;
    left: 0 !important;
    position: absolute !important;
    right: 0 !important;
    top: 8% !important;
    z-index: 2 !important;
}

@media only screen and (max-width: 1023.98px) and (min-width: 741px), only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.li:before  {
    top: 20px !important;
}
}

.li:before  {
    display: block !important;
}

.ptype-grid-img {
    height: auto !important;
    left: 0 !important;
    margin: auto !important;
    max-width: 100% !important;
    position: absolute !important;
    right: 0 !important;
    top: 22% !important;
    width: 90% !important;
    margin-top: auto !important;
    margin-right: auto !important;
    margin-bottom: auto !important;
    margin-left: auto !important;
}

@media only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid-img {
    top: 60px !important;
    width: 170px !important;
}
}

.lazy-loaded  {
    -webkit-transition: opacity 0.5s !important;
    transition: opacity 0.5s !important;
    transition-duration: 0.5s !important;
    transition-timing-function: ease !important;
    transition-delay: 0s !important;
    transition-property: opacity !important;
}

.ptype-grid-img  {
    top: 0 !important;
}

@media only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid-img  {
    width: 100px !important;
}
}

.ptype-grid-a  {
    padding-bottom: 0 !important;
}

@media only screen and (max-width: 1280.98px) and (min-width: 1024px), only screen and (max-width: 1600.98px) and (min-width: 1281px), only screen and (min-width: 1601px){
.ptype-grid-a  {
    padding-top: 86px !important;
}
}

.dd-none
{
  display: none !important;
}
.dd-block
{
  display: block;
}

.homeicon
{
  width: 30px;
  height: 30px;
}
.hometext
{
  display: none;
  margin-left: 4px;
}
@media screen and (max-width: 997px)
{
  .homeicon
  {
    width: 25px;
    height: 25px;
    margin-left: 6px;
  }
  .hometext
  {
    display: inline
  }
  #logo
  {
    transform: scale(0.9) !important;
  }
  #logo a img
  {
    top: -7px !important;
    height: 80px;
  }
}
.fb-ic:hover > i
{
    transform: scale(1.3);
    color: black !important;
    transition: 0.5s
}
.li-ic:hover > i
{
    transform: scale(1.3);
    color: black !important;
    transition: 0.5s
}
.ins-ic:hover > i
{
    transform: scale(1.3);
    color: black !important;
    transition: 0.5s
}
#topcontrol
{
  padding: 10px;
  z-index: 200;
}
        </style>
        <!--<script src="../../journal-theme.digitalatelier.cloud/1/catalog/view/theme/journal3/assets/9fe358ea7a46c1dfe26d06bd9c0fea6a.js"  defer ></script>-->
        <!-- Font Awesome -->
        <script src="/js/9fe358ea7a46c1dfe26d06bd9c0fea6a.js"  defer ></script>
    {{--@yield('styles')--}}

			</head>
			<body class="" >
				<div class="mobile-container mobile-main-menu-container">
					<div class="mobile-wrapper-header">
						<span>Menu</span>
						<a class="x"></a>
					</div>
					<div class="mobile-main-menu-wrapper"></div>
				</div>
				<div class="mobile-container mobile-filter-container ">
					<div class="mobile-wrapper-header"></div>
					<div class="mobile-filter-wrapper"></div>
				</div>
				<div class="mobile-container mobile-cart-content-container ">
					<div class="mobile-wrapper-header ">
						<span>Your Cart</span>
						<a class="x"></a>
          </div>

        	<div id="cart-content" class="cart-content">
          <ul>
          <li class="cart-products">
                            <table class="table">
                              <tbody>
                              @if(session()->has('cart'))
                              @foreach(session()->get('cart')->getContents() as $key => $data)
                                <tr>
                                  <td class="text-center td-image">
                                    <a href="javascript:void(0)">
                                      <img src="{{ $data['product']->images()->first() ? asset('main/storage/app/public/'.$data['product']->images()->first()->image) :'/admin/img/no-photo.png'}}" alt="{{ $data['product']->name }}" title="{{ $data['product']->name }}" width="60" height="60">
                                    </a>
                                  </td>
                                  <td class="text-left td-name" style="font-size: unset;"><a href="javascript:void(0)">{{ $data['product']->name }} <br>{{$data['qty'] }} {{ $data['product']->discount_price }}</a><br></td>
                                  <td class="text-center td-remove" style="width: 23%; font-size: smaller"> {{ $data['total_price'] }}</td>
                                </tr>
                              @endforeach
		                          @endif
                            </tbody>
                          </table>
                        </li>

                        <li class="cart-totals">
                        <div>
                          <table class="table table-bordered">
                            <tbody>
                            @if(session()->has('cart'))
                                <tr>
                                    <td class="text-right td-total-title">Sub-Total</td>
                                    <td class="text-right td-total-text">PKR {{session()->get('cart')->getTotalPrice()}}</td>
                                </tr>
                            @endif
                            </tbody>
                          </table>
                        </div>
                        @if(session()->has('cart'))
                          <div class="cart-buttons">
                          <a href="{{route('products.cart')}}" class='btn btn-primary' style='color: white; font-size: unset;'>View Cart</a>
                          <a href="{{route('products.checkout')}}" class='btn btn-danger' style='color: white; font-size: unset;'>Checkout</a>
                          </div>

                          @else
                          <p class="text-center cart-empty">Your shopping cart is empty!</p>
                          @endif
                      </li>
                    </ul>
                      </div>
				</div>
				<div class="site-wrapper">
					<header class="header-classic" style="z-index: 25" id="hdd">
						<div class="header header-classic header-lg  active">
							<div class="top-bar navbar-nav ">
								<div class="top-menu top-menu-2" style="width:100%">
									<ul class="j-menu" style="display: flex; justify-content: space-evenly">
                  
                    <li class="menu-item top-menu-item">
											<a href="{{route('sub-category.show', 22)}}" >
												<span class="links-text">Body Kits & Splitters</span>
											</a>
                    </li>
                    
                    <li class="menu-item top-menu-item">
											<a href="{{route('sub-category.show', 18)}}" >
												<span class="links-text">Floor Mats</span>
											</a>
										</li>

                    <li class="menu-item top-menu-item">
											<a href="{{route('sub-category.show', 52)}}" >
												<span class="links-text">Tail Lights</span>
											</a>
										</li>
                  
                    <li class="menu-item top-menu-item">
											<a href="{{route('sub-category.show', 40)}}" >
												<span class="links-text">Headlights</span>
											</a>
                    </li>
                    
                    <li class="menu-item top-menu-item">
											<a href="{{route('sub-category.show', 186)}}" >
												<span class="links-text">Car Polish & Wax</span>
											</a>
										</li>
                    
									</ul>
								</div>

								<div class="third-menu" style="display:none">
									<div class="top-menu top-menu-14">
										<ul class="j-menu">
											<!--<li class="menu-item top-menu-item top-menu-item-1 dropdown drop-menu">
												<a href="index465d.html?route=product/catalog"  class="dropdown-toggle" data-toggle="dropdown">
													<span class="links-text">More Menus</span>
												</a>
												<div class="dropdown-menu j-dropdown">
													<ul class="j-menu">
														<li class="menu-item top-menu-item-2">
															<a>
																<span class="links-text">The Best Menu</span>
															</a>
														</li>
														<li class="menu-item top-menu-item-3">
															<a>
																<span class="links-text">Options You Will</span>
															</a>
														</li>
														<li class="menu-item top-menu-item-4">
															<a>
																<span class="links-text">Ever Find</span>
															</a>
														</li>
														<li class="menu-item top-menu-item-5">
															<a>
																<span class="links-text">In a Theme</span>
															</a>
														</li>
													</ul>
												</div>
											</li>-->
											<li class="menu-item top-menu-item">
												<a href="http://www.wasap.my/+923224677726" >
													<span class="links-text"><i class="fab fa-whatsapp" style="padding-right: 10px"></i>+92 322 46 77 726</span>
												</a>
											</li>
										</ul>
									</div>
                </div>
                
              </div>
              <div class="row" style="display: flex; justify-content: center; align-items:center; background: white">
                <div class="col-md-2" style="display: flex; justify-content: center; align-items:center">
                  <a href="/" style="display: flex; justify-content: center; align-items:center; min-height: 150px"><img src="/images/logo.png" alt="" style="width:80%; margin:auto"></a>
                </div>
                <div class="col-md-10">
							<div class="mid-bar navbar-nav" style="    max-width: 1340px;">
								<div class="desktop-logo-wrapper" style="display:none">
									<div id="logo" style="transform: scale(1); margin-top: -12px;">
										<a href="/">
											<img src="/images/logo.png" srcset="/images/logo.png 1x, /images/logo.png 2x" width="170" height="70" alt="MegaMotorSports.Pk" title="MegaMotorSports.Pk" style="margin:auto; left: 0 !important; position: relative  !important; margin: auto !important"/>
										</a>
									</div>
								</div>
								<div class="desktop-search-wrapper full-search default-search-wrapper">
									<div id="search" class="dropdown">
										<button class="dropdown-toggle search-trigger" data-toggle="dropdown"></button>
										<div class="dropdown-menu j-dropdown">
											<form action="{{url('/products/search')}}" method="GET" style="widht: inherit; height: inherit">
                      <div class="header-search">
												<input type="text" name="keyword" value="" placeholder="Search By Make Model Year, Product Type, Part Number, or Brand...." class="search-input"/>
												<button type="submit" class="search-button" data-search-url=""></button>
											</div>
                      </form>
										</div>
									</div>
								</div>
								<div class="classic-cart-wrapper">
									<div class="top-menu secondary-menu">
										<div class="top-menu top-menu-240">
											<ul class="j-menu">
												<li class="menu-item top-menu-item top-menu-item-1">
                        @if(auth()->user())
													<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter" style="color: #363d47">
														<span class="links-text text-center">My Profile</span>
                            </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                          @else
                          <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter" style="color: #363d47">
														<span class="links-text">Login</span>
													</a>
                          @endif
												</li>
											</ul>
										</div>
									</div>
									<div class="desktop-cart-wrapper default-cart-wrapper">
										<div id="cart" class="dropdown">
											<a data-toggle="dropdown" data-loading-text="Loading..." class="dropdown-toggle cart-heading" href="javascript:void(0)">
												{{--<span id="cart-total">@if(session()->has('cart'))--}}
                        {{--{{ session('cart')->getTotalQty() }}  --}}
                        {{--@else--}}
                        {{--0--}}
                        {{--@endif--}}
                        {{--item(s)</span>--}}
												<i class="fa fa-shopping-cart"></i>
												<span id="cart-items" class="count-badge">
                        @if(session()->has('cart'))
                        {{ session('cart')->getTotalQty() }}
                        @else
                        0
                        @endif</span>
                      </a>
                      <div id="cart-content" class="dropdown-menu cart-content j-dropdown">
                      <ul>
                          <li class="cart-products">
                            <table class="table">
                              <tbody>
                              @if(session()->has('cart'))
                              @foreach(session()->get('cart')->getContents() as $key => $data)
                                <tr>
                                  <td class="text-center td-image">
                                    <a href="javascript:void(0)">
                                      <img src="{{ $data['product']->images()->first() ? asset('main/storage/app/public/'.$data['product']->images()->first()->image) :'/admin/img/no-photo.png'}}" alt="{{ $data['product']->name }}" title="{{ $data['product']->name }}" width="60" height="60">
                                    </a>
                                  </td>
                                  <td class="text-left td-name" style="font-size: unset;"><a href="javascript:void(0)">{{ $data['product']->name }} <br> {{$data['qty']}} x {{ $data['product']->discount_price }}</a><br></td>
                                  <td class="text-center td-remove" style="width: 23%; font-size: smaller">RS : {{ $data['total_price'] }}</td>
                                </tr>
                              @endforeach
		                          @endif
                            </tbody>
                          </table>
                        </li>

                        <li class="cart-totals">
                           <div>
                          <table class="table table-bordered">
                            <tbody>
                            @if(session()->has('cart'))
                              <tr>
                                <td class="text-right td-total-title">Sub-Total</td>
                                <td class="text-right td-total-text">PKR {{session()->get('cart')->getTotalPrice()}}</td>
                              </tr>
                                @endif
                            </tbody>
                          </table>
                          </div>
                           @if(session()->has('cart'))
                          <div class="cart-buttons">
                          <a href="{{route('products.cart')}}" class='btn btn-primary' style='color: white; font-size: unset;'>View Cart</a>
                          <a href="{{route('products.checkout')}}" class='btn btn-danger' style='color: white; font-size: unset;'>Checkout</a>
                          </div>

                            @else
                          <p class="text-center cart-empty">Your shopping cart is empty!</p>
                          @endif
                      </li>
                    </ul>
											</div>
                    </div>
                    
                  </div>
                  <li class="menu-item top-menu-item" style="list-style: none; margin-left: 20px">
                        <a href="http://www.wasap.my/+923224677726" style="color: var(--blue-color); font-size: unset"><img src="/images/whatsapp-icon.png" width="30" height="30" style="margin-left:50px !important"><br>+92 322 46 77 726</a> 
                        </li>
								</div>
							</div>
							<div class="desktop-main-menu-wrapper menu-default has-menu-2 navbar-nav" style="max-width: fit-content; width: 1366px !important; transform: translate(-59%); left: 50%; top: 110px; position: absolute;">
								<div class="menu-trigger menu-item main-menu-item">
									<ul class="j-menu">
										<li>
											<a>Menu</a>
										</li>
									</ul>
								</div>
								<div id="main-menu" class="main-menu main-menu-3">
									<ul class="j-menu">
                  <li class="menu-item main-menu-item main-menu-item-2 mega-menu dropdown drop-menu menu-mg">
											<a href="/"   class="dropdown-toggle" data-toggle="dropdown" >
												<span class="links-text"><img src="/images/home-ico.png" class="homeicon"><span class="hometext">Home</span></span>
											</a>

                  </li>
                  @foreach($allCategories as $category)
										<li class="menu-item main-menu-item main-menu-item-2 mega-menu dropdown drop-menu menu-mg">
											<a href="javascript:void(0)"   class="dropdown-toggle" data-toggle="dropdown" >
												<span class="links-text"><img class="imgCat" src="{{$category->image ? asset('main/storage/app/public/'.$category->image) :'/admin/img/no-photo.png'}}" width="35" height="25">{{ $category->name }}</span>
												<span class="open-menu collapsed" data-toggle="collapse" data-target="#{{ $category->id }}" >
													<i class="fa fa-plus"></i>
												</span>
											</a>
											<div class="dropdown-menu j-dropdown " id="{{ $category->id }}">
                      <div class="mega-menu-content menu-mg-c">
                      <h2 style="margin-left: 54px !important; padding-top: 25px; font-weight: bold">{{ $category->name }}</h2>
                      <ul class="ptype-grid -col-9" style="margin-top: 0 !important; display: flex; justify-content: center;">

                      @foreach($category->childrens as $subCategory)
                        <li class="li" data-lazy-load-enabled="1">
                          <img class="ptype-grid-img lazy-loaded" src="{{$subCategory->image ? asset('main/storage/app/public/'.$subCategory->image) :'/admin/img/no-photo.png'}}" data-src="{{$subCategory->image ? asset('main/storage/app/public/'.$subCategory->image) :'/admin/img/no-photo.png'}}" alt="{{ $subCategory->name }}" style="opacity: 1;">
                          <a class="ptype-grid-a -simple-title" href="{{route('sub-category.show', $subCategory->id)}}">
                          {{ $subCategory->name }}
                          </a>
                        </li>
                        @endforeach
                      </ul>



                        </div>
                        <ul class="j-menu menu-mg-d">
                        @foreach($category->childrens as $subCategory)
													<li class="menu-item menu-item-c105">
														<a href="{{route('sub-category.show', $subCategory->id)}}"  >
															<span class="links-text"> <img src="{{$subCategory->image ? asset('main/storage/app/public/'.$subCategory->image) :'/admin/img/no-photo.png'}}" width="35" height="25"> {{ $subCategory->name }}</span>
															<span class="count-badge ">3</span>
														</a>

													</li>
                          @endforeach
												</ul>
											</div>
										</li>
                    @endforeach

									</ul>
								</div>
								<div id="main-menu-2" class="main-menu main-menu-64" style="display: none;">
									<ul class="j-menu">
										<li class="menu-item main-menu-item main-menu-item-1 multi-level drop-menu " >
											<a href="tel:18005556787" >
												<span class="links-text">1.800.555.6789</span>
											</a>
										</li>
										<li class="menu-item main-menu-item main-menu-item-2 multi-level drop-menu " >
                    @if(auth()->user())
													<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">
														<span class="links-text text-center" >My Profile</span>
                            </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                          @else
                          <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">
														<span class="links-text">Login</span>
													</a>
                          @endif
										</li>
									</ul>
								</div>
              </div>
              </div>{{--- Col 10--}}
              </div>{{--- Row--}}
						</div>
						<div class="mobile-header mobile-default mobile-1">
							<div class="mobile-top-bar">
								<div class="mobile-top-menu-wrapper" style="width: 100%">
									<div class="top-menu top-menu-13" style="width: 100%">
										<ul class="j-menu" style="width: 100%">
											<li class="menu-item top-menu-item top-menu-item-1">
                      @if(auth()->user())
													<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">
														<span class="links-text text-center">My Profile</span>
                            </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                          @else
                          <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">
														<span class="links-text">Login</span>
													</a>
                          @endif
											</li>
                      <li class="menu-item top-menu item" style="margin-left: auto"><a href="http://www.wasap.my/+923224677726">
														<span class="links-text text-center" style="font-weight: bold; font-size: 110%"><i class="fab fa-whatsapp" style="padding-right: 10px"></i>+92 322 46 77 726</span>
                            </a></li>
										</ul>
									</div>
								</div>
								<div class="language-currency top-menu">
									<div class="mobile-currency-wrapper"></div>
									<div class="mobile-language-wrapper"></div>
								</div>
							</div>
							<div class="mobile-bar sticky-bar">
								<div class="mobile-logo-wrapper"></div>
								<div class="mobile-bar-group">
									<div class="menu-trigger"></div>
									<div class="mobile-search-wrapper mini-search"></div>
									<div class="mobile-cart-wrapper mini-cart"></div>
								</div>
							</div>
						</div>
          </header>
         <div class="bootstrap-menu">

         <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">

  <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
  <div class="modal-dialog modal-dialog-centered" role="document">


    <div class="modal-content">
      <div class="modal-body">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="text-center">
        @if(auth()->user())
        <h4 style="font-weight: normal">{{ auth()->user()->name }}</h4>
        <img src="/images/user-circle.png" width="85" height="85" class="mb-3">
        <a href="javascript:void(0)" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-large btn-primary d-flex p-1 mt-2">{{ __('Logout') }}</a>



        @else
        <h4 style="font-weight: normal">My Account</h4>
        <img src="/images/user-circle.png" width="85" height="85" class="mb-3">
        <a href="/login" class="btn btn-large btn-primary d-flex p-1 mt-2">Sign In</a>
        <h5>New Customer? <a href="/login" class="btn btn-secondary" style="font-weight: normal">Sign Up</a></h5>

        @endif
        </div>
      </div>
    </div>
  </div>
</div>

         </div>
          @yield('content')

          @include('frontend.partials.footer')

        </div>

      <script src="/js/c0eb1eb5cc0326400b3644f4bc2c870e.js"  defer ></script>
			<!-- Footer -->
     
  
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<!--<script src="{{ mix('js/app.js') }}"></script>-->
<!--<script src="/js/mburger.js"></script>
<script src="/js/mmenu.js"></script>-->

<script type="text/javascript">
var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="/images/top-button.png" width="50" height="50"/>',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();
</script>


<!--
<script>
$(window).scroll(function(){
    if ($(window).scrollTop() >= 450) {
        if($(window).width() <= 922)
        {
          $('#hdd').addClass('sticky-header');
        }

    }
    else {
        $('#hdd').removeClass('sticky-header');
    }
});</script>
-->
<!--<script>
    document.addEventListener("DOMContentLoaded", function(event) { 
        var scrollpos = localStorage.getItem('scrollpos');
        if (scrollpos) window.scrollTo(0, scrollpos);
    });

    window.onbeforeunload = function(e) {
        localStorage.setItem('scrollpos', window.scrollY);
    };
</script>-->

<script>
 if ($(window).width() < 1028) {
     $('.menu-mg').addClass('multi-level');
     $('.menu-mg').removeClass('mega-menu');
     $('.menu-mg-c').addClass('dd-none');
     $('.menu-mg-d').removeClass('dd-blocks');
    }
    else
    {
     $('.menu-mg-c').removeClass('dd-none');
     $('.menu-mg-d').addClass('dd-none');
    };
   

</script>
<script>
/*
    if(window.location.href.indexOf("products") > -1 && window.location.href.indexOf("cart") <= 0) {
        jQuery("body").css("display","block");
    }else{
        jQuery("body").css("display","none");
    }*/
</script>
<script type="text/javascript">
   /* if(window.location.href.indexOf("products") > -1 && window.location.href.indexOf("cart") <= 0) {
        jQuery("body").css("display","block");

    }else{
        jQuery(document).ready(function(){
          //  delay();
        });

        function delay() {
            var secs = 100;
            setTimeout('initFadeIn()', secs);
        }

        function initFadeIn() {
          //  jQuery("body").css("visibility","visible");
           // jQuery("body").css("display","none");
            //jQuery("body").fadeIn(1200);
        }
    }
*/
</script>
<script>
    function submitSearchForm(){
        document.getElementById("search-form").submit();
    }

    $(document).ready(function() {
        $(document).on('change', '.make', function () {
            let make_id = $(this).val();
            if (make_id) {
                $.ajax({
                    url: '/products/' + make_id + '/models',
                    type: 'get',
                    success: function (response) {
                        $('.model').html(response);
                    }
                });
            }

        })
    });
</script>

@yield('scripts')
<script>
vw = $(window).outerWidth();

$('#cart').click(flip);

if (vw > 768) {
 $('.card').hover(
function() {
  $('.description').toggleClass('show');
  $('.image-wrapper').toggleClass('shrink');
}
)
}

function flip() {
  $('#cart').addClass('flipped');
  $('#cart').addClass('added');
  $('.backside').addClass('show');
  $('.front').addClass('hide');
}


</script>
</body>
</html>
