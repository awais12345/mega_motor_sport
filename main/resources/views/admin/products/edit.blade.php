<form method="POST" action="{{route('admin.products.update', $product->id)}}" enctype="multipart/form-data">

    {{ csrf_field() }}
    <div class="modal-body" id="edit-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="name">Name <sup>*</sup></label>
                    <input id="name" type="text" class="form-control" name="name"
                           value="{{$product->name}}" required>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="category">Category <sup>*</sup></label>
                    <select multiple id="category" class="form-control select2 category" name="category_ids[]" required>
                        <option value="">Select</option>
                        @foreach($categories as $category)
                            <option
                                value="{{$category->id}}" {{ count($product->categories) && in_array($category->id , $product->categories()->pluck('categories.id')->toArray()) ? 'selected' : ''}}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="sub-category">Sub Category <sup>*</sup></label>
                    <select multiple id="sub-category" class="form-control select2 sub-category" name="sub_category_ids[]" required>
                        <option value="">Select</option>
                        @foreach($subCategories as $subCategory)
                            <option
                                value="{{$subCategory->id}}"
                                {{ count($product->subCategories) && in_array($subCategory->id , @$product->subCategories()->pluck('sub_categories.id')->toArray()) ? 'selected' : ''}}>{{ $subCategory->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="price">Price <sup>*</sup></label>
                    <input id="price" type="text" class="form-control" name="price"
                           value="{{$product->price}}" required>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="discount-price">Discount Price </label>
                    <input id="discount-price" type="text" class="form-control"
                           name="discount_price" value="{{$product->discount_price}}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="qty">Qty</label>
                    <input id="qty" type="number" min="0" class="form-control" name="qty"
                           value="{{$product->qty}}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="make">Make</label>
                    <select id="make" class="form-control select2 make" name="make_ids[]" multiple>
                        <option value="">Select</option>
                        @foreach($makes as $make)
                            <option
                                value="{{$make->id}}" {{ count($product->makes) && in_array($make->id, $product->makes()->pluck('makes.id')->toArray()) ? 'selected' : ''}}>{{ $make->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="model">Model</label>
                    <select id="model" class="form-control select2 model" name="make_model_ids[]" multiple>
                        <option value="">Select</option>
                        @foreach($models as $model)
                            <option
                                value="{{ $model->id }}" {{count($product->makeModels) && in_array($model->id, $product->makeModels()->pluck('make_models.id')->toArray()) ? 'selected' : ''}}>{{ $model->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="year">Year</label>
                    <select id="year" class="form-control select2" name="years[]" multiple>
                        <option value="">Select</option>
                        @foreach(getYears() as $year)
                            <option
                                value="{{$year}}" {{count($productYears) && in_array($year, $productYears) ? 'selected' : ''}}>{{$year}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        {{--        <div class="row">--}}
        {{--            <div class="col-sm-4">--}}
        {{--                <div class="form-group">--}}
        {{--                    <label for="color">Color</label>--}}
        {{--                    <input id="color" type="text" name="color" class="form-control"--}}
        {{--                           value="{{$product->color}}">--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="col-sm-4">--}}
        {{--                <div class="form-group">--}}
        {{--                    <label for="size">Size</label>--}}
        {{--                    <input id="size" type="text" name="size" class="form-control"--}}
        {{--                           value="{{$product->size}}">--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="size">Size</label>
                    <select class="form-control select2" name="size" id="size">
                        <option value="">Select</option>
                        <option value="x-small" {{$product->size == 'x-small' ? 'selected' : '' }}>X-Small</option>
                        <option value="small" {{ $product->size == 'small' ? 'selected' : '' }}>Small</option>
                        <option value="medium" {{ $product->size == 'medium' ? 'selected' : '' }}>Medium</option>
                        <option value="large" {{ $product->size == 'large' ? 'selected' : '' }}>Large</option>
                        <option value="x-large" {{ $product->size == 'x-large' ? 'selected' : '' }}>X-Large</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="vendor">Vendor</label>
                    <input type="text" name="vendor" id="vendor" class="form-control" value="{{ $product->vendor }}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="video_url">Video URL</label>
                    <input type="text" name="video_url" id="video_url" class="form-control" value="{{ $product->video_url }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="shipping_charges">Shipping Charges</label>
                    <input type="text" name="shipping_charges" id="shipping_charges" class="form-control" value="{{$product->shipping_charges}}">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="sku">SKU Code</label>
                    <input type="text" name="sku" id="sku" class="form-control" value="{{$product->sku}}" disabled>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="is_avaliable">Stock Status</label>
                    <select id="is_avaliable" class="form-control select2 is_avaliable" name="is_avaliable">
                        <option value="1" {{$product->is_available ? 'selected' : ''}}>Available Stock</option>
                        <option value="0" {{!$product->is_available ? 'selected' : ''}}>Out of Stock</option>
                    </select>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea id="description" name="description"
                              class="form-control wysiwyg">{!! $product->description  !!}</textarea>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="is_free_shipping">Is Free Shipping ?</label>
                    <input type="checkbox" name="is_free_shipping" id="is_free_shipping"
                           class="checkbox checkbox-primary"
                           value="1"
                           {{ $product->is_free_shipping ? 'checked' : '' }}
                    >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="img" id="customFile">
                    <label class="custom-file-label"  for="customFile">Choose Timeline image</label>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-sm-12">
            Images
                <div class="input-images-2"></div>
            </div>
            

                @if($product->img)
                    <div class="col-12">
                        <h2>Timeline Image</h2>
                        <img
                            src="{{asset('main/storage/app/public/'.$product->img)}}"
                            id="profile-img-tag" class="img-profile" alt="not found" style="height: 200px;width:100%">
                    </div>
                @endif

        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</form>
<script>
$(document).ready(function(){
    let preloaded = [
            @foreach($product->images as $image)
    {id: {{ $loop->index }}, src: "{{asset('main/storage/app/public/'.$image->image)}}"},
    @endforeach
];

$('.input-images-2').imageUploader({
    preloaded: preloaded,
    imagesInputName: 'images',
    preloadedInputName: 'old'
});
});
</script>
