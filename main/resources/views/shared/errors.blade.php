<div class="alerts mt-5">
@if (Session::has('info'))
	<div class="alert alert-info alert-dismissible" style="font-size: initial;">
            <span type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</span>
            {!!  Session::get('info') !!}
	</div>
@endif

@if (Session::has('error'))
<div class="alert alert-danger alert-dismissible" style="font-size: initial;">
	<span type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</span>
	{!!  Session::get('error') !!}
</div>
@endif

@if (Session::has('success'))
<div class="alert alert-success alert-dismissible" style="font-size: initial;">
	<span type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</span>
	{!!  Session::get('success') !!}
</div>
@endif

@if (Session::has('warning'))
<div class="alert alert-warning alert-dismissible" style="font-size: initial;">
    <span type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</span>
    {!!  Session::get('warning') !!}
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible" style="font-size: initial;">
    <span type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</span>

      @foreach($errors->getMessages() as $errors)
    	@foreach ($errors as $error)
    		{!! $error !!}<br>
    	@endforeach
    @endforeach

</div>
@endif
</div>
