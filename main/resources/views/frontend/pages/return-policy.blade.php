@extends('frontend.master')
@section('styles')
    <!--<link rel="stylesheet" href="/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Bhaina+2:wght@500&display=swap" rel="stylesheet">
    <style>
  .hero-section
        {
            height: 200px;
            display: flex;
            align-items: center;
            justify-content: center;
            overflow: hidden
        }
        .hero-section img
        {
            position: absolute;
            z-index: -1;
            filter: blur(4px);
            height: inherit;
            width: 100% !important
        }
        .bg
        {
            position: absolute;
            height: 220px;
            width: 100%;
            filter: blur(2px);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            z-index: -1;
            background: url('/images/her-bg.jpg')
        }

    </style>
@endsection

@section('content')
<div class="bootstrap-menu">
        <div class="hero-section">
            <div class="bg"></div>
            <h2 class="text-uppercase">Return Policy</h2>
        </div>
        <div class="container mt-5 pt-2">
           <div class="row">
           <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-10">
                    <h3 style="margin-top: 0;">Returns Policy</h3>
                    <ol class="custom-counter">
                        <li>If your product is defective / damaged or the order is Incorrect/Incomplete at the time of delivery, please contact us within the applicable time period of return. Your product may be eligible for refund or replacement depending on the product category and condition. Please see the detailed terms in the relevant category below</li>
                        <li>Please note that not all products are eligible for a return, if the product is "No longer needed"</li>
                        <li>For device related issues after usage or the expiration of the return window, we will refer you to the brand warranty center (if applicable). For more information on warranty claims please view our<a href="/terms-conditions"> Warranty Policy</a></li>

                    </ol>
                    <p>You will always find the relevant terms on the product page (Return Policy tab).</p>
                </div>
            </div>


            




            <div class="row">
                <div class="col-sm-10">
                    <h3>Valid reasons to return an item</h3>
                    <ol class="custom-counter">
                        <li>Delivered Product is damaged (physically destroyed or broken) / defective (dead on arrival)</li>

                        <li>Delivered Product is Incorrect (different than the one presented on our website) / Incomplete (missing parts)</li>

                        <li>
                            Delivered Product is “No longer needed”* (implies that you no longer have a use for the product / you have changed your mind about the purchase / the size of a fashion product does not fit / you do not like the product after opening the package) *Eligible for selected products only
                        </li>
                    </ol>
                </div>
            </div>

        </div><div class="col-sm-4" style="padding-left: 0;">

<div class="boxed">
    <h3 style="text-align: center;">Conditions for Returns</h3>

    <ol class="olclass" style="margin-top: -4px;">
        <li>The product must be unused, unworn, unwashed and without any flaws. Fashion products can be tried on to see  if they fit and will still be considered unworn</li>

        <li>The product must include the original tags, user manual, warranty cards, freebies and accessories</li>
        <li>The product must be returned in the original and undamaged manufacturer packaging / box. If the product was delivered in a second layer of SehgalMotors.Pkpackaging, it must be returned in the same condition with return shipping label attached. Do not put tape or stickers on the manufacturers box</li>

    </ol>

</div>


<div class="notice">
    <p>If a product is returned to us in an inadequate condition, we reserve the right to send it back to you.</p>
</div>


</div>
           </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
@endsection
