<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = Role::query()->create([
            'name' => 'Admin',
            'slug' => Str::slug('admin'),
        ]);

        $customer_role = Role::query()->create([
            'name' => 'customer',
            'slug' => Str::slug('customer'),
        ]);

        $user = User::query()->create([
            'name' => 'Muhammad Talha',
            'email' => 'admin@admin.com',
            'password' => bcrypt('000000')
        ]);

        $user->roles()->sync($admin_role->id);

        $user = User::query()->create([
            'name' => 'Candidate',
            'email' => 'candidate@example.com',
            'password' => bcrypt('000000')
        ]);

        $user->roles()->sync($customer_role->id);
    }

}
