@extends('frontend.master')
@section('styles')
    <!--<link rel="stylesheet" href="/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Bhaina+2:wght@500&display=swap" rel="stylesheet">
    <style>
         .hero-section
        {
            height: 200px;
            display: flex;
            align-items: center;
            justify-content: center;
            overflow: hidden
        }
        .hero-section img
        {
            position: absolute;
            z-index: -1;
            filter: blur(4px);
            height: inherit;
            width: 100% !important
        }
        .bg
        {
            position: absolute;
            height: 220px;
            width: 100%;
            background-position: center bottom;
            z-index: -1;
            background: url('/images/her-bg.jpg')
        }



    </style>
@endsection

@section('content')
<div class="bootstrap-menu">
        <div class="hero-section">
            <div class="bg"></div>
            <h2 class="text-uppercase">FAQs Section</h2>
        </div>
        <div class="container mt-5 mb-5">
            <h5>Here are few Most Frequently Answered Questions, if you have any other query, feel free to contact us.</h5>
              <!--Accordion wrapper-->
<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

<!-- Accordion card -->
<div class="card">

  <!-- Card header -->
  <div class="card-header" role="tab" id="headingOne1">
    <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
      aria-controls="collapseOne1">
      <h5 class="mb-0" style="width: 100%">
      How can I pay for the product(s)?  <i class="glyphicon glyphicon-chevron-down" style="margin-right:0"></i> 
      </h5>
    </a>
  </div>

  <!-- Card body -->
  <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
    data-parent="#accordionEx">
    <div class="card-body">
    There are four methods through which you can pay on SehgalMotors.pk by using your debit/credit cards, EasyPaisa mobile accounts or via any EasyPaisa Shop, though the fourth method Cash on Delivery (COD) is only available in a few selected cities. For more details visit our Payment Options page.
    </div>
  </div>

</div>
<!-- Accordion card -->


<!-- Accordion card -->
<div class="card">

  <!-- Card header -->
  <div class="card-header" role="tab" id="headingOne1">
    <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
      aria-controls="collapseOne1">
      <h5 class="mb-0" style="width: 100%">
      How can I track the product(s) I have ordered?  <i class="glyphicon glyphicon-chevron-down" style="margin-right:0"></i> 
      </h5>
    </a>
  </div>

  <!-- Card body -->
  <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
    data-parent="#accordionEx">
    <div class="card-body">
    Yes, product(s) can be returned if and only if the returned commodity fulfills our Returns Policy. To know about our returns policy kindly visit the Returns Policy page, for returning a product go to our Returns page. To know about how to obtain a refund go to our Refunds Policy page.
    If you have any specific queries Contact Us
    </div>
  </div>

</div>
<!-- Accordion card -->


</div>
<!-- Accordion wrapper -->
        </div>
    </div>
    </div>

@endsection

@section('scripts')
@endsection
