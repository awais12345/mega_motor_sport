<form method="POST" action="{{route('admin.sub-categories.update', $subCategory)}}" enctype="multipart/form-data">

    {{ csrf_field() }}

    <div class="modal-body">
        <div class="form-group">
            <label for="parent">Parent Category <sup>*</sup></label><br>
            <select id="parent" name="parent_category" class="form-control select2" required>
                <option value="">Select</option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}" {{$subCategory->parent &&
                        $subCategory->parent->id == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="name">Name <sup>*</sup></label>
            <input id="name" type="text" class="form-control" value="{{$subCategory->name}}" name="name" required>
        </div>

        <div class="form-group">
            <label for="name">Is Free Shipping</label>
            <input id="is_free_shipping" type="checkbox"
                   class="checkbox checkbox-primary"
                   value="1"
                   {{ $subCategory->is_free_shipping ? 'checked' : ''}}
                   name="is_free_shipping" >
        </div>

        <div class="custom-file">
            <input type="file" class="custom-file-input" name="image" id="customFile">
            <label class="custom-file-label" for="customFile">Choose Image</label>
        </div>

        <div class="form-group mt-2">
            <img src="{{$subCategory->image ? asset('main/storage/app/public/'.$subCategory->image) :'/admin/img/no-photo.png'}}" id="profile-img-tag" class="img-profile" alt="not found" style="height: 200px;width:100%">
        </div>

    </div>


    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</form>
