<header class="head_header">
    <nav class="head-stores-h">
        <div class="wrap">
            <ul class="head-stores js-head-stores" style="overflow: hidden">
            <li><a class="imgwhitelogo" href="/"><img src="/images/logo-white.png" alt="" width="60" height="30"></a></li>
                @foreach(\App\Models\SubCategory::query()->latest()->limit(6)->get() as $subCategory)

                    {{--                <li>--}}
                    {{--                    <div class="item head-stores-icon -active -i1-15">Car &amp; Truck</div>--}}
                    {{--                </li>--}}
                    <li><a href="{{ route('sub-category.show', $subCategory->id) }}"
                           class="item head-stores-icon -i5-0 js-new-tab">{{ $subCategory->name }}</a></li>
                @endforeach
            </ul>
            <script class="js-header-stores-template" type="text/x-handlebars-template">
                <div class="head-stores-dd">
                    <ul class="items">
                        <li><span data-hlk="#" class="item head-stores-icon -i1-31 js-new-tab">Semi Truck</span>
                        </li>
                        <li><span data-hlk="#"
                                  class="item head-stores-icon -i1-6 -hot js-store-link-block js-new-tab">Motorcycle</span>
                        </li>
                        <li><span data-hlk="#" class="item head-stores-icon -i1-20 js-new-tab">Powersports</span>
                        </li>
                        <li><span data-hlk="#" class="item head-stores-icon -i3-0 js-new-tab">RV / Camper</span>
                        </li>
                        <li><span data-hlk="#" class="item head-stores-icon -i4-0 js-new-tab">Boating</span></li>
                        <li><span data-hlk="#" class="item head-stores-icon -i2-0 js-new-tab">Recreation</span></li>
                        <li><span data-hlk="#" class="item head-stores-icon -i5-0 js-new-tab">Tools</span></li>
                    </ul>
                </div>
            </script>
        </div>
    </nav>
    <div style="z-index: 1200;">

        <nav id="sidenav" style="overflow: hidden;">
            <ul>
                <li><a href="/">Home</a></li>
                {{--                    <li class="active"><a href="javascript:void(0)" style="text-align: center; font-weight: bold;">Departments</a>--}}
                {{--                    </li>--}}
                @foreach($allCategories as $category)
                    <li><span> <img
                                src="{{$category->img ? asset('main/storage/app/public/'.$category->img) :'/admin/img/no-photo.png'}}"
                                class="img-icon"> {{ $category->name }}</span>
                        <ul>
                            @foreach($category->childrens as $subCategory)
                                <li><a href="{{route('sub-category.show', $subCategory->id)}}"><img
                                            src="{{$subCategory->image ? asset('main/storage/app/public/'.$subCategory->image) :'/admin/img/no-photo.png'}}"
                                            class="img-icon">{{ $subCategory->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach

                <li></li>
                <li><a href="/login" style="text-align: center; font-weight: bold;">Login / Register</a></li>
                <li><a href="/products/search" style="text-align: center; font-weight: bold;">Search</a></li>
                {{--                    <li><a href="{{route('products.create')}}" style="text-align: center; font-weight: bold;">Add--}}
                {{--                            Product</a></li>--}}
            </ul>
        </nav>
	 <nav id="cartmenu" style="overflow: hidden;">
                    <ul>
                        <li class="text-center"><h4 style="padding: 10px; font-size: 16px !important; margin: auto;">Shopping Cart</h4></li>
                        <li>
                                    
                            <table style="width: 100%;">
		@if(session()->has('cart'))
		@foreach(session()->get('cart')->getContents() as $key => $data)
<form method="GET" action="{{ route('products.update.cart', $data['product']->id) }}">
<tr>
                                <td style="width: 20%; padding: 7px;"><img src="{{ $data['product']->images()->first() ? asset('main/storage/app/public/'.$data['product']->images()->first()->image) :'/admin/img/no-photo.png'}}" width="60" height="60"></td>
                                <td style="text-align: left;"><p style="line-height: 20px; padding-top: 5px;">{{ $data['product']->name }}</p></td>
                                <td style="line-height: 55px; width: 10%;"><a href="{{ route('products.remove.cart', $data['product']->id) }}" style="padding: 8px;"><i class="fas fa-times"></i></a></td>
                                </tr>
 </form>
           		@endforeach
		@endif
                                
                            </table>                                    
                        
                    </li>
                        

                    </ul>
                </nav>
    </div>
    <div class="header-top">

        <div class="wrap">
            <a class="head_logo_a -carid -dnone" href="/">MEGA MOTORS</a>
            <span><a class="left-menu-icon mburger" href="#sidenav">
                        <b></b>
                        <b></b>
                        <b></b>
                    </a></span>
            @if(session()->has('cart'))
                <div class="row custom-cart float-right">
	<style>
	.cart-iconnn
{
font-size: 20px !important;
}
@media only screen and (max-width: 744px) {
  .cart-iconnn {
    margin-top: 11px !important;
  }
}

</style>
                    <a href="#cartmenu" id="cart" class="cart-iconnn" style=" color:var(--red-color);"><i class="fas fa-cart-arrow-down"></i><span
                            class="badge">{{ session('cart')->getTotalQty() }}</span></a>
                </div>
            @endif
            {{--            <ul class="nav-tool-wrap">--}}

            {{--                <li class="header-dd-hover-element -with-padding js-garage-header-menu" aria-haspopup="true">--}}
            {{--                    <div class="nav-tool -garage -arrow">--}}
            {{--                        <span class="js-nav-tool-title -txt -arrow -without-content">Garage</span>--}}
            {{--                        <span class="icon -icon-garage"><span--}}
            {{--                                class="count-item -type-1 js-my-garage-counter"></span>--}}
            {{--                            </span>--}}
            {{--                        <div class="header-garage-subtitle js-header-garage-mmy"></div>--}}
            {{--                    </div>--}}
            {{--                    <div class="mygarage-dd header-dd-h">--}}
            {{--                        <div class="head-dd-loader js-my-garage-dd-loader"></div>--}}
            {{--                        <div class="js-my-garage-dd-content" data-vehicle-garage-id="15"></div>--}}
            {{--                    </div>--}}
            {{--                </li>--}}

            {{--            </ul>--}}

            <div class="header-account header-dd-hover-element js-account-header-menu">
                <div class="nav-tool">
                    <span class="js-nav-tool-title -txt -arrow">
                        @if(auth()->user())
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                        {{ auth()->user()->name }}
                    </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
<a class="dropdown-item" href="{{ route('logout') }}"
   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>                    </div>
                        @else
                            <a href="/login" class="link">Login / Register</a>
                        @endif
                    </span>

                </div>
                <div class="header-account-dd header-dd-h js-account-dd-holder">
                    <div class="head-dd-loader"></div>
                </div>
            </div>
            <label class="header-search-btn js-search-btn" data-toggle-class="search-show">
                <form action="{{url('/products/search')}}" method="GET">
                    <input class="search-form-field" name="keyword" type="text"
                           style="position: absolute; margin-top: -39px; width: 91%;" placeholder="Search....">
                    <button type="submit" class="search-form-button"><i class="fas fa-search srch-btn"></i></button>
                </form>
            </label>
        </div>
    </div>
    <nav class="head-dd-h">
        <div class="wrap">
            <div class="head-dd-help-center">
                <div
                    class="help-center-nav-btn -arrow -arrow-red header-dd-hover-element js-help-center-header-menu"
                    aria-haspopup="true">
                    {{--                    <span>Help Center</span>--}}
                    {{--                    <div class="header-help-center-dd header-dd-h js-help-center-dd-holder">--}}
                    {{--                        <div class="head-dd-loader"></div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
            <ul class="head-dd js-head-depts">
                <li class="it js-head-dept" data-id="407" data-vehicle-id="15" data-store-id="0">
                    <span class="head-dd-main"><a class="link" href="/">Home</a></span>
                </li>
                @foreach(\App\Models\SubCategory::query()->limit(8)->get() as $subCategory)
                    <li class="it js-head-dept" data-id="407" data-vehicle-id="15" data-store-id="0">
                        <span class="head-dd-main"><a class="link"
                                                      href="{{route('sub-category.show', $subCategory->id)}}">{{ $subCategory->name }}</a></span>
                    </li>
                @endforeach

            </ul>
        </div>
    </nav>
    <div class="head-dd-cont-holder js-head-dd-cont-holder">
        <div class="wrap">
            <div class="head-dd-cont-holder-close js-head-dd-cont-holder-close"></div>
        </div>
    </div>
</header>
