<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'slug'];

    /**
     * @param $slug
     * @return |null
     * @throws \Exception
     */
    public static function findBySlug($slug)
    {
        $role = static::where('slug', $slug)->first();
        if ($role) {
            return $role;
        }

        throw new \Exception('role not found against ' . $slug . ' slug');
    }
}
