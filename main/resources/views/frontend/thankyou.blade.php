@extends('frontend.master')
@section('title'){{'Thank-you - MegaMotorSports.PK'}}@stop

@section('content')
    <div class="container">

        <div class="row" style="margin-top: 10%; margin-bottom: 5%">
            @include('shared.errors')

            <div class="col-12">
                <h1 >THANK YOU!</h1><br>
                <h3 >WE HAVE RECEIVED YOUR ORDER!</h3>
            </div>

        </div>
    </div>
@endsection

@section('scripts')

@endsection
