
<form method="POST" action="{{route('admin.models.update', $model)}}">

    {{ csrf_field() }}

    <div class="modal-body" id="edit-body">
        <div class="form-group">
            <label for="name">Name <sup>*</sup></label>
            <input id="name" type="text" class="form-control" value="{{$model->name}}" name="name" required>
        </div>
        <div class="form-group">
            <label for="make_id">Make <sup>*</sup></label>
            <select id="make_id" class="form-control select2" name="make_id" required>
                <option value="">Select</option>
                @foreach($makes as $make)
                    <option value="{{$make->id}}" {{$make->id == @$model->make->id ? 'selected' : ''}}>{{ $make->name }}</option>
                @endforeach
            </select>
        </div>
    </div>


    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</form>
