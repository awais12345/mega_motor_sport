@extends('admin.master')
@section('title') Dashboard @stop

@section('content')





    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Begin Breadcrumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Products</li>
            </ol>
        </nav>
        <!-- end Breadcrumb -->

        <!-- Page Heading -->

        <div class="card">
            <div class="card-body">

                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="col-sm-8">
                        <h1 class="h3 mb-0 text-gray-800">Products</h1>
                    </div>
                    <div>
                        <a href="#add-product" class="btn btn-primary btn-sm" data-toggle="modal">Add Product</a>
                    </div>
                </div>

                @include('shared.errors')

                <div class="table-responsive">
                    <table class="table table-hover table-sm" id="producttable">
                        <thead>
                        <tr class="text-center">
                            <th scope="col">#</th>
                            <th scope="col">SKU</th>
                            <th scope="col">Name</th>
                            <th scope="col">Category</th>
                            <th scope="col">Sub Category</th>
                            <th scope="col">make</th>
                            <th scope="col">model</th>
                            <th scope="col">qty</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($products as $key => $product)
                            <tr class="text-center">
                                <th scope="row">{{ $key+1 }}</th>
                                <td>{{ $product->sku }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->categories()->first() ? $product->categories()->first()->name : '' }}</td>
                                <td>{{ $product->subCategories()->first() ? $product->subCategories()->first()->name : '' }}</td>
                                <td>{{ $product->makes()->first() ? $product->makes()->first()->name : '' }}</td>
                                <td>{{ $product->makeModels()->first() ? $product->makeModels()->first()->name : '' }}</td>
                                <td>{{ @$product->qty }}</td>
                                <td>
                                    <a href="Javascript:void(0)" data-id="{{$product->id}}" data-toggle="modal"
                                       class="btn btn-sm btn-success edit-product">edit</a>
                                    <a href="{{route('admin.products.destroy', $product->id)}}"
                                       class="btn btn-sm btn-danger">delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="10" class="text-center text-danger">Record Not Found!</th>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- The Modal -->
        <div class="modal fade" id="add-product">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add product</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form method="POST" action="{{route('admin.products.store')}}" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name">Name <sup>*</sup></label>
                                        <input id="name" type="text" class="form-control" name="name"
                                               value="{{old('name')}}" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="category">Category <sup>*</sup></label>
                                        <select id="category" class="form-control select2 category" name="category_ids[]"
                                                required multiple>
                                            <option value="">Select</option>
                                            @foreach($categories as $category)
                                                <option
                                                    value="{{$category->id}}" {{ old('category_ids') && in_array($category->id, old('category_ids')) ? 'selected' : ''}}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="sub-category">Sub Category <sup>*</sup></label>
                                        <select id="sub-category" class="form-control select2 sub-category"
                                                name="sub_category_ids[]"
                                                required multiple>
                                            <option value="">Select</option>
                                            @foreach($subCategories as $subCategory)
                                                <option
                                                    value="{{$subCategory->id}}" {{ old('sub_category_ids') && in_array($subCategory->id , old('sub_category_ids')) ? 'selected' : ''}}>{{ $subCategory->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="price">Price <sup>*</sup></label>
                                        <input id="price" type="text" class="form-control" name="price"
                                               value="{{old('price')}}" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="discount-price">Discount Price </label>
                                        <input id="discount-price" type="text" class="form-control"
                                               name="discount_price" value="{{old('discount_price')}}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="qty">Qty</label>
                                        <input id="qty" type="number" min="0" class="form-control" name="qty"
                                               value="{{old('qty')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="make">Make</label>
                                        <select id="make" class="form-control select2 make" name="make_ids[]" multiple>
                                            <option value="">Select</option>
                                            @foreach($makes as $make)
                                                <option
                                                    value="{{$make->id}}" {{ old('make_ids') && in_array($make->id, old('make_ids')) ? 'selected' : ''}}>{{ $make->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="model">Model</label>
                                        <select id="model" class="form-control select2 model" name="make_model_ids[]" multiple>
                                            <option value="">Select</option>
                                            @foreach($models as $model)
                                                <option value="{{ $model->id }}" {{old('make_model_ids') && in_array($model->id , old('make_model_ids')) ? 'selected' : ''}}>{{ $model->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="year">Year</label>
                                        <select class="form-control select2" name="years[]" multiple>
                                            <option value="">Select</option>
                                            @foreach(getYears() as $year)
                                                <option
                                                    value="{{$year}}" {{ old('years[]') && in_array($year, old('years[]')) ? 'selected' : ''}}>{{$year}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-sm-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="color">Color</label>--}}
                            {{--                                        <input id="color" type="text" name="color" class="form-control"--}}
                            {{--                                               value="{{old('color')}}">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-sm-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="size">Size</label>--}}
                            {{--                                        <select class="form-control select2">--}}
                            {{--                                            <option value="">Select</option>--}}
                            {{--                                            <option value="x-small" {{old('size') == 'x-small' ? 'selected' : ''}}>X-Small</option>--}}
                            {{--                                            <option value="small" {{old('size') == 'small' ? 'selected' : ''}}>Small</option>--}}
                            {{--                                            <option value="medium" {{old('size') == 'medium' ? 'selected' : ''}}>Medium</option>--}}
                            {{--                                            <option value="large" {{old('size') == 'large' ? 'selected' : ''}}>Large</option>--}}
                            {{--                                            <option value="x-large" {{old('size') == 'x-large' ? 'selected' : ''}}>X-Large</option>--}}
                            {{--                                        </select>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="size">Size</label>
                                        <select class="form-control select2" name="size" id="size">
                                            <option value="">Select</option>
                                            <option value="x-small" {{old('size') == 'x-small' ? 'selected' : ''}}>
                                                X-Small
                                            </option>
                                            <option value="small" {{old('size') == 'small' ? 'selected' : ''}}>Small
                                            </option>
                                            <option value="medium" {{old('size') == 'medium' ? 'selected' : ''}}>
                                                Medium
                                            </option>
                                            <option value="large" {{old('size') == 'large' ? 'selected' : ''}}>Large
                                            </option>
                                            <option value="x-large" {{old('size') == 'x-large' ? 'selected' : ''}}>
                                                X-Large
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="vendor">Vendor</label>
                                        <input type="text" name="vendor" id="vendor" class="form-control" value="{{old('vendor')}}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="video_url">Video URL</label>
                                        <input type="text" name="video_url" id="video_url" class="form-control" value="{{old('video_url')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="shipping_charges">Shipping Charges</label>
                                        <input type="text" name="shipping_charges" id="shipping_charges" class="form-control" value="{{old('shipping_charges')}}">
                                    </div>
                                </div>
{{--                                <div class="col-sm-4">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="sku">SKU Code</label>--}}
{{--                                        <input type="text" name="sku" id="sku" class="form-control" disabled>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="is_avaliable">Stock Status</label>
                                        <select id="is_avaliable" class="form-control select2 is_avaliable" name="is_avaliable">
                                            <option value="1">Available Stock</option>
                                            <option value="0">Out of Stock</option>
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-12">

                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea id="description" name="description"
                                                  class="form-control wysiwyg">{{old('description')}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="is_free_shipping">Is Free Shipping ?</label>
                                        <input type="checkbox" name="is_free_shipping" id="is_free_shipping"
                                               class="checkbox checkbox-primary"
                                               value="1"
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-12">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="img" id="customFile">
                                        <label class="custom-file-label"  for="customFile">Choose Timeline image</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                Images
                                <div class="input-images-1"></div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
        <div class="modal fade" id="edit-product-modal">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Edit product</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div id="edit-body">
                            
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
@section('scripts')

    <script>
   

   
        $(document).ready(function () {
            // Add the following code if you want the name of the file appear on select
            $(".custom-file-input").on("change", function () {
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });

            $(document).on('click', '.edit-product', function (e) {
                let product_id = $(this).data('id');
                e.preventDefault();
                $.ajax({
                    url: '/admin/products/' + product_id + '/edit',
                    type: 'get',
                    success: function (response) {
                        $('#edit-body').html(response);
                        $('#edit-product-modal').modal('show');
                        $(".custom-file-input").on("change", function () {
                            var fileName = $(this).val().split("\\").pop();
                            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                        });
                    },
                    complete: function () {
                        $('.select2').select2({
                            width: '100%'
                        });
                    }
                });
            });

            $(document).on('change', '.category', function () {
                let cat_ids = $(this).val();
                if (cat_ids) {
                    $.ajax({
                        url: '/admin/products/sub-categories/options',
                        data:{
                            ids : cat_ids
                        },
                        type: 'get',
                        success: function (response) {
                            $('.sub-category').html(response);
                        }
                    });
                }

            });

            $(document).on('change', '.make', function () {
                let make_ids = $(this).val();
                if (make_ids) {
                    $.ajax({
                        url: '/admin/products/models/options',
                        type: 'get',
                        data:{
                            ids : make_ids
                        },
                        success: function (response) {
                            $('.model').html(response);
                        }
                    });
                }

            })
            $('#edit-product-modal').on('hide.bs.modal', function () {
                tinymce.remove('#edit-product-modal textarea');
                tinymce.remove('#add-product textarea');
            });
            $('#add-product').on('hide.bs.modal', function () {
                tinymce.remove('#edit-product-modal textarea');
                tinymce.remove('#add-product textarea');
            });

            $('.modal').on('shown.bs.modal', function (e) {
                tinymce.init({
                    selector: 'textarea.wysiwyg',
                    height: 200,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table paste code help wordcount'
                    ],
                    toolbar: 'undo redo | formatselect | ' +
                        'bold italic backcolor | alignleft aligncenter ' +
                        'alignright alignjustify | bullist numlist outdent indent | ' +
                        'removeformat | help',
                });
            })

        });
    </script>
@endsection
