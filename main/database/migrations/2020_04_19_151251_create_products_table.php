<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('name');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('sub_category_id');
            $table->unsignedInteger('make_id')->nullable();
            $table->unsignedInteger('make_model_id')->nullable();
            $table->year('year')->nullable();
            $table->string('description')->nullable();
            $table->decimal('price');
            $table->decimal('discount_price');
            $table->unsignedBigInteger('qty')->nullable();
            $table->string('color')->nullable();
            $table->string('size')->nullable();
            $table->string('img')->nullable();
            $table->string('img2')->nullable();
            $table->string('img3')->nullable();
            $table->string('img4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
