<style>
 #make{
     padding-top: 22px !important;
     font-size: 16px;
 }
 #model{
     padding-top: 22px !important;
     font-size: 16px;
 }
 #year{
     padding-top: 22px !important;
     font-size: 16px;
 }
@media screen and (max-width: 735px) {
  #make {
    padding-top: 2px !important;
  }
  #model {
    padding-top: 2px !important;
  }
  #year {
    padding-top: 2px !important;
  }
  .aside_slct_wrap {
    height: 27px;
margin-left: 16%;
width: 65%;
  }
}

</style>
<div class="bootstrap-menu">
<div class="head-nav -with-mmy" style="overflow: hidden;">
    <li><img class="head-nav-img" src="/images/05_big.jpg" style="width:100% !important"></li>
    <div class="simple-slider-wrap js-simple-slider" bis_skin_checked="1">
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" style="width: 100%; height: 100%;">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                      <img src="/images/F1.jpg" class="d-block w-100" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="/images/F2.jpg" class="d-block w-100" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="/images/F3.jpg" class="d-block w-100" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="/images/F4.jpg" class="d-block w-100" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="/images/F5.jpg" class="d-block w-100" alt="...">
                  </div>
                </div>
              </div>
    </div>
    <div class="head-nav-inner">
        <div class="head-nav-title">Select your vehicle</div>
        <div class="head-nav-subtitle">Customize, Modify, Upgrade, Replace</div>
        <div class="select-vehicle-spacer -index js-select-vehicle-namespace js-select-vehicle-fixed-point"
             id="select-vehicle-popup" data-namespace="select_vehicle_home_page" style="">
            <div class="select-vehicle" data-purpose="home-page">
                <div class="content -reset-styles">
                    <form method="GET" action="{{route('products.search')}}" id="search-form">
                        <div class="tab -active js-select-vehicle-mmy-tab">
                            <div class="select-vehicle-content-spacer js-vehicle-spacer">
                                <div class="select-vehicle-items _clear-modern">
                                    <div class="tab-garage-popup-15 hidden"
                                         data-namespace="select_vehicle_home_page"
                                         data-change="'', 'p', '61', '11', '0', [], false" data-vehicle-type="15"
                                         data-vehicle-subtype=""
                                         data-url-family=""
                                         data-submodel-preload-needed="">
                                    </div>

                                    <div class="select-vehicle-col">
                                        <div class="aside_slct_wrap">
                                            <select class="form-control aside_slct -not-refine -with-marker make"
                                                    id="make" name="make_id" >
                                                <option value="" disabled selected>1 &nbsp;&nbsp;|&nbsp;&nbsp; MAKE
                                                </option>
                                                @foreach(getMakes() as $make)
                                                    <option value="{{ $make->id }}"
                                                        {{isset($make_id) && $make_id == $make->id ? 'selected' : ''}}>
                                                        {{ $make->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="select-vehicle-col">
                                        <div class="aside_slct_wrap">
                                            <select
                                                class="form-control aside_slct -not-refine -with-marker model"
                                                name="model_id" id="model">
                                                <option value="" disabled selected>2 &nbsp;&nbsp;|&nbsp;&nbsp;MODEL</option>
                                                @if(isset($model_id))
                                                <option value="{{ $model_id }}"
                                                    {{\App\Models\MakeModel::where('id', $model_id)->exists() ? 'selected' : ''}}>
                                                    {{ \App\Models\MakeModel::where('id', $model_id)->first()->name }}</option>
                                                    @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="select-vehicle-col">
                                        <div class="aside_slct_wrap">
                                            <select class="form-control aside_slct -not-refine -with-marker"
                                                    name="year" id="year">
                                                <option value="" disabled selected>3 &nbsp;&nbsp;|&nbsp;&nbsp;YEAR
                                                </option>
                                                @foreach(getYears() as $loopYear)
                                                    <option value="{{ $loopYear }}" {{isset($year) && $loopYear == $year ? 'selected' : ''}}>
                                                        {{ $loopYear }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div onclick="submitSearchForm()"
                                         class="js-select-vehicle-btn select-vehicle-button -after-selects"
                                         tabindex="0"
                                         data-namespace="select_vehicle_home_page">GO
                                    </div>
                                    <div class="aside_slct_load -index-mmy"></div>


                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <span class="select-vehicle-fixed-close"></span></div>
        </div>
    </div>
    </div>
{{--    <div class="head-shop-by-links"><span class="head-shop-by-links-item"><a class="link"--}}
{{--                                                                             href="#">shop by product</a><span--}}
{{--                class="count"> (14,304,493)</span></span><span class="head-shop-by-links-item"><a class="link"--}}
{{--                                                                                                  href="#">shop by brand</a><span--}}
{{--                class="count"> (3,251)</span></span>--}}
{{--    </div>--}}
</div>
