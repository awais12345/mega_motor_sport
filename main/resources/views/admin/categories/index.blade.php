@extends('admin.master')
@section('title') Dashboard @stop
@section('content')


    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Begin Breadcrumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Categories</li>
            </ol>
        </nav>
        <!-- end Breadcrumb -->

        <!-- Page Heading -->

        <div class="card">
            <div class="card-body">

                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="col-sm-8">
                        <h1 class="h3 mb-0 text-gray-800">Categories</h1>
                    </div>
                    <div>
                        <a href="#add-category" class="btn btn-primary btn-sm" data-toggle="modal">Add Category</a>
                    </div>
                </div>

                @include('shared.errors')

                <div class="table-responsive">
                    <table class="table table-hover table-sm" id="cattable">
                        <thead>
                        <tr class="text-center">
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">image</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($categories as $key => $category)
                            <tr class="text-center">
                                <th scope="row">{{ $key+1 }}</th>
                                <td>{{ $category->name }}</td>
                                <td>
                                    <img src="{{$category->img ? asset('main/storage/app/public/'.$category->img) :'/admin/img/no-photo.png'}}" id="profile-img-tag" class="img-profile" alt="not found" style="width: 50px">
                                </td>
                                <td>
                                    <a href="Javascript:void(0)" data-id="{{$category->id}}" data-toggle="modal" class="btn btn-sm btn-success edit-category">edit</a>
                                    <a href="{{route('admin.categories.destroy', $category->id)}}" class="btn btn-sm btn-danger">delete</a>
                                </td>
                            </tr>
                        @empty
                        <tr>
                            <th colspan="5" class="text-center text-danger">Record Not Found!</th>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- The Modal -->
        <div class="modal fade" id="add-category">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add Category</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form method="POST" action="{{route('admin.categories.store')}}" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name">Name <sup>*</sup></label>
                                <input id="name" type="text" class="form-control" name="name" required>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="img" id="customFile">
                                <label class="custom-file-label"  for="customFile">Choose Image</label>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
        <div class="modal fade" id="edit-category-modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Category</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div id="edit-body">

                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
@section('scripts')
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
        
        $('.edit-category').on('click', function (e) {
            let category_id = $(this).data('id');
            e.preventDefault();
            $.ajax({
                url: '/admin/categories/' + category_id + '/edit',
                type: 'get',
                success: function (response) {
                    $('#edit-body').html(response);
                    $('#edit-category-modal').modal('show');
                    $(".custom-file-input").on("change", function() {
                        var fileName = $(this).val().split("\\").pop();
                        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                    });
                }
            });
        })
    </script>
@endsection
