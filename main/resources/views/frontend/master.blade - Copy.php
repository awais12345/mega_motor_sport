<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <style data-url="/css/critical.css?v=08c771b6">
        :root {
            --blue-color: #1b75bc;
            --red-color: #c1272d;
        }

        .body {
            background-color: #fff;
            color: #111;
            font: 62.5% "Roboto", "Arial", "Helvetica", sans-serif;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            text-align: center;
            display: none;
        }

        div, span, h1, h2, p, em, img, strong, b, u, i, dl, dt, dd, ol, ul, li, fieldset, form, label, table, tr, th, td {
            background: transparent;
            border: 0;
            font-size: 100%;
            margin: 0;
            outline: 0;
            padding: 0;
            vertical-align: baseline
        }

        a, [data-hlk] {
            background: transparent;
            color: #3761bf;
            cursor: pointer;
            font-size: 100%;
            margin: 0;
            padding: 0;
            text-decoration: none;
            vertical-align: baseline
        }

        a:hover, [data-hlk]:hover {
            text-decoration: underline
        }

        table {
            border-collapse: collapse;
            border-spacing: 0
        }

        table td, table th {
            vertical-align: top
        }

        [type='submit'] {
            -webkit-appearance: none;
            border-radius: 0
        }

        input[type='search'] {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none
        }

        input::-webkit-search-cancel-button {
            display: none
        }

        input::-webkit-search-decoration {
            display: none
        }

        input::-ms-clear {
            display: none
        }

        iframe {
            border: 0
        }

        html {
            overflow-x: hidden
        }

        html, body {
            min-height: 100%
        }

        body {
            position: relative
        }

        input::-ms-clear, input::-ms-reveal {
            display: none
        }

        textarea {
            resize: vertical
        }

        #scrollfix {
            position: relative;
            width: 100%;
            z-index: 5
        }

        .hiddenScroll #scrollfix {
            overflow: hidden
        }

        .left-panel, .right-panel {
            display: none;
            position: absolute;
            top: 0
        }

        @-webkit-keyframes appear {
            0% {
                opacity: 0
            }
            100% {
                opacity: 1
            }
        }

        @keyframes appear {
            0% {
                opacity: 0
            }
            100% {
                opacity: 1
            }
        }

        .main-content {
            position: relative;
            width: 100%;
            z-index: 1
        }

        .main-content.-overlay.-animate::after {
            -webkit-animation: appear 0.4s;
            animation: appear 0.4s
        }

        .main-content.-selects-overlay-show {
            z-index: auto
        }

        .wrap {
            margin: 0 auto;
            min-width: 320px;
            position: relative;
            text-align: left
        }

        .wrap .wrap {
            margin: 0;
            min-width: 0
        }

        .product-list-with-filter-holder {
            box-sizing: border-box;
            width: 100%
        }

        .mobile-small-show, .mobile-medium-show, .mobile-large-show, .desktop-hide, .mobile-show {
            display: none !important
        }

        .hidden {
            display: none !important
        }

        .ov_hidden {
            overflow: hidden !important
        }

        .ov-x-auto {
            overflow-x: auto !important
        }

        .block {
            display: block !important
        }

        .clear {
            clear: both;
            height: 0;
            overflow: hidden
        }

        ._clear-modern::before, ._clear-modern::after {
            clear: both;
            content: '';
            display: table
        }

        .float_right {
            float: right
        }

        .float_left {
            float: left
        }

        .pos_r {
            position: relative
        }

        .row::before, .row::after {
            clear: both;
            content: '';
            display: table
        }

        template {
            display: none
        }

        .break-word {
            word-wrap: break-word
        }

        .h1-change-btn-wrap {
            margin: 16px 8px
        }

        .h1-fat {
            color: #111;
            display: block;
            font: 500 2.2em/1.2 "Roboto", "Arial", "Helvetica", sans-serif;
            margin: 16px 2%
        }

        .h1-fat > span {
            font-weight: 300
        }

        ._store-red {
            background-color: #d41e2b !important
        }

        ._store-orange {
            background-color: #fea934 !important
        }

        ._store-grey-blue {
            background-color: #455874 !important
        }

        ._store-blue {
            background-color: #0883d4 !important
        }

        ._store-gray {
            background-color: #686c73 !important
        }

        ._store-dark-orange {
            background-color: #fc6200 !important
        }

        ._store-green {
            background-color: #1e824c !important
        }

        .head-stores-h {
            background-color: var(--blue-color);
            height: 48px
        }

        .head-stores {
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
            align-items: flex-start;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            justify-content: flex-start;
            list-style: none
        }
/*
        .head-stores::after {
            background-color: #505b6a;
            border-radius: 4px;
            color: #fff;
            content: 'Shop our Stores';
            display: block;
            font-size: 1.1em;
            line-height: 1.5;
            padding: 6px 12px;
            position: absolute;
            right: 10px;
            top: 10px
        }
*/
        .head-stores.-active::after {
            background: url(https://cdn.carid.com/css/prod-images/6b14ad75.svg) 50% 50% no-repeat;
            background-size: 20px auto;
            border-radius: 0;
            content: '';
            height: 40px;
            opacity: 0.7;
            padding: 0;
            right: 8px;
            top: 4px;
            width: 40px
        }

        .head-stores > li {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            font-size: 1.4em;
            font-weight: 300;
            line-height: 1.17
        }

        .head-stores > li:hover > .item:not(.-active) {
            cursor: pointer;
            opacity: 1;
            text-decoration: none
        }

        .head-stores > li > .item {
            box-sizing: border-box;
            display: block;
            height: 48px;
            padding-left: 70px;
            padding-right: 25px;
            padding-top: 18px;
            position: relative;
            white-space: nowrap
        }

        .head-stores > li > .item.-active {
            color: #fff;
            cursor: default;
            font-weight: 700
        }

        .head-stores > li > .item:not(.-active) {
            display: none;
            text-indent: -999em
        }

        .head_header {
            width: 100%
        }

        .header-top {
            background-color: #f0f0f0;
            height: 48px
        }

        .header-top > .wrap {
            height: 100%
        }

        .header-top .header-dd-h {
            display: none
        }

        .header-account {
            display: none
        }

        .left-menu-icon {
            background: url(https://cdn.carid.com/css/prod-images/adf4276e.svg) 50% 50% no-repeat;
            background-size: 24px 18px;
            cursor: pointer;
            display: block;
            height: 48px;
            left: 0;
            position: absolute;
            top: 0;
            width: 56px;
            z-index: 1
        }

        .no-touch .left-menu-icon:hover {
            opacity: 0.7
        }
        @media screen and (max-width:767px){
            .head_logo_a
            {
               display: none;
            }
            .header-search-btn{
                left: 64px !important;
            }
            .-sm-white{
                display: block;
            }
            .imgwhitelogo{
                display: block !important;
            }
        }
        .imgwhitelogo{
                display: none;
                top: 50%;
                left: 50%;
                transform: translate(-50%,0);
                position: absolute;
                padding: 8px;
            }
        .head_logo_a {
            background: url(/images/logo.png) no-repeat 100% 12px;
            background-size: 60px auto;
            bottom: -4px;
            left: 56px;
            position: absolute;
            text-indent: -999em;
            top: -4px;
            width: 65px;
            z-index: 3
        }
        .-sm-white {
            background: url(/images/logo-white.png) no-repeat 100% 12px;
            background-size: 60px auto;
            bottom: -4px;
            left: 56px;
            position: absolute;
            text-indent: -999em;
            top: -4px;
            width: 65px;
            z-index: 3
        }

        .header-dd-hover-element:hover .header-dd-h {
            display: none;
            opacity: 1;
            -webkit-transform: translateY(0);
            transform: translateY(0);
            -webkit-transition: opacity 0.3s ease 0.2s, visibility 0s ease 0.2s, -webkit-transform 0.3s ease 0.2s;
            transition: opacity 0.3s ease 0.2s, visibility 0s ease 0.2s, -webkit-transform 0.3s ease 0.2s;
            transition: opacity 0.3s ease 0.2s, transform 0.3s ease 0.2s, visibility 0s ease 0.2s;
            transition: opacity 0.3s ease 0.2s, transform 0.3s ease 0.2s, visibility 0s ease 0.2s, -webkit-transform 0.3s ease 0.2s;
            visibility: visible
        }

        .no-transform .header-dd-hover-element:hover .header-dd-h {
            display: block
        }

        .nav-tool-wrap {
            float: right;
            list-style: none
        }

        .nav-tool-wrap > li {
            display: inline-block;
            vertical-align: top
        }

        .nav-tool {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            font-size: 13px;
            font-weight: 500;
            height: 48px;
            line-height: 1.2;
            min-width: 48px;
            position: relative
        }

        .nav-tool .-txt {
            display: none
        }

        .nav-tool > .icon {
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: 100% auto;
            height: 27px;
            left: 50%;
            margin-left: -14px;
            position: absolute;
            top: 11px;
            width: 27px
        }

        .nav-tool, .nav-tool .-txt {
            color: #111
        }

        .nav-tool .count-item {
            text-indent: 0
        }

        .acc-text {
            display: inline-block;
            overflow: hidden;
            width: 0
        }

        .header-garage-subtitle {
            display: none
        }

        .nav-tool.-account {
            display: none
        }

        .nav-tool.-wishlist, .nav-tool.-garage {
            display: none
        }

        .header-search-btn {
            background: #fff;
            border: 1px solid #d8d8d8;
            border-radius: 4px;
            bottom: -4px;
            display: block;
            font-size: 1.3em;
            font-weight: 300;
            left: 146px;
            position: absolute;
            right: 48px;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            top: 6px
        }

        .header-search-btn::before {
            content: "Search...";
            left: 16px;
            position: relative;
            top: 9px
        }

        .header-search-btn::after {
            background: transparent url(https://cdn.carid.com/css/prod-images/aa0c55ee.svg) no-repeat 50% 50%;
            background-size: 25px auto;
            bottom: 0;
            content: '';
            height: 34px;
            padding: 0;
            position: absolute;
            right: 8px;
            top: 0;
            width: 34px
        }

        .count-item {
            background-color: #d4252a;
            border-radius: 15px;
            box-sizing: border-box;
            color: #fff;
            font-size: 11px;
            line-height: 13px;
            min-width: 8px;
            padding: 0 3px;
            text-align: center
        }

        .count-item.-type-1 {
            border-radius: 50%;
            font-size: 8px;
            font-weight: 700;
            line-height: 16px;
            position: absolute;
            right: -3px;
            text-align: center;
            top: -1px;
            width: 16px
        }

        .count-item.-type-1::after {
            border: 1px solid #fff;
            border-radius: 50%;
            bottom: -1px;
            content: '';
            left: -1px;
            position: absolute;
            right: -1px;
            top: -1px
        }

        .count-item.-type-2 {
            border-radius: 50%;
            display: inline-block;
            font-size: 8px;
            line-height: 15px;
            text-align: center;
            vertical-align: middle;
            width: 15px
        }

        .left-menu-vehicles-title .count-item.-type-2 {
            margin-left: 2px
        }

        .count-item.-garage {
            left: 46px;
            position: absolute;
            top: 10px
        }

        .count-item.-garage::after {
            border: 1px solid #fff;
            border-radius: 50%;
            bottom: -1px;
            content: '';
            left: -1px;
            position: absolute;
            right: -1px;
            top: -1px
        }

        .head-dd-h {
            display: none
        }

        .head-dd {
            color: #757575;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            height: 35px;
            list-style: none;
            margin-left: 6px;
            overflow: hidden;
            padding-top: 5px;
            text-align: left;
            vertical-align: top
        }

        .head-dd > li {
            box-sizing: border-box;
            cursor: pointer;
            display: inline-block;
            position: relative;
            vertical-align: top
        }

        .head-dd > .it {
            height: 100%;
            -webkit-box-ordinal-group: 2;
            -webkit-order: 1;
            order: 1
        }

        .head-dd > .it > .head-dd-main {
            display: inline-block;
            font-size: 14px;
            font-weight: 500;
            line-height: 1;
            padding: 10px;
            position: relative;
            text-decoration: none
        }

        .head-dd > .it > .head-dd-main, .head-dd > .it > .head-dd-main > .link {
            color: #111
        }

        .head-dd > .it > .head-dd-main > .link:hover {
            text-decoration: none
        }

        .head-dd > .it > .head-dd-main > .link > span, .head-dd > .it > .head-dd-main .extra, .head-dd > .it > .head-dd-main .extra + .link {
            display: none
        }

        .head-dd > .it:hover > .head-dd-main, .head-dd > .it:hover > .head-dd-main > .link, .head-dd > .it.loading > .head-dd-main, .head-dd > .it.loading > .head-dd-main > .link, .head-dd > .it.active > .head-dd-main, .head-dd > .it.active > .head-dd-main > .link {
            color: #ffac00
        }

        .head-dd > .it.after-more {
            -webkit-box-ordinal-group: 4;
            -webkit-order: 3;
            order: 3
        }

        .head-dd .head-dd-more {
            -webkit-box-ordinal-group: 3 !important;
            -webkit-order: 2 !important;
            order: 2 !important;
            visibility: hidden
        }

        .head-dd-help-center {
            float: right;
            margin-right: 12px;
            padding-top: 5px
        }

        .head-dd-cont-holder {
            display: none
        }

        .head_img {
            background: #f0f0f0;
            line-height: 0;
            min-width: 1000px;
            overflow: hidden;
            position: relative;
            text-align: center
        }

        .head_img_minh {
            min-width: 0
        }

        .head_img_aligner {
            bottom: 0;
            height: 100%;
            left: 50%;
            margin-left: -2500px;
            position: absolute;
            top: 0;
            width: 5000px
        }

        .head_img_aligner .head-img-overlay > img, .head_img_aligner .head-img-overlay > a, .head_img_aligner .head-img-overlay > a > img {
            display: block;
            height: 100%;
            margin: 0 auto;
            width: auto
        }

        .head-img-overlay {
            display: inline-block;
            height: 100%;
            overflow: hidden;
            position: relative;
            width: 100%
        }

        .head-img-overlay::after {
            background: url(https://cdn.carid.com/css/prod-images/b2c6e3e8.svg) repeat 0 0;
            content: '';
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%
        }

        .head-img-overlay.-transparent::after {
            background: none
        }

        .head-nav {
            -webkit-box-align: center;
            -webkit-align-items: center;
            align-items: center;
            background: #757575;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            height: 230px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            position: relative;
            width: 100%
        }

        .head-nav::after {
            background: #111;
            content: '';
            display: block;
            height: 100%;
            left: 0;
            margin: 0 auto;
            max-width: 1920px;
            opacity: 0.3;
            position: absolute;
            right: 0;
            top: 0;
            width: 100%
        }

        .head-nav-img {
            display: block;
            height: 100%;
            left: 50%;
            position: absolute;
            top: 0;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
            width: auto
        }

        .head-nav-inner {
            box-sizing: border-box;
            margin-top: 8px;
            min-width: 320px;
            padding: 0 16px;
            position: relative;
            text-align: center;
            width: 100%;
            z-index: 3
        }

        .head-nav.-with-mmy .head-nav-inner {
            padding: 0 8px;
            z-index: 21
        }

        .head-nav-title {
            color: #fff;
            font-size: 2.4em;
            font-weight: 700;
            line-height: 1.4;
            margin-bottom: 10px;
            text-transform: uppercase
        }

        .head-nav.-with-mmy .head-nav-title {
            line-height: 1
        }

        .head-nav-subtitle {
            display: none
        }

        .head-nav.-with-mmy .head-nav-subtitle {
            margin-bottom: 32px
        }

        .simple-slider-wrap {
            display: block;
                height: 550px;
                left: 50%;
                margin-left: -960px;
                overflow: hidden;
                position: absolute;
                top: 0;
                width: 1350px;
        }

        .simple-slider {
            list-style: none;
            position: relative;
            -webkit-transform: translateX(1920px);
            transform: translateX(1920px);
            -webkit-transition: -webkit-transform 500ms ease;
            transition: -webkit-transform 500ms ease;
            transition: transform 500ms ease;
            transition: transform 500ms ease, -webkit-transform 500ms ease
        }

        .simple-slider.-loaded {
            -webkit-transform: translateX(0);
            transform: translateX(0)
        }

        .simple-slider.-no-transition {
            -webkit-transition: none;
            transition: none
        }

        .simple-slider > .slide {
            float: left;
            height: 0;
            opacity: 0;
            width: 1920px
        }

        .simple-slider > .slide.-loaded {
            height: auto;
            opacity: 1
        }

        .head-main-logo {
            bottom: 30px;
            left: 0;
            position: absolute;
            right: 0;
            z-index: 1
        }

        .head-main-logo.-with-main-select-bar {
            bottom: 50px
        }

        .head-main-logo > .logo {
            margin: 0 8px
        }

        .main-select-bar-h {
            position: relative;
            z-index: 21
        }

        .main-select-bar-h.-bottom-offset {
            margin-bottom: 64px
        }

        .head-nav-inner > .select-vehicle-spacer {
            margin: 0 0 18px
        }

        .head-shop-by-links {
            bottom: 32px;
            left: 0;
            position: absolute;
            text-align: center;
            width: 100%;
            z-index: 1
        }

        .head-shop-by-links-item, .head-shop-by-links-item > .link {
            color: #fff;
            font-size: 14px;
            line-height: 16px;
            text-align: center;
            text-transform: uppercase
        }

        .head-shop-by-links-item:first-child {
            margin-right: 17px;
            padding-right: 17px;
            position: relative
        }

        .head-shop-by-links-item:first-child::after {
            border: 1px solid #fff;
            content: '';
            height: 17px;
            position: absolute;
            right: 0;
            top: -1px
        }

        .head-shop-by-links-item > .link {
            font-weight: 700
        }

        .head-shop-by-links-item > .count {
            display: none
        }

        .select-vehicle-spacer {
            color: #a9a9a9;
            margin: -20px 8px 0;
            position: relative
        }

        .select-vehicle-spacer.-hidden {
            height: 0;
            margin: 0;
            overflow: hidden;
            position: relative
        }

        .select-vehicle-spacer.-hidden.-fixed {
            overflow: visible
        }

        .select-vehicle-spacer.-home-page {
            margin-bottom: 50px
        }

        .select-vehicle-spacer.-in-popup {
            margin: 0 !important;
            position: static
        }

        .select-vehicle-spacer.-in-popup::after {
            content: '';
            height: 350px;
            left: 0;
            position: absolute;
            top: 100%;
            width: 1px;
            z-index: -1
        }

        .select-vehicle-spacer.-top-indent {
            margin-top: 20px
        }

        .select-vehicle {
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        .select-vehicle-spacer.-fixed .select-vehicle {
            background: #363d47;
            left: 0;
            margin: 0 auto;
            min-width: 1000px;
            position: fixed;
            right: 0;
            top: 0;
            width: 100%;
            z-index: 101
        }

        .select-vehicle::after {
            display: none
        }

        .no-js .select-vehicle::after {
            content: '';
            display: block;
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%
        }

        .touch .select-vehicle {
            position: static
        }

        .select-vehicle > .nav {
            height: 50px
        }

        .select-vehicle-spacer.-fixed .select-vehicle > .nav {
            display: none
        }

        .select-vehicle > .nav > .link {
            background-color: #363d47;
            background-image: none;
            border-radius: 4px 4px 0 0;
            box-sizing: border-box;
            color: #fff;
            cursor: pointer;
            -webkit-box-flex: 1;
            -webkit-flex: 1;
            flex: 1;
            float: left;
            font-size: 1.6em;
            font-weight: 500;
            height: 101%;
            line-height: 1;
            margin-left: 2px;
            padding: 16px 24px 18px;
            position: relative
        }

        .select-vehicle > .nav > .link::after {
            content: '';
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%
        }

        .select-vehicle > .nav > .link:first-child {
            border-color: transparent;
            border-left: none;
            margin-left: 0
        }

        .select-vehicle-spacer.-in-popup .select-vehicle > .nav > .link {
            background-image: none
        }

        .select-vehicle > .nav > .link.-light {
            background-color: #d8d8d8;
            color: #464646
        }

        .select-vehicle > .nav > .link.-light:not(.-active) {
            box-shadow: inset 0 -10px 20px -10px rgba(0, 0, 0, 0.25)
        }

        .select-vehicle-spacer.-in-popup .select-vehicle > .nav > .link.-light {
            background-color: #d8d8d8
        }

        .select-vehicle > .nav > .link.-active {
            cursor: default;
            opacity: 1
        }

        .select-vehicle > .nav > .link.-active::after {
            display: none
        }

        .select-vehicle > .nav.-garage {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex
        }

        .select-vehicle > .nav.-garage > .link {
            background-repeat: no-repeat;
            display: block;
            padding-left: 24px;
            padding-right: 8px;
            width: auto
        }

        .select-vehicle > .nav.-garage > .link:first-child {
            min-width: auto;
            width: auto
        }

        .select-vehicle > .nav.-garage > .link:not(.-active) {
            background-color: #414955
        }

        .select-vehicle > .nav.-garage > .link.-i1-15 {
            background-image: url(https://cdn.carid.com/css/prod-images/ae5be278.svg)
        }

        .select-vehicle > .nav.-garage > .link.-i1-31 {
            background-image: url(https://cdn.carid.com/css/prod-images/e225162b.svg)
        }

        .select-vehicle > .nav.-garage > .link.-i1-6 {
            background-image: url(https://cdn.carid.com/css/prod-images/5494e65d.svg)
        }

        .select-vehicle > .nav.-garage > .link.-i1-20 {
            background-image: url(https://cdn.carid.com/css/prod-images/536dcc29.svg)
        }

        .select-vehicle > .nav.-garage > .link > .link-title {
            color: #fff;
            display: none
        }

        .select-vehicle > .nav.-garage > .link.-active {
            opacity: 1;
            padding-left: 54px
        }

        .select-vehicle > .nav.-garage > .link.-active > .link-title {
            display: block;
            font-weight: 700;
            white-space: nowrap
        }

        .select-vehicle > .content {
            background: #363d47;
            border-radius: 0 0 4px 4px;
            position: relative
        }

        .inspiration-gallery-indent .select-vehicle > .content {
            border-radius: 4px
        }

        .select-vehicle-spacer.-fixed .select-vehicle > .content {
            margin: 0 auto;
            max-width: 1296px;
            min-width: 1000px;
            padding: 0;
            width: 94%
        }

        .select-vehicle > .content.-reset-styles {
            background: transparent;
            border-radius: 0
        }

        .select-vehicle > .content > .tab {
            border-radius: 4px;
            display: none;
            height: 0;
            overflow: hidden;
            position: absolute;
            visibility: hidden;
            width: 100%
        }

        .select-vehicle > .content > .tab.-active {
            border-radius: 0 4px 4px;
            display: block;
            height: auto;
            overflow: visible;
            position: static;
            visibility: visible
        }

        .select-vehicle > .content > .tab.-light {
            background-color: #d8d8d8
        }

        .select-vehicle-content-spacer {
            padding: 24px 20px;
            position: relative
        }

        .select-vehicle-spacer:not(.-fixed) .content.-gallery .select-vehicle-content-spacer {
            padding: 20px 20px 12px
        }

        .select-vehicle-spacer.-index:not(.-fixed) .select-vehicle-content-spacer {
            padding: 0
        }

        .select-vehicle-spacer.-fixed .select-vehicle-content-spacer {
            padding: 10px 8px
        }

        .select-vehicle-content-spacer.-preload-height {
            min-height: 56px
        }

        .select-vehicle-col, .select-vehicle-col-container {
            -webkit-box-flex: 1;
            -webkit-flex: 1;
            flex: 1
        }

        .aside_slct_wrap {
            height: 48px;
            margin-bottom: 8px;
            position: relative;
            width: 100%
        }

        .ship-wordwide-popup .aside_slct_wrap {
            height: 48px
        }

        .select-vehicle-items-inner {
            display: block
        }

        .select-vehicle-items {
            display: block;
            position: relative;
            width: 100%;
            z-index: 22
        }

        .select-vehicle-items.-half {
            width: auto
        }

        .select-vehicle-items.-front-size, .select-vehicle-items.-rear-size.-rear-active {
            display: block;
            z-index: auto
        }

        .select-vehicle-items.-rear-size {
            display: none
        }

        .select-vehicle-items.-tires-load-speed-index {
            position: static
        }

        .aside_slct.-not-refine {
            background: #fff;
            border: none;
            border-radius: 4px;
            box-sizing: border-box;
            color: #464646;
            -webkit-box-flex: 1;
            -webkit-flex: 1;
            flex: 1;
            height: auto;
            margin: 0;
            padding: 25px 10px 7px;
            position: absolute;
            text-align: left;
            width: 100%
        }

        .aside_slct
        .-not-refine::before {
            background: url(https://cdn.carid.com/css/prod-images/f3dd7cc3.svg) no-repeat 0 0;
            background-size: 100% auto;
            content: '';
            height: 8px;
            position: absolute;
            right: 20px;
            top: 23px;
            -webkit-transition: -webkit-transform 0.1s;
            transition: -webkit-transform 0.1s;
            transition: transform 0.1s;
            transition: transform 0.1s, -webkit-transform 0.1s;
            width: 13px
        }

        .aside_slct.-not-refine::after {
            color: #464646;
            content: attr(data-placeholder);
            font-size: 0.8rem;
            left: 24px;
            overflow: hidden;
            position: absolute;
            text-overflow: ellipsis;
            text-transform: uppercase;
            top: 16px;
            -webkit-transition: top 0.1s;
            transition: top 0.1s;
            white-space: nowrap;
            width: 70%
        }

        .aside_slct.-not-refine.-active {
            box-shadow: 0 0 0 2px #3761bf;
            z-index: 1000
        }

        .aside_slct.-not-refine.-active::before {
            background-image: url(https://cdn.carid.com/css/prod-images/d1a95e81.svg);
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg)
        }

        .aside_slct.-not-refine.-active::after, .aside_slct.-not-refine.-selected::after {
            color: #757575;
            font-size: 0.6rem;
            top: 9px
        }

        .aside_slct.-not-refine.-tire-popup::after {
            top: 19px
        }

        .aside_slct.-not-refine.-active.-tire-popup::after, .aside_slct.-not-refine.-selected.-tire-popup::after {
            top: 12px
        }

        .select-vehicle-col.-with-border .aside_slct.-not-refine {
            border: 1px solid #757575
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine {
            padding-bottom: 9px;
            padding-top: 23px
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine::before {
            top: 22px
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine::after {
            top: 16px
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine.-active::after, .select-vehicle-spacer.-fixed .aside_slct.-not-refine.-selected::after {
            top: 8px
        }

        .aside_slct.-not-refine.-with-marker::after {
            left: 48px
        }

        .aside_slct.-not-refine.-with-marker .aside_slct_value {
            margin-left: 44px
        }

        .aside_slct.-not-refine.-disabled, .aside_slct.-not-refine.-disabled > .aside_slct_value {
            background-color: #f0f0f0 !important;
            color: #717d90
        }

        .aside_slct.-not-refine.-bordered {
            box-shadow: inset 0 0 0 1px #a9a9a9
        }

        .aside_slct.-not-refine.-bordered::before {
            right: 15px;
            top: 22px
        }

        .aside_slct.-not-refine.-bordered.-active {
            box-shadow: 0 0 0 2px #3761bf
        }

        .aside_slct.-not-refine.-unfocused {
            background: #d8d8d8
        }

        .aside_slct.-not-refine.-unfocused::after {
            color: #757575
        }

        .aside_slct.-not-refine .aside_slct_value {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            color: #111;
            font-size: 1.4em;
            height: 16px;
            line-height: 1.1;
            margin-left: 14px;
            padding-left: 0
        }

        .aside_slct.-not-refine .aside-slct-marker {
            border-right: 1px solid #a9a9a9;
            color: #757575;
            font-size: 1.4em;
            height: 26px;
            left: 0;
            line-height: 27px;
            padding: 0 12px 0 16px;
            position: absolute;
            top: 12px
        }

        .select-vehicle-spacer.-fixed .aside_slct.-not-refine .aside-slct-marker {
            top: 11px
        }

        .select-vehicle-button {
            background: #d4252a;
            border-radius: 4px;
            color: #fff;
            cursor: pointer;
            font: 500 1.4em/1.2 "Roboto", "Arial", "Helvetica", sans-serif;
            min-width: 40px;
            padding: 20px 0;
            text-align: center;
            text-transform: uppercase
        }

        .select-vehicle-spacer.-fixed .select-vehicle-button {
            padding: 16px 0 14px
        }

        .select-vehicle-button.-disabled, .select-vehicle-button.-disabled:hover {
            background-color: #757575;
            cursor: default;
            font-weight: 700
        }

        .select-vehicle-button:hover, .select-vehicle-button:focus:not(.-disabled) {
            background: #be2126;
            text-decoration: none
        }

        .select-vehicle-button.-after-selects {
            float: none;
            margin-left: 0
        }

        .select-vehicle-button.-full-width {
            box-sizing: border-box;
            width: 100%
        }

        .select-vehicle-button.-tire-size-pp {
            margin-top: 52px
        }

        .select-vehicle-items.-front-size.-rear-active .select-vehicle-button {
            display: none
        }

        .select-vehicle-bottom-line {
            line-height: 20px;
            margin: 15px 0 0 15px
        }

        .select-vehicle-bottom-line.-no-margin {
            margin: 0
        }

        .select-vehicle-spacer.-fixed .select-vehicle-bottom-line {
            display: none
        }

        .my-garage-line {
            background: url(https://cdn.carid.com/css/prod-images/e8eccc2d.svg) 16px 50% no-repeat;
            border-left: 1px solid #d8d8d8;
            display: inline-block;
            height: 18px;
            margin-left: 10px;
            padding-left: 16px;
            vertical-align: bottom
        }

        .my-garage-line > .header-dd-hover-element {
            display: inline-block
        }

        .my-garage-line .mygarage-dd {
            left: 0;
            margin-top: 12px;
            right: 0;
            top: 74%;
            z-index: 23
        }

        .my-garage-line-title {
            cursor: pointer;
            display: inline-block;
            line-height: 1.2;
            padding-left: 28px;
            padding-right: 10px;
            vertical-align: top
        }

        .main-select-bar-overlay {
            background: #111;
            height: 100%;
            left: 0;
            opacity: 0.5;
            position: fixed;
            top: 0;
            width: 100%
        }

        .breadcrumbs-holder {
            margin: 16px 8px -12px;
            overflow: hidden;
            position: relative
        }

        .breadcrumbs-holder::after {
            box-shadow: 0 0 4px 4px #fff;
            content: '';
            height: 100%;
            position: absolute;
            right: -1px;
            top: 0;
            width: 1px;
            z-index: 1
        }

        .body-bg-type1 .breadcrumbs-holder::after {
            box-shadow: 0 0 4px 4px #f0f0f0
        }

        .body-bg-type1.-dark .breadcrumbs-holder::after {
            box-shadow: 0 0 4px 4px #d8d8d8
        }

        .breadcrumbs-holder.-static {
            margin-top: 0;
            padding-top: 20px
        }

        .breadcrumbs-holder.-my-acc {
            float: left;
            margin-bottom: 0;
            margin-top: 10px
        }

        .breadcrumbs, .select-vehicle-dept-switchers {
            color: #111;
            font-size: 1.2em;
            font-weight: 300;
            list-style: none;
            position: relative;
            text-transform: capitalize;
            z-index: 1
        }

        .breadcrumbs > .item, .select-vehicle-dept-switchers > .item {
            display: inline-block
        }

        .breadcrumbs > .item.-active, .select-vehicle-dept-switchers > .item.-active {
            font-weight: 700
        }

        .breadcrumbs > .item::after, .select-vehicle-dept-switchers > .item::after {
            display: inline-block;
            font-style: normal;
            text-align: center;
            width: 22px
        }

        .breadcrumbs > .item:last-child, .select-vehicle-dept-switchers > .item:last-child {
            padding-right: 5px
        }

        .breadcrumbs > .item:last-child::after, .select-vehicle-dept-switchers > .item:last-child::after {
            display: none
        }

        .breadcrumbs > .item > .link, .select-vehicle-dept-switchers > .item > .link {
            color: #111
        }

        .breadcrumbs > .item::after {
            content: '\203a';
            font-size: 1.6em;
            line-height: 0.58;
            position: relative;
            top: 2px
        }

        .select-vehicle-dept-switchers {
            display: none
        }

        .select-vehicle-dept-switchers > .item::after {
            content: '\007c'
        }

        .ptype-grid-h {
            padding: 1px 0
        }

        .ptype-grid-h:last-child {
            margin-bottom: 0
        }

        .ptype-grid-h2 {
            color: #111;
            font-size: 2.2em;
            font-weight: 500;
            line-height: 1.17;
            margin-bottom: 16px;
            text-align: center
        }

        .ptype-grid-h2 > .inner {
            color: inherit;
            display: -webkit-inline-box;
            display: -webkit-inline-flex;
            display: inline-flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column
        }

        .ptype-grid-h2 > .inner > .descr {
            display: none
        }

        .ptype-grid {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            list-style: none;
            margin: 16px 0 32px;
            overflow: hidden
        }

        .ptype-grid.-no-margin {
            margin: 0
        }

        .ptype-grid.-center {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center
        }

        .ptype-grid.-tile > .li {
            background: #fff
        }

        .ptype-grid > .li {
            box-sizing: border-box;
            height: auto;
            margin: 2%;
            overflow: hidden;
            position: relative;
            text-align: center;
            width: 46%
        }

        .ptype-grid > .li::before {
            color: #a9a9a9;
            content: attr(data-qty);
            font-size: 1.1em;
            left: 0;
            position: absolute;
            right: 0;
            top: 8%;
            z-index: 2
        }

        .ptype-grid.-col-6 > .li::before {
            display: none
        }

        .ptype-grid.-col-6 > .li > .lazy-loading {
            display: block;
            padding-top: 52%;
            top: 22%;
            width: 80%
        }

        .ptype-grid.-col-5 > .li > .lazy-loading {
            display: block;
            padding-top: 52%;
            top: 22%;
            width: 80%
        }

        .ptype-grid.-col-9 > .li {
            margin: 8px 0.65%;
            width: 31.833333%
        }

        .ptype-grid.-col-9 > .li::before {
            display: block
        }

        .ptype-grid.-col-9 > .li > .lazy-loading {
            background: #d8d8d8;
            display: block;
            padding-top: 46%;
            width: 70%
        }

        .ptype-grid-img {
            height: auto;
            left: 0;
            margin: auto;
            max-width: 100%;
            position: absolute;
            right: 0;
            top: 22%;
            width: 90%
        }

        .ptype-grid.-col-9 .ptype-grid-img {
            top: 0
        }

        .ptype-grid-a {
            background: url(https://cdn.carid.com/css/prod-images/44d36080.svg) 0 0 no-repeat;
            box-sizing: border-box;
            color: #111;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            font-size: 1.4em;
            font-weight: 500;
            left: 0;
            line-height: 1.3;
            padding: 95% 8px 12px;
            position: relative;
            table-layout: fixed;
            text-decoration: none;
            width: 100%
        }

        .ptype-grid.-col-9 .ptype-grid-a {
            padding-bottom: 0
        }

        .ptype-grid-a:hover {
            text-decoration: none
        }

        .ptype-grid-a:hover > .ptype-grid-title, .ptype-grid-a.-simple-title:hover {
            text-decoration: none
        }

        .ptype-grid-a::before {
            background: url(https://cdn.carid.com/css/prod-images/44d36080.svg) 0 0 no-repeat;
            content: '';
            display: block;
            height: 150%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 2
        }

        .ptype-grid-a::after {
            color: #757575;
            content: attr(data-descr);
            font-size: 12px;
            font-weight: 400;
            line-height: 1.25;
            position: relative;
            text-transform: none
        }

        .ptype-grid-a.-simple-title::after {
            padding-top: 6px
        }

        .ptype-grid-a.-simple-title, .ptype-grid-title {
            font-weight: 400
        }

        .app-section-content .ptype-grid-a.-simple-title, .app-section-content .ptype-grid-title {
            font-weight: 300
        }

        .ptype-grid-title {
            display: block;
            padding-bottom: 6px
        }

        .ptype-grid-title.-truncate {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 100%
        }

        .ptype-grid6 {
            list-style: none;
            overflow: hidden
        }

        .ptype-grid6 > li {
            float: left;
            vertical-align: top;
            width: 16.666666%
        }

        .ptype-grid6-item {
            border: 2px solid transparent;
            border-radius: 2px;
            box-sizing: border-box;
            height: 100%;
            margin: 0 1px;
            position: relative;
            text-align: center;
            -webkit-transition: border-color 0.1s ease-out;
            transition: border-color 0.1s ease-out
        }

        .ptype-grid6-item:hover, .ptype-grid6-item.-active {
            border-color: #ffac00
        }

        .ptype-grid6-item > .title {
            color: #464646;
            display: block;
            font-size: 1.5em;
            line-height: 1.2;
            padding: 64% 7px 5%;
            position: relative
        }

        .ptype-grid6-item > .title.-linklike {
            cursor: pointer
        }

        .ptype-grid6-item > .title.-linklike:hover {
            text-decoration: underline
        }

        .ptype-grid6-item > .img {
            height: auto;
            left: 0;
            margin: auto;
            max-width: 100%;
            position: absolute;
            right: 0;
            top: 8px;
            width: 80%
        }

        .ptype-shifter-wrap {
            display: none
        }

        .ptype-lst-parent {
            margin-bottom: 32px;
            margin-top: 16px
        }

        .ptype-lst-nav-h {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
            z-index: 11
        }

        .ptype-lst-nav {
            height: 63px;
            letter-spacing: -0.31em;
            list-style: none;
            position: relative;
            width: 200%
        }

        .ptype-lst-nav > .item {
            box-sizing: border-box;
            display: inline-block;
            letter-spacing: normal;
            padding: 4px 0;
            position: relative;
            -webkit-transition: left 0.4s, background 0.2s ease;
            transition: left 0.4s, background 0.2s ease;
            vertical-align: top;
            width: 10%
        }

        .ptype-lst-nav > .item.-hide {
            display: none
        }

        .ptype-lst-nav > .item > .link {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            color: #111;
            display: block;
            font-size: 1em;
            font-weight: 400;
            line-height: 1.538em;
            text-align: center
        }

        .ptype-lst-nav > .item > .link:hover {
            text-decoration: none
        }

        .ptype-lst-nav > .item > .link.active {
            font-weight: 400;
            text-decoration: none
        }

        .ptype-lst-nav > .item > .link.active::after {
            border-bottom: 2px solid #717d90;
            bottom: -2px;
            content: '';
            display: block;
            left: 0;
            position: absolute;
            width: 100%
        }

        .ptype-lst-icon {
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: 0, auto 27px;
            display: block;
            height: 40px;
            position: relative;
            width: 100%
        }

        .ptype-lst-nav.-fixed .ptype-lst-icon {
            background-position: 50% 50%
        }

        .service-types-shifter {
            margin: 10px 0 40px !important
        }

        .service-types-list {
            list-style: none;
            overflow: hidden
        }

        .service-types-list > .service-types-item {
            float: left;
            height: 100%;
            position: relative;
            vertical-align: top;
            width: 150px
        }

        .service-types-list > .service-types-item > .link {
            color: #111
        }

        .service-types-list > .service-types-item > .link > .text {
            display: block;
            font-size: 16px;
            padding: 70% 6px 15%;
            position: relative
        }

        .service-types-list > .service-types-item > .link > .img {
            height: auto;
            left: 0;
            margin: auto;
            max-width: 100%;
            position: absolute;
            right: 0;
            width: 95%
        }

        .shifter {
            position: relative;
            width: 100%
        }

        .module-loader {
            position: relative
        }

        .module-loader::after {
            -webkit-animation: preloader-rotate 0.7s infinite linear;
            animation: preloader-rotate 0.7s infinite linear;
            background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMCIgdmlld0JveD0iMCAwIDMwIDMwIj48cGF0aCBkPSJNMTUgMzBhMTUgMTUgMCAxMTE1LTE1IDE1IDE1IDAgMDEtMTUgMTV6bTAtMjcuNzNBMTIuNzMgMTIuNzMgMCAxMDI3LjczIDE1IDEyLjc1IDEyLjc1IDAgMDAxNSAyLjI3eiIgZmlsbD0iIzFhMWExYSIgb3BhY2l0eT0iLjciLz48cGF0aCBkPSJNMjcuNzEgMTUuNDhBMTIuNzQgMTIuNzQgMCAwMTE1IDI3LjczVjMwYTE1IDE1IDAgMDAxNS0xNC41MnoiIGZpbGw9IiNmZmYiIG9wYWNpdHk9Ii43Ii8+PC9zdmc+);
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: contain;
            content: '';
            height: 30px;
            left: 50%;
            position: absolute;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            -webkit-transform-origin: 50% 50%;
            transform-origin: 50% 50%;
            width: 30px
        }

        .module-loader.nopreload::after {
            content: none !important
        }

        .gbox_loader {
            display: none
        }

        .gal_po_holder {
            display: none
        }

        .aside_slct_opts {
            display: none
        }

        .big-video-block {
            display: none
        }

        .subcats1-item {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            font-size: 1.6em;
            min-height: 4em
        }

        .prod_grd, .prod_lst {
            list-style: none
        }

        .prod_grd > li, .prod_lst > li {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex
        }

        .prod_grd .lst_a, .prod_lst .lst_a {
            color: #464646
        }

        .prod_grd .lst_ic_h::before, .prod_lst .lst_ic_h::before {
            content: '';
            display: block;
            padding-top: 100%;
            width: 100%
        }

        .prod_grd .lst_ic_h > .lazy-loading, .prod_lst .lst_ic_h > .lazy-loading {
            display: block;
            padding-bottom: 100%;
            width: 100%
        }

        .prod_grd .lst-descr-text, .prod_grd .lst_id, .prod_lst .lst-descr-text, .prod_lst .lst_id {
            display: none
        }

        .prod_grd {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap
        }

        .prod_grd > li {
            -webkit-box-align: stretch;
            -webkit-align-items: stretch;
            align-items: stretch;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column;
            margin: 0 1.8% 16px;
            width: 46.4%
        }

        .prod_grd .lst_ic_h {
            overflow: hidden;
            position: relative;
            width: 100%
        }

        .prod_grd .lst_ic {
            bottom: 0;
            height: auto;
            margin: auto;
            position: absolute;
            top: 0;
            width: 100%
        }

        .prod_grd .lst_featured_video, .prod_grd .lst_features, .prod_grd .lst-type-icons, .prod_grd .prod_avail {
            background-image: none;
            display: none
        }

        .prod_lst {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column
        }

        .prod_lst > li {
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            flex-direction: row;
            margin: 8px 8px 0;
            padding-bottom: 10px;
            position: relative
        }

        .prod_lst .lst_ic_h {
            -webkit-flex-shrink: 0;
            flex-shrink: 0;
            position: relative;
            width: 50%
        }

        .prod_lst .lst_ic {
            height: auto;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%
        }

        .prod_lst .lst_main {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            flex-direction: column;
            -webkit-flex-shrink: 1;
            flex-shrink: 1;
            margin-left: 5%;
            min-width: 0;
            position: relative;
            width: 100%
        }

        @media only screen and (max-width: 420.98px) {
            #js-media-query-state {
                -webkit-transform: scale(7);
                transform: scale(7)
            }

            #js-media-query-state::after {
                content: 'mobile-small'
            }

            .mobile-small-show {
                display: block !important
            }

            .mobile-small-hide {
                display: none !important
            }

            .head_img_minh::after {
                padding-bottom: 50%
            }

            .head_img_minh.-big::after {
                padding-bottom: 70%
            }

            .head_img_aligner.-align-left {
                left: 86%
            }

            .head_img_minh.-big > .head_img_aligner.-has-mobile-src {
                left: 80%
            }

            .head-nav.-with-mmy, .head-nav.-with-mmy .simple-slider-wrap {
                height: 374px
            }

            .select-vehicle > .nav.-garage > .link {
                background-position: 2px 48%;
                font-size: 1.4em;
                line-height: 1.3
            }

            .select-vehicle > .nav.-garage > .link.-i1-31 {
                background-position: 8px 48%
            }

            .select-vehicle > .nav.-garage > .link.-i1-31.-active {
                padding-left: 60px
            }

            .ptype-grid.-col-6 > li {
                margin: 4px 1%;
                width: 31.333333%
            }

            .ptype-grid.-col-6 .ptype-grid-img {
                width: 80%
            }

            .ptype-grid.-col-5 > li {
                margin: 4px 1%;
                width: 48%;
            }

            .ptype-grid.-col-5 > li::before {
                display: none
            }

            .ptype-grid.-col-5 .ptype-grid-img {
                width: 80%
            }

            .ptype-grid.-col-6 .ptype-grid-a {
                padding-top: 100%
            }

            .ptype-grid.-col-9 .ptype-grid-a {
                padding-top: 60%
            }
        }

        @media only screen and (min-width: 421px) and (max-width: 740.98px) {
            #js-media-query-state {
                -webkit-transform: scale(6);
                transform: scale(6)
            }

            #js-media-query-state::after {
                content: 'mobile-medium'
            }

            .mobile-medium-show {
                display: block !important
            }

            .mobile-medium-hide {
                display: none !important
            }

            .h1-fat {
                font-size: 3em;
                margin: 16px 8px
            }

            .nav-tool {
                margin-right: 10px
            }

            .header-search-btn {
                right: 64px
            }

            .head_img_minh::after {
                padding-bottom: 44%
            }

            .head_img_aligner.-align-left {
                left: 75%
            }

            .head_img_minh.-big > .head_img_aligner.-has-mobile-src {
                left: 50% !important
            }

            .head-nav.-with-mmy, .head-nav.-with-mmy .simple-slider-wrap {
                height: 386px
            }

            .head-nav.-with-mmy .head-nav-inner {
                padding-left: 40px;
                padding-right: 40px
            }

            .select-vehicle > .nav.-garage > .link {
                font-size: 1.4em;
                line-height: 1.3;
                padding-right: 12px
            }

            .my-garage-line .mygarage-dd {
                box-sizing: border-box;
                max-width: 100%;
                min-width: 310px
            }

            .ptype-grid-h:last-child {
                margin-bottom: 10px
            }

            .ptype-grid > .li {
                margin: 6px 1%;
                width: 31.333%
            }

            .ptype-grid.-col-5 .ptype-grid-a, .ptype-grid.-col-6 .ptype-grid-a {
                padding-bottom: 16px;
                padding-top: 100% !important;
            }

            .ptype-grid.-col-5 .ptype-grid-img, .ptype-grid.-col-6 .ptype-grid-img {
                top: 14%
            }

            .ptype-grid.-col-6 > .li > .lazy-loading {
                padding-top: 58%;
                top: 14%;
                width: 90%
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 58%;
                top: 14%;
                width: 90%
            }

            .ptype-grid-a {
                padding-top: 97%
            }

            .ptype-grid.-col-9 .ptype-grid-a {
                padding-top: 56%
            }

            .ptype-lst-nav > .item {
                width: 7.14%
            }

            .service-types-shifter {
                padding-top: 20px
            }

            .service-types-list > .service-types-item {
                width: 33.33333333%
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px) {
            #js-media-query-state {
                -webkit-transform: scale(5);
                transform: scale(5)
            }

            #js-media-query-state::after {
                content: 'mobile-large'
            }

            .mobile-large-show {
                display: block !important
            }

            .mobile-large-hide {
                display: none !important
            }

            .h1-fat.-searchresult {
                margin-bottom: 18px
            }

            .head-stores {
                margin-left: 44px
            }

            .head-stores > li.hot::after {
                right: 0
            }

            .head-stores > li > .item {
                padding-right: 8px
            }

            .head-stores > li > .item.-active {
                padding-right: 25px
            }

            .header-top {
                height: 80px
            }

            .header-top .header-dd-h {
                top: 64px
            }

            .header-account {
                margin-top: 15px
            }

            .head_logo_a {

                background-size: 122px auto;
                bottom: 0;
                left: 80px;
            }
            .-sm-white {

background-size: 122px auto;
bottom: 0;
left: 80px;
}

            .head_logo_a.-carid::after {
                height: 20px;
                left: 96px;
                top: 30px;
                width: 120px
            }
            .-sm-white.-carid::after {
                height: 20px;
                left: 96px;
                top: 30px;
                width: 120px
            }

            .nav-tool-wrap {
                margin-top: 15px
            }

            .header-search-btn {
                margin: 0 210px 0 300px;
                top: 20px
            }

            .header-search-btn.-short-logo {
                margin-left: 164px
            }

            .header-search-btn.-no-garage {
                margin-right: 160px
            }

            .head-dd > .it > .head-dd-main {
                padding-right: 16px
            }

            .head_img_minh::after {
                padding-bottom: 38.4%
            }

            .head_img_minh.-big::after {
                padding-bottom: 336px
            }

            .head_img_aligner.-align-left {
                left: 66%
            }

            .head-nav.-with-mmy, .head-nav.-with-mmy .simple-slider-wrap {
                height: 360px
            }

            .head-nav.-with-mmy .head-nav-inner {
                max-width: 920px
            }

            .head-nav-title {
                font-size: 3.8em
            }

            .head-nav.-with-mmy .head-nav-title {
                font-size: 4em;
                line-height: 1.26;
                margin-bottom: 8px
            }

            .head-nav.-with-mmy .head-nav-subtitle {
                font-size: 1.8em
            }

            .select-vehicle > .nav > .link {
                background-position: right 20px top 50%
            }

            .select-vehicle > .nav > .link:first-child {
                min-width: 300px
            }

            .select-vehicle-items.-tires {
                display: block
            }

            .aside_slct.-not-refine::after {
                width: 44%
            }

            .my-garage-line .mygarage-dd-container {
                min-width: 416px
            }

            .ptype-grid-h {
                margin-top: 20px
            }

            .ptype-grid-h:last-child {
                margin-bottom: 40px
            }

            .ptype-grid > .li {
                margin: 8px 1%;
                width: 23%
            }

            .ptype-grid.-col-6 > .li > .lazy-loading {
                padding-top: 58%;
                width: 90%
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 58%;
                width: 90%
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                padding-top: 39%;
                width: 60%
            }

            .ptype-grid.-col-9 .ptype-grid-img {
                width: 60%
            }

            .ptype-grid-a {
                font-size: 1.4em;
                padding-top: 105%
            }

            .ptype-grid.-col-9 .ptype-grid-a {
                padding-top: 48%
            }

            .ptype-grid-a::after {
                font-size: 11px;
                line-height: 1
            }

            .ptype-lst-nav-h {
                margin: 0 8px
            }

            .ptype-lst-nav > .item {
                width: 5.55%
            }

            .ptype-lst-nav:not(.-fixed) .ptype-lst-icon {
                background-size: 0, auto 36px;
                margin-bottom: 5px
            }

            .service-types-list > .service-types-item {
                width: 25%
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px) {
            #js-media-query-state {
                -webkit-transform: scale(4);
                transform: scale(4)
            }

            #js-media-query-state::after {
                content: 'desktop-small'
            }

            .desktop-small-hide {
                display: none !important
            }

            .head-stores {
                margin-left: 50px
            }

            .head-stores > li.hot::after {
                right: -8px
            }

            .head-stores > li > .item {
                padding-left: 54px;
                padding-right: 10px
            }

            .header-top {
                height: 88px
            }

            .header-top .header-dd-h {
                top: 84px
            }

            .head_img_minh {
                height: 346px
            }

            .head_img_aligner.-align-left {
                left: 62%
            }

            .head-nav-title {
                font-size: 5.2em
            }

            .select-vehicle > .nav > .link {
                width: 26%
            }

            .select-vehicle > .nav > .link:first-child {
                width: 36%
            }

            .ptype-grid.-col-5 > .li >
            .lazy-loading {
                padding-top: 52%;
                width: 80%
            }

            .ptype-grid.-col-9 {
                width: 80%
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                padding-top: 42%
            }

            .ptype-grid-img {
                width: 80%
            }

            .ptype-grid-a {
                padding-top: 88%
            }

            .ptype-grid6-item > .title {
                font-size: 1.4em
            }

            .ptype-lst-nav:not(.-fixed) {
                margin: 0 auto;
                width: 80%
            }

            .service-types-list > .service-types-item {
                width: 20%
            }
        }

        @media only screen and (min-width: 1281px) and (max-width: 1600.98px) {
            #js-media-query-state {
                -webkit-transform: scale(3);
                transform: scale(3)
            }

            #js-media-query-state::after {
                content: 'desktop-medium'
            }

            .head-dd {
                margin-left: 162px
            }

            .head-dd > .it > .head-dd-main {
                padding-left: 16px;
                padding-right: 16px
            }

            .head_img_minh {
                height: 372px
            }

            .head_img_aligner.-align-left {
                left: 58%
            }

            .head-nav-title {
                font-size: 5.4em
            }

            .ptype-grid.-col-6 > .li > .lazy-loading {
                padding-top: 46%;
                top: 60px;
                width: 170px
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 46%;
                top: 60px;
                width: 170px
            }

            .ptype-grid.-col-9 {
                width: 75%
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                padding-top: 35%
            }

            .ptype-lst-nav:not(.-fixed) {
                margin: 0 auto;
                width: 75%
            }
        }

        @media only screen and (min-width: 1601px) {
            #js-media-query-state {
                -webkit-transform: scale(2);
                transform: scale(2)
            }

            #js-media-query-state::after {
                content: 'desktop-large'
            }

            .product-list-with-filter-holder {
                margin-right: -300px;
                padding-right: 300px
            }

            .head-dd > .it > .head-dd-main > .link > span, .head-dd > .it > .head-dd-main .extra, .head-dd > .it > .head-dd-main .extra + .link {
                display: inline
            }

            .head_img_minh {
                height: 384px
            }

            .head_img_minh.-big {
                height: 550px
            }

            .head-img-holder {
                height: 460px;
                overflow: hidden
            }

            .head-img-holder .head_img_minh {
                top: 50%;
                -webkit-transform: translate(0, -50%);
                transform: translate(0, -50%)
            }

            .head-nav-title {
                font-size: 5.6em
            }

            .ptype-grid.-col-6 > .li {
                width: 15.16%
            }

            .ptype-grid.-col-6 > .li > .lazy-loading {
                padding-top: 56%;
                top: 60px;
                width: 170px
            }

            .ptype-grid.-col-5 > .li > .lazy-loading {
                padding-top: 46%;
                top: 60px;
                width: 170px
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                padding-top: 50%
            }

            .ptype-lst-nav.-fixed .ptype-lst-icon {
                background-size: 0, auto 46px;
                height: 56px;
                margin-bottom: 8px
            }
        }

        @media only screen and (max-width: 420.98px), only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px) {
            .main_wide p img {
                height: auto;
                max-width: 100%
            }

            .wrap .wrap {
                padding: 0
            }

            .desktop-show {
                display: none !important
            }

            .desktop-hide {
                display: block !important
            }

            .mobile-show {
                display: block !important
            }

            .mobile-hide {
                display: none !important
            }

            .head_img_minh::after {
                content: '';
                display: block;
                width: 100%
            }

            .select-vehicle > .nav.enabled {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex;
                width: 100%
            }

            .select-vehicle > .nav.enabled > .link {
                background-image: none;
                float: none
            }

            .select-vehicle > .nav:not(.enabled) > .link {
                display: none
            }

            .select-vehicle > .nav:not(.enabled) > .link:first-child {
                display: block;
                width: 100%
            }

            .select-vehicle > .content > .tab.-light {
                border-radius: 0 0 4px 4px
            }

            .select-vehicle > .nav:not(.enabled) + .content .select-vehicle-content-spacer {
                padding-top: 0
            }

            .aside_slct.-not-refine.-with-marker .aside_slct_value {
                margin-left: 38px
            }

            .nav.enabled > .link:not(.-active) .my-garage-line {
                z-index: 0
            }

            .breadcrumbs-holder .breadcrumbs-ov {
                overflow-x: auto;
                padding-bottom: 12px
            }

            .breadcrumbs {
                white-space: nowrap
            }

            .ptype-lst-parent {
                width: 100%
            }

            .ptype-lst-nav > .item:first-child {
                margin-left: 0
            }
        }

        @media only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px) {
            .landing_pages_descr > a, .landing_pages_descr .text-height-overflow > a {
                margin: 0 !important;
                max-width: 48%
            }

            .landing_pages_descr > a > img, .landing_pages_descr .text-height-overflow > a > img {
                height: auto !important;
                margin: 32px 1% !important;
                max-width: 98%;
                width: auto !important
            }

            .head_img_minh.-big > .head_img_aligner.-align-left {
                left: 54%
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .landing_pages_descr h2 {
                font-size: 2.1em
            }

            .landing_pages_descr ul {
                margin-left: 3em
            }

            .main-content {
                margin-top: 0
            }

            .main-content.-overlay::after {
                background: rgba(0, 0, 0, 0.75);
                content: '';
                height: 100%;
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 25
            }

            .h1-fat {
                font-size: 3.6em;
                margin: 24px 8px 40px
            }

            .h1-fat.-searchresult {
                margin-top: 0
            }

            .head-stores-h {
                height: 32px
            }

            .head-stores::after {
                display: none
            }

            .head-stores > li {
                font-size: 1.2em;
                position: relative
            }

            .head-stores > li.hot::after {
                background: url(https://cdn.carid.com/css/prod-images/0f575720.svg) no-repeat left top;
                background-size: 100% auto;
                content: '';
                height: 10px;
                position: absolute;
                top: 2px;
                width: 22px
            }

            .head-stores > li > .item {
                height: 32px;
                padding-top: 9px
            }

            .head-stores > li > .item:not(.-active) {
                color: #fff;
                display: block;
                opacity: 0.6;
                -webkit-transition: opacity 0.25s;
                transition: opacity 0.25s
            }

            .header-top {
                background-color: #fff
            }

            .header-top .header-dd-h {
                display: block
            }

            .left-menu-icon {
                background-size: 29px 24px;
                height: 80px;
                width: 60px
            }

            .header-dd-h {
                font-size: 10px;
                opacity: 0;
                position: absolute;
                -webkit-transform: translateY(-10px);
                transform: translateY(-10px);
                -webkit-transition: opacity 0.3s ease 0.2s, visibility 0s ease 0.3s, -webkit-transform 0.3s ease 0.2s;
                transition: opacity 0.3s ease 0.2s, visibility 0s ease 0.3s, -webkit-transform 0.3s ease 0.2s;
                transition: opacity 0.3s ease 0.2s, transform 0.3s ease 0.2s, visibility 0s ease 0.3s;
                transition: opacity 0.3s ease 0.2s, transform 0.3s ease 0.2s, visibility 0s ease 0.3s, -webkit-transform 0.3s ease 0.2s;
                visibility: hidden
            }

            .no-transform .header-dd-h {
                display: none
            }

            .header-dd-hover-element:hover .header-dd-h {
                display: block
            }

            .help-center-nav-btn {
                color: #d4252a;
                cursor: pointer;
                line-height: 1;
                padding: 10px 14px 10px 10px;
                position: relative
            }

            .help-center-nav-btn > span {
                font-size: 14px;
                font-weight: 500
            }

            .nav-tool-wrap {
                margin-right: 0
            }

            .nav-tool.-account {
                display: block;
                padding-left: 30px
            }

            .nav-tool.-wishlist, .nav-tool.-garage {
                display: block
            }

            .header-search-btn {
                border-color: #757575;
                cursor: pointer;
                left: auto;
                line-height: 40px;
                padding: 0 75px 0 20px;
                position: relative;
                right: auto
            }

            .header-search-btn::before {
                display: block;
                left: auto;
                overflow: hidden;
                text-overflow: ellipsis;
                top: auto;
                white-space: nowrap
            }

            .header-search-btn::after {
                background-size: 100% auto;
                height: 23px;
                right: 12px;
                top: 8px;
                width: 25px
            }

            .head-dd-h {
                border-bottom: 3px solid #464646;
                display: block;
                height: 40px;
                width: 100%
            }

            .head-nav {
                height: 550px
            }

            .head-nav-inner {
                margin-top: 0
            }

            .head-nav-title {
                letter-spacing: 0.02em;
                line-height: 1.1;
                margin-bottom: 20px
            }

            .head-nav.-with-mmy .head-nav-title {
                letter-spacing: normal
            }

            .head-nav-subtitle {
                color: #fff;
                display: block;
                font-size: 2.1em;
                font-weight: 300;
                letter-spacing: 0.02em;
                line-height: 1.2;
                margin-bottom: 48px
            }

            .simple-slider-wrap {
                display: block;
                height: 550px;
                left: 50%;
                margin-left: -960px;
                overflow: hidden;
                position: absolute;
                top: 0;
                width: 1920px
            }

            .head-main-logo.-with-main-select-bar {
                bottom: 104px
            }

            .head-nav-inner > .select-vehicle-spacer {
                margin-bottom: 26px
            }

            .head-shop-by-links-item, .head-shop-by-links-item > .link {
                text-align: left
            }

            .head-shop-by-links-item > .count {
                display: inline
            }

            .select-vehicle-spacer {
                margin-top: -74px
            }

            .select-vehicle-spacer.-top-indent {
                margin-top: 32px
            }

            .select-vehicle > .nav.-garage > .link {
                padding-right: 24px
            }

            .select-vehicle > .nav.-garage > .link > .link-title {
                display: block
            }

            .select-vehicle > .content > .tab {
                display: block
            }

            .select-vehicle-spacer:not(.-fixed) .content.-gallery .select-vehicle-content-spacer {
                padding: 24px
            }

            .aside_slct_wrap {
                height: 56px;
                margin-bottom: 0
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct_wrap {
                height: 63px
            }

            .select-vehicle-spacer.-fixed .aside_slct_wrap {
                height: 48px
            }

            .select-vehicle-content-spacer.-two-columns .aside_slct_wrap {
                margin-bottom: 16px
            }

            .select-vehicle-content-spacer.-two-columns .-tires .aside_slct_wrap {
                margin-bottom: 0
            }

            .select-vehicle-items-inner {
                -webkit-box-align: center;
                -webkit-align-items: center;
                align-items: center;
                display: -webkit-box;
                display: -webkit-flex;
                display: flex;
                -webkit-flex-wrap: wrap;
                flex-wrap: wrap;
                width: 100%
            }

            .select-vehicle-items {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex
            }

            .select-vehicle-items.-half {
                width: 50%
            }

            .aside_slct.-not-refine {
                margin-bottom: 0;
                padding-bottom: 12px;
                padding-top: 28px
            }

            .aside_slct.-not-refine::before {
                top: 26px
            }

            .aside_slct.-not-refine::after {
                top: 21px
            }

            .aside_slct.-not-refine.-active::after, .aside_slct.-not-refine.-selected::after {
                top: 12px
            }

            .aside_slct.-not-refine.-tire-popup::after {
                top: 24px
            }

            .aside_slct.-not-refine.-active.-tire-popup::after, .aside_slct.-not-refine.-selected.-tire-popup::after {
                top: 15px
            }

            .select-vehicle-col + .select-vehicle-col > .aside_slct_wrap > .aside_slct.-not-refine {
                margin: 0 0 0 8px;
                width: calc(100% - 8px)
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine {
                padding-bottom: 15px;
                padding-top: 32px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine::before {
                right: 16px;
                top: 30px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine::after {
                top: 24px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine.-active::after, .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine.-selected::after {
                top: 16px
            }

            .aside_slct.-not-refine.-bordered::before {
                top: 26px
            }

            .select-vehicle-spacer.-index .aside_slct.-not-refine .aside_slct_value {
                font-size: 1.5em
            }

            .aside_slct.-not-refine .aside-slct-marker {
                top: 16px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .aside_slct.-not-refine .aside-slct-marker {
                font-size: 1.5em;
                height: 26px;
                line-height: 1.75;
                top: 20px
            }

            .select-vehicle-spacer.-index:not(.-fixed) .select-vehicle-button {
                padding-bottom: 23px;
                padding-top: 23px
            }

            .select-vehicle-button.-after-selects {
                margin-left: 8px;
                width: 10%
            }

            .select-vehicle-button.-tire-size-pp {
                margin-top: 0
            }

            .tires-sizes-selection .select-vehicle-items.-front-size.-rear-active .select-vehicle-button {
                display: block;
                visibility: hidden
            }

            .my-garage-line .mygarage-dd-container.-both-empty {
                min-width: 302px
            }

            .breadcrumbs-container {
                margin-bottom: 24px;
                margin-top: 24px;
                overflow: hidden
            }

            .breadcrumbs-holder {
                margin: 24px 8px
            }

            .breadcrumbs-container .breadcrumbs-holder {
                margin-bottom: 0;
                margin-top: 0
            }

            .breadcrumbs-holder.-product {
                margin: 15px 8px
            }

            .select-vehicle-dept-switchers {
                display: block;
                float: right;
                margin: 0 8px 0 20px;
                z-index: 2
            }

            .ptype-grid-bg {
                background-position: 40% 0;
                background-repeat: no-repeat;
                margin-top: 18px;
                padding-top: 24px
            }

            .ptype-grid-h2 {
                font-size: 2.8em;
                margin-bottom: 32px
            }

            .ptype-grid-h2 > .inner > .name {
                margin-bottom: 14px
            }

            .ptype-grid-h2 > .inner > .descr {
                color: #757575;
                display: block;
                font-size: 0.428em;
                font-weight: 500;
                line-height: 1.16;
                -webkit-box-ordinal-group: 3;
                -webkit-order: 2;
                order: 2
            }

            .ptype-grid > .li::before {
                top: 20px
            }

            .ptype-grid.-col-6 > .li::before {
                display: block
            }

            .ptype-grid.-col-9 > .li {
                width: 126px
            }

            .ptype-lst-parent {
                margin-top: 0
            }
        }

        @media only screen and (orientation: landscape), only screen and (orientation: portrait) {
            body {
                -webkit-text-size-adjust: 100%
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            #scrollfix {
                min-width: 1000px
            }

            .wrap {
                max-width: 1296px;
                min-width: 1000px;
                width: 94%
            }

            .wrap .wrap {
                margin: 0 auto
            }

            .product-list-with-filter-holder {
                float: right
            }

            .h1-fat.-searchresult {
                margin-bottom: 34px
            }

            .head-stores > li {
                display: block;
                -webkit-box-flex: 0;
                -webkit-flex-grow: 0;
                flex-grow: 0;
                -webkit-flex-shrink: 1;
                flex-shrink: 1
            }

            .head-stores > li > .item:not(.-active) {
                text-indent: 0
            }

            .head_header {
                min-width: 1000px
            }

            .header-account {
                margin-right: 13px;
                margin-top: 38px
            }

            /*.header-account::before {*/
            /*    background: #757575;*/
            /*    content: '';*/
            /*    display: block;*/
            /*    float: right;*/
            /*    height: 15px;*/
            /*    margin-top: 8px;*/
            /*    width: 1px*/
            /*}*/

            .left-menu-icon {
                top: 35px
            }

            .head_logo_a {

                background-size: 155px auto;
                bottom: -46px;
                left: 104px;
                width: 170px;
            }
            .-sm-white {

background-size: 155px auto;
bottom: -46px;
left: 104px;
width: 170px;
}

            .head_logo_a::before {
                bottom: 14%;
                color: #fff;
                content: attr(data-text);
                display: inline-block;
                font-size: 0.9em;
                left: 0;
                position: absolute;
                right: 0;
                text-align: center;
                text-indent: 0;
                z-index: 2
            }

            .head_logo_a.-carid::after {
                height: 24px;
                left: 106px;
                top: 42px;
                width: 149px
            }

            .-sm-white::before {
                bottom: 14%;
                color: #fff;
                content: attr(data-text);
                display: inline-block;
                font-size: 0.9em;
                left: 0;
                position: absolute;
                right: 0;
                text-align: center;
                text-indent: 0;
                z-index: 2
            }

            .-sm-white.-carid::after {
                height: 24px;
                left: 106px;
                top: 42px;
                width: 149px
            }

            .nav-tool-wrap {
                margin-top: 38px
            }

            .nav-tool {
                height: 34px;
                margin: 0 10px;
                min-width: 30px
            }

            .nav-tool .-txt {
                display: inline-block;
                padding-right: 14px;
                position: relative;
                text-align: left;
                text-indent: 0
            }

            .nav-tool > .icon {
                left: 10px;
                top: 1px
            }

            .header-garage-subtitle {
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
                color: #757575;
                display: block;
                font-weight: 400;
                text-transform: none
            }

            .nav-tool.-account {
                width: 124px
            }

            .nav-tool.-garage {
                padding-left: 32px;
                width: 140px
            }

            .header-search-btn {
                margin: 0 405px 0 340px;
                top: 32px
            }

            .header-search-btn::before { /*content:"Search by Make Model Year, Product Type, Part Number, or Brand..."*/
            }

            .header-search-btn.-short-logo {
                margin-left: 180px
            }

            .header-search-btn.-no-garage {
                margin-right: 252px
            }

            .header-search-btn.-no-garage::before {
                content: "Search by Product Type, Part Number, or Brand..."
            }

            .count-item.-type-1 {
                right: -6px;
                top: -3px
            }

            .head_img_minh {
                min-width: 1000px
            }

            .head-img-overlay {
                width: auto
            }

            .head-nav-inner {
                max-width: 1296px;
                min-width: 1000px;
                width: 94%
            }

            .head-nav.-with-mmy .head-nav-inner {
                max-width: 1296px
            }

            .select-vehicle > .nav > .link {
                background-position: 95% 48%;
                background-repeat: no-repeat;
                border-left: none;
                margin-left: 5px
            }

            .select-vehicle > .nav > .link.-icon-select-vehicle {
                background-image: url(https://cdn.carid.com/css/prod-images/b34eea73.svg)
            }

            .select-vehicle > .nav > .link.-icon-size {
                background-image: url(https://cdn.carid.com/css/prod-images/95367a29.svg)
            }

            .select-vehicle > .nav > .link.-icon-star {
                background-image: url(https://cdn.carid.com/css/prod-images/2c6612f4.svg)
            }

            .select-vehicle-spacer.-in-popup .select-vehicle > .nav > .link.-light {
                background-image: none
            }

            .select-vehicle > .nav > .link.-light.-icon-size {
                background-image: url(https://cdn.carid.com/css/prod-images/a08d6515.svg)
            }

            .select-vehicle > .nav > .link.-light.-icon-star {
                background-image: url(https://cdn.carid.com/css/prod-images/8a9215a3.svg)
            }

            .select-vehicle > .nav > .link.-light.-icon-specifications {
                background-image: url(https://cdn.carid.com/css/prod-images/986aa652.svg)
            }

            .select-vehicle > .nav.-garage {
                display: block
            }

            .select-vehicle > .content {
                border-radius: 0 4px 4px
            }

            .select-vehicle-col-container-flex {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex
            }

            .modal-wrap .footer .select-vehicle-col-container-flex {
                display: block
            }

            .select-vehicle-items.-front-size.-rear-active.-in-popup, .select-vehicle-items.-rear-size.-rear-active.-in-popup {
                margin-top: 12px
            }

            .select-vehicle-items.-front-size.-in-popup {
                margin-top: 12px
            }

            .select-vehicle-items .aside_slct.-not-refine::after {
                width: 65%
            }

            .aside_slct.-not-refine.-with-marker::after {
                left: 55px
            }

            .aside_slct.-not-refine .aside-slct-marker {
                padding: 0 17px
            }

            .my-garage-line .mygarage-dd-container {
                min-width: 480px
            }

            .ptype-grid-h {
                margin-top: 0
            }

            .ptype-grid-h:last-child {
                margin-bottom: 56px
            }

            .ptype-grid > .li {
                margin: 8px 0.65%;
                width: 23.5%
            }

            .ptype-grid.-col-5 > .li {
                width: 18.7%
            }

            .ptype-grid.-col-9 {
                margin: 44px auto
            }

            .ptype-grid.-col-9 > .li > .lazy-loading {
                width: 100px
            }

            .ptype-grid.-col-9 .ptype-grid-img {
                width: 100px
            }

            .ptype-grid-a {
                font-size: 1.6em;
                left: auto
            }

            .ptype-grid.-col-9 .ptype-grid-a {
                padding-top: 86px
            }

            .ptype-grid-a:hover > .ptype-grid-title, .ptype-grid-a.-simple-title:hover {
                text-decoration: underline
            }

            .ptype-shifter-wrap {
                border-bottom: 1px solid #d8d8d8;
                display: block;
                margin: 0 1% 36px
            }

            .ptype-lst-nav-h {
                background-color: transparent
            }

            .ptype-lst-nav {
                height: auto;
                width: auto
            }

            .ptype-lst-nav:not(.-fixed) {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -webkit-flex-direction: row;
                flex-direction: row;
                -webkit-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -webkit-justify-content: center;
                justify-content: center
            }

            .ptype-lst-nav.-additional-row:not(.-fixed) {
                margin: 0 auto;
                width: 84%
            }

            .ptype-lst-nav > .item {
                margin: 8px 6px;
                width: 130px
            }

            .ptype-lst-nav > .item > .link {
                font-size: 1.6em;
                line-height: 1
            }

            .ptype-lst-nav.-additional-row:not(.-fixed) > .item {
                width: 13%
            }

            .ptype-lst-icon {
                background-position: 50% 0;
                background-size: 100px auto, 0;
                height: 88px
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px) {
            .product-list-with-filter-holder {
                margin-right: -250px;
                padding-right: 250px
            }

            .head_img_minh.-big > .head_img_aligner.-align-left {
                left: 50%
            }

            .head_img_minh.-big > .head_img_aligner img {
                height: 110.5%;
                margin-top: -16px
            }

            .ptype-grid.-col-6 > .li {
                width: 18.7%
            }

            .ptype-lst-nav.-fixed .ptype-lst-icon {
                background-size: 0, auto 32px;
                height: 40px;
                margin-bottom: 0
            }
        }

        @media only screen and (max-width: 420.98px), only screen and (min-width: 421px) and (max-width: 740.98px) {
            .h1-fat.-mobile-inline {
                display: inline;
                margin-left: 0
            }

            .head-stores > li > .item:not(.-active) {
                background: none
            }

            .head_header {
                border-bottom: 1px solid #d8d8d8;
                left: 0;
                position: relative;
                right: 0;
                top: 0;
                z-index: 3
            }

            .head-dd-cont-holder {
                display: none !important
            }

            .head-shop-by-links.-special-position {
                bottom: 24px
            }

            .select-vehicle > .nav:not(.enabled) > .link {
                cursor: auto
            }

            .select-vehicle > .content > .tab.-with-garage-tab {
                padding-top: 24px
            }

            .-two-columns .select-vehicle-col, .-two-columns .select-vehicle-col-container {
                -webkit-flex-basis: auto;
                flex-basis: auto
            }

            .select-vehicle-button, .select-vehicle-button.-after-selects {
                padding: 8px
            }

            .nav.enabled .my-garage-line {
                background-position: 0 50%;
                border-left: none;
                left: 20px;
                margin-left: 0;
                min-width: 118px;
                padding-left: 0;
                position: absolute;
                top: 60px;
                z-index: 24
            }

            .ptype-grid-bg {
                background: none !important
            }

            .ptype-grid.-col-5 .ptype-grid-a.-descr-outer::after, .ptype-grid.-col-6 .ptype-grid-a.-descr-outer::after {
                display: none
            }

            .ptype-grid.-col-5 > li::before {
                display: none
            }

            .ptype-grid.-col-9 .ptype-grid-img {
                width: 70%
            }
        }

        @media only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .head-stores {
                margin-left: 46px
            }

            .head-stores > li.hot::after {
                right: 6px
            }

            .header-top {
                height: 88px
            }

            .header-top .header-dd-h {
                top: 88px
            }

            .select-vehicle > .nav > .link {
                width: 28.5%
            }

            .select-vehicle > .nav > .link:first-child {
                width: 30.5%
            }

            .ptype-grid-img {
                top: 60px;
                width: 170px
            }

            .ptype-grid-a {
                padding-top: 200px
            }

            .service-types-list > .service-types-item {
                width: 16.666666%
            }
        }

        @media only screen and (max-width: 420.98px), only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px) {
            .head-stores > li > .item > .item-text-additional {
                display: none
            }
        }

        @media only screen and (min-width: 421px) and (max-width: 740.98px), only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px), only screen and (min-width: 1601px) {
            .header-account {
                display: block;
                float: right
            }

            .select-vehicle > .nav.-garage > .link {
                background-position: 12px 48%;
                padding-left: 64px
            }

            .select-vehicle > .nav.-garage > .link.-i1-31 {
                background-position: 22px 48%;
                padding-left: 74px
            }

            .select-vehicle > .nav.-garage > .link.-i1-31.-active {
                padding-left: 74px
            }

            .select-vehicle > .nav.-garage > .link.-active {
                padding-left: 64px
            }

            .my-garage-line .mygarage-dd {
                right: auto
            }

            .ptype-grid {
                margin-top: 32px
            }

            .ptype-grid-a {
                padding-bottom: 24px
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1601px) {
            .head-dd {
                margin-left: 168px
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px) {
            .my-garage-line .mygarage-dd {
                margin-top: 8px
            }
        }

        @media only screen and (min-width: 741px) and (max-width: 1023.98px), only screen and (min-width: 1024px) and (max-width: 1280.98px), only screen and (min-width: 1281px) and (max-width: 1600.98px) {
            .ptype-grid.-col-9 > .li {
                margin-left: 10px;
                margin-right: 10px
            }
        }

        #sidenav {
            font: "Roboto", "Arial", "Helvetica", sans-serif !important;
            font-size: 160%;
            text-align: left;
        }

        .img-icon {
            margin-top: -10px;
            margin-bottom: -15px;
            margin-right: 15px;
            width: 15%;

        }

        @media only screen and (min-width: 767px) and (max-width: 1024px) {
            .head_logo_a {
                width: 150px !important;
            }
        }
        @media screen and (max-width: 767px){
            .dnone{
                display: none !important; 
            }

        }
        
        @media only screen and (max-width:440px){
            .search-form-button
            {
                top: 9px !important;
                left: 85% !important;
            }
        }
        .search-form-field
        {
            border: none;
            border-bottom: 0.1px solid #7e7e7e;
            height: 39px;
        }
        .search-form-field:focus{
            outline: none !important;
            border:none;
            border-bottom: 0.1px solid #7e7e7e;
        }

        .search-form-button{
            position: absolute;
            top: 00px;
            left: 90%;
            width: 10%;
            height: 100%;
            background: transparent;
            border: none;
            z-index:1000;
        }
        .srch-btn{
            pointer-events: none;
            z-index: 1000;
        }

        .search-form-button:focus, .search-form-button:hover{
            background-color : rgba(246, 244, 244, 0.68);
            outline: none !important;
            border:none;
        }
        @media only screen and (max-width:736px){
            .search-form-field
            {
                top: 37px;
                left: 10px;
                width: 95% !important;
                height: 33px !important;
                margin-top: -36px !important;
            }
            .search-form-button
            {
                z-index: 1000;
                top: 0px !important;
                font-size: larger;
            }

            .prop-name
            {
                display: none;
            }
            .prop-number
            {
                margin-top: -4px;
            }

        }

    </style>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.1/css/mdb.min.css" rel="stylesheet">
    <link href="/css/enhance.css?v=e06beeea" rel="stylesheet">
    <link href="/css/fonts.css?v=1a68779c" rel="stylesheet">
    <link href="/css/mmenu.css" rel="stylesheet">
    <link href="/css/mburger.css" rel="stylesheet">
    <link href="/css/custom.css?v=02" rel="stylesheet">
    @yield('styles')
<style>

    @media only screen and (min-width : 320px) {
        #cart {
            font-size: 10px;
            margin-top: 20px;
            margin-right: 22px;

        }
        .badge {
            background-color: #6394F8;
            border-radius: 9px;
            color: white;
            display: inline-block;
            font-size: 8px;
            line-height: 1;
            padding: 1px 2px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }

    /* Extra Small Devices, Phones */
    @media only screen and (min-width : 480px) {
        #cart {
            font-size: 12px;
            margin-top: 20px;
            margin-right: 27px;

        }
        .badge {
            background-color: #6394F8;
            border-radius: 10px;
            color: white;
            display: inline-block;
            font-size: 8px;
            line-height: 1;
            padding: 2px 4px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        #cart {
            font-size: 16px;
            margin-top: 30px;
            margin-right: 30px;
        }
        .badge {
            background-color: #6394F8;
            border-radius: 10px;
            color: white;
            display: inline-block;
            font-size: 12px;
            line-height: 1;
            padding: 3px 7px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }

    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {
        #cart {
            font-size: 16px;
            margin-top: 40px;
            margin-right: 30px;
        }
        .badge {
            background-color: #6394F8;
            border-radius: 10px;
            color: white;
            display: inline-block;
            font-size: 12px;
            line-height: 1;
            padding: 3px 7px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }

    /* Large Devices, Wide Screens */
    @media only screen and (min-width : 1200px) {
        #cart {
            font-size: 16px;
            margin-top: 40px;
            margin-right: 10px;
        }
        .badge {
            background-color: #6394F8;
            border-radius: 10px;
            color: white;
            display: inline-block;
            font-size: 12px;
            line-height: 1;
            padding: 3px 7px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }
    }


    .shopping-cart {
        margin: 20px 0;
        float: right;
        background: white;
        width: 320px;
        position: relative;
        border-radius: 3px;
        padding: 20px;
    }

    .

    .shopping-cart-total {
        float: right;
    }

    .shopping-cart:after {
        bottom: 100%;
        left: 89%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-bottom-color: white;
        border-width: 8px;
        margin-left: -8px;
    }

    .cart-icon {
        color: #515783;
        font-size: 24px;
        margin-right: 7px;
        float: left;
    }

    }

</style>
</head>
<body class="body">

<div id="main-page">
    @include('frontend.partials.header')

    @yield('content')

    @include('frontend.partials.footer')
</div>
<div class="loader"></div>

<!-- Footer -->
<script src="{{ mix('js/app.js') }}"></script>
<script src="/js/mburger.js"></script>
<script src="/js/mmenu.js"></script>
<script>

    if(window.location.href.indexOf("products") > -1 && window.location.href.indexOf("cart") <= 0) {
        jQuery("body").css("display","block");
    }else{
        jQuery("body").css("display","none");
    }
</script>
<script type="text/javascript">
    if(window.location.href.indexOf("products") > -1 && window.location.href.indexOf("cart") <= 0) {
        jQuery("body").css("display","block");

    }else{
        jQuery(document).ready(function(){
            delay();
        });

        function delay() {
            var secs = 100;
            setTimeout('initFadeIn()', secs);
        }

        function initFadeIn() {
            jQuery("body").css("visibility","visible");
            jQuery("body").css("display","none");
            jQuery("body").fadeIn(1200);
        }
    }

</script>
<script>
    function submitSearchForm(){
        document.getElementById("search-form").submit();
    }
    $(document).ready(function() {
        $(document).on('change', '.make', function () {
            let make_id = $(this).val();
            if (make_id) {
                $.ajax({
                    url: '/products/' + make_id + '/models',
                    type: 'get',
                    success: function (response) {
                        $('.model').html(response);
                    }
                });
            }

        })
    });
</script>

<script>
    Mmenu.configs.classNames.selected = "active";
    Mmenu.configs.offCanvas.page.selector = "#main-page";

    document.addEventListener(
        "DOMContentLoaded", () => {
            new Mmenu("#sidenav", {
                "extensions": [
                    "pagedim-black",

                ]
            });
        }
    );
document.addEventListener(
        "DOMContentLoaded", () => {
            new Mmenu( "#cartmenu", {
               "extensions": [
                  "pagedim-black",
                  "position-right",
               ],
               "navbar": {
                "title": "MegaMotorSport.Pk",
                "content": ["close"]
                },
                "navbars": [
                  {
                     "position": "bottom",
                     "content": [
                        "<a href='{{route('products.cart')}}' class='btn btn-primary' style='color: white;'>View Cart</a>"
                     ]
                  },
                  {
                     "position": "bottom",
                     "content": [
                        "<a href='{{route('products.checkout')}}' class='btn btn-danger' style='color: white;'>Checkout</a>"
                     ]
                  }

               ]

 
            });
        }
    );

</script>

@yield('scripts')

</body>
</html>
