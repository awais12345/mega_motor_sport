@extends('admin.master')
@section('title') Dashboard @stop

@section('content')


    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Begin Breadcrumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Orders</li>
            </ol>
        </nav>
        <!-- end Breadcrumb -->

        <!-- Page Heading -->

        <div class="card">
            <div class="card-body">

                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="col-sm-8">
                        <h1 class="h3 mb-0 text-gray-800">Orders</h1>
                    </div>
                </div>

                @include('shared.errors')

                <div class="table-responsive">
                    <table class="table table-hover table-sm" id="ordertable">
                        <thead>
                        <tr class="text-center">
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Email</th>
                            <th scope="col">City</th>
                            <th scope="col">Status</th>
                            <th scope="col">Total</th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($orders as $key => $order)
                            <tr class="text-center">
                                <th scope="row">{{ $key+1 }}</th>
                                <th scope="row">{{ $order->full_name }}</th>
                                <th scope="row">{{ $order->phone }}</th>
                                <th scope="row">{{ $order->email }}</th>
                                <th scope="row">{{ $order->city }}</th>
                                <th scope="row">{!! $order->status_label !!} </th>
                                <th scope="row">{{ $order->total }}</th>
                                <th scope="row">{{ date('d-F-y', strtotime($order->created_at)) }}</th>
                                <td>
                                    <div class="dropdown">
                                        <a class="link" href="#" role="button" id="dropdownMenuLink1"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class='fa fa-ellipsis-v'></i>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{route('admin.orders.show', $order->id)}}">View</a>
                                            <a class="dropdown-item"
                                               href="{{route('admin.orders.status', [$order->id, 'pending'])}}">Pending</a>
                                            <a class="dropdown-item"
                                               href="{{route('admin.orders.status', [$order->id, 'delivered'])}}">Delivered</a>
                                            <a class="dropdown-item"
                                               href="{{route('admin.orders.status', [$order->id, 'completed'])}}">Complete</a>
                                            <a class="dropdown-item"
                                               href="{{route('admin.orders.status', [$order->id, 'canceled'])}}">Cancel</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="10" class="text-center text-danger">Record Not Found!</th>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
@section('scripts')

@endsection
