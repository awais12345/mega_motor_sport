<?php

use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', function () {
    return view('welcome');
})->name('dashboard');

Route::get('/auth/redirect/{provider}', 'Admin\SocialController@redirect');
Route::get('/callback/{provider}', 'Admin\SocialController@callback');

Route::group(['namespace' => 'frontend'], function(){
    Route::group(['prefix' => '/products', 'as' => 'products.'], function () {
        Route::get('/cart', 'ProductController@viewCart')->name('cart');
        Route::get('/checkout', 'ProductController@checkout')->name('checkout');
        Route::get('/add-product', 'ProductController@create')->name('create');
        Route::get('/{make}/models', 'ProductController@modelOptions');
        Route::get('/search', 'ProductController@search')->name('search');
        Route::get('/{product}', 'ProductController@show')->name('show');
        Route::get('add-to-cart/{product}', 'ProductController@addToCart')->name('add.cart');
        Route::get('update-cart/{product}', 'ProductController@updateCart')->name('update.cart');
        Route::get('remove-cart/{product}', 'ProductController@removeCart')->name('remove.cart');
        Route::post('place-order', 'ProductController@placeOrder')->name('place-order');
    });

    Route::group(['prefix' => '/sub-categories', 'as' => 'sub-category.'], function(){
       Route::get('/{subCategory}', 'SubCategoryController@show')->name('show');
    });

    Route::get('/track-my-order', 'PagesController@trackMyOrder');
    Route::get('/my-account', 'PagesController@myAccount');
    Route::get('/return-policy', 'PagesController@returnPolicy');
    Route::get('/faqs', 'PagesController@faqs');
    Route::get('/about-us', 'PagesController@aboutUs');
    Route::get('/contact-us', 'PagesController@contactUs');
    Route::post('send/contact-us/email', 'PagesController@sendContactUsEmail')->name('contact-us.email');
    Route::get('/feedback', 'PagesController@feedback');
    Route::get('/terms-and-conditions', 'PagesController@termsAndConditions');
    Route::get('/privacy-policy', 'PagesController@privacyPolicy');
    Route::get('/thank-you','ProductController@thankyou')->name('thankyou');
});



//admin routes
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin'], 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {
        Route::get('/', 'CategoryController@index')->name('index');
        Route::post('/', 'CategoryController@store')->name('store');
        Route::get('/{category}/edit', 'CategoryController@edit')->name('edit');
        Route::post('/{category}', 'CategoryController@update')->name('update');
        Route::get('/{category}/delete', 'CategoryController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'sub-categories', 'as' => 'sub-categories.'], function () {
        Route::get('/', 'SubCategoryController@index')->name('index');
        Route::post('/', 'SubCategoryController@store')->name('store');
        Route::get('/{subCategory}/edit', 'SubCategoryController@edit')->name('edit');
        Route::post('/{subCategory}', 'SubCategoryController@update')->name('update');
        Route::get('/{subCategory}/delete', 'SubCategoryController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'makes', 'as' => 'makes.'], function () {
        Route::get('/', 'MakeController@index')->name('index');
        Route::post('/', 'MakeController@store')->name('store');
        Route::get('/{make}/edit', 'MakeController@edit')->name('edit');
        Route::post('/{make}', 'MakeController@update')->name('update');
        Route::get('/{make}/delete', 'MakeController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'models', 'as' => 'models.'], function () {
        Route::get('/', 'MakeModelController@index')->name('index');
        Route::post('/', 'MakeModelController@store')->name('store');
        Route::get('/{model}/edit', 'MakeModelController@edit')->name('edit');
        Route::post('/{model}', 'MakeModelController@update')->name('update');
        Route::get('/{model}/delete', 'MakeModelController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'products', 'as' => 'products.'], function () {
        Route::get('/', 'ProductController@index')->name('index');
        Route::post('/', 'ProductController@store')->name('store');
        Route::get('/{product}/edit', 'ProductController@edit')->name('edit');
        Route::post('/{product}', 'ProductController@update')->name('update');
        Route::get('/{product}/delete', 'ProductController@destroy')->name('destroy');
        Route::get('/sub-categories/options', 'ProductController@subCategoryOptions')->name('sub-categories');
        Route::get('/models/options', 'ProductController@modelsOptions');
    });

    Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
        Route::get('/', 'OrderController@index')->name('index');
        Route::get('/{order}', 'OrderController@show')->name('show');
        Route::get('/{order}/{status}', 'OrderController@updateStatus')->name('status');
    });

});


Route::get('/home', function () {
    if (isAdministratorUser()) {
        return redirect()->route('admin.dashboard');
    } else {
        return redirect()->route('dashboard');
    }
});

Route::get('/migrate', function () {

    echo '<br>init migrate:install...';
    Artisan::call('migrate');
    echo 'done migrate:install';

});

Route::get('/migrate/{key}', function ($key) {

    echo '<br>init migrate:' . $key . '...';
    Artisan::call('migrate:' . $key);
    echo 'done migrate:' . $key;

});

Route::get('/db-seed', function () {
    try {
        echo '<br>init db:seed...';
        Artisan::call('db:seed');
        echo 'done db:seed';

    } catch (Exception $e) {
        return $e->getMessage();
    }

});

Route::get('/import', function (){
   $products = Product::all();
   foreach ($products as $product){
       if($product->category_id){
           $product->categories()->sync($product->category_id);
       }
       if($product->sub_category_id){
           $product->subCategories()->sync($product->sub_category_id);
       }
       if($product->make_id){
           $product->makes()->sync($product->make_id);
       }
       if($product->make_model_id){
           $product->makeModels()->sync($product->make_model_id);
       }
   }
   return 'imported successfully!';
});


Route::get('add/sku/', function (){
    $products = Product::all();
    foreach ($products as $product){
        $product->update(['sku' => substr(md5(microtime()), 0, 6)]);
    }
});
