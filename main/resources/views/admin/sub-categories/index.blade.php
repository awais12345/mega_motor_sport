@extends('admin.master')
@section('title') Dashboard @stop
@section('content')


    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Begin Breadcrumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Sub Categories</li>
            </ol>
        </nav>
        <!-- end Breadcrumb -->

        <!-- Page Heading -->

        <div class="card">
            <div class="card-body">

                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <div class="col-sm-8">
                        <h1 class="h3 mb-0 text-gray-800">Sub Categories</h1>
                    </div>
                    <div>
                        <a href="#add-category" class="btn btn-primary btn-sm" data-toggle="modal">Add Category</a>
                    </div>
                </div>

                @include('shared.errors')

                <div class="table-responsive">
                    <table class="table table-hover table-sm" id="subtable">
                        <thead>
                        <tr class="text-center">
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Parent Category</th>
                            <th scope="col">Image</th>
                            <th scope="col">Shipping</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($subCategories as $key => $subCategory)
                            <tr class="text-center">
                                <th scope="row">{{ $key+1 }}</th>
                                <td>{{ $subCategory->name }}</td>
                                <td>{{ @$subCategory->parent->name  }}</td>
                                <td>
                                    <img src="{{$subCategory->image ? asset('main/storage/app/public/'.$subCategory->image) :'/admin/img/no-photo.png'}}" id="profile-img-tag" class="img-profile" alt="not found" style="width: 50px">
                                </td>
                                <td>
                                    @if($subCategory->is_free_shipping)
                                        <span class="badge badge-success">Free</span>
                                    @else
                                        <span class="badge badge-warning">Chargeable</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="Javascript:void(0)" data-id="{{$subCategory->id}}" data-toggle="modal" class="btn btn-sm btn-success edit-sub-category">edit</a>
                                    <a href="{{route('admin.sub-categories.destroy', $subCategory->id)}}" class="btn btn-sm btn-danger">delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="5" class="text-center text-danger">Record Not Found!</th>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                 {{--   {{$subCategories->links()}}  --}}
                </div>
            </div>
        </div>
        <!-- The Modal -->
        <div class="modal fade" id="add-category">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add Sub Category</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form method="POST" action="{{route('admin.sub-categories.store')}}" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="parent">Parent Category <sup>*</sup></label><br>
                                <select id="parent" name="parent_category" class="form-control select2" required>
                                    <option value="">Select</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="name">Name <sup>*</sup></label>
                                <input id="name" type="text" class="form-control" name="name" required>
                            </div>

                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="image" id="customFile">
                                <label class="custom-file-label"  for="customFile">Choose Image</label>
                            </div>

                        </div>



                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
        <div class="modal fade" id="edit-sub-category-modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Category</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div id="edit-body">

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
@section('scripts')
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        $('.edit-sub-category').on('click', function (e) {
            let category_id = $(this).data('id');
            e.preventDefault();
            $.ajax({
                url: '/admin/sub-categories/' + category_id + '/edit',
                type: 'get',
                success: function (response) {
                    $('#edit-body').html(response);
                    $('#edit-sub-category-modal').modal('show');
                    $(".custom-file-input").on("change", function() {
                        var fileName = $(this).val().split("\\").pop();
                        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                    });
                }
            });
        })
    </script>
@endsection
