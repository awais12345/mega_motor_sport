@extends('frontend.master')
@section('title'){{'Home - MegaMotorSports.PK'}}@stop
@section('content')
    @include('frontend.partials.inner-nav')
<style>





.ribbon3 {
    width: 59px;
    height: 27px;
    font-size: 11px;
    line-height: 27px;
    padding-left: 15px;
    position: absolute;
    left: -8px;
    top: 20px;
    font-weight: bolder;
    color: #fff;
    z-index: 2;
    background: var(--red-color);
  }
  .ribbon3:before, .ribbon3:after {
    content: "";
    position: absolute;
  }
  .ribbon3:before {
    height: 0;
    width: 0;
    top: -8.5px;
    left: 0.1px;
    border-bottom: 9px solid black;
    border-left: 9px solid transparent;
  }
  .ribbon3:after {
    height: 0;
    width: 0;
    right: -14.5px;
    border-top: 13px solid transparent;
    border-bottom: 14px solid transparent;
    border-left: 16px solid var(--red-color);
  }
  .buy-now-btn{
      padding: 4px 38px;
      border: 2px solid var(--blue-color);
      background-color: var(--blue-color);
      color: #FFF;
  }
  .buy-now-price
  {
      font-size: 110%; font-weight: 700; padding: 4px 40px; color: var(--blue-color); border: 2px solid var(--blue-color); text-align: center !important; width: 100%
  }
  @media only screen and (max-width: 360px) {
      .buy-now-btn{
          padding: 4px 12px
      }
      .buy-now-price{
          padding: 4px 12px
      }
      .price-section{
          flex-direction: column !important;
      }
  }
  @media only screen and (max-width: 500px)
  {
      .price-section{
          flex-direction: column !important;
      }
      .buy-now-price{
          font-size: smaller;
      }
      .ico
      {
          position: relative !important;
      }
      del::after
      {
          content: '\a';
          white-space: pre
      }
  }
  @media only screen and (max-width: 1557px) {
      .buy-now-btn{
          padding: 4px 17px
      }
      .buy-now-price{
          padding: 4px 34px
      }
      .price-section{
          flex-direction: row;
      }

  }
  @media only screen and (max-width: 1290px) {
      .buy-now-btn{
          padding: 4px 18px
      }
      .buy-now-price{
          padding: 4px 9px
      }
      .price-section{
          flex-direction: row;
      }
  }
  .price-section{
      flex-direction: row;
  }





</style>
<div class="bootstrap-menu">
    <div class="container-fluid">
        @foreach($allCategories as $category)
            <section class="mainCatSection indexCatSec ">
                @include('shared.errors')
                <div class="main-overlay" bis_skin_checked="1"></div>
                <div class="container-fluid" bis_skin_checked="1">
                    <div class="row" bis_skin_checked="1">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 nopadcat" bis_skin_checked="1">

                            <a class="mainCatLink" href="#" bis_skin_checked="1"><h2 style="color:var(--blue-color); ">
                                    {{$category->name}}
                                    <small>Accessories</small></h2></a>
                            <ul class="categoryList list-unstyled">
                                @foreach($category->childrens as $subCategory)
                                    <li><a href="{{route('sub-category.show', $subCategory->id)}}"
                                           bis_skin_checked="1">{{$subCategory->name}}</a></li>
                                @endforeach
                            </ul>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 nopadcat" bis_skin_checked="1">
                            <div class="productSlider"
                                 style="border-right:1px solid #e1e1e1;border-left:1px solid #e1e1e1; border-top:2px solid #7a3901"
                                 bis_skin_checked="1">
                                <div bis_skin_checked="1">
                                    <ul style="padding:0px;"
                                        class="owl-carousel owl-theme catProSlider list-inline indexProductList p-hover shopList">
                                        @foreach($category->products()->inRandomOrder()->limit(16)->get() as $product)
                                            <li class="transall item " style="min-height: 290px;"><!---->
                                                <div class="li-item">
                                                    @if($product->price != $product->discount_price)

      <span class="ribbon3">{{ $product->discount }} % Off</span>

                                                    @endif

                                                    <a href="{{route('products.show', $product->id)}}">

                                                    <div class="p-image" bis_skin_checked="1">


                                                        <img id="ProductImage-16129"
                                                            <?php
                                                                if(!is_null($product->img)){
                                                                    $imgSrc = asset('main/storage/app/public/'.$product->img);
                                                                }
                                                                elseif(!is_null($product->images()->first()->image)){
                                                                    $imgSrc = asset('main/storage/app/public/'.$product->images()->first()->image);
                                                                }
                                                                else{
                                                                    $imgSrc = '/admin/img/no-photo.png';
                                                                }
                                                            ?>
                                                             productimage="{{$imgSrc}}"
                                                             src="{{$imgSrc}}"
                                                             alt="{{$product->name}}"
                                                             style="height: 135px !important; width: auto !important;"
                                                             class="img-responsive">
                                                             
                                                    </div>
                                                    <p id="ProductTitle-16129"
                                                       producttitle="{{$product->name}}">
                                                        {{$product->name}}
                                                    </p>
                                                    <input type="hidden" id="pProductSku-16129" value="16129">
                                                    </a>
                                                    <div class="container-fluid">
                                                    <div class="row text-center">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 price-section" style="margin-top: 10px;display: flex; justify-content: center; flex-direction: row !important">

                                                        <a class="m-0 buy-now-btn" style="width: 10%; color: #FFF; background-color:var(--blue-color);border: inherit" href="{{ route('products.add.cart', [$product->id]) }}">
                                                        <span  style="font-size: large">	<i class="fa fa-shopping-cart ico" style="position: absolute; top: 50%; transform: translate(-30%,-50%); color:#fff"></i></span>
                                                        </a>
                                                    <span class="m-0 buy-now-price" >
                                                        @if($product->price != $product->discount_price)
                                                        <del style="color: var(--red-color); font-size: 80%;">Rs: {{ thousand_separator($product->price) }}</del>
                                                        @endif
                                                        Rs: {{ thousand_separator($product->discount_price) }}
                                                    </span>
                                                    </div>
                                                    </div>


                                                </div>

                                                </div>
                                                <!--<a href="{{route('products.show', $product->id)}}" class="mask" bis_skin_checked="1"></a>-->
                                                <div class="cartBtn" bis_skin_checked="1">
                                                <!--<a href="{{route('products.show', $product->id)}}" class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>-->
                                                </div>
                                            </li>

                                        @endforeach
                                        {{--                                <li class="transall item ">--}}
                                        {{--                                    <a href="#" bis_skin_checked="1">--}}

                                        {{--                                            <span id="productCircle-7772">--}}
                                        {{--                                                <span class="circle">--}}
                                        {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                        {{--                                                    <span class="usp">--}}
                                        {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                        {{--                                                            off<br>--}}
                                        {{--                                                            <span class="usp_off">--}}
                                        {{--                                                                25 %--}}
                                        {{--                                                            </span>--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </span>--}}
                                        {{--                                                </span>--}}
                                        {{--                                            </span>--}}

                                        {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                        {{--                                            <img id="ProductImage-7772"--}}
                                        {{--                                                 productimage="images/productimages/636583695190348246.jpg"--}}
                                        {{--                                                 src="/images/productimages/636583695190348246.jpg" alt=""--}}
                                        {{--                                                 class="img-responsive">--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <p id="ProductTitle-7772"--}}
                                        {{--                                           producttitle="Honda BRV Fog Lamps / Fog Lights DRL Covers Dual - Model 2017-2019">--}}
                                        {{--                                            Honda BRV Fog Lamps / Fog Lights DRL Covers Dual - Model 2017-2019--}}
                                        {{--                                        </p>--}}
                                        {{--                                        <input type="hidden" id="pProductSku-7772" value="304289">--}}
                                        {{--                                        <div class="p-price" id="ProductPrice-7772" productprice="Rs: 6,000"--}}
                                        {{--                                             bis_skin_checked="1">--}}
                                        {{--                                                <span class="sale">--}}
                                        {{--                                                    <s>--}}
                                        {{--                                                        Rs: 8,015--}}
                                        {{--                                                    </s>--}}
                                        {{--                                                </span>Rs: 6,000--}}
                                        {{--                                        </div>--}}

                                        {{--                                    </a>--}}
                                        {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                        {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                        {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="7772"--}}
                                        {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                        {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                        {{--                                        <a href="javascript:void(0)" productid="7772" onclick="addToCart(this)"--}}
                                        {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                        {{--                                    </div>--}}
                                        {{--                                </li>--}}
                                        {{--                                <li class="transall item ">--}}
                                        {{--                                    <a href="#" bis_skin_checked="1">--}}

                                        {{--                                            <span id="productCircle-15525">--}}
                                        {{--                                                <span class="circle">--}}
                                        {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                        {{--                                                    <span class="usp">--}}
                                        {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                        {{--                                                            off<br>--}}
                                        {{--                                                            <span class="usp_off">--}}
                                        {{--                                                                36 %--}}
                                        {{--                                                            </span>--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </span>--}}
                                        {{--                                                </span>--}}
                                        {{--                                            </span>--}}

                                        {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                        {{--                                            <img id="ProductImage-15525"--}}
                                        {{--                                                 productimage="images/productimages/636818612535444534.jpg"--}}
                                        {{--                                                 src="/images/productimages/636818612535444534.jpg"--}}
                                        {{--                                                 alt="Dual Switch Button | Change Over Switch | Switch Allowing to Switch between Two Devices-SehgalMotors.Pk"--}}
                                        {{--                                                 class="img-responsive">--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <p id="ProductTitle-15525"--}}
                                        {{--                                           producttitle="Dual Switch Button | Change Over Switch | Switch Allowing to Switch between Two Devices">--}}
                                        {{--                                            Dual Switch Button | Change Over Switch | Switch Allowing to Switch between--}}
                                        {{--                                            Two Devices--}}
                                        {{--                                        </p>--}}
                                        {{--                                        <input type="hidden" id="pProductSku-15525" value="15525">--}}
                                        {{--                                        <div class="p-price" id="ProductPrice-15525" productprice="Rs: 350"--}}
                                        {{--                                             bis_skin_checked="1">--}}
                                        {{--                                                <span class="sale">--}}
                                        {{--                                                    <s>--}}
                                        {{--                                                        Rs: 550--}}
                                        {{--                                                    </s>--}}
                                        {{--                                                </span>Rs: 350--}}
                                        {{--                                        </div>--}}

                                        {{--                                    </a>--}}
                                        {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                        {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                        {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="15525"--}}
                                        {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                        {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                        {{--                                        <a href="javascript:void(0)" productid="15525" onclick="addToCart(this)"--}}
                                        {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                        {{--                                    </div>--}}
                                        {{--                                </li>--}}
                                        {{--                                <li class="transall item ">--}}
                                        {{--                                    <a href="#" bis_skin_checked="1">--}}

                                        {{--                                            <span id="productCircle-14471">--}}
                                        {{--                                                <span class="circle">--}}
                                        {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                        {{--                                                    <span class="usp">--}}
                                        {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                        {{--                                                            off<br>--}}
                                        {{--                                                            <span class="usp_off">--}}
                                        {{--                                                                52 %--}}
                                        {{--                                                            </span>--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </span>--}}
                                        {{--                                                </span>--}}
                                        {{--                                            </span>--}}

                                        {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                        {{--                                            <img id="ProductImage-14471"--}}
                                        {{--                                                 productimage="images/productimages/637060701338663032.jpg"--}}
                                        {{--                                                 src="/images/productimages/637060701338663032.jpg"--}}
                                        {{--                                                 alt="Universal LED SMD Eye Shape Cree Bar - Each | Super Bright Vision-SehgalMotors.Pk"--}}
                                        {{--                                                 class="img-responsive">--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <p id="ProductTitle-14471"--}}
                                        {{--                                           producttitle="Universal LED SMD Eye Shape Cree Bar - Each | Super Bright Vision">--}}
                                        {{--                                            Universal LED SMD Eye Shape Cree Bar - Each | Super Bright Vision--}}
                                        {{--                                        </p>--}}
                                        {{--                                        <input type="hidden" id="pProductSku-14471" value="14471">--}}
                                        {{--                                        <div class="p-price" id="ProductPrice-14471" productprice="Rs: 799"--}}
                                        {{--                                             bis_skin_checked="1">--}}
                                        {{--                                                <span class="sale">--}}
                                        {{--                                                    <s>--}}
                                        {{--                                                        Rs: 1,690--}}
                                        {{--                                                    </s>--}}
                                        {{--                                                </span>Rs: 799--}}
                                        {{--                                        </div>--}}

                                        {{--                                    </a>--}}
                                        {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                        {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                        {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="14471"--}}
                                        {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                        {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                        {{--                                        <a href="javascript:void(0)" productid="14471" onclick="addToCart(this)"--}}
                                        {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                        {{--                                    </div>--}}
                                        {{--                                </li>--}}
                                        {{--                                <li class="transall item ">--}}
                                        {{--                                    <a href="#" bis_skin_checked="1">--}}

                                        {{--                                            <span id="productCircle-3827">--}}
                                        {{--                                                <span class="circle">--}}
                                        {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                        {{--                                                    <span class="usp">--}}
                                        {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                        {{--                                                            off<br>--}}
                                        {{--                                                            <span class="usp_off">--}}
                                        {{--                                                                19 %--}}
                                        {{--                                                            </span>--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </span>--}}
                                        {{--                                                </span>--}}
                                        {{--                                            </span>--}}

                                        {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                        {{--                                            <img id="ProductImage-3827"--}}
                                        {{--                                                 productimage="images/productimages/636945713459425357.jpg"--}}
                                        {{--                                                 src="/images/productimages/636945713459425357.jpg"--}}
                                        {{--                                                 alt="Maximus Flexible Headlight / Head Lamp Dual Color DRL with 200 SMD | Audi Style Look | InstalLED Inside-SehgalMotors.Pk"--}}
                                        {{--                                                 class="img-responsive">--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <p id="ProductTitle-3827"--}}
                                        {{--                                           producttitle="Maximus Flexible Headlight / Head Lamp Dual Color DRL with 200 SMD | Audi Style Look | InstalLED Inside">--}}
                                        {{--                                            Maximus Flexible Headlight / Head Lamp Dual Color DRL with 200 SMD | Audi--}}
                                        {{--                                            Style Look | InstalLED Inside--}}
                                        {{--                                        </p>--}}
                                        {{--                                        <input type="hidden" id="pProductSku-3827" value="6592">--}}
                                        {{--                                        <div class="p-price" id="ProductPrice-3827" productprice="Rs: 3,053"--}}
                                        {{--                                             bis_skin_checked="1">--}}
                                        {{--                                                <span class="sale">--}}
                                        {{--                                                    <s>--}}
                                        {{--                                                        Rs: 3,780--}}
                                        {{--                                                    </s>--}}
                                        {{--                                                </span>Rs: 3,053--}}
                                        {{--                                        </div>--}}

                                        {{--                                    </a>--}}
                                        {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                        {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                        {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="3827"--}}
                                        {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                        {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                        {{--                                        <a href="javascript:void(0)" productid="3827" onclick="addToCart(this)"--}}
                                        {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                        {{--                                    </div>--}}
                                        {{--                                </li>--}}
                                        {{--                                <li class="transall item ">--}}
                                        {{--                                    <a href="#" bis_skin_checked="1">--}}

                                        {{--                                            <span id="productCircle-4577">--}}
                                        {{--                                                <span class="circle">--}}
                                        {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                        {{--                                                    <span class="usp">--}}
                                        {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                        {{--                                                            off<br>--}}
                                        {{--                                                            <span class="usp_off">--}}
                                        {{--                                                                30 %--}}
                                        {{--                                                            </span>--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </span>--}}
                                        {{--                                                </span>--}}
                                        {{--                                            </span>--}}

                                        {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                        {{--                                            <img id="ProductImage-4577"--}}
                                        {{--                                                 productimage="images/productimages/636441206517772102-SMD-Fog-angel-eyes.jpg"--}}
                                        {{--                                                 src="/images/productimages/636441206517772102-SMD-Fog-angel-eyes.jpg"--}}
                                        {{--                                                 alt="Police Red and Blue Flashers For Dashboard With LED-SehgalMotors.Pk"--}}
                                        {{--                                                 class="img-responsive">--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <p id="ProductTitle-4577"--}}
                                        {{--                                           producttitle="Police Red and Blue Flashers For Dashboard With LED">--}}
                                        {{--                                            Police Red and Blue Flashers For Dashboard With LED--}}
                                        {{--                                        </p>--}}
                                        {{--                                        <input type="hidden" id="pProductSku-4577" value="5074">--}}
                                        {{--                                        <div class="p-price" id="ProductPrice-4577" productprice="Rs: 700"--}}
                                        {{--                                             bis_skin_checked="1">--}}
                                        {{--                                                <span class="sale">--}}
                                        {{--                                                    <s>--}}
                                        {{--                                                        Rs: 1,000--}}
                                        {{--                                                    </s>--}}
                                        {{--                                                </span>Rs: 700--}}
                                        {{--                                        </div>--}}

                                        {{--                                    </a>--}}
                                        {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                        {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                        {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="4577"--}}
                                        {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                        {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                        {{--                                        <a href="javascript:void(0)" productid="4577" onclick="addToCart(this)"--}}
                                        {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                        {{--                                    </div>--}}
                                        {{--                                </li>--}}
                                        {{--                                <li class="transall item ">--}}
                                        {{--                                    <a href="#" bis_skin_checked="1">--}}

                                        {{--                                            <span id="productCircle-15525">--}}
                                        {{--                                                <span class="circle">--}}
                                        {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                        {{--                                                    <span class="usp">--}}
                                        {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                        {{--                                                            off<br>--}}
                                        {{--                                                            <span class="usp_off">--}}
                                        {{--                                                                36 %--}}
                                        {{--                                                            </span>--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </span>--}}
                                        {{--                                                </span>--}}
                                        {{--                                            </span>--}}

                                        {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                        {{--                                            <img id="ProductImage-15525"--}}
                                        {{--                                                 productimage="images/productimages/636818612535444534.jpg"--}}
                                        {{--                                                 src="/images/productimages/636818612535444534.jpg"--}}
                                        {{--                                                 alt="Dual Switch Button | Change Over Switch | Switch Allowing to Switch between Two Devices-SehgalMotors.Pk"--}}
                                        {{--                                                 class="img-responsive">--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <p id="ProductTitle-15525"--}}
                                        {{--                                           producttitle="Dual Switch Button | Change Over Switch | Switch Allowing to Switch between Two Devices">--}}
                                        {{--                                            Dual Switch Button | Change Over Switch | Switch Allowing to Switch between--}}
                                        {{--                                            Two Devices--}}
                                        {{--                                        </p>--}}
                                        {{--                                        <input type="hidden" id="pProductSku-15525" value="15525">--}}
                                        {{--                                        <div class="p-price" id="ProductPrice-15525" productprice="Rs: 350"--}}
                                        {{--                                             bis_skin_checked="1">--}}
                                        {{--                                                <span class="sale">--}}
                                        {{--                                                    <s>--}}
                                        {{--                                                        Rs: 550--}}
                                        {{--                                                    </s>--}}
                                        {{--                                                </span>Rs: 350--}}
                                        {{--                                        </div>--}}

                                        {{--                                    </a>--}}
                                        {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                        {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                        {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="15525"--}}
                                        {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                        {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                        {{--                                        <a href="javascript:void(0)" productid="15525" onclick="addToCart(this)"--}}
                                        {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                        {{--                                    </div>--}}
                                        {{--                                </li>--}}
                                        {{--                                <li class="transall item ">--}}
                                        {{--                                    <a href="#" bis_skin_checked="1">--}}

                                        {{--                                            <span id="productCircle-7772">--}}
                                        {{--                                                <span class="circle">--}}
                                        {{--                                                    <img src="/images/circle.png" alt="circle">--}}
                                        {{--                                                    <span class="usp">--}}
                                        {{--                                                        <div class="usp_1" bis_skin_checked="1">--}}
                                        {{--                                                            off<br>--}}
                                        {{--                                                            <span class="usp_off">--}}
                                        {{--                                                                25 %--}}
                                        {{--                                                            </span>--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </span>--}}
                                        {{--                                                </span>--}}
                                        {{--                                            </span>--}}

                                        {{--                                        <div class="p-image" bis_skin_checked="1">--}}
                                        {{--                                            <img id="ProductImage-7772"--}}
                                        {{--                                                 productimage="images/productimages/636583695190348246.jpg"--}}
                                        {{--                                                 src="/images/productimages/636583695190348246.jpg" alt=""--}}
                                        {{--                                                 class="img-responsive">--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <p id="ProductTitle-7772"--}}
                                        {{--                                           producttitle="Honda BRV Fog Lamps / Fog Lights DRL Covers Dual - Model 2017-2019">--}}
                                        {{--                                            Honda BRV Fog Lamps / Fog Lights DRL Covers Dual - Model 2017-2019--}}
                                        {{--                                        </p>--}}
                                        {{--                                        <input type="hidden" id="pProductSku-7772" value="304289">--}}
                                        {{--                                        <div class="p-price" id="ProductPrice-7772" productprice="Rs: 6,000"--}}
                                        {{--                                             bis_skin_checked="1">--}}
                                        {{--                                                <span class="sale">--}}
                                        {{--                                                    <s>--}}
                                        {{--                                                        Rs: 8,015--}}
                                        {{--                                                    </s>--}}
                                        {{--                                                </span>Rs: 6,000--}}
                                        {{--                                        </div>--}}

                                        {{--                                    </a>--}}
                                        {{--                                    <a href="#" class="mask" bis_skin_checked="1"></a>--}}
                                        {{--                                    <div class="cartBtn" bis_skin_checked="1">--}}
                                        {{--                                        <a href="javascript:void(0)" onclick="QuickView(this)" quickproductid="7772"--}}
                                        {{--                                           class="btn btn-search" bis_skin_checked="1"><img--}}
                                        {{--                                                src="/images/icon-search-btn.png" alt="search"></a>--}}
                                        {{--                                        <a href="javascript:void(0)" productid="7772" onclick="addToCart(this)"--}}
                                        {{--                                           class="btn btn-cart-sm" bis_skin_checked="1">Buy</a>--}}
                                        {{--                                    </div>--}}
                                        {{--                                </li>--}}


                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </section>
        @endforeach
    </div>
    </div>
@endsection

@section('scripts')

@endsection
