@extends('frontend.master')
@section('title'){{'Checkout - MegaMotorSports.PK'}}@stop
@section('content')
<div class="bootstrap-menu">
    <div class="container">
        @include('shared.errors')
        <div class="py-0 text-center">
            <h2>Checkout</h2>
            <h6 class="text-info"><i class="far fa-bell"></i> If Order Amount Total is less than 1000, a delivery charges of 200 will be added. </h6>
        </div>

{{--        <!--<div class="row text-left">--}}
{{--            <div class="col-md-4 order-md-2 mb-4">--}}
{{--             <h4 class="text-center">Your Cart</h4>--}}
{{--                <table id="cart" class="table table-hover table-responsive">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th>Product</th>--}}
{{--                        <th>qty</th>--}}
{{--                        <th>Price</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}

{{--                    @if(session()->has('cart'))--}}
{{--                        @forelse(session()->get('cart')->getContents() as $data)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $data['product']->name }}</td>--}}
{{--                                <th>{{ $data['qty'] }}</th>--}}
{{--                                <td>{{ $data['total_price'] }}</td>--}}
{{--                            </tr>--}}
{{--                        @empty--}}
{{--                            <tr><td colspan="5" class="text-center text-danger">Your Cart is empty!</td></tr>--}}
{{--                            @endforelse--}}
{{--                    <tr>--}}
{{--                        <td></td>--}}
{{--                        <td></td>--}}
{{--                        <td><strong>Total: {{ session()->get('cart')->getTotalPrice()  }}</strong></td>--}}
{{--                    </tr>--}}
{{--                    @else--}}
{{--                        <tr>--}}
{{--                            <td colspan="3" class="text-danger">Cart is empty</td>--}}
{{--                        </tr>--}}
{{--                    @endif()--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--            </div>--}}
{{--            <div class="col-md-8 order-md-1">--}}
{{--                <h4 class="mb-3">Billing address</h4>--}}
{{--                <form action="{{route('products.place-order')}}" method="POST" class="needs-validation" novalidate="">--}}
{{--                    @csrf--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-6 mb-3">--}}
{{--                            <label for="firstName">First name</label>--}}
{{--                            <input value="{{old('first_name') ? old('first_name') : ''}}" type="text" class="form-control" name="first_name" id="firstName" placeholder="" value="" required>--}}
{{--                            <div class="invalid-feedback">--}}
{{--                                Valid first name is required.--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-6 mb-3">--}}
{{--                            <label for="lastName">Last name</label>--}}
{{--                            <input value="{{old('last_name') ? old('last_name') : ''}}" type="text" class="form-control" name="last_name" id="lastName" placeholder="" value="">--}}
{{--                            <div class="invalid-feedback">--}}
{{--                                Valid last name is required.--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="mb-3">--}}
{{--                        <label for="phone">Phone</label>--}}
{{--                        <input value="{{old('phone') ? old('phone') : ''}}" type="text" class="form-control" name="phone" id="phone" placeholder="03001234567" required>--}}
{{--                        <div class="invalid-feedback">--}}
{{--                            Please enter a valid phone number for shipping updates.--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="mb-3">--}}
{{--                        <label for="email">Email <span class="text-muted">(Optional)</span></label>--}}
{{--                        <input value="{{old('email') ? old('email') : ''}}" type="email" class="form-control" name="email" id="email" placeholder="you@example.com">--}}
{{--                        <div class="invalid-feedback">--}}
{{--                            Please enter a valid email address for shipping updates.--}}
{{--                        </div>--}}
{{--                    </div>--}}


{{--                    <div class="mb-3">--}}
{{--                        <label for="address">Address</label>--}}
{{--                        <input value="{{old('address') ? old('address') : ''}}" name="address" type="text" class="form-control" id="address" placeholder="1234 Main St" required>--}}
{{--                        <div class="invalid-feedback">--}}
{{--                            Please enter your shipping address.--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="row">--}}

{{--                        <div class="col-md-4 mb-3">--}}
{{--                            <label for="city">City</label>--}}
{{--                            <input value="{{old('city') ? old('city') : ''}}" type="text" class="form-control" name="city" id="city">--}}
{{--                            <div class="invalid-feedback">--}}
{{--                                Please provide a valid City.--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4 mb-3">--}}
{{--                            <label for="province">province</label>--}}
{{--                            <input value="{{old('province') ? old('province') : ''}}" type="text" class="form-control" name="province" id="province">--}}
{{--                            <div class="invalid-feedback">--}}
{{--                                Please provide a valid Province.--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-3 mb-3">--}}
{{--                            <label for="zip">Zip</label>--}}
{{--                            <input value="{{old('zip') ? old('zip') : ''}}" type="text" class="form-control" name="zip" id="zip" placeholder="" required="">--}}
{{--                            <div class="invalid-feedback">--}}
{{--                                Zip code required.--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}



{{--                    <hr class="mb-4">--}}
{{--                    <button class="btn btn-primary btn-lg btn-block" type="submit">Place Order</button>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>-->--}}
<style>
      .header__btn {
  -webkit-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.2s;
          transition-duration: 0.2s;
  -webkit-transition-timing-function: linear;
          transition-timing-function: linear;
  -webkit-transition-delay: 0s;
          transition-delay: 0s;
  padding: 10px 20px;
  display: inline-block;
  margin-right: 10px;
  background-color: #fff;
  border: 1px solid #2c2c2c;
  border-radius: 3px;
  cursor: pointer;
  outline: none;
}
.header__btn:last-child {
  margin-right: 0;
}
.header__btn:hover, .header__btn.js-active {
  color: #fff;
  background-color: #2c2c2c;
}

.header1 {
  max-width: 600px;
  margin: 50px auto;
  text-align: center;
}

.header__title {
  margin-bottom: 30px;
  font-size: 2.1rem;
}

.content {
  width: 95%;
  margin: 0 auto 50px;
}

.content__title {
  margin-bottom: 40px;
  font-size: 20px;
  text-align: center;
}

.content__title--m-sm {
  margin-bottom: 10px;
}

.multisteps-form__progress {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(0, 2fr));
}

.multisteps-form__progress-btn {
  -webkit-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.15s;
          transition-duration: 0.15s;
  -webkit-transition-timing-function: linear;
          transition-timing-function: linear;
  -webkit-transition-delay: 0s;
          transition-delay: 0s;
  position: relative;
  padding-top: 20px;
  color: rgba(108, 117, 125, 0.7);
  text-indent: -9999px;
  border: none;
  background-color: transparent;
  outline: none !important;
  cursor: pointer;
}
@media (min-width: 500px) {
  .multisteps-form__progress-btn {
    text-indent: 0;
  }
}
.multisteps-form__progress-btn:before {
  position: absolute;
  top: 0;
  left: 50%;
  display: block;
  width: 13px;
  height: 13px;
  content: '';
  -webkit-transform: translateX(-50%);
          transform: translateX(-50%);
  -webkit-transition: all 0.15s linear 0s, -webkit-transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
  transition: all 0.15s linear 0s, -webkit-transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
  transition: all 0.15s linear 0s, transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
  transition: all 0.15s linear 0s, transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s, -webkit-transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
  border: 2px solid currentColor;
  border-radius: 50%;
  background-color: #fff;
  box-sizing: border-box;
  z-index: 3;
}
.multisteps-form__progress-btn:after {
  position: absolute;
  top: 5px;
  left: calc(-50% - 13px / 2);
  -webkit-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.15s;
          transition-duration: 0.15s;
  -webkit-transition-timing-function: linear;
          transition-timing-function: linear;
  -webkit-transition-delay: 0s;
          transition-delay: 0s;
  display: block;
  width: 100%;
  height: 2px;
  content: '';
  background-color: currentColor;
  z-index: 1;
}
.multisteps-form__progress-btn:first-child:after {
  display: none;
}
.multisteps-form__progress-btn.js-active {
  color: #007bff;
}
.multisteps-form__progress-btn.js-active:before {
  -webkit-transform: translateX(-50%) scale(1.2);
          transform: translateX(-50%) scale(1.2);
  background-color: currentColor;
}

.multisteps-form__form {
  position: relative;
}

.multisteps-form__panel {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 0;
  opacity: 0;
  visibility: hidden;
}
.multisteps-form__panel.js-active {
  height: auto;
  opacity: 1;
  visibility: visible;
}
.multisteps-form__panel[data-animation="scaleOut"] {
  -webkit-transform: scale(1.1);
          transform: scale(1.1);
}
.multisteps-form__panel[data-animation="scaleOut"].js-active {
  -webkit-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.2s;
          transition-duration: 0.2s;
  -webkit-transition-timing-function: linear;
          transition-timing-function: linear;
  -webkit-transition-delay: 0s;
          transition-delay: 0s;
  -webkit-transform: scale(1);
          transform: scale(1);
}
.multisteps-form__panel[data-animation="slideHorz"] {
  left: 50px;
}
.multisteps-form__panel[data-animation="slideHorz"].js-active {
  -webkit-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.25s;
          transition-duration: 0.25s;
  -webkit-transition-timing-function: cubic-bezier(0.2, 1.13, 0.38, 1.43);
          transition-timing-function: cubic-bezier(0.2, 1.13, 0.38, 1.43);
  -webkit-transition-delay: 0s;
          transition-delay: 0s;
  left: 0;
}
.multisteps-form__panel[data-animation="slideVert"] {
  top: 30px;
}
.multisteps-form__panel[data-animation="slideVert"].js-active {
  -webkit-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.2s;
          transition-duration: 0.2s;
  -webkit-transition-timing-function: linear;
          transition-timing-function: linear;
  -webkit-transition-delay: 0s;
          transition-delay: 0s;
  top: 0;
}
.multisteps-form__panel[data-animation="fadeIn"].js-active {
  -webkit-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.3s;
          transition-duration: 0.3s;
  -webkit-transition-timing-function: linear;
          transition-timing-function: linear;
  -webkit-transition-delay: 0s;
          transition-delay: 0s;
}
.multisteps-form__panel[data-animation="scaleIn"] {
  -webkit-transform: scale(0.9);
          transform: scale(0.9);
}
.multisteps-form__panel[data-animation="scaleIn"].js-active {
  -webkit-transition-property: all;
  transition-property: all;
  -webkit-transition-duration: 0.2s;
          transition-duration: 0.2s;
  -webkit-transition-timing-function: linear;
          transition-timing-function: linear;
  -webkit-transition-delay: 0s;
          transition-delay: 0s;
  -webkit-transform: scale(1);
          transform: scale(1);
}
.imgcheckout img
{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%)
}
@media only screen and (max-width: 767px) {
    .imgcheckout
{
  display: none;
}
}
</style>

          <div class="row">
<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
<div class="content">
  <!--content inner-->
  <div class="content__inner mt-0">
    <div class="container">
      <h2 class="content__title">Complete Following Steps to Place Your Order</h2>
    </div>
    <div class="container-fluid">
      <!--multisteps-form-->
      <div class="multisteps-form">
        <!--progress bar-->
        <div class="row">
          <div class="col-12 col-lg-12 ml-auto mr-auto mb-4">
            <div class="multisteps-form__progress">
              <button class="multisteps-form__progress-btn js-active" type="button" title="User Info">Personal Information</button>
              <button class="multisteps-form__progress-btn" type="button" title="Address">Address</button>
              <button class="multisteps-form__progress-btn" type="button" title="Order Info">Order Info</button>
              <button class="multisteps-form__progress-btn" type="button" title="Comments">Confirmation</button>
            </div>
          </div>
        </div>
        <!--form panels-->
        <div class="row">
          <div class="col-12 col-lg-12 m-auto">
           <form class="multisteps-form__form" action="{{route('products.place-order')}}" method="POST" class="needs-validation" novalidate="">
                    @csrf
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="slideVert">
                <h4 class="multisteps-form__title">Your Personal Details</h4>
                <div class="multisteps-form__content">
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <input value="{{old('first_name') ? old('first_name') : ''}}" class="multisteps-form__input form-control" type="text" name="first_name" placeholder="First Name" required/>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <input value="{{old('last_name') ? old('last_name') : ''}}" type="text" class="multisteps-form__input form-control" type="text" name="last_name" placeholder="Last Name"/>
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-12">
                      <input class="multisteps-form__input form-control" value="{{old('phone') ? old('phone') : ''}}" name="phone" type="text" placeholder="Phone Number" required/>
                    </div>
                    <div class="col-12 mt-4 col-sm-12 mt-sm-4">
                      <input class="multisteps-form__input form-control" value="{{old('email') ? old('email') : ''}}" name="email" type="email" placeholder="Email Address"/>
                    </div>
                  </div>
                  <!---<div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <input class="multisteps-form__input form-control" type="password" placeholder="Password"/>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <input class="multisteps-form__input form-control" type="password" placeholder="Repeat Password"/>
                    </div>
                  </div>-->
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Proceed</button>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="slideVert">
                <h3 class="multisteps-form__title">Your Address</h3>
                <div class="multisteps-form__content">
                  <div class="form-row mt-4">
                    <div class="col">
                      <input class="multisteps-form__input form-control" value="{{old('address') ? old('address') : ''}}" name="address" type="text" placeholder="Address" required/>
                    </div>
                  </div>
                  <!--<div class="form-row mt-4">
                    <div class="col">
                      <input class="multisteps-form__input form-control" value="{{old('address') ? old('address') : ''}}" name="address" type="text" placeholder="Address 2"/>
                    </div>
                  </div>-->
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <input class="multisteps-form__input form-control" type="text" placeholder="City" value="{{old('city') ? old('city') : ''}}" name="city"/>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                    <input class="multisteps-form__input form-control" type="text" placeholder="Province" value="{{old('province') ? old('province') : ''}}" name="province"/>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                    <input value="{{old('zip') ? old('zip') : ''}}" type="text" class="multisteps-form__input form-control" name="zip" id="zip" placeholder="Zip" required="">
                    </div>
                  </div>
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                    <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Proceed</button>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="slideVert">
                <h3 class="multisteps-form__title">Your Order Info</h3>
                <div class="multisteps-form__content">
                  <div class="row">
                    <div class="col-12 col-md-12 mt-4">
                      <table  class="table table-hover ">
                        <tbody>

                        @if(session()->has('cart'))
                            @forelse(session()->get('cart')->getContents() as $data)
                                <tr>
                                <td style="width: 20%; padding: 7px;"><img class="rounded" src="{{ $data['product']->images()->first() ? asset('main/storage/app/public/'.$data['product']->images()->first()->image) :'/admin/img/no-photo.png'}}" width="60" height="60"></td>
                                    <td>{{ $data['product']->name }}</td>
                                    <th>{{ $data['qty'] }}</th>
                                    <td>Rs. {{ $data['total_price'] }}</td>
                                </tr>
                                @empty
                                    <tr>
                                        <td class="text-danger text-center" colspan="5">Your Cart is empty</td>
                                    </tr>
                            @endforelse

                            <tr>
                                <td colspan="4"><strong>Total: {{ session()->get('cart')->getTotalPrice()  }} Rs.</strong></td>
                            </tr>
                            @if(session()->get('cart')->getTotalPrice() < 1000)
                              <tr>
                                  <td colspan="4"><stron class="text-info">Delivery Chargs: +200 Rs.</strong></td>
                              </tr>
                              <tr>
                                  <td colspan="4"><strong class="font-weight-bold">Grand Total: {{ session()->get('cart')->getTotalPrice()+200  }} Rs.</strong></td>
                              </tr>
                            @endif
                        @else
                            <tr>
                                <td colspan="4" class="text-danger">Cart is empty</td>
                            </tr>
                        @endif()
                        </tbody>
                    </table>
                    </div>
                  </div>
                  <div class="row">
                    <div class="button-row d-flex mt-4 col-12">
                      <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                      <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Proceed</button>
                    </div>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="slideVert">
                <h3 class="multisteps-form__title">Payment Option Selection</h3>
                <div class="multisteps-form__content">
                  <div class="form-row mt-4">
                    <select name="payment" id="pay" class="form-control" onchange="bank()">
                      <option>Select Payment Option</option>
                      <option value="cod">Cash On Delivery</option>
                      <option value="bt">Bank Transfer</option>
                    </select>
                    <div class="list-group"><br>

                          <b>Meezan Bank</b><br>
                          Title: Mega Motor Sports<br>
                          A/C: 1134-0104559786<br><br>

                          <b>Standard Chartered</b><br>
                          Title: Bilal Ahmad Zahoor<br>
                          A/C #: 01309214301<br><br>
                      
                          <b>Jazz Cash / Easy Paisa</b><br>
                          Title: Bilal Ahmad<br>
                          A/C #: 03224677726
                      </span>
                    </div>
                  </div>
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                    <button class="btn btn-success ml-auto" type="submit" title="Send">Place Your Order</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6 imgcheckout">
  <img src="/images/checkoutimg.jpg" alt="" style="width: 100%;">
</div>
</div>
<script>
  const DOMstrings = {
  stepsBtnClass: 'multisteps-form__progress-btn',
  stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
  stepsBar: document.querySelector('.multisteps-form__progress'),
  stepsForm: document.querySelector('.multisteps-form__form'),
  stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
  stepFormPanelClass: 'multisteps-form__panel',
  stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
  stepPrevBtnClass: 'js-btn-prev',
  stepNextBtnClass: 'js-btn-next'
};

//remove class from a set of items
const removeClasses = (elemSet, className) => {

  elemSet.forEach(elem => {

    elem.classList.remove(className);

  });

};

//return exect parent node of the element
const findParent = (elem, parentClass) => {

  let currentNode = elem;

  while(! (currentNode.classList.contains(parentClass))) {
    currentNode = currentNode.parentNode;
  }

  return currentNode;

};

//get active button step number
const getActiveStep = elem => {
  return Array.from(DOMstrings.stepsBtns).indexOf(elem);
};

//set all steps before clicked (and clicked too) to active
const setActiveStep = (activeStepNum) => {

  //remove active state from all the state
  removeClasses(DOMstrings.stepsBtns, 'js-active');

  //set picked items to active
  DOMstrings.stepsBtns.forEach((elem, index) => {

    if(index <= (activeStepNum) ) {
      elem.classList.add('js-active');
    }

  });
};

//get active panel
const getActivePanel = () => {

  let activePanel;

  DOMstrings.stepFormPanels.forEach(elem => {

    if(elem.classList.contains('js-active')) {

      activePanel = elem;

    }

  });

  return activePanel;

};

//open active panel (and close unactive panels)
const setActivePanel = activePanelNum => {

  //remove active class from all the panels
  removeClasses(DOMstrings.stepFormPanels, 'js-active');

  //show active panel
  DOMstrings.stepFormPanels.forEach((elem, index) => {
    if(index === (activePanelNum)) {

      elem.classList.add('js-active');

      setFormHeight(elem);

    }
  })

};

//set form height equal to current panel height
const formHeight = (activePanel) => {

  const activePanelHeight = activePanel.offsetHeight;

  DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;

};

const setFormHeight = () => {
  const activePanel = getActivePanel();

  formHeight(activePanel);
}

//STEPS BAR CLICK FUNCTION
DOMstrings.stepsBar.addEventListener('click', e => {

  //check if click target is a step button
  const eventTarget = e.target;

  if(!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
    return;
  }

  //get active button step number
  const activeStep = getActiveStep(eventTarget);

  //set all steps before clicked (and clicked too) to active
  setActiveStep(activeStep);

  //open active panel
  setActivePanel(activeStep);
});

//PREV/NEXT BTNS CLICK
DOMstrings.stepsForm.addEventListener('click', e => {

  const eventTarget = e.target;

  //check if we clicked on `PREV` or NEXT` buttons
  if(! ( (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) || (eventTarget.classList.contains(`${DOMstrings.stepNextBtnClass}`)) ) )
  {
    return;
  }

  //find active panel
  const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);

  let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);

  //set active step and active panel onclick
  if(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
    activePanelNum--;

  } else {

    activePanelNum++;

  }

  setActiveStep(activePanelNum);
  setActivePanel(activePanelNum);

});

//SETTING PROPER FORM HEIGHT ONLOAD
window.addEventListener('load', setFormHeight, false);

//SETTING PROPER FORM HEIGHT ONRESIZE
window.addEventListener('resize', setFormHeight, false);

//changing animation via animation select !!!YOU DON'T NEED THIS CODE (if you want to change animation type, just change form panels data-attr)

const setAnimationType = (newType) => {
  DOMstrings.stepFormPanels.forEach(elem => {
    elem.dataset.animation = newType;
  })
};

  </script>
        <br><br>
    </div>
    </div>
@endsection

@section('scripts')

@endsection
