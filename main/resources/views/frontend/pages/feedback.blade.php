@extends('frontend.master')
@section('styles')
    <!--<link rel="stylesheet" href="/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Bhaina+2:wght@500&display=swap" rel="stylesheet">
    <style>
        .hero-section
        {
            height: 200px;
            display: flex;
            align-items: center;
            justify-content: center;
            overflow: hidden
        }
        .hero-section img
        {
            position: absolute;
            z-index: -1;
            filter: blur(4px);
            height: inherit;
            width: 100% !important
        }
        .bg
        {
            position: absolute;
            height: 220px;
            width: 100%;
            filter: blur(2px);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            z-index: -1;
            background: url('/images/her-bg.jpg')
        }
    </style>
@endsection

@section('content')
    <div class="bootstrap-menu">
        <div class="hero-section">
            <div class="bg"></div>
            <h2 class="text-uppercase">Feedback</h2>
        </div>
        <div class="container mt-4">
            <h1 class="text-uppercase">For Feedback and Queries:</h1>
            <p>
            Separate and apart from User Content, you may have the ability to submit questions, comments suggestions, reviews, ideas, plans, designs, notes, proposals, drawings, original or creative materials and other information regarding this Site, Us and our products or services (collectively "Feedback"). You agree that Feedback is non-confidential and shall become Our sole property. We shall own exclusive rights, including all intellectual property rights, in and to such Feedback and shall be entitled to the unrestricted use and dissemination of the Feedback for any purpose, commercial or otherwise, without acknowledgment or compensation to you.
            </p>
            <br>
            <p>
            We welcome your questions, comments, and concerns about privacy. Please send us any and all feedback pertaining to privacy, or any other issue on our email: <b>Info@Megamotorsports.pk</b> or Whats app us on: <b>+92 322 46 77 726.</b>
            </p>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
@endsection
