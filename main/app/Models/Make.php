<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Make extends Model
{
   protected $guarded = [];

   public function makeModels(){
      return $this->hasMany(MakeModel::class);
   }
}
