<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SubCategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::all();
        $subCategories = SubCategory::query()->with('parent')->get();
        return view('admin.sub-categories.index', compact('categories', 'subCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|min:2|max:64', 'parent_category' => 'required']);
        DB::beginTransaction();
        try {
            $subCategory = SubCategory::query()->create([
                'slug' => Str::slug($request->name),
                'name' => $request->name,
                'category_id' => $request->parent_category
            ]);
            $this->saveImage($request, $subCategory);
            DB::commit();
            session()->flash('success', 'Added Successfully!');
            return redirect()->route('admin.sub-categories.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', $exception->getMessage());
            return back()->withInput($request->all());
        }
    }


    /**
     * @param SubCategory $subCategory
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(SubCategory $subCategory)
    {
        $categories = Category::all();
        return view('admin.sub-categories.edit', compact('subCategory', 'categories'));
    }

    /**
     * @param Request $request
     * @param SubCategory $subCategory
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, SubCategory $subCategory)
    {
        $this->validate($request, ['name' => 'required|min:2|max:64', 'parent_category' => 'required']);
        DB::beginTransaction();
        try {
            $subCategory->update([
                'slug' => Str::slug($request->name),
                'name' => $request->name,
                'category_id' => $request->parent_category,
                'is_free_shipping' => $request->filled('is_free_shipping') ? true : false,
            ]);
            $this->saveImage($request, $subCategory);

            $subCategory->products()->update([
                'is_free_shipping' => $subCategory->is_free_shipping
            ]);

            DB::commit();
            session()->flash('success', 'Updated Successfully!');
            return redirect()->route('admin.sub-categories.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', $exception->getMessage());
            return back()->withInput($request->all());
        }
    }

    /**
     * @param SubCategory $subCategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(SubCategory $subCategory)
    {
        DB::beginTransaction();
        try {
            $subCategory->delete();
            DB::commit();
            session()->flash('success', 'Deleted Successfully!');
            return redirect()->route('admin.sub-categories.index');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Something Went Wrong!');
            return back();
        }
    }

    /**
     * @param Request $request
     * @param $subCategory
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveImage(Request $request, $subCategory)
    {
        if ($request->hasFile('image')) {
            $this->validate($request, [
                'image' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            $subCategory->image = storeImage($request->file('image'), 'categories');
        }
        $subCategory->save();
    }
}
