<option value="" disabled>Select</option>
@foreach($subCategories as $subCategory)
    <option
        value="{{$subCategory->id}}">{{ $subCategory->name }}</option>
@endforeach
