
<form method="POST" action="{{route('admin.makes.update', $make)}}">

    {{ csrf_field() }}

    <div class="modal-body" id="edit-body">
        <div class="form-group">
            <label for="name">Name <sup>*</sup></label>
            <input id="name" type="text" class="form-control" value="{{$make->name}}" name="name" required>
        </div>
    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</form>
