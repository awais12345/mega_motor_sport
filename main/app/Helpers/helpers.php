<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

if (!function_exists('getUser')) {

    /**
     * @param string $user_id
     * @return mixed
     */
    function getUser($user_id = '')
    {
        if (empty($user_id)) {
            return Auth::user();
        } else {
            return User::query()->find($user_id);
        }

    }
}
if (!function_exists('isAdministratorUser')) {
    /**
     * @return bool
     */
    function isAdministratorUser()
    {
        $user = getUser();
        return $user ? $user->inRole('admin') : false;
    }
}
if (!function_exists('thousand_separator')) {

    /**
     * @return mixed
     */
    function thousand_separator($value, $decimal = 0)
    {
        return number_format((float)$value, $decimal, '.', ',');
    }
}

if (!function_exists('round_normal')) {

    /**
     * @return mixed
     */
    function round_normal($value, $decimal = 0)
    {
        return number_format((float)$value, $decimal, '.', '');
    }
}

if (!function_exists('storeImage')) {

    /**
     * Default Store Image to river_images folder
     *
     * @param $image
     * @param null $name
     * @param string $folder
     * @return mixed
     */
    function storeImage($image, $folder = 'avatars', $file_name = null)
    {
        if (is_null($file_name)) {
            $file_name = Str::random(6);
        }
        $name = $file_name . '-' . rand(1, 6000) . '.' . $image->extension();
        return \Storage::disk('public')->putFileAs($folder, $image, $name);
    }
}

if (!function_exists('getMakes')) {

    /**
     * @return array
     */
    function getMakes()
    {
        return \App\Models\Make::all();
    }
}


if (!function_exists('getYears')) {

    /**
     * @return array
     */
    function getYears()
    {
        $dates = [];
        $today = Carbon::today();
        for ($i = 0; $i <= 90; $i++) {
            array_push($dates, $today->format('Y'));
            $today->subYear(1);
        }
        return $dates;
    }
}

if (!function_exists('getPendingOrdersCount')) {

    /**
     * @return array
     */
    function getPendingOrdersCount()
    {
        return \App\Models\Order::where('status', \App\Models\Order::STATUS_PENDING)->count();
    }
}
if (!function_exists('getCompletedOrdersCount')) {

    /**
     * @return array
     */
    function getCompletedOrdersCount()
    {
        return \App\Models\Order::where('status', \App\Models\Order::STATUS_COMPLETED)->count();
    }
}
if (!function_exists('getCanceledOrdersCount')) {

    /**
     * @return array
     */
    function getCanceledOrdersCount()
    {
        return \App\Models\Order::where('status', \App\Models\Order::STATUS_CANCELED)->count();
    }
}
if (!function_exists('getDeliveredOrdersCount')) {

    /**
     * @return array
     */
    function getDeliveredOrdersCount()
    {
        return \App\Models\Order::where('status', \App\Models\Order::STATUS_DELIVERED)->count();
    }
}

if (!function_exists('getTotalEarnings')) {

    /**
     * @return array
     */
    function getTotalEarnings()
    {
        return  \App\Models\Order::where('status', \App\Models\Order::STATUS_COMPLETED)->sum('total');
    }
}
