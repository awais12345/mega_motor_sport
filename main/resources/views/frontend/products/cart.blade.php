@extends('frontend.master')
@section('title'){{'Cart - MegaMotorSport.PK'}}@stop

@section('content')
<div class="bootstrap-menu">
    <div class="container">
        @include('shared.errors')

        <div class="row mt-5">
            <h3 class="text-center">Cart Items</h3>
            <table id="cart" class="table table-hover table-responsive" style="width:100%">
                <thead>
                <tr>
                    <th style="width: 10%">Image</th>
                    <th style="width:10%">Name</th>
                    <th style="width:10%">Price</th>
                    <th style="width:15%">Shipping Charges</th>
                    <th style="width:10%">Quantity</th>
                    <th style="width:10%" class="text-center">Subtotal</th>
                    <th style="width: 35%">actions</th>
                </tr>
                </thead>
                <tbody>
                @if(session()->has('cart'))
                    @forelse(session()->get('cart')->getContents() as $key => $data)
                        <form method="GET" action="{{ route('products.update.cart', $data['product']->id) }}">
                        <tr>
                                <td>
                                <img src="{{ $data['product']->images()->first() ? asset('main/storage/app/public/'.$data['product']->images()->first()->image) :'/admin/img/no-photo.png'}}" style="width: 60px;height: 60px" alt="..."
                                     class="img-responsive"/>
                                </td>
                                <td>{{ $data['product']->name }}</td>
                                <td class="text-center">{{ $data['product']->discount_price }}</td>
                                <td class="text-center">{{ $data['product']->shipping_charges }}</td>
                                <td>
                                    <input type="number" name="qty" class="form-control text-center" value="{{$data['qty']}}">
                                </td>
                                <td class="text-center">{{ @$data['total_price'] }}</td>
                                <td class="actions">
                                    <button type="submit" class="btn btn-success btn-sm" title="refresh"><i
                                            class="glyphicon glyphicon-refresh"></i></button>
                                    <a href="{{ route('products.remove.cart', $data['product']->id) }}" class="btn btn-danger btn-sm" title="remove from cart"><i
                                            class="glyphicon glyphicon-remove"></i></a>
                                </td>
                        </tr>
                        </form>
                        @empty
                        <tr>
                            <td colspan="10" class="text-center text-danger">Your Cart is empty!</td>
                        </tr>
                    @endforelse
                    <tr>
                        <th colspan="5"></th>
                        <th><strong>Total: {{session()->get('cart')->getTotalPrice()}}</strong></th>
                        <th></th>
                        <th></th>
                    </tr>
                @endif
                <tr>

                </tr>
                </tbody>

            </table>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="/" class="btn btn-block btn-warning float-left">Continue Shopping</a>
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <a href="{{route('products.checkout')}}" class="btn btn-block btn-success float-right">Checkout</a>
            </div>
        </div>
        <br><br>
    </div>
</div>
@endsection

@section('scripts')

@endsection
