<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    protected $appends = ['discount','total_price'];

    public static $ALL_RELATIONS = ['categories', 'subCategories', 'makes', 'makeModels'];

    /**
     * @return float|int
     */
    public function getDiscountAttribute()
    {
        return round((($this->price - $this->discount_price) / $this->price) * 100);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function years()
    {
        return $this->hasMany(ProductYear::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function makes()
    {
        return $this->belongsToMany(Make::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function makeModels()
    {
        return $this->belongsToMany(MakeModel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subCategories()
    {
        return $this->belongsToMany(SubCategory::class);
    }

    public function getTotalPriceAttribute()
    {
        if ($this->is_free_shipping) {
            return $this->discount_price;
        }
        return $this->discount_price + $this->shipping_charges;
    }

    public function getDiscountPriceAttribute($value){
        return round($value);
    }

    public function getPriceAttribute($value){
        return round($value);
    }

    public function getShippingChargesAttribute($value){
        return round($value);
    }
}
