<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <title>Search Result - MegaMotorSports.PK</title>
    <style>
        body {
            padding-bottom: 30px;
            position: relative;
            min-height: 100%;
        }

        a {
            transition: background 0.2s, color 0.2s;
        }
        a:hover,
        a:focus {
            text-decoration: none;
        }

        #wrapper {
            padding-left: 0;
            transition: all 0.5s ease;
            position: relative;
        }

        #sidebar-wrapper {
            z-index: 1000;
            position: fixed;
            left: 250px;
            width: 0;
            height: 100%;
            margin-left: -250px;
            overflow-y: auto;
            overflow-x: hidden;
            background: #eee;
            transition: all 0.5s ease;
        }

        #wrapper.toggled #sidebar-wrapper {
            width: 250px;
            z-index: 1000;
        }

        .sidebar-brand {
            position: absolute;
            top: 0;
            width: 250px;
            padding: 20px 0;
        }
        .sidebar-brand h4 {
            margin: 0;
            font-weight: 400;
            font-size: 22px;
            color: #333;
            padding-left: 12px;
        }

        .sidebar-nav {
            position: absolute;
            top: 55px;
            width: 250px;
            margin: 0;
            padding: 0;
            list-style: none;
        }
        .sidebar-nav > li {
            text-indent: 10px;
            line-height: 42px;
        }
        .sidebar-nav > li a {
            display: block;
            text-decoration: none;
            color: #939393;
            font-weight: 600;
            font-size: 18px;
            padding-left: 20px;
        }
        .sidebar-nav > li > a:hover,
        .sidebar-nav > li.active > a {
            text-decoration: none;
            color: #fff;
            background: royalblue;
        }
        .sidebar-nav > li > a i.fa {
            font-size: 24px;
            width: 60px;
        }

        #navbar-wrapper {
            width: 100%;
            position: absolute;
            z-index: 2;
            display: none;
        }
        #wrapper.toggled #navbar-wrapper {
            position: absolute;
            margin-right: -250px;
        }
        #navbar-wrapper .navbar {
            border-width: 0 0 0 0;
            background-color: #eee;
            font-size: 24px;
            margin-bottom: 0;
            border-radius: 0;
        }
        #navbar-wrapper .navbar a {
            color: #757575;
        }
        #navbar-wrapper .navbar a:hover {
            color: royalblue;
        }

        #content-wrapper {
            width: 100%;
            position: fixed;
            padding: 15px;
            top: 100px;
        }
        #wrapper.toggled #content-wrapper {
            position: absolute;
            margin-right: -250px;
        }

        @media (min-width: 992px) {
            #wrapper {
                padding-left: 250px;
            }

            #wrapper.toggled {
                padding-left: 60px;
            }

            #sidebar-wrapper {
                width: 250px;
            }

            #wrapper.toggled #sidebar-wrapper {
                width: 60px;
            }
            /*
              #navbar-wrapper
                {
                    display: block;
                }
            */
            #wrapper.toggled #navbar-wrapper {
                position: absolute;
                margin-right: -190px;
            }

            #wrapper.toggled #content-wrapper {
                position: absolute;
                margin-right: -190px;
            }

            #navbar-wrapper {
                position: relative;
            }

            #wrapper.toggled {
                padding-left: 60px;
            }

            #content-wrapper {
                position: relative;
                top: 0;
            }

            #wrapper.toggled #navbar-wrapper,
            #wrapper.toggled #content-wrapper {
                position: relative;
                margin-right: 60px;
            }
        }

        @media (min-width: 768px) and (max-width: 991px) {

            #wrapper {
                padding-left: 60px;
            }

            #sidebar-wrapper {
                width: 60px;
            }

            #wrapper.toggled #navbar-wrapper {
                position: absolute;
                margin-right: -250px;
            }

            #wrapper.toggled #content-wrapper {
                position: absolute;
                margin-right: -250px;
            }

            #navbar-wrapper {
                display: block;
                position: relative;
            }

            #wrapper.toggled {
                padding-left: 250px;
            }

            #content-wrapper {
                position: relative;
                top: 0;
            }

            #wrapper.toggled #navbar-wrapper,
            #wrapper.toggled #content-wrapper {
                position: relative;
                margin-right: 250px;
            }
        }

        @media (max-width: 767px) {
            #wrapper {
                padding-left: 0;
            }

            #sidebar-wrapper {
                width: 0;
            }

            #wrapper.toggled #sidebar-wrapper {
                width: 250px;
            }
            #wrapper.toggled #navbar-wrapper {
                position: absolute;
                margin-right: -250px;
            }

            #wrapper.toggled #content-wrapper {
                position: absolute;
                margin-right: -250px;
            }

            #navbar-wrapper {
                position: relative;
                display: block;
            }

            #wrapper.toggled {
                padding-left: 250px;
            }

            #content-wrapper {
                position: relative;
                top: 0;
            }

            #wrapper.toggled #navbar-wrapper,
            #wrapper.toggled #content-wrapper {
                position: relative;
                margin-right: 250px;
            }
        }

        /*============= Side Bar CSS========================*/



        .sideSection .container
        {
            background-color: #fff;
            box-sizing: border-box;
            border: .8px solid #7C7C7C;
        }
        .rightSection .container
        {
            margin: 10px auto;
            background-color: #fff;
            border-radius: 5px;
            padding: 10px 5px 10px 5px;
        }
        .fancy-cards{
            text-align: center;
            margin-top: 5px auto;
        }
        .fancy-cards .fancy-card{
            display: inline-block;
            position: relative;
            box-sizing: border-box;
        }

        .fancy-card .top,
        .fancy-card .middle,
        .fancy-card .bottom{
            position: relative;
            border-radius: 3px;
            overflow: hidden;
            width: 310px;
            height: 310px;
            transition: transform 300ms linear-out;
            margin-top: 4px;
        }
        .fancy-card .top{
            z-index: 3;
            transform: scale(1.0);
            transition: transform 300ms cubic-bezier(0.22, 0.61, 0.36, 1);
            background-image: url('https://static-1.gumroad.com/res/gumroad/assets/collections/food_and_cooking_thumb-34fb9ef316a7b01177529839891c3a03.jpg');
            background-size: cover;
            background-position: center;
            box-shadow: 0px 1px 3px rgba(25,25,25,0.30);
        }
        .fancy-card .middle{
            position: absolute;
            background: #aaa;
            top: 0px;
            z-index: 2;
            transform: rotate(0deg);
            transition: transform 250ms cubic-bezier(0.68, -0.55, 0.27, 1.55);
        }
        .fancy-card .bottom{
            position: absolute;
            background: #ccc;
            top: 0px;
            z-index: 1;
            transform: rotate(0deg);
            transition: transform 250ms cubic-bezier(0.68, -0.55, 0.27, 1.55);
        }

        .fancy-card .caption{
            overflow: hidden;
            background: rgba(255,255,255,0.75);
            padding: 8px 5px;
            position: absolute;
            bottom: 0px;
            left: 0px;
            width: 100%;
        }
        .fancy-card .caption .title{
            color: #222;
            margin: 0px 0px 15px 0px;
            font-size: 1.4rem;
        }
        .fancy-card .caption .button{
            display: inline-block;
            color: #333;
            text-decoration: none;
            border: solid 1px #555;
            padding: 7px 13px;
            background-color: transparent;
            transition: all 300ms ease-in;
        }


        /*hovering*/
        .fancy-card:hover .top{
            transform: scale(1.05);
        }
        .fancy-card:hover .middle{
            transform: rotate(-7deg);
            box-shadow: 1px 1px 2px rgba(74, 74, 74, 0.35);
        }
        .fancy-card:hover .bottom{
            transform: rotate(7deg);
            box-shadow: 1px 1px 2px rgba(113, 113, 113, 0.35);
        }

        .fancy-card:hover .button{
            background: rgba(0,0,0,0.8);
            color: #fff;
            border: 0px;
            width: 80%;
        }
        /* ============== Toggle Css  ===================*/
        .body-section {
            padding-bottom: 30px;
            position: relative;
            min-height: 100%;
        }

        .body-section #wrapper {
            padding-left: 0;
            transition: all 0.5s ease;
            position: relative;
        }

        .body-section #sidebar-wrapper {
            z-index: 1000;
            position: fixed;
            left: 250px;
            width: 0;
            height: 100%;
            margin-left: -250px;
            overflow-y: auto;
            overflow-x: hidden;
            background: #222;
            transition: all 0.5s ease;
        }

        .body-section #wrapper.toggled #sidebar-wrapper {
            width: 250px;
            z-index: 1;
        }

        .body-section .sidebar-brand {
            position: absolute;
            top: 0;
            width: 250px;
            text-align: center;
            padding: 20px 0;
        }
        .body-section .sidebar-brand h2 {
            margin: 0;
            font-weight: 600;
            font-size: 24px;
            color: #fff;
        }

        .body-section .sidebar-nav {
            position: absolute;
            top: 75px;
            width: 250px;
            margin: 0;
            padding: 0;
            list-style: none;
        }
        .body-section .sidebar-nav > li {
            text-indent: 10px;
            line-height: 42px;
        }
        .body-section .sidebar-nav > li a {
            display: block;
            text-decoration: none;
            color: #757575;
            font-weight: 600;
            font-size: 18px;
        }
        .body-section .sidebar-nav > li > a:hover,
        .body-section .sidebar-nav > li.active > a {
            text-decoration: none;
            color: #fff;
            background: royalblue;
        }
        .body-section .sidebar-nav > li > a i.fa {
            font-size: 24px;
            width: 60px;
        }

        .body-section #navbar-wrapper {
            width: 100%;
            position: absolute;
            z-index: 2;
        }
        .body-section #wrapper.toggled #navbar-wrapper {
            position: absolute;
            margin-right: -250px;
        }
        .body-section #navbar-wrapper .navbar {
            border-width: 0 0 0 0;
            background-color: #eee;
            font-size: 24px;
            margin-bottom: 0;
            border-radius: 0;
        }
        .body-section #navbar-wrapper .navbar a {
            color: #757575;
        }
        .body-section #navbar-wrapper .navbar a:hover {
            color: royalblue;
        }

        .body-section #content-wrapper {
            width: 100%;
            position: fixed;
            padding: 15px;
            top: 100px;
        }
        .body-section #wrapper.toggled #content-wrapper {
            position: absolute;
            margin-right: -250px;
        }

        @media (min-width: 992px) {
            .body-section #wrapper {
                padding-left: 250px;
            }

            .body-section #wrapper.toggled {
                padding-left: 60px;
            }

            .body-section #sidebar-wrapper {
                width: 250px;
            }

            .body-section #wrapper.toggled #sidebar-wrapper {
                width: 60px;
            }

            .body-section #wrapper.toggled #navbar-wrapper {
                position: absolute;
                margin-right: -190px;
            }

            .body-section #wrapper.toggled #content-wrapper {
                position: absolute;
                margin-right: -190px;
            }

            .body-section #navbar-wrapper {
                position: relative;
            }

            .body-section #wrapper.toggled {
                padding-left: 60px;
            }

            .body-section #content-wrapper {
                position: relative;
                top: 0;
            }

            .body-section #wrapper.toggled #navbar-wrapper,
            .body-section #wrapper.toggled #content-wrapper {
                position: relative;
                margin-right: 60px;
            }
        }

        @media (min-width: 768px) and (max-width: 991px) {
            .body-section #wrapper {
                padding-left: 60px;
            }

            .body-section #sidebar-wrapper {
                width: 60px;
            }

            .body-section #wrapper.toggled #navbar-wrapper {
                position: absolute;
                margin-right: -250px;
            }

            .body-section #wrapper.toggled #content-wrapper {
                position: absolute;
                margin-right: -250px;
            }

            .body-section #navbar-wrapper {
                position: relative;
            }

            .body-section #wrapper.toggled {
                padding-left: 250px;
            }

            .body-section #content-wrapper {
                position: relative;
                top: 0;
            }

            .body-section #wrapper.toggled #navbar-wrapper,
            .body-section #wrapper.toggled #content-wrapper {
                position: relative;
                margin-right: 250px;
            }
        }

        @media (max-width: 767px) {
            .body-section #wrapper {
                padding-left: 0;
            }

            .body-section #sidebar-wrapper {
                width: 0;
            }

            .body-section #wrapper.toggled #sidebar-wrapper {
                width: 250px;
            }
            .body-section #wrapper.toggled #navbar-wrapper {
                position: absolute;
                margin-right: -250px;
            }

            .body-section #wrapper.toggled #content-wrapper {
                position: absolute;
                margin-right: -250px;
            }

            .body-section #navbar-wrapper {
                position: relative;
            }

            .body-section #wrapper.toggled {
                padding-left: 250px;
            }

            .body-section #content-wrapper {
                position: relative;
                top: 0;
            }

            .body-section #wrapper.toggled #navbar-wrapper,
            .body-section #wrapper.toggled #content-wrapper {
                position: relative;
                margin-right: 250px;
            }
        }
    </style>
</head>

<body>


<div id="wrapper">

    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <h4>NEED FOR</h4>
        </div>
        <ul class="sidebar-nav">
            <li class="active">
                <a href="#">Dash Kits</a>
            </li>
            <li>
                <a href="#">Floor Mats</a>
            </li>
            <li>
                <a href="#">Seat Covers</a>
            </li>
            <li>
                <a href="#">Steering Wheels</a>
            </li>
            <li>
                <a href="#">Sun Shades</a>
            </li>
            <li>
                <a href="#">Custome Gauges</a>
            </li>
            <li>
                <a href="#">Cargo Linears</a>
            </li>
            <li>
                <a href="#">Seats</a>
            </li>
            <li>
                <a href="#">Dash Covers</a>
            </li>
            <li>
                <a href="#">Shift Knobs</a>
            </li>
            <li>
                <a href="#">Car Organizers</a>
            </li>
            <li>
                <a href="#">Van equipment</a>
            </li>
            <li>
                <a href="#">Pedals</a>
            </li>
            <li>
                <a href="#">Pet Travel</a>
            </li>
            <li>
                <a href="#">Auto Detailing</a>
            </li>
        </ul>
    </aside>

    <div id="navbar-wrapper">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="#" class="navbar-brand" id="sidebar-toggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
        </nav>
    </div>

    <section id="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="body-section">

                    <div class="row">
                        <div class="col-md-12 rightSection">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="fancy-cards">
                                        <div class="fancy-card">
                                            <div class="top">
                                                <div class="caption">
                                                    <h3 class="title">Product Name <span>$29.55</span></h3>
                                                    <a href="" class="button"><i class="fa fa-cart-plus"></i> Add to cart</a>
                                                </div>
                                            </div>
                                            <div class="middle"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </section>

</div>





<script src="/js/mburger.js"></script>
<script src="/js/mmenu.js"></script>
<script>
    Mmenu.configs.classNames.selected = "active";
    Mmenu.configs.offCanvas.page.selector = "#main-page";

    document.addEventListener(
        "DOMContentLoaded", () => {
            new Mmenu( "#sidenav", {
                "extensions": [
                    "pagedim-black",

                ]
            });
        }
    );
</script>
<script>
    const $button  = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper');

    $button.addEventListener('click', (e) => {
        e.preventDefault();
        $wrapper.classList.toggle('toggled');
    });
</script>
</body>
</html>
