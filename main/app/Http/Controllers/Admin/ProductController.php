<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Make;
use App\Models\MakeModel;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductYear;
use App\Models\SubCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductController extends Controller
{

    private $rules;


    public function index()
    {
        $products = Product::query()->with(Product::$ALL_RELATIONS)->get();
        $categories = Category::all();
        $subCategories = SubCategory::all();
        $makes = Make::all();
        $models = MakeModel::all();
        return view('admin.products.index', compact('products', 'categories', 'subCategories', 'makes', 'models'));
    }


    public function store(Request $request)
    {
        $this->validateRequest($request);

        $video_url = null;
        if ($request->filled('video_url')) {
            $video_url = $this->getYoutubeEmbedUrl($request->video_url);
        }
        DB::beginTransaction();
        try {
            $product = Product::query()->create([
                'name' => $request->name,
                'sku' => substr(md5(microtime()), 0, 6),
                'slug' => Str::slug($request->name),
                'price' => $request->price,
                'discount_price' => $request->filled('discount_price') ? $request->discount_price : $request->price,
//                'make_id' => $request->filled('make_id') ? $request->make_id : null,
//                'make_model_id' => $request->filled('make_model_id') ? $request->make_model_id : null,
                'description' => $request->filled('description') ? $request->description : null,
                'qty' => $request->filled('qty') ? $request->qty : null,
                'color' => $request->filled('color') ? $request->color : null,
                'size' => $request->filled('size') ? $request->size : null,
                'vendor' => $request->filled('vendor') ? $request->vendor : null,
                'video_url' => $video_url,
                'shipping_charges' => $request->filled('shipping_charges') ? $request->shipping_charges : 0.00,
                'is_free_shipping' => $request->filled('is_free_shipping') ? true : false,
                'is_available' => (boolean)$request->is_avaliable
                ]);

            $product->categories()->sync($request->category_ids);
            $product->subCategories()->sync($request->sub_category_ids);
            $product->makes()->sync($request->make_ids);
            $product->makeModels()->sync($request->make_model_ids);

            if ($request->filled('years')) {
                foreach ($request->years as $year) {
                    ProductYear::query()->create([
                        'product_id' => $product->id,
                        'year' => $year
                    ]);
                }
            }


            $this->saveImages($request, $product);
            DB::commit();
            session()->flash('success', 'Added Successfully!');
            return redirect()->route('admin.products.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', $exception->getMessage());
            return back()->withInput($request->all());
        }

    }


    public function edit(Product $product)
    {
        $categories = Category::all();
        $subCategories = SubCategory::all();
        $makes = Make::all();
        $models = MakeModel::all();
        $productYears = $product->years()->pluck('year')->toArray();
        return view('admin.products.edit', compact('product', 'categories', 'subCategories', 'makes', 'models', 'productYears'));
    }


    public function update(Request $request, Product $product)
    {
        $this->validateRequest($request, $product->id);
        $video_url = null;
        if ($request->filled('video_url')) {
            $video_url = $this->getYoutubeEmbedUrl($request->video_url);
        }
        DB::beginTransaction();
        try {
            $product->update([
                'name' => $request->name,
                'slug' => Str::slug($request->name),
//                'category_id' => $request->category_id,
//                'sub_category_id' => $request->sub_category_id,
//                'make_id' => $request->filled('make_id') ? $request->make_id : null,
//                'make_model_id' => $request->filled('make_model_id') ? $request->make_model_id : null,
                'description' => $request->filled('description') ? $request->description : null,
                'price' => $request->price,
                'discount_price' => $request->filled('discount_price') ? $request->discount_price : $request->price,
                'qty' => $request->filled('qty') ? $request->qty : null,
                'color' => $request->filled('color') ? $request->color : null,
                'size' => $request->filled('size') ? $request->size : null,
                'vendor' => $request->filled('vendor') ? $request->vendor : null,
                'video_url' => $video_url,
                'shipping_charges' => $request->filled('shipping_charges') ? $request->shipping_charges : 0.00,
                'is_free_shipping' => $request->filled('is_free_shipping') ? true : false,
                'is_available' => (boolean)$request->is_avaliable
            ]);

            $product->categories()->sync($request->category_ids);
            $product->subCategories()->sync($request->sub_category_ids);
            $product->makes()->sync($request->make_ids);
            $product->makeModels()->sync($request->make_model_ids);

            ProductYear::query()->where('product_id', $product->id)->delete();

            if ($request->filled('years')) {
                foreach ($request->years as $year) {
                    ProductYear::query()->create([
                        'product_id' => $product->id,
                        'year' => $year
                    ]);
                }
            }

            $this->saveImages($request, $product);

            DB::commit();
            session()->flash('success', 'Updated Successfully!');
            return redirect()->route('admin.products.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', $exception->getMessage());
            return back()->withInput($request->all());
        }

    }


    public function destroy(Product $product)
    {
        DB::beginTransaction();
        try {
            $product->delete();
            DB::commit();
            session()->flash('success', 'Deleted Successfully!');
            return redirect()->route('admin.products.index');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Something Went Wrong!');
            return back();
        }
    }

    /**
     * @param Request $request
     * @param $product
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveImages(Request $request, $product)
    {
        if ($request->hasFile('images')) {
            // $product->images()->delete();
            $this->validate($request, [
                'images.*' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            foreach ($request->images as $image) {
                ProductImage::query()->create([
                    'product_id' => $product->id,
                    'image' => storeImage($image, 'products'),
                ]);
            }
        }
        if ($request->hasFile('img')) {
            $this->validate($request, [
                'img' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            $product->img = storeImage($request->file('img'), 'products');
            $product->save();
        }
    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subCategoryOptions()
    {
        $subCategories = SubCategory::query()->whereIn('category_id', \request()->ids)->get();
        return view('admin.products.partials.sub-categories', compact('subCategories'));
    }

    /**
     * @param Make $make
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modelsOptions()
    {
        $models = MakeModel::query()->whereIn('make_id', \request()->ids)->get();
        return view('admin.products.partials.models', compact('models'));
    }

    function getYoutubeEmbedUrl($url)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id;
    }

    private function validateRequest(Request $request, $product_id = null){
        $rule = $product_id ? ',' . $product_id : '';
        unset($request['sku']);
        $rules = [
            'name' => 'required|min:2|max:64',
            'category_ids' => 'required|array',
            'sub_category_ids' => 'required|array',
            'price' => 'required|numeric',
            'discount_price' => 'nullable|numeric',
            'shipping_charges' => 'nullable|numeric'
        ];
        $this->validate($request, $rules);
    }
}
