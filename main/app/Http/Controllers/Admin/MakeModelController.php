<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Make;
use App\Models\MakeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MakeModelController extends Controller
{
    public function index()
    {
        $models = MakeModel::query()->with('make')->get();
        $makes = Make::all();
        return view('admin.models.index', compact('models', 'makes'));
    }


    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|min:2|max:64', 'make_id' => 'required']);
        DB::beginTransaction();
        try {
            MakeModel::query()->create([
                'slug' => Str::slug($request->name),
                'name' => $request->name,
                'make_id' => $request->make_id,
            ]);
            DB::commit();
            session()->flash('success', 'Added Successfully!');
            return redirect()->route('admin.models.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', 'Something went wrong!');
            return back()->withInput($request->all());
        }

    }


    public function edit(MakeModel $model)
    {
        $makes = Make::all();
        return view('admin.models.edit', compact('model', 'makes'));
    }


    public function update(Request $request, MakeModel $model)
    {
        $this->validate($request, ['name' => 'required|min:2|max:64', 'make_id' => 'required']);
        DB::beginTransaction();
        try {
            $model->update([
                'slug' => Str::slug($request->name),
                'name' => $request->name,
                'make_id' => $request->make_id,
            ]);
            DB::commit();
            session()->flash('success', 'Added Successfully!');
            return redirect()->route('admin.models.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('error', 'Something went wrong!');
            return back()->withInput($request->all());
        }

    }


    public function destroy(MakeModel $model)
    {
        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            session()->flash('success', 'Deleted Successfully!');
            return redirect()->route('admin.models.index');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Something Went Wrong!');
            return back();
        }
    }
}
