<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrens(){
        return $this->hasMany(SubCategory::class, 'category_id');
    }

    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
